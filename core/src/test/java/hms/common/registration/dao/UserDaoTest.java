/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.dao;

import hms.common.registration.common.Condition;
import hms.common.registration.model.SubCorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserStatus;
import hms.common.registration.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.*;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class UserDaoTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private UserDao userDao;

    SubCorporateUser user;

    @BeforeClass
    public void setUp() throws Exception {

        user = new SubCorporateUser();
        user.setFirstName("isuru");
        user.setMobileNo("254708686869");
        user.setUserType(UserType.SUB_CORPORATE);
        user.setUserStatus(UserStatus.INITIAL);
        user.setEmail("isuruw@hsenidmobile.com");
        user.setUserId("20120627162718713");
        user.setEmailVerificationCode("9938c00b846b56e4ec337237b5f04b68");
        user.setUsername("isuru");
        userDao.persist(user);
    }

    @Test
    public void testFindUserIdListByConditionList() {

        List<Condition> conditionList = new ArrayList<>();
        conditionList.add(new Condition("firstName", "%isuru%", "LIKE"));
        conditionList.add(new Condition("userType", UserType.SUB_CORPORATE, "="));
        List<Long> userIdList = userDao.findUserIdListByConditionList(conditionList);

        assertEquals(1, userIdList.size());
    }

    @Test
    public void testFindUserByMsisdn() {
        User foundUser = userDao.findByMsisdnAndStatus("254708686869", UserStatus.INITIAL);
        assertNotNull(foundUser);
        assertEquals("isuru", foundUser.getFirstName());
    }

    @Test
    public void testFindUserByEmailVerificationCodeAndUserID() {
        User foundUser = userDao.findUserByEmailVerificationCodeAndUserID("20120627162718713", "9938c00b846b56e4ec337237b5f04b68");
        assertNotNull(foundUser);
        assertEquals("isuru", foundUser.getUsername());
    }

    @AfterClass
    public void tearDown() {
        userDao.remove(user);
    }

}
