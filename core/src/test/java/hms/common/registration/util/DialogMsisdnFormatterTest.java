package hms.common.registration.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static hms.common.registration.util.DialogMsisdnFormatter.*;
import static org.testng.Assert.assertEquals;

@ContextConfiguration(locations = {"classpath:dialog-connect-test.xml"})
public class DialogMsisdnFormatterTest extends AbstractTestNGSpringContextTests{

    @Autowired
    private DialogMsisdnFormatter dialogMsisdnFormatter;

    @Test
    public void testFormatMsisdnToNbl1() throws Exception {
        final String msisdn = dialogMsisdnFormatter.formatMsisdnToNbl("94775038419");
        assertEquals(msisdn, "94775038419");
    }

    @Test(expectedExceptions = InvalidDialogNumberException.class)
    public void testFormatMsisdnToNbl2() throws Exception {
        final String msisdn = dialogMsisdnFormatter.formatMsisdnToNbl("9477503841");
    }

    @Test
    public void testFormatMsisdnToNbl3() throws Exception {
        final String msisdn = dialogMsisdnFormatter.formatMsisdnToNbl("+94775038419");
        assertEquals(msisdn, "94775038419");
    }

    @Test
    public void testFormatMsisdnToNbl4() throws Exception {
        final String msisdn = dialogMsisdnFormatter.formatMsisdnToNbl("0775038419");
        assertEquals(msisdn, "94775038419");
    }

    @Test
    public void testFormatMsisdnToNbl5() throws Exception {
        final String msisdn = dialogMsisdnFormatter.formatMsisdnToNbl("775038419");
        assertEquals(msisdn, "94775038419");
    }
}
