package hms.common.registration.util;

import hms.common.registration.model.UserType;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static hms.common.registration.util.UserGroupToTypeMapper.GroupTypeMappingNotFoundException;

public class UserGroupToTypeMapperTest {

    @Test
    public void testGetUserTypeByGroup1() throws GroupTypeMappingNotFoundException {
        UserType type = UserGroupToTypeMapper.getUserTypeByGroup("CORPORATE_USER");
        assertNotNull(type);
        assertEquals(type, UserType.CORPORATE);
    }

    @Test(expectedExceptions = GroupTypeMappingNotFoundException.class)
    public void testGetUserTypeByGroup2() throws GroupTypeMappingNotFoundException {
        UserType type = UserGroupToTypeMapper.getUserTypeByGroup("CORPORATE");
    }
}
