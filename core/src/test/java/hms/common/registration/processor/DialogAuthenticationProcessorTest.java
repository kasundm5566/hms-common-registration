package hms.common.registration.processor;

import hms.common.registration.connectors.api.AuthConnector;
import hms.common.registration.connectors.api.base.AuthRequest;
import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;
import hms.common.registration.connectors.api.parser.AuthRequestParser;
import hms.common.registration.connectors.dialog.base.DialogAuthResponse;
import hms.common.registration.connectors.dialog.base.DialogUser;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import hms.common.registration.util.DialogMsisdnFormatter;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static hms.common.registration.connectors.dialog.base.DialogAuthResponse.*;
import static hms.common.registration.connectors.dialog.base.DialogConnectKeyBox.MOBILE_NO;
import static hms.common.registration.connectors.dialog.base.DialogConnectStatus.*;
import static org.testng.Assert.assertEquals;

@ContextConfiguration(locations = {"classpath:dialog-connect-test.xml"})
public class DialogAuthenticationProcessorTest extends AbstractTestNGSpringContextTests{

    @Autowired
    DialogMsisdnFormatter dialogMsisdnFormatter;

    @Test
    public void authenticate1() throws InvalidRequestTypeException, DataManipulationException {

        DialogAuthenticationProcessor dialogAuthenticationProcessor = new DialogAuthenticationProcessor();


        final Mockery context = new Mockery();
        final AuthRequestParser authRequestParser = context.mock(AuthRequestParser.class);
        final AuthConnector authConnector = context.mock(AuthConnector.class);
        final UserService userService = context.mock(UserService.class);
        final UserGroupService userGroupService = context.mock(UserGroupService.class);
        final SessionRepo sessionRepo = new InMemorySessionRepo();

        dialogAuthenticationProcessor.setConnector(authConnector);
        dialogAuthenticationProcessor.setAuthRequestParser(authRequestParser);
        dialogAuthenticationProcessor.setUserService(userService);
        dialogAuthenticationProcessor.setUserGroupService(userGroupService);
        dialogAuthenticationProcessor.setDialogMsisdnFormatter(dialogMsisdnFormatter);
        dialogAuthenticationProcessor.setSessionRepo(sessionRepo);

        final DialogUser dialogUser = new DialogUser();
        dialogUser.setPhoneNo("775038419");
        dialogUser.setUserId("775038419");
        final DialogAuthResponse dialogAuthResponse = new DialogAuthResponseBuilder().withCode(SUCCESS.getStatusCode()).
                                                                                        withStatus("Success").
                                                                                        withDescription("Successful").
                                                                                        withUser(dialogUser).build();
        final User user = new User();
        user.setUserId("793738373838393939");
        user.setUsername("775038419");

        final UserGroup userGroup = new UserGroup("CONSUMER_USER", "consumer user group.");

        context.checking(new Expectations(){
            {

                oneOf(authRequestParser).parse(with(any(Map.class)));
                oneOf(authConnector).authenticateUser(with(any(AuthRequest.class))); will(returnValue(dialogAuthResponse));
                oneOf(userService).findUserByName(with("775038419"));will(returnValue(null));
                oneOf(userGroupService).findUserGroupByGroupName("CONSUMER_USER");will(returnValue(userGroup));
                oneOf(userService).persist(with(any(User.class)));
            }
        });

        final Map<String, Object> response = dialogAuthenticationProcessor.authenticate(Collections.EMPTY_MAP);

        context.assertIsSatisfied();

        assertEquals(response.get(MOBILE_NO), "94775038419");
    }
}