/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.AssertJUnit.assertEquals;

@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class UserGroupServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserGroupService userGroupService;

    UserGroup userGroup;

    @BeforeClass
    public void setUp() throws DataManipulationException {
        userGroup = new UserGroup();
        userGroup.setDescription("corporate user group");
        userGroup.setName("CORPORATE_USER");
        userGroupService.persist(userGroup);
    }

    @Test
    public void testFindAllUserGroupNames() {
        final List<String> userGroupNames = userGroupService.findAllUserGroupNames();
        assertEquals(userGroupNames.size(), 1);
        assertEquals(userGroupNames.get(0), "CORPORATE_USER");
    }

    @Test
    public void testFindUserGroupByGroupName() {
        final UserGroup userGroupNames = userGroupService.findUserGroupByGroupName("CORPORATE_USER");
        assertEquals(userGroupNames.getName(), "CORPORATE_USER");
    }

    @AfterClass
    public void tearDown() throws DataManipulationException {
        userGroupService.remove(userGroup);
    }

}
