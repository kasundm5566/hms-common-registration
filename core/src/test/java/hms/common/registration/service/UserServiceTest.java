/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.model.UserStatus;
import hms.common.registration.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.*;

import java.util.*;

import static org.testng.Assert.*;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */

//@Test(singleThreaded = true)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class UserServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserService userService;

    User user;

    @BeforeClass
    public void setUp() throws DataManipulationException {
        user = new User();
        user.setUsername("testUser1");
        user.setUserId("123456");
        user.setAddress("Address test");
        user.setMpin("1234");
        user.setDomain("internal");
        user.setDomainId("123");
        user.setMobileNo("123654");
        user.setBirthday(new Date());
        user.setFirstName("first test");
        user.setLastName("last test");
        user.setEmail("test@test.com");
        user.setUserStatus(UserStatus.INITIAL);
        user.setUserType(UserType.ADMIN_USER);
        userService.persist(user);
    }

    @Test
    public void testFindUserByName() throws DataManipulationException {
        user = userService.findUserByName("testUser1");
        assertEquals(user.getUsername(), "testUser1");
    }

    @Test
    public void testFindUserByID() throws DataManipulationException {
        user = userService.findUserByUserId("123456");
        assertEquals(user.getUserId(), "123456");
    }


    @Test
    public void testFindUserByMsisdn() throws DataManipulationException {
        final User user = userService.findUserByMsisdnAndStatus("123654", UserStatus.INITIAL);
        assertEquals(user.getUsername(), "testUser1");
    }

    @Test
    public void testSuspendUserByUserID() throws DataManipulationException {
        final User user = userService.findUserByUserId(this.user.getUserId());
        user.suspend();
        userService.merge(user);
        final User suspendedUser = userService.findUserByUserId(this.user.getUserId());
        assertEquals(suspendedUser.isEnabled(), false);
    }

    @Test
    public void testAllAvailableUsers() throws DataManipulationException {
        List<User> allUsers = userService.getAllAvailableUsers();
        assertEquals(allUsers.size(), 1);
    }

    @AfterClass
    public void tearDown() throws DataManipulationException {
        userService.remove(user);
    }
}