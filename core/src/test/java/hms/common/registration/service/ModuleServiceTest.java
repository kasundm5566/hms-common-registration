/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class ModuleServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ModuleService moduleService;

    private Module module;

    @BeforeClass
    public void setUp() throws DataManipulationException {
        module = new Module();
        module.setDescription("common registration module");
        module.setModuleName("COMMON_REG");
        module.setRolePrefix("ROLE_REG");
        moduleService.persist(module);
    }

    @Test
    public void testFindAllModules() throws DataManipulationException {
        List<String> modules = moduleService.findAllModuleNames();
        assertEquals(modules.size(), 1);
    }


    @Test
    public void testFindModuleByName() throws DataManipulationException {
        List<String> modules = moduleService.findAllModuleNames();
        assertEquals(modules.size(), 1);
        assertEquals(modules.get(0), "COMMON_REG");
    }

    @Test
    public void testSearchModules() throws DataManipulationException {
        List<Module> searchResults = moduleService.searchModules("COMMON_REG");
        assertEquals(searchResults.size(), 1);
        assertEquals(searchResults.get(0).getModuleName(), "COMMON_REG");
    }


    @Test
    public void testUpdate() throws DataManipulationException {
        Module module = moduleService.findModuleByName("COMMON_REG");
        module.setDescription("updated module description");
        moduleService.update(module);
        assertEquals(module.getDescription(), "updated module description");
    }

    @AfterClass
    public void tearDown() throws DataManipulationException {
        moduleService.remove(module);
    }
}
