/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.processor;

import com.google.common.base.Optional;
import hms.common.registration.connectors.dialog.base.AuthenticationStatus;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import hms.common.registration.util.DialogMsisdnFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.common.registration.connectors.dialog.base.DialogConnectKeyBox.*;

public final class DialogSessionProcessor implements SessionProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DialogSessionProcessor.class);

    private UserService userService;
    private DialogMsisdnFormatter dialogMsisdnFormatter;
    private SessionRepo sessionRepo;

    public Map<String, Object> validate(Map<String, Object> request) {

        try {
            String sessionId = (String) request.get(SESSION_ID);

            if(!Optional.fromNullable(sessionId).isPresent()) {
                logger.error("Invalid request parameter(s) - sessionId [{}]  not allowed to be empty.", sessionId);                ;
                return createSimpleResponse(AuthenticationStatus.ERROR_INVALID_REQUEST);
            }

            String username = sessionRepo.validate(sessionId);

            if(Optional.fromNullable(username).isPresent()) {
                try {
                    return createSessionValidationResponse(username);
                } catch (Exception e) {
                    return createSimpleResponse(AuthenticationStatus.ERROR_INTERNAL);
                }
            } else {
                return createSimpleResponse(AuthenticationStatus.ERROR_SESSION_VALIDATION);
            }
        } catch (Exception e) {
            logger.error("Error occurred while validation the session. [{}]", e);
            return createSimpleResponse(AuthenticationStatus.ERROR_SESSION_VALIDATION);
        }
    }

    @Override
    public Map<String, Object> logout(Map<String, Object> request) {
        try {
            sessionRepo.removeSession((String)request.get(SESSION_ID));
            return createSimpleResponse(AuthenticationStatus.SUCCESS);
        } catch (SessionRepo.SessionNotFoundException e) {
            return createSimpleResponse(AuthenticationStatus.ERROR_SESSION_VALIDATION);
        }
    }

    private Map<String, Object> createSimpleResponse(AuthenticationStatus status) {
        Map<String, Object> response = new HashMap<>();
        response.put(STATUS_CODE, status.getStatusCode());
        response.put(STATUS_DESCRIPTION, status.getStatusDescription());
        return response;
    }

    private Map<String, Object> createSessionValidationResponse(String username) throws DataManipulationException, DialogMsisdnFormatter.InvalidDialogNumberException {
        AuthenticationStatus status = AuthenticationStatus.SUCCESS;
        Map<String, Object> response = new HashMap<>();
        User user = userService.findUserByName(username);
        response.put(MOBILE_NO, dialogMsisdnFormatter.formatMsisdnToNbl(user.getMobileNo()));
        response.put(USERNAME, user.getUsername());
        response.put(USER_ID, user.getUserId());
        response.put(STATUS_CODE, status.getStatusCode());
        response.put(STATUS_DESCRIPTION, status.getStatusDescription());
        return response;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setDialogMsisdnFormatter(DialogMsisdnFormatter dialogMsisdnFormatter) {
        this.dialogMsisdnFormatter = dialogMsisdnFormatter;
    }

    public void setSessionRepo(SessionRepo sessionRepo) {
        this.sessionRepo = sessionRepo;
    }
}