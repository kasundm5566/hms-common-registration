/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.processor;

import java.util.HashMap;
import java.util.Map;

public class MockSessionProcessor implements SessionProcessor {
    @Override
    public Map<String, Object> validate(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", "0");
        return response;
    }

    @Override
    public Map<String, Object> logout(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", "0");
        return response;
    }
}
