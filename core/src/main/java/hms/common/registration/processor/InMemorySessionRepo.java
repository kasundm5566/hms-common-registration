package hms.common.registration.processor;

import hms.common.registration.util.SessionKeyGenerator;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class InMemorySessionRepo implements SessionRepo {

    private static final String DATE_PATTERN = "YYYYMMddHHmmssSSS";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
    private static final String DELIMITER = "#####";
    private static final Map<String, String> SESSION_MAP = new HashMap<>();

    @Override
    public synchronized String addSession(String username, String msisdn) {
        String sessionKeySource = username + DELIMITER + dateFormat.format(new Date()) + DELIMITER + msisdn;
        try {
            String sessionKey = SessionKeyGenerator.generateKey(sessionKeySource);
            SESSION_MAP.put(sessionKey, username);
            return sessionKey;
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new AssertionError("Session key generation failed.");
        }
    }

    @Override
    public synchronized void removeSession(String sessionId) throws SessionNotFoundException {
        if(!SESSION_MAP.containsKey(sessionId)) {
            throw new SessionNotFoundException();
        }
        SESSION_MAP.remove(sessionId);
    }

    @Override
    public synchronized String validate(String sessionId) throws SessionNotFoundException {
        if(!SESSION_MAP.containsKey(sessionId)) {
            throw new SessionNotFoundException();
        }
        return SESSION_MAP.get(sessionId);
    }
}