/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.processor;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import hms.common.registration.connectors.api.AuthConnector;
import hms.common.registration.connectors.api.base.AuthRequest;
import hms.common.registration.connectors.api.base.AuthResponse;
import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;
import hms.common.registration.connectors.dialog.base.DialogAuthResponse;
import hms.common.registration.connectors.dialog.base.AuthenticationStatus;
import hms.common.registration.connectors.dialog.base.DialogConnectStatus;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.connectors.api.parser.AuthRequestParser;
import hms.common.registration.model.UserGroup;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import hms.common.registration.util.DialogMsisdnFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.connectors.dialog.base.AuthenticationStatus.*;

import javax.persistence.NoResultException;
import static hms.common.registration.connectors.dialog.base.DialogConnectKeyBox.*;
import static hms.common.registration.util.DialogMsisdnFormatter.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class DialogAuthenticationProcessor implements AuthenticationProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DialogAuthenticationProcessor.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    private AuthConnector connector;
    private AuthRequestParser authRequestParser;
    private UserService userService;
    private UserGroupService userGroupService;
    private DialogMsisdnFormatter dialogMsisdnFormatter;
    private SessionRepo sessionRepo;

    @Override
    public Map<String, Object> authenticate(Map<String, Object> request) {

        logger.debug("Authentication request = [{}] received to authentication processor.", request);

        AuthRequest authRequest = null;
        AuthResponse authResponse = null;

        try {
            authRequest = authRequestParser.parse(request);

        } catch (InvalidRequestTypeException e) {
            logger.error("Request parameters are invalid.", e);
            return createErrorResponse(ERROR_INVALID_REQUEST);
        }

        try {
            authResponse = connector.authenticateUser(authRequest);
        } catch (ClientHandlerException | UniformInterfaceException e) {
            logger.error("Error occurred while connecting to the dialog connect xml api. [{}]", e);
            return createErrorResponse(ERROR_DIALOG_CONNECT);
        } catch (InvalidRequestTypeException e) {
            return createErrorResponse(ERROR_INVALID_REQUEST);
        }

        DialogAuthResponse dialogAuthResponse = (DialogAuthResponse) authResponse;
        if(!DialogConnectStatus.SUCCESS.equals(DialogConnectStatus.getStatusByCode(dialogAuthResponse.getCode()))) {
            return createErrorResponse(ERROR_CODE_RECEIVED_FROM_DIALOG);
        }

        String msisdn = dialogAuthResponse.getUser().getPhoneNo();
        String username = dialogAuthResponse.getUser().getUserId();

        boolean userExist;
        User user = new User();

        try {
            user = userService.findUserByName(username);
            logger.debug("Retrieved user from repository = [{}]", user);
            userExist = Optional.fromNullable(user).isPresent();
        } catch (NoResultException e) {
            userExist = false;
        } catch (DataManipulationException e) {
            return createErrorResponse(ERROR_INTERNAL);
        }


        if(!userExist) {
            user = createUserFromDialogConnectAuthResponse(dialogAuthResponse);

            try {
                logger.debug("User not exist in repository, going to save to repository = [{}].", user);
                userService.persist(user);

            } catch (DataManipulationException e) {
                logger.error("Error occurred while persisting user to the repository. [{}]", e);
                return createErrorResponse(ERROR_INTERNAL);
            }
        }

        try {
            return createAuthSuccessResponse(msisdn, username, user);
        } catch (InvalidDialogNumberException e) {
            return createErrorResponse(ERROR_INTERNAL);
        }
    }

    private User createUserFromDialogConnectAuthResponse(DialogAuthResponse dialogAuthResponse) {
        User user = new User();
        Date date = new Date();

        String firstName = Optional.fromNullable(dialogAuthResponse.getUser()).isPresent() && Optional.fromNullable(dialogAuthResponse.getUser().getFirstName()).isPresent()
                ? dialogAuthResponse.getUser().getFirstName() : "";
        String lastName =  Optional.fromNullable(dialogAuthResponse.getUser()).isPresent() && Optional.fromNullable(dialogAuthResponse.getUser().getLastName()).isPresent()
                ? dialogAuthResponse.getUser().getLastName() : "";

        UserGroup consumerUserGroup = userGroupService.findUserGroupByGroupName("CONSUMER_USER");

        user.setCorporateUser(false);
        user.setEmailVerified(false);
        user.setLdapUser(false);
        user.setMsisdnVerified(false);
        user.setLastModifiedTime(date);
        user.setCurrentLogin(date);
        user.setGender(dialogAuthResponse.getUser().getGender());
        user.setUserId(dateFormat.format(date));
        user.setDomain("dialog");
        user.setDomainId(dialogAuthResponse.getUser().getUserId());
        user.setUserType(UserType.INDIVIDUAL);
        user.setUsername(dialogAuthResponse.getUser().getUserId());
        user.setMobileNo(dialogAuthResponse.getUser().getPhoneNo());
        user.setEmail(dialogAuthResponse.getUser().getEmail());

        user.setFirstName(firstName + lastName);
        user.setUserGroup(consumerUserGroup);
        user.activate();

        return user;
    }

    private Map createAuthSuccessResponse(String msisdn, String username, User user) throws InvalidDialogNumberException {
        final AuthenticationStatus authenticationStatus = SUCCESS;
        final String formattedMsisdn = dialogMsisdnFormatter.formatMsisdnToNbl(msisdn);

        String sessionKey = sessionRepo.addSession(username, formattedMsisdn);

        final ImmutableMap<Object, Object> response = ImmutableMap.builder().
                                                        put(STATUS_CODE, authenticationStatus.getStatusCode()).
                                                        put(STATUS_DESCRIPTION, authenticationStatus.getStatusDescription()).
                                                        put(MOBILE_NO, formattedMsisdn).
                                                        put(USERNAME, username).
                                                        put(USER_ID, user.getUserId()).
                                                        put(SESSION_ID, sessionKey).build();

        return (Map)response;
    }

    private Map<String, Object> createErrorResponse(AuthenticationStatus authenticationStatus) {
        final ImmutableMap<Object, Object> response = ImmutableMap.builder().
                put(STATUS_CODE, authenticationStatus.getStatusCode()).
                put(STATUS_DESCRIPTION, authenticationStatus.getStatusDescription()).build();

        return (Map)response;
    }

    public void setConnector(AuthConnector connector) {
        this.connector = connector;
    }

    public void setAuthRequestParser(AuthRequestParser authRequestParser) {
        this.authRequestParser = authRequestParser;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserGroupService(UserGroupService userGroupService) {
        this.userGroupService = userGroupService;
    }

    public void setDialogMsisdnFormatter(DialogMsisdnFormatter dialogMsisdnFormatter) {
        this.dialogMsisdnFormatter = dialogMsisdnFormatter;
    }

    public void setSessionRepo(SessionRepo sessionRepo) {
        this.sessionRepo = sessionRepo;
    }
}