package hms.common.registration.processor;

/**
 * Created with IntelliJ IDEA.
 * User: isuruanu
 * Date: 11/24/13
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SessionRepo {

    String addSession(String username, String msisdn);

    void removeSession(String sessionId) throws SessionNotFoundException;

    String validate(String sessionId) throws SessionNotFoundException;

    public static class SessionNotFoundException extends Exception {
        public SessionNotFoundException() {
            super("Session not found.");
        }
    }
}
