/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.processor;

import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;
import hms.common.registration.connectors.mock.base.MockAuthResponse;
import hms.common.registration.connectors.mock.builder.MockAuthResponseBuilder;
import hms.common.registration.connectors.mock.parser.MockAuthRequestParser;
import hms.common.registration.exception.MalformedRequestException;

import java.util.Map;

public class MockAuthenticationProcessor implements AuthenticationProcessor {

    private final MockAuthResponseBuilder builder;
    private final MockAuthRequestParser parser;

    public MockAuthenticationProcessor(MockAuthResponseBuilder builder, MockAuthRequestParser parser) {
        this.builder = builder;
        this.parser = parser;
    }

    @Override
    public Map<String, Object> authenticate(Map<String, Object> request) {
        return builder.build(new MockAuthResponse());
    }
}
