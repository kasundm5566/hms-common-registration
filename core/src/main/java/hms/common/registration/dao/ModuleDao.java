/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.model.Module;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Repository
@Deprecated
@Transactional
public class ModuleDao {

    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "Module";

    @Transactional
    public Module merge(Module entity) {
        Module mergedModule = this.entityManager.merge(entity);
        entityManager.flush();
        return mergedModule;
    }

    @Transactional
    public void persist(Module module) {
        entityManager.persist(module);
        entityManager.flush();
    }

    @Transactional
    public void remove(Module module) {
        if (entityManager.contains(module)) {
            entityManager.remove(module);
        } else {
            Module attached = entityManager.find(Module.class,
                    module.getId());
            entityManager.remove(attached);
        }
        entityManager.flush();
    }

    public Module findModuleByName(String moduleName) {
        Query query = entityManager.createQuery("SELECT module FROM " + entityName
                + " module WHERE module.moduleName=:name").setParameter("name", moduleName);
        Module module = (Module) query.getSingleResult();
        return module;

    }

    @SuppressWarnings("unchecked")
    public List<String> findAllModuleNames() {
        Query qryFindAll = entityManager.createQuery("SELECT module.moduleName FROM " + entityName + " module");
        return qryFindAll.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Module> searchModules(String searchKey) {
        Query qryFindAll = entityManager.createQuery("SELECT module FROM " + entityName + " module WHERE " +
                "module.moduleName LIKE :searchKey").setParameter("searchKey", "%" + searchKey + "%");
        return qryFindAll.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Module> findAll() {
        Query qryFindAll = entityManager.createQuery("SELECT module FROM " + entityName + " module");
        return qryFindAll.getResultList();
    }
}

