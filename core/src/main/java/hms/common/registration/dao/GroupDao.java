/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.model.ModuleGroup;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Repository
@Transactional
@Deprecated
public class GroupDao {

    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "Group";

    @Transactional
    public ModuleGroup merge(ModuleGroup moduleGroup) {
        ModuleGroup mergedModuleGroup = this.entityManager.merge(moduleGroup);
        entityManager.flush();
        return mergedModuleGroup;
    }

    @Transactional
    public void persist(ModuleGroup moduleGroup) {
        entityManager.persist(moduleGroup);
        entityManager.flush();
    }

    @Transactional
    public void remove(ModuleGroup moduleGroup) {
        if (entityManager.contains(moduleGroup)) {
            entityManager.remove(moduleGroup);
        } else {
            ModuleGroup attached = entityManager.find(ModuleGroup.class, moduleGroup.getId());
            entityManager.remove(attached);
        }
        entityManager.flush();
    }
}
