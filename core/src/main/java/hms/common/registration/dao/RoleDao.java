/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Repository
@Transactional
@Deprecated
public class RoleDao {
    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "Role";

    @Transactional
    public Role merge(Role role) {
        Role mergedRole = this.entityManager.merge(role);
        entityManager.flush();
        return mergedRole;
    }

    @Transactional
    public void persist(Role role) {
        entityManager.persist(role);
        entityManager.flush();
    }

    @Transactional
    public void remove(Role role) {
        if (entityManager.contains(role)) {
            entityManager.remove(role);
        } else {
            Role attached = entityManager.find(Role.class,
                    role.getId());
            entityManager.remove(attached);
        }
        entityManager.flush();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<Role> findAllRoles() {
        Query query = entityManager.createQuery("SELECT role FROM "  + entityName + " role");
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<Role> findAllRolesByModule(Module module) {
        Query query = entityManager.createQuery("SELECT role FROM " + entityName + " role WHERE " +
                "role.module=:module").setParameter("module", module);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<String> findAllRoleNames() {
        Query query = entityManager.createQuery("SELECT role.name FROM "  + entityName + " role");
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public Role findAllRoleByName(String roleName) {
        Query query = entityManager.createQuery("SELECT role FROM " + entityName + " role WHERE " +
                "role.name=:name").setParameter("name", roleName);
        return (Role) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<Long> findAllRoleIdsByModule(Module module) {
        Query query = entityManager.createQuery("SELECT role.id FROM " + entityName + " role WHERE " +
                "role.module=:module").setParameter("module", module);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<Long> findAllRoleIds() {
        Query query = entityManager.createQuery("SELECT role.id FROM "  + entityName + " role");
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<Role> findAllRolesByModuleWithPaging(Module module, int indexStart, int recordsPerPage) {
        Query query = entityManager.createQuery("SELECT role FROM " + entityName + " role WHERE " +
                "role.module=:module").setParameter("module", module);
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<Role> findAllRolesWithPaging(int indexStart, int recordsPerPage) {
        Query query = entityManager.createQuery("SELECT role FROM "  + entityName + " role");
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);
        return query.getResultList();
    }
}

