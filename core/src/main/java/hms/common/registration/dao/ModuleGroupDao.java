/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.model.ModuleGroup;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Module Group DAO implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Deprecated
@Repository
@Transactional
public class ModuleGroupDao {

    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "ModuleGroup";

    @Transactional
    public ModuleGroup merge(ModuleGroup moduleGroup) {
        ModuleGroup mergedModuleGroup = this.entityManager.merge(moduleGroup);
        entityManager.flush();
        return mergedModuleGroup;
    }

    @Transactional
    public void persist(ModuleGroup moduleGroup) {
        entityManager.persist(moduleGroup);
        entityManager.flush();
    }

    @Transactional
    public void remove(ModuleGroup moduleGroup) {
        if (entityManager.contains(moduleGroup)) {
            entityManager.remove(moduleGroup);
        } else {
            ModuleGroup attached = entityManager.find(ModuleGroup.class,
                    moduleGroup.getId());
            entityManager.remove(attached);
        }
        entityManager.flush();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<String> findAllModuleGroupNames() {
        Query qryFindAll = entityManager.createQuery("SELECT moduleGroup.name FROM " + entityName + " moduleGroup");
        return qryFindAll.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public ModuleGroup findModuleGroupByName(String groupName) {
        Query qryFindAll = entityManager.createQuery("SELECT moduleGroup FROM " + entityName + " moduleGroup WHERE " +
                "moduleGroup.name=:name").setParameter("name", groupName);
        return (ModuleGroup) qryFindAll.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    @Transactional (readOnly = true)
    public List<ModuleGroup> findAllModuleGroups() {
        Query query = entityManager.createQuery("SELECT moduleGroup FROM " + entityName + " moduleGroup");
        return query.getResultList();
    }

    public List<ModuleGroup> findModuleGroupListByName(String groupName) {
        Query query = entityManager.createQuery("SELECT moduleGroup FROM " + entityName + " moduleGroup WHERE " +
                "moduleGroup.name LIKE :name").setParameter("name", groupName + "%");
        return query.getResultList();
    }

    public List<String> findAllModuleGroupNamesExceptVirtualGroups(String sub_corporate_virtual_group_begin_keyword) {
        Query qryFindAll = entityManager.createQuery("SELECT moduleGroup.name FROM " + entityName + " moduleGroup WHERE " +
                 "moduleGroup.name NOT LIKE :name").setParameter("name", sub_corporate_virtual_group_begin_keyword + "%");
        return qryFindAll.getResultList();
    }
}