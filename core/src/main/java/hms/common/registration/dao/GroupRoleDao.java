/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.model.GroupRole;
import hms.common.registration.model.ModuleGroup;
import hms.common.registration.model.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Deprecated
@Repository
@Transactional
public class GroupRoleDao {

    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "GroupRole";

    @Transactional
    public GroupRole merge(GroupRole groupRole) {
        GroupRole mergedGroupRole = this.entityManager.merge(groupRole);
        entityManager.flush();
        return mergedGroupRole;
    }

    @Transactional
    public void persist(GroupRole groupRole) {
        entityManager.persist(groupRole);
        entityManager.flush();
    }

    @Transactional
    public void remove(GroupRole groupRole) {
        if (entityManager.contains(groupRole)) {
            entityManager.remove(groupRole);
        } else {
            GroupRole attached = entityManager.find(GroupRole.class,
                    groupRole.getId());
            entityManager.remove(attached);
        }
        entityManager.flush();
    }

    public List<ModuleGroup> findModuleGroupByRole(Role role) {
        Query query = entityManager.createQuery("SELECT groupRole.group FROM " + entityName + " groupRole WHERE " +
                "groupRole.role=:role").setParameter("role", role);
        return query.getResultList();
    }

    public List<ModuleGroup> findModuleGroupByRoleHavingName(Role role, String groupName) {
        Query query = entityManager.createQuery("SELECT groupRole.group FROM " + entityName + " groupRole WHERE " +
                "groupRole.role=:role AND groupRole.group.name LIKE :groupName").setParameter("role", role).setParameter("groupName", groupName + "%");
        return query.getResultList();
    }

    public List<Role> findRolesAssignedToGroup(ModuleGroup group) {
        Query query = entityManager.createQuery("SELECT groupRole.role FROM " + entityName + " groupRole WHERE " +
                "groupRole.group=:group").setParameter("group", group);
        return query.getResultList();
    }

    public void removeSubUsersGroupRole(ModuleGroup moduleGroup, Role role) {
        Query query = entityManager.createQuery("SELECT groupRole FROM " + entityName + " groupRole WHERE " +
                "groupRole.group=:group AND groupRole.role=:role");
        query.setParameter("group", moduleGroup);
        query.setParameter("role",role);
        GroupRole groupRole = (GroupRole) query.getSingleResult();
        remove(groupRole);
    }

    public GroupRole findGroupRoleHavingModuleGroupAndName(ModuleGroup moduleGroup, String roleName) {
        Query query = entityManager.createQuery("SELECT groupRole FROM " + entityName + " groupRole WHERE " +
                "groupRole.group=:group AND groupRole.role.name=:role");
        query.setParameter("group", moduleGroup);
        query.setParameter("role",roleName);
        return (GroupRole) query.getSingleResult();
    }
}
