/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.common.Condition;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.model.UserStatus;
import hms.common.registration.model.UserType;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static hms.common.registration.util.CommonRegistrationLogger.getBeneficiaryJSONObject;
import static hms.common.registration.util.CommonRegistrationLogger.log;
import static hms.common.registration.util.RegistrationFeatureRegistry.isBeneficiaryAvailable;
import static hms.common.registration.util.RegistrationFeatureRegistry.isReconciliationDetailsEnabled;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Repository
@Transactional
public class UserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

    private static final String findUserByNameAndPasswordQuery = "select user FROM User as user where user.username = :userName and user.password = :password";

    private static final String findUserActivationStatusByEmailVerificationCodeAndUserIdQuery =
            "select user FROM User user WHERE user.emailVerificationCode = :verificationCode and user.userId = :userId and user.emailVerified = true";

    private static final String findUserByEmailQuery = "select user FROM User user WHERE user.email = :userEmail and user.userId <> :userId and user.userType='CORPORATE'";

    private final static String findByMsisdnAndStatusQuery = "select user FROM User as user where user.mobileNo= :mobileNo AND user.userStatus = :userStatus";

    private final static String findByMsisdnQuery = "select user FROM User as user where user.mobileNo= :mobileNo";

    private static final String findUserByEmailVerificationCodeAndUserIdQuery = "select user FROM User user WHERE user.emailVerificationCode = :verificationCode and user.userId = :userId";

    /**
     * Map containing relevant fields of the User table
     */
    private static final Map<String, String> searchByParamsQueryMap = new HashMap<String, String>();

    static {
        searchByParamsQueryMap.put("username", "user.username like '");
        searchByParamsQueryMap.put("firstName", "user.firstName like '");
        searchByParamsQueryMap.put("status", "user.userStatus='");
        searchByParamsQueryMap.put("type", "user.userType='");
        searchByParamsQueryMap.put("coporateParent", "user.corperateParentId.username='");
    }

    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "User";

    private void logCorporateUserInfo(User user) {
        if (!user.isCorporateUser()) {
            return;
        }

        if (isBeneficiaryAvailable()) {
            JSONObject object = getBeneficiaryJSONObject(user.getBeneficiaryName(), user.getBankCode(),
                    user.getBankBranchCode(), user.getBankBranchName(), user.getBankAccountNumber());
            log(user.getUserId(), object);
        } else if (isReconciliationDetailsEnabled()) {
            log(user);
        }
    }

    @Transactional(readOnly = true)
    public User findUserByName(String userName) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " as user where user.username= '" + userName + "'");
        User user = (User) query.getSingleResult();
        return user;
    }


    @Transactional(readOnly = true)
    public User findUserByEmail(String email) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " as user where user.email= '" + email + "'");
        User user = (User) query.getSingleResult();
        return user;
    }

    @Transactional(readOnly = true)
    public User findUserById(long id) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " as user where user.id= " + id + "");
        User user = (User) query.getSingleResult();
        return user;
    }

    @Transactional(readOnly = true)
    public User findUserByUserId(String userId) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " as user where user.userId = " + userId + "");
        User user = (User) query.getSingleResult();
        return user;
    }

    @Transactional(readOnly = true)
    public User findUserByDomainId(String domain, String domainId) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " as user where user.domain = :domain and user.domainId = :domainId ");
        return (User) query.setParameter("domain", domain).setParameter("domainId", domainId).getSingleResult();
    }

    @Transactional
    public User merge(User user) {
        User merged = entityManager.merge(user);
        entityManager.flush();

        logCorporateUserInfo(user);

        return merged;
    }

    @Transactional
    public void persist(User user) {
        entityManager.persist(user);
        entityManager.flush();

        logCorporateUserInfo(user);
    }

    @Transactional
    public void remove(User user) {
        if (entityManager.contains(user)) {
            entityManager.remove(user);
        } else {
            User attached = entityManager.find(User.class,
                    user.getId());
            entityManager.remove(attached);
        }
        entityManager.flush();
    }

    @Transactional
    public User convertUser(User previousUser, User newUser) {
        remove(previousUser);
        persist(newUser);
        return newUser;
    }

    @Transactional
    public User findByEmailVerificationCode(String emailVerificationCode) {
        Query query = entityManager.createQuery("FROM " + entityName + " as user where " +
                "user.emailVerificationCode='" + emailVerificationCode + "'");
        User user = (User) query.getSingleResult();
        return user;
    }

    @Transactional(readOnly = true)
    public User findByMsisdn(String msisdn) {
        Query query = entityManager.createQuery(findByMsisdnQuery);
        query.setParameter("mobileNo", msisdn);
        User user = (User) query.getSingleResult();
        return user;
    }

    @Transactional(readOnly = true)
    public User findByMsisdnAndStatus(String msisdn, UserStatus userStatus) {
        Query query = entityManager.createQuery(findByMsisdnAndStatusQuery);
        query.setParameter("mobileNo", msisdn);
        query.setParameter("userStatus", userStatus);
        User user = (User) query.getSingleResult();
        return user;
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<User> findAllUsers() {
        Query query = entityManager.createQuery("SELECT user FROM " + entityName + " user");
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<User> findAllUsersWithPaging(int start, int limit) {
        Query query = entityManager.createQuery("SELECT user FROM " + entityName + " user").setFirstResult(start).setMaxResults(limit);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public int findAllUsersCount() {
        Query query = entityManager.createQuery("SELECT user FROM " + entityName + " user");
        return query.getResultList().size();
    }

    @Transactional(readOnly = true)
    public List<User> getUserList(Map<String, Object> parameters) {
        Query query = entityManager.createQuery(getQueryString(parameters,
                "FROM " + entityName + " as user where ", false));
        return query.getResultList();
    }

    /**
     * Create the query string containing the given parameters
     *
     * @param parameters
     * @return query string
     */
    private String getQueryString(Map<String, Object> parameters, String baseString, boolean moreParamsExists) {
        StringBuilder stringBuilder = new StringBuilder(baseString);
        if (parameters.isEmpty() && !moreParamsExists) {
            stringBuilder.replace(stringBuilder.length() - " WHERE ".length(), stringBuilder.length(), "");
        } else if (!parameters.isEmpty()) {
            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                String key = entry.getKey();
                String sqlSegment = searchByParamsQueryMap.get(key);
                if (sqlSegment != null) {
                    stringBuilder.append(sqlSegment);
                    if (sqlSegment.contains("like")) {
                        stringBuilder.append(parameters.get(key) + "%'");
                    } else {
                        stringBuilder.append(parameters.get(key) + "'");
                    }
                    stringBuilder.append(" AND ");
                }
            }
            stringBuilder.replace(stringBuilder.length() - " AND ".length(), stringBuilder.length(), "");
        }
        return stringBuilder.toString();
    }

    /**
     * @param indexStart
     * @param recordsPerPage
     * @param parameters
     * @return
     */

    @Transactional(readOnly = true)
    public List<User> getUserListWithPaging(int indexStart, int recordsPerPage, Map<String, Object> parameters) {

        Query query = entityManager.createQuery(getQueryString(parameters,
                "FROM " + entityName + " as user where ", false));
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);
        return query.getResultList();
    }

    public List<String> findAllUserNamesByType(UserType userType) {
        Query query = entityManager.createQuery("SELECT user.username FROM " + entityName + " user WHERE " +
                "user.userType='" + userType + "'");
        return query.getResultList();
    }

    @Transactional(readOnly = true)
    public List<Long> getUserIdList(Map<String, Object> parameterMap) {
        String queryString = getQueryString(parameterMap,
                "SELECT user.id FROM " + entityName + " user WHERE ", false);
        Query query = entityManager.createQuery(queryString);
        return query.getResultList();
    }

//    @Transactional(readOnly = true)
//     public List<Long> getUserIdListParameterMapAndUserGroup(Map<String, Object> parameterMap, UserGroup userGroup) {
//           String queryString = getQueryString(parameterMap,
//                   "SELECT user.id FROM " + entityName + " user WHERE user.userGroup = :userGroup ", true);
//           Query query = entityManager.createCustomQuery(queryString);
//            query.setParameter("userGroup", userGroup);
//           return query.getResultList();
//       }

    /**
     * set mobile no field null
     * if msisdn is not verified within th given interval from last message send date
     *
     * @param interval
     */
    public void removeAllExpiredMsisdns(TimeUnit timeUnit, long interval) {

        Date date = new Date(new Date().getTime() - timeUnit.toMillis(interval));
        Query query = entityManager.createQuery("FROM " + entityName + " as user WHERE " +
                "user.msisdnVerified='" + false + "' AND " +
                "user.userType='" + UserType.INDIVIDUAL + "' AND " +
                "user.userStatus='" + UserStatus.INITIAL + "' AND " +
                "user.lastSmsSendDate<'" + date + "'");

        List<User> users = query.getResultList();
        for (User user : users) {
            remove(user);
        }
    }

    public void removeAllExpiredEmailAddresses(TimeUnit timeUnit, long checkingInterval) {

        Date date = new Date(new Date().getTime() - timeUnit.toMillis(checkingInterval));
        Query query = entityManager.createQuery("FROM " + entityName + " as user WHERE " +
                "user.emailVerified='" + false + "' AND " +
                "user.userType='" + UserType.CORPORATE + "' AND " +
                "user.userStatus='" + UserStatus.INITIAL + "' AND " +
                "user.lastEmailSendDate<'" + date + "'");

        List<User> users = query.getResultList();
        for (User user : users) {
            remove(user);
        }
    }

    public boolean isMsisdnAlreadyTaken(String msisdn) {
        Query query = entityManager.createQuery("SELECT user FROM " + entityName + " user WHERE " +
                "user.mobileNo='" + msisdn + "' AND user.userStatus<>'" + UserStatus.INITIAL + "'");
        return !query.getResultList().isEmpty();
    }

    public boolean isEmailAvailableForUserId(String email, String userId) {
        Query query = entityManager.createQuery(findUserByEmailQuery);
        query.setParameter("userEmail", email);
        query.setParameter("userId", userId);
        return query.getResultList().isEmpty();
    }

    public User findUserByEmailVerificationCodeAndUserID(String userID, String emailVerification) {

        User user = null;
        Query query = entityManager.createQuery(findUserByEmailVerificationCodeAndUserIdQuery);
        query.setParameter("verificationCode", emailVerification);
        query.setParameter("userId", userID);
        user = (User) query.getSingleResult();

        return user;

    }

    public User updateUserAccountActivationStatus(String userID, String emailVerification) {


        User user_updated = findUserByEmailVerificationCodeAndUserID(userID, emailVerification);

        user_updated.setEmailVerified(true);

        merge(user_updated);

        return user_updated;
    }

    public boolean findUserActivationStatusByEmailVerificationCodeAndUserId(String userId, String emailVerification) {

        try {
            Query query = entityManager.createQuery(findUserActivationStatusByEmailVerificationCodeAndUserIdQuery);
            query.setParameter("verificationCode", emailVerification);
            query.setParameter("userId", userId);
            User user = (User) query.getSingleResult();
            return true;

        } catch (Exception ex) {

            return false;
        }


    }

    @Transactional
    public List<User> getUsersOfTheGivenUserGroup(UserGroup userGroup) {
        long userGroupId = userGroup.getId();

        Query query = entityManager.createQuery("FROM " + entityName + " as user WHERE " +
                "user.userGroup=" + userGroupId + "");

        List<User> retrievedUsers = query.getResultList();

        return retrievedUsers;

    }

    public List<Long> findUserIdListByConditionList(List<Condition> conditionList) {

        logger.debug("Searching all user ids by condition list {}", conditionList);

        String jpaQuery = "SELECT user.id FROM User as user" + createQueryString(conditionList);
        Query query = entityManager.createQuery(jpaQuery);

        logger.debug("Executing JPA QUERY {}", jpaQuery);

        for (Condition condition : conditionList) {

            query.setParameter(condition.getKey(), condition.getValue());
        }

        return query.getResultList();
    }

    public List<User> getUserListByConditionList(int indexStart, int recordsPerPage, List<Condition> conditionList) {

        logger.debug("Searching all user by condition list {}", conditionList);

        String jpaQuery = "FROM " + entityName + " as user" + createQueryString(conditionList);
        Query query = entityManager.createQuery(jpaQuery);
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);

        logger.debug("Executing JPA QUERY {}", jpaQuery);

        for (Condition condition : conditionList) {

            query.setParameter(condition.getKey(), condition.getValue());
        }

        return query.getResultList();

    }

    private String createQueryString(List<Condition> conditionList) {
        if (conditionList.isEmpty()) {
            return "";
        }

        StringBuilder stringQuery = new StringBuilder();
        stringQuery.append(" where ");

        for (Condition condition : conditionList) {

            String key = condition.getKey();
            String operator = condition.getOperator();
            String value = ":" + condition.getKey().toString();

            stringQuery.append("user." + key + " " + operator + " " + value + " AND ");
        }

        if (!conditionList.isEmpty()) {
            stringQuery.replace(stringQuery.length() - 4, stringQuery.length(), "");
        }

        return stringQuery.toString();
    }

    public User findUserByName(String userName, String password) {
        Query query = entityManager.createQuery(findUserByNameAndPasswordQuery);
        query.setParameter("userName", userName);
        query.setParameter("password", password);
        return (User) query.getSingleResult();
    }
}

