/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.dao;

import hms.common.registration.model.ModuleGroup;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Repository
@Transactional
public class UserGroupDao {
    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "UserGroup";

    @Transactional
    public UserGroup merge(UserGroup userGroup) {
        UserGroup merged = this.entityManager.merge(userGroup);
        entityManager.flush();
        return merged;
    }

    @Transactional(readOnly = true)
    public List<User> findAllUsersByModuleGroupWithPaging(ModuleGroup moduleGroup, int indexStart, int recordsPerPage) {
        Query query = entityManager.createQuery("SELECT userGroup.user FROM " + entityName
                + " userGroup WHERE userGroup.group=:group").setParameter("group", moduleGroup);
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);
        return query.getResultList();
    }

    /**
     * Map containing relevant fields of the User table
     */
    private static Map<String, String> searchByParamsQueryMap = new HashMap<String, String>();
    static {
        searchByParamsQueryMap.put("username", "userGroup.user.username like '");
        searchByParamsQueryMap.put("firstName", "userGroup.user.firstName like '");
        searchByParamsQueryMap.put("status", "userGroup.user.userStatus like '");
        searchByParamsQueryMap.put("type", "userGroup.user.userType like '");
    }

    /**
     *
     * @param parameters
     * @param baseString
     * @return
     */
    private String getQueryString(Map<String, Object> parameters, String baseString) {

        StringBuilder stringBuilder = new StringBuilder(baseString);
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            String key = entry.getKey();
            String sqlSegment = searchByParamsQueryMap.get(key);
            if (sqlSegment != null) {
                stringBuilder.append(" AND ");
                stringBuilder.append(sqlSegment);
                stringBuilder.append(parameters.get(key) + "%'");
            }
        }
        return stringBuilder.toString();
    }

    public List<Long> findAllUserIdsByModuleGroup(ModuleGroup moduleGroup, Map<String, Object> parameterMap) {
        Query query = entityManager.createQuery(getQueryString(parameterMap,
                "SELECT userGroup.user.id FROM " + entityName
                + " userGroup WHERE userGroup.group=:group")).setParameter("group", moduleGroup);
        return query.getResultList();
    }

    public UserGroup findUserGroupHavingUserAndGroup(String username, ModuleGroup moduleGroup) {
        Query query = entityManager.createQuery("SELECT userGroup FROM " + entityName
                + " userGroup WHERE userGroup.user.username=:user AND userGroup.group=:group");
        query = query.setParameter("user", username);
        query = query.setParameter("group", moduleGroup);
        return (UserGroup) query.getSingleResult();
    }

    public List<UserGroup> findAllGroupsByModuleGroup(ModuleGroup moduleGroup) {
        Query query = entityManager.createQuery("SELECT userGroup FROM " + entityName
                + " userGroup WHERE userGroup.group=:group").setParameter("group", moduleGroup);
        return query.getResultList();
    }

    public UserGroup findUserGroupWithAllUsers(long groupId) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " userGroup left join fetch userGroup.users WHERE userGroup.id = :groupId").setParameter("groupId", groupId);
        return (UserGroup) query.getSingleResult();
    }


//    public List<UserGroup> findAllUserGroups() {
//        Query query = entityManager.createQuery(" FROM " + entityName
//                + "");
//        return query.getResultList();
//    }


}

