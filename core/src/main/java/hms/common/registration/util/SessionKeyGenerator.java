/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Hex;

public final class SessionKeyGenerator {

    public static String generateKey(String text) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        return md5Encryption(text);
    }

    private static String md5Encryption(String value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return new String(Hex.encodeHex(MessageDigest.getInstance("MD5").digest(value.getBytes("UTF-8"))));
    }

}