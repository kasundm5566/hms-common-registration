/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RegistrationFeatureRegistry {

    private static boolean beneficiaryAvailable;
    private static boolean autoSpCreationEnabled;

    private static boolean corporateUserEmailVerifyEnabled;
    private static boolean corporateUserMsisdnVerifyEnabled;
    private static boolean corporateUserMsisdnVerifySkippable;
    private static boolean consumerUserEmailVerifyEnabled;
    private static boolean consumerUserMsisdnVerifyEnabled;

    private static boolean captchaValidationEnabled;
    private static boolean mpinEnabled;
    private static boolean corporateUserEnabled;
    private static boolean consumerUserEnabled;
    private static boolean subCorporateUserEnabled;

    private static boolean ldapOptionEnabled;
    private static boolean helpLinkEnabled;
    private static boolean externalTermsLinkEnabled;

    private static boolean firstTimeLoginMessage;
    private static boolean msisdnVerificationLayoutWithRetry;

    private static boolean professionFieldVisible;
    private static boolean ldapFieldVisible;

    private static boolean reconciliationDetailsEnabled;
    private static boolean showTermsAndCondition;
    private static boolean showLeftSideBanner;

    private static boolean multipleDeveloperAccountTypeEnabled;
    private static boolean accountRecoveryUsingEmail;
    private static boolean msisdnToEmailFormattingEnabled;

    static {
        setBeneficiaryAvailable(false);
        setAutoSpCreationEnabled(false);
        setCorporateUserEmailVerifyEnabled(false);
        setCorporateUserMsisdnVerifyEnabled(false);
        setCorporateUserMsisdnVerifySkippable(false);
        setConsumerUserEmailVerifyEnabled(false);
        setConsumerUserMsisdnVerifyEnabled(false);
        setCaptchaValidationEnabled(false);
        setMpinEnabled(false);
        setCorporateUserEnabled(false);
        setConsumerUserEnabled(false);
        setSubCorporateUserEnabled(false);
        setLdapOptionEnabled(false);
        setHelpLinkEnabled(false);
        setExternalTermsLinkEnabled(false);
        setMsisdnVerificationLayoutWithRetry(false);
        setFirstTimeLoginMessage(false);
        setLdapFieldVisible(false);
        setProfessionFieldVisible(false);
        setReconciliationDetailsEnabled(false);
        setShowTermsAndCondition(false);
        setShowLeftSideBanner(false);
        setMultipleDeveloperAccountTypeEnabled(false);
        setAccountRecoveryUsingEmail(false);
        setMsisdnToEmailFormattingEnabled(false);
    }
    /*
    * Setter should be private static to statically bind
    * */
    private static void setBeneficiaryAvailable(boolean beneficiaryAvailable) {
        RegistrationFeatureRegistry.beneficiaryAvailable = beneficiaryAvailable;
    }

    private static void setAutoSpCreationEnabled(boolean autoSpCreationEnabled) {
        RegistrationFeatureRegistry.autoSpCreationEnabled = autoSpCreationEnabled;
    }

    private static void setCorporateUserEmailVerifyEnabled(boolean corporateUserEmailVerifyEnabled) {
        RegistrationFeatureRegistry.corporateUserEmailVerifyEnabled = corporateUserEmailVerifyEnabled;
    }

    private static void setCorporateUserMsisdnVerifyEnabled(boolean corporateUserMsisdnVerifyEnabled) {
        RegistrationFeatureRegistry.corporateUserMsisdnVerifyEnabled = corporateUserMsisdnVerifyEnabled;
    }

    private static void setConsumerUserEmailVerifyEnabled(boolean consumerUserEmailVerifyEnabled) {
        RegistrationFeatureRegistry.consumerUserEmailVerifyEnabled = consumerUserEmailVerifyEnabled;
    }

    private static void setConsumerUserMsisdnVerifyEnabled(boolean consumerUserMsisdnVerifyEnabled) {
        RegistrationFeatureRegistry.consumerUserMsisdnVerifyEnabled = consumerUserMsisdnVerifyEnabled;
    }

    private static void setCaptchaValidationEnabled(boolean captchaValidationEnabled) {
        RegistrationFeatureRegistry.captchaValidationEnabled = captchaValidationEnabled;
    }

    private static void setMpinEnabled(boolean mpinEnabled) {
        RegistrationFeatureRegistry.mpinEnabled = mpinEnabled;
    }

    private static void setCorporateUserEnabled(boolean corporateUserEnabled) {
        RegistrationFeatureRegistry.corporateUserEnabled = corporateUserEnabled;
    }

    private static void setConsumerUserEnabled(boolean consumerUserEnabled) {
        RegistrationFeatureRegistry.consumerUserEnabled = consumerUserEnabled;
    }

    private static void setSubCorporateUserEnabled(boolean subCorporateUserEnabled) {
        RegistrationFeatureRegistry.subCorporateUserEnabled = subCorporateUserEnabled;
    }

    private static void setLdapOptionEnabled(boolean ldapOptionEnabled) {
        RegistrationFeatureRegistry.ldapOptionEnabled = ldapOptionEnabled;
    }

    private static void setHelpLinkEnabled(boolean helpLinkEnabled) {
        RegistrationFeatureRegistry.helpLinkEnabled = helpLinkEnabled;
    }

    private static void setExternalTermsLinkEnabled(boolean externalTermsLinkEnabled) {
        RegistrationFeatureRegistry.externalTermsLinkEnabled = externalTermsLinkEnabled;
    }

    private static void setFirstTimeLoginMessage(boolean firstTimeLoginMessage) {
        RegistrationFeatureRegistry.firstTimeLoginMessage = firstTimeLoginMessage;
    }

    private static void setMsisdnVerificationLayoutWithRetry(boolean msisdnVerificationLayoutWithRetry) {
        RegistrationFeatureRegistry.msisdnVerificationLayoutWithRetry = msisdnVerificationLayoutWithRetry;
    }

    private static void setCorporateUserMsisdnVerifySkippable(boolean corporateUserMsisdnVerifySkippable) {
        RegistrationFeatureRegistry.corporateUserMsisdnVerifySkippable = corporateUserMsisdnVerifySkippable;
    }

    private static void setReconciliationDetailsEnabled(boolean reconciliationDetailsEnabled) {
        RegistrationFeatureRegistry.reconciliationDetailsEnabled = reconciliationDetailsEnabled;
    }

    private static void setShowTermsAndCondition(boolean showTermsAndCondition) {
        RegistrationFeatureRegistry.showTermsAndCondition = showTermsAndCondition;
    }

    private static void setShowLeftSideBanner(boolean showLeftSideBanner) {
        RegistrationFeatureRegistry.showLeftSideBanner = showLeftSideBanner;
    }

    private static void setMultipleDeveloperAccountTypeEnabled(boolean multipleDeveloperAccountTypeEnabled) {
        RegistrationFeatureRegistry.multipleDeveloperAccountTypeEnabled = multipleDeveloperAccountTypeEnabled;
    }

    private static void setAccountRecoveryUsingEmail(boolean accountRecoveryUsingEmail) {
        RegistrationFeatureRegistry.accountRecoveryUsingEmail = accountRecoveryUsingEmail;
    }

    private static void setMsisdnToEmailFormattingEnabled(boolean msisdnToEmailFormattingEnabled) {
        RegistrationFeatureRegistry.msisdnToEmailFormattingEnabled = msisdnToEmailFormattingEnabled;
    }

    public static boolean isBeneficiaryAvailable() {
        return beneficiaryAvailable;
    }

    public static boolean isAutoSpCreationEnabled() {
        return autoSpCreationEnabled;
    }

    public static boolean isCorporateUserEmailVerifyEnabled() {
        return corporateUserEmailVerifyEnabled;
    }

    public static boolean isCorporateUserMsisdnVerifyEnabled() {
        return corporateUserMsisdnVerifyEnabled;
    }

    public static boolean isConsumerUserEmailVerifyEnabled() {
        return consumerUserEmailVerifyEnabled;
    }

    public static boolean isConsumerUserMsisdnVerifyEnabled() {
        return consumerUserMsisdnVerifyEnabled;
    }

    public static boolean isCaptchaValidationEnabled() {
        return captchaValidationEnabled;
    }

    public static boolean isMpinEnabled() {
        return mpinEnabled;
    }

    public static boolean isCorporateUserEnabled() {
        return corporateUserEnabled;
    }

    public static boolean isConsumerUserEnabled() {
        return consumerUserEnabled;
    }

    public static boolean isSubCorporateUserEnabled() {
        return subCorporateUserEnabled;
    }

    public static boolean isLdapOptionEnabled() {
        return ldapOptionEnabled;
    }

    public static boolean isHelpLinkEnabled() {
        return helpLinkEnabled;
    }

    public static boolean isExternalTermsLinkEnabled() {
        return externalTermsLinkEnabled;
    }

    public static boolean isFirstTimeLoginMessageEnabled() {
        return firstTimeLoginMessage;
    }

    public static boolean isFirstTimeLoginMessage() {
        return firstTimeLoginMessage;
    }

    public static boolean isMsisdnVerificationLayoutWithRetry() {
        return msisdnVerificationLayoutWithRetry;
    }

    public static boolean isCorporateUserMsisdnVerifySkippable(){
        return corporateUserMsisdnVerifySkippable;
    }

    public static boolean isProfessionFieldVisible() {
        return professionFieldVisible;
    }

    public static void setProfessionFieldVisible(boolean professionFieldVisible) {
        RegistrationFeatureRegistry.professionFieldVisible = professionFieldVisible;
    }

    public static boolean isLdapFieldVisible() {
        return ldapFieldVisible;
    }

    public static void setLdapFieldVisible(boolean ldapFieldVisible) {
        RegistrationFeatureRegistry.ldapFieldVisible = ldapFieldVisible;
    }

    public static boolean isReconciliationDetailsEnabled() {
        return reconciliationDetailsEnabled;
    }

    public static boolean isShowTermsAndCondition() {
        return showTermsAndCondition;
    }

    public static boolean isShowLeftSideBanner() {
        return showLeftSideBanner;
    }

    public static boolean isMultipleDeveloperAccountTypeEnabled() {
        return multipleDeveloperAccountTypeEnabled;
    }

    public static boolean isAccountRecoveryUsingEmail() {
        return accountRecoveryUsingEmail;
    }

    public static boolean isMsisdnToEmailFormattingEnabled() {
        return msisdnToEmailFormattingEnabled;
    }
}
