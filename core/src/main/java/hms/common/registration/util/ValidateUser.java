/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

import hms.common.registration.model.User;

import static hms.common.registration.model.UserType.CORPORATE;
import static hms.common.registration.model.UserType.INDIVIDUAL;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ValidateUser {

    public static boolean isValidUser(User user) {
        if (user.getUserType().equals(INDIVIDUAL)) {
            return isVerified(isConsumerUserEmailVerifyEnabled(), user.isEmailVerified()) &&
                    isVerified(isConsumerUserMsisdnVerifyEnabled(), user.isMsisdnVerified());
        } else if (user.getUserType().equals(CORPORATE)) {
            return isVerified(isCorporateUserEmailVerifyEnabled(), user.isEmailVerified()) &&
                    (isVerified(isCorporateUserMsisdnVerifyEnabled(), user.isMsisdnVerified()) ||
                      (isCorporateUserMsisdnVerifySkippable() && user.isMsisdnVerifySkip())) ;
        }
        return true;
    }

    public static boolean isVerified(boolean x, boolean y) {
        return !(x && !y);
    }
}
