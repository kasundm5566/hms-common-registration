/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

import hms.common.registration.model.UserType;

import java.util.HashMap;
import java.util.Map;

import static hms.common.registration.model.UserType.*;

public class UserGroupToTypeMapper {

    private static final Map<String, UserType> userGroups = new HashMap<>();

    static {
        userGroups.put("SDP_ADMIN", ADMIN_USER);
        userGroups.put("SYSTEM_ADMIN", ADMIN_USER);
        userGroups.put("READONLY_ADMIN", ADMIN_USER);
        userGroups.put("CORPORATE_USER", CORPORATE);
        userGroups.put("CONSUMER_USER", INDIVIDUAL);
        userGroups.put("SUB_CORPORATE_USER", SUB_CORPORATE);
        userGroups.put("SUPER_CORPORATE_USER", CORPORATE);
        userGroups.put("CustomerCare", INDIVIDUAL);
        userGroups.put("CUSTOMER_CARE_USER", AUDITING_USER);
        userGroups.put("SDP_MANAGER", ADMIN_USER);
    }

    public static UserType getUserTypeByGroup(String group) throws GroupTypeMappingNotFoundException {
        if (!userGroups.containsKey(group)) {
            throw new GroupTypeMappingNotFoundException("Group " + group + " does not contain a corresponding user type.");
        }
        return userGroups.get(group);
    }

    public static final class GroupTypeMappingNotFoundException extends Exception {
        public GroupTypeMappingNotFoundException(String message) {
            super(message);
        }
    }

}
