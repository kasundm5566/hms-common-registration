package hms.common.registration.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DialogMsisdnFormatter {

    private String numberFormatRegEx;
    private String nblDialogPrefix;
    private Pattern pattern;

    public String formatMsisdnToNbl(String msisdn) throws InvalidDialogNumberException {
        final Matcher matcher = pattern.matcher(msisdn);

        if(!matcher.matches()) {
            throw  new InvalidDialogNumberException(msisdn);
        }

        return new StringBuilder().append(nblDialogPrefix).
                                    append(matcher.group("operatorCode")).
                                    append(matcher.group("suffix")).toString();
    }

    public void setNumberFormatRegEx(String numberFormatRegEx) {
        this.numberFormatRegEx = numberFormatRegEx;
        this.pattern = Pattern.compile(this.numberFormatRegEx);
    }

    public void setNblDialogPrefix(String nblDialogPrefix) {
        this.nblDialogPrefix = nblDialogPrefix;
    }


    public static class InvalidDialogNumberException extends Exception{
        public InvalidDialogNumberException(String msisdn) {
            super(String.format("[%s] is an Invalid Dialog Number.", msisdn));
        }
    }
}
