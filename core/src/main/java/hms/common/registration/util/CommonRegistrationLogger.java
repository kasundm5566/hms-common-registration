/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class CommonRegistrationLogger {

    private static final Logger reportLog = LoggerFactory.getLogger("reporting.log");


    public static final String logPattern;
        /*
        coorporate_user_id|
        service_provider_type|
        tax_identification_number|
        business_licence_number|
        mpaisa_account_number*/

    public enum LogField {
        CORPORATE_USER_ID(corporateUserId),
        SERVICE_PROVIDER_TYPE(serviceProviderType),
        TAX_IDENTIFICATION_NUMBER(taxIdentificationNumber),
        BUSINESS_LICENCE_NUMBER(businessLicenseNumber),
        MPAISA_ACCOUNT_NUMBER(businessId);

        private Function function;

        LogField(Function function) {
            this.function = function;
        }

        public Function getFunction() {
            return function;
        }
    }

    private static Function emptyString = new Function() {
        @Override
        public Object apply(Object input) {
            return "";
        }
    };

    private static Function<CorporateUser, Object> corporateUserId = new Function<CorporateUser, Object>() {
        @Override
        public Object apply(CorporateUser input) {
            if(!Optional.fromNullable(input).isPresent()) {
                return "";
            }
            return Objects.firstNonNull(input.getUserId(), "");
        }
    };

    private static Function<CorporateUser, Object> serviceProviderType = new Function<CorporateUser, Object>() {
        @Override
        public Object apply(CorporateUser input) {
            if(!Optional.fromNullable(input).isPresent()) {
                return "";
            }
            return Objects.firstNonNull(input.getDeveloperType(), "");
        }
    };

    private static Function<CorporateUser, Object> businessId = new Function<CorporateUser, Object>() {
        @Override
        public Object apply(CorporateUser input) {
            if(!Optional.fromNullable(input).isPresent()) {
                return "";
            }
            return Objects.firstNonNull(input.getBusinessId(), "");
        }
    };

    private static Function<CorporateUser, Object> businessLicenseNumber = new Function<CorporateUser, Object>() {
        @Override
        public Object apply(CorporateUser input) {
            if(!Optional.fromNullable(input).isPresent()) {
                return "";
            }
            return Objects.firstNonNull(input.getBusinessLicenseNumber(), "");
        }
    };

    private static Function<CorporateUser, Object> taxIdentificationNumber = new Function<CorporateUser, Object>() {
        @Override
        public Object apply(CorporateUser input) {
            if(!Optional.fromNullable(input).isPresent()) {
                return "";
            }
            return Objects.firstNonNull(input.getTin(), "");
        }
    };

    static {
         StringBuilder stringBuilder = new StringBuilder("{}");
        for (int i = 1; i < LogField.values().length; i++) {
            stringBuilder.append("|{}");
        }
        logPattern = stringBuilder.toString();
    }

    private static Logger getLogger() {
        return reportLog;
    }

    public static void log(Object corporate_user_id, Object sp_beneficiary) {
        getLogger().info("{}|{}", new Object[]{corporate_user_id, sp_beneficiary});
    }

    public static void log(final User user) {
        List<Object> list = Lists.transform(Arrays.asList(LogField.values()), new Function<LogField, Object>() {
            @Override
            public Object apply(LogField input) {
                return input.getFunction().apply(user);
            }
        });

        getLogger().info(logPattern, list.toArray());
    }

    public static JSONObject getBeneficiaryJSONObject(String name, String bankCode, String branchCode, String branchName, String bankAccountNumber) {
        JSONObject object = new JSONObject();
        try {
            object.put("name", name);
            object.put("bankCode", bankCode);
            object.put("branchCode", branchCode);
            object.put("branchName", branchName);
            object.put("bankAccountNumber", bankAccountNumber);
        } catch (JSONException ex) {
        }
        return object;
    }
}
