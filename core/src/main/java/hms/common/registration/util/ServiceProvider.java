/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

import hms.common.rest.util.JsonBodyProvider;
import hms.common.registration.model.User;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ServiceProvider {

    private static final Logger logger = LoggerFactory.getLogger(ServiceProvider.class);
    private static final Logger snmpLogger = LoggerFactory.getLogger("snmp_logger");
    private static final Logger serviceFailLogger = LoggerFactory.getLogger("sp_creation_fail_logger");

    private String spCreateUrl;
    private List<String> spParametersList;
    private Map<String, Object> defaultSpParameterValueMap;

    private boolean provisioningDown = false;
    private String provisioningConnectionFailTrap;
    private String provisioningConnectionSuccessTrap;
    private String spCreationFailTrap;

    private Map<String, Object> createSpMapFromUser(User user) {
        Map<String, Object> spMapFromUser = new HashMap<String, Object>();
        spMapFromUser.putAll(defaultSpParameterValueMap);

        spMapFromUser.put("coop-user-id", user.getUserId());
        spMapFromUser.put("coop-user-name", user.getUsername());
        spMapFromUser.put("created-by", user.getUsername());

        logger.debug("Creating the sp user for provisioning [{}]", spMapFromUser.toString());

        return spMapFromUser;
    }

    public void createDefaultSpUser(User user) {

        Map<String, Object> spMapFromUser = this.createSpMapFromUser(user);

        LinkedList<Object> providers = new LinkedList<Object>();
        providers.add(new JsonBodyProvider());

        WebClient webClient = WebClient.create(spCreateUrl, providers);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");

        try {
            Map<String, Object> provResponse = (HashMap) webClient.post(spMapFromUser, Map.class);
            String statusCode = (String) provResponse.get("status-code");
            logger.debug("SP Creation Status: [{}]", provResponse);
            if (!"S1000".equals(statusCode)) {
                serviceFailLogger.error(spMapFromUser.toString());
                snmpLogger.info(spCreationFailTrap);
            }
            else {
                if(provisioningDown) {
                    provisioningDown = false;
                    snmpLogger.info(provisioningConnectionSuccessTrap);
                }
            }
        } catch (ClientWebApplicationException e) {
            if(!provisioningDown) {
                provisioningDown = true;
                snmpLogger.info(provisioningConnectionFailTrap);
            }
            serviceFailLogger.error(spMapFromUser.toString());
        }
    }

    public void setSpCreateUrl(String spCreateUrl) {
        this.spCreateUrl = spCreateUrl;
    }

    public void setSpCreationFailTrap(String spCreationFailTrap) {
        this.spCreationFailTrap = spCreationFailTrap;
    }

    public void setProvisioningConnectionFailTrap(String provisioningConnectionFailTrap) {
        this.provisioningConnectionFailTrap = provisioningConnectionFailTrap;
    }

    public void setProvisioningConnectionSuccessTrap(String provisioningConnectionSuccessTrap) {
        this.provisioningConnectionSuccessTrap = provisioningConnectionSuccessTrap;
    }

    public void setDefaultSpParameterValueMap(Map<String, Object> defaultSpParameterValueMap) {
        this.defaultSpParameterValueMap = defaultSpParameterValueMap;
    }

}
