/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest.strategy;

import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;

/**
 * This used to process the REST API Requests
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface RequestProcessHandler {

    RegistrationResponseMessage createResponse(RegistrationRequestMessage registrationRequestMessage);

}
