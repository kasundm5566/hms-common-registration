/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest.strategy.impl;

import hms.common.registration.api.common.RequestType;
import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.request.UserStateChangeRequestMessage;
import hms.common.registration.api.response.BasicRegistrationResponseMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.service.UserService;
import hms.common.registration.rest.strategy.RequestProcessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.api.common.RequestType.*;
import static hms.common.registration.api.common.StatusCodes.*;

/**
 * Handles the Change User Status Requests
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserStateChangeRequestHandler implements RequestProcessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserStateChangeRequestHandler.class);
    private UserService userService;

    @Override
    public RegistrationResponseMessage createResponse(RegistrationRequestMessage registrationRequestMessage) {
        LOGGER.info("User state change request received [{}] ", registrationRequestMessage);
        UserStateChangeRequestMessage reqMessage = null;
        if (registrationRequestMessage instanceof UserStateChangeRequestMessage) {
            reqMessage = (UserStateChangeRequestMessage)registrationRequestMessage;
        } else {
            LOGGER.error("Unsupported Request Message Type Found [{}]" + registrationRequestMessage);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
        }
        String userId = reqMessage.getUserId();
        try {
            RequestType requestType = reqMessage.getRequestType();
            return generateUserStateChangeResponse(userId, requestType);
        } catch (Throwable e) {
            LOGGER.error("Error while changing the user status for User ID [ " + userId +" ]", e);
            return createErrorResponse(INTERNAL_ERROR);
        }
    }

    private RegistrationResponseMessage generateUserStateChangeResponse(String userId, RequestType requestType) throws DataManipulationException {
        if(requestType == USER_ACTIVATE) {
            LOGGER.debug("Activating User ID [{}]", userId);
            userService.activateUserByUserId(userId);
            return createSuccessResponse();
        } else if (requestType == USER_SUSPEND) {
            LOGGER.debug("Suspending User ID [{}]", userId);
            userService.suspendUserByUserId(userId);
            return createSuccessResponse();
        }  else {
            LOGGER.error("Unsupported Request Type Found !");
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
        }
    }

    private BasicRegistrationResponseMessage createSuccessResponse() {
        BasicRegistrationResponseMessage response = new BasicRegistrationResponseMessage();
        response.setStatusCode(SUCCESS);
        return response;
    }

    private BasicRegistrationResponseMessage createErrorResponse(StatusCodes statusCodes) {
        BasicRegistrationResponseMessage response = new BasicRegistrationResponseMessage();
        response.setStatusCode(statusCodes);
        return response;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}