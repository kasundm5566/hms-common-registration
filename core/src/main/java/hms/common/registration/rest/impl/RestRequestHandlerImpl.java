/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest.impl;

import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.request.*;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;
import hms.common.registration.rest.RestRequestHandler;
import hms.common.registration.rest.strategy.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.api.common.StatusCodes.UNSUPPORTED_REQUEST_TYPE;

/**
 * Coordinates the REST API Request to the corresponding Request Handler
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RestRequestHandlerImpl implements RestRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestRequestHandlerImpl.class);
    private UserStateChangeRequestHandler userStateChangeRequestHandler;
    private UserAuthenticationRequestHandler userAuthenticationRequestHandler;
    private UserDetailRequestHandler userDetailRequestHandler;
    private UserDetailByMsisdnRequestHandler userDetailByMsisdnRequestHandler;
    private UserDetailByUsernameRequestHandler userDetailByUsernameRequestHandler;

    @Override
    public RegistrationResponseMessage execute(RegistrationRequestMessage registrationRequestMessage) {
        logger.debug("REST API Message Received..... [{}]", registrationRequestMessage);
        if (registrationRequestMessage instanceof UserStateChangeRequestMessage) {
            return userStateChangeRequestHandler.createResponse(registrationRequestMessage);
        } else if (registrationRequestMessage instanceof UserDetailRequestMessage) {
            return userDetailRequestHandler.createResponse(registrationRequestMessage);
        } else if (registrationRequestMessage instanceof UserDetailByMsisdnRequestMessage) {
            return userDetailByMsisdnRequestHandler.createResponse(registrationRequestMessage);
        } else if (registrationRequestMessage instanceof UserDetailByUsernameRequestMessage) {
            return userDetailByUsernameRequestHandler.createResponse(registrationRequestMessage);
        } else if (registrationRequestMessage instanceof UserAuthenticationRequestMessage) {
            return userAuthenticationRequestHandler.createResponse((registrationRequestMessage));
        }
        logger.error("Unsupported Request Message Type Found [{}]" + registrationRequestMessage);
        return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
    }

    private BasicUserResponseMessage createErrorResponse(StatusCodes statusCode) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setStatusCode(statusCode);
        return response;
    }

    public void setUserStateChangeRequestHandler(UserStateChangeRequestHandler userStateChangeRequestHandler) {
        this.userStateChangeRequestHandler = userStateChangeRequestHandler;
    }

    public void setUserAuthenticationRequestHandler(UserAuthenticationRequestHandler userAuthenticationRequestHandler) {
        this.userAuthenticationRequestHandler = userAuthenticationRequestHandler;
    }

    public void setUserDetailRequestHandler(UserDetailRequestHandler userDetailRequestHandler) {
        this.userDetailRequestHandler = userDetailRequestHandler;
    }

    public void setUserDetailByMsisdnRequestHandler(UserDetailByMsisdnRequestHandler userDetailByMsisdnRequestHandler) {
        this.userDetailByMsisdnRequestHandler = userDetailByMsisdnRequestHandler;
    }

    public void setUserDetailByUsernameRequestHandler(UserDetailByUsernameRequestHandler userDetailByUsernameRequestHandler) {
        this.userDetailByUsernameRequestHandler = userDetailByUsernameRequestHandler;
    }
}