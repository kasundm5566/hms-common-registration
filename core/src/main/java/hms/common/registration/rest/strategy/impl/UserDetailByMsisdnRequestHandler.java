/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest.strategy.impl;

import hms.common.registration.api.common.RequestType;
import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.common.UserType;
import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.request.UserDetailByMsisdnRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserStatus;
import hms.common.registration.service.UserService;
import hms.common.registration.rest.strategy.RequestProcessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.api.common.RequestType.USER_ADDITIONAL_DETAILS;
import static hms.common.registration.api.common.RequestType.USER_BASIC_DETAILS;
import static hms.common.registration.api.common.StatusCodes.*;
import static hms.common.registration.api.common.UserStatus.getEnum;
import static hms.common.registration.api.util.RestApiKeys.*;
import static hms.common.registration.api.util.RestApiKeys.PHONE_MODEL;
import static hms.common.registration.api.util.RestApiKeys.PHONE_TYPE;
import static hms.common.registration.api.util.RestApiKeys.LAST_LOGIN_TIME;

/**
 * Handles the Get User Detail Requests by MSISDN
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class UserDetailByMsisdnRequestHandler implements RequestProcessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailByMsisdnRequestHandler.class);
    private UserService userService;

    @Override
    public RegistrationResponseMessage createResponse(RegistrationRequestMessage registrationRequestMessage) {
        LOGGER.info("User Get Detail by MSISDN Request Received [{}] ", registrationRequestMessage);
        UserDetailByMsisdnRequestMessage reqMessage = null;
        if (registrationRequestMessage instanceof UserDetailByMsisdnRequestMessage) {
            reqMessage = (UserDetailByMsisdnRequestMessage)registrationRequestMessage;
        } else {
            LOGGER.error("Unsupported Request Message Type Found [{}]" + registrationRequestMessage);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
        }
        String msisdn = reqMessage.getMsisdn();
        try {
            RequestType requestType = reqMessage.getRequestType();
            User user = userService.findUserByMsisdnAndStatus(msisdn, UserStatus.ACTIVE);
            if(user == null) {
                LOGGER.error("No Active User Found with the given msisdn [{}]", msisdn);
                return createErrorResponse(USER_NOT_FOUND);
            }  else if (user.getUserStatus() != UserStatus.ACTIVE) {
                LOGGER.error("No User Active with the given msisdn [{}], He is [{}]", msisdn, user.getUserStatus());
                return createErrorResponse(StatusCodes.USER_NOT_ACTIVE);
            } else {
                if(requestType == USER_BASIC_DETAILS) {
                    LOGGER.debug("Requesting User Basic Details for User MSISDN [{}]", msisdn);
                    return createSuccessBasicUserResponse(user);
                } else if(requestType == USER_ADDITIONAL_DETAILS) {
                    LOGGER.debug("Requesting User Additional Details for User MSISDN [{}]", msisdn);
                    BasicUserResponseMessage basicUserResponseMessage = createSuccessBasicUserResponse(user);
                    return setAdditionalData(user, basicUserResponseMessage, reqMessage.getModuleName());
                } else {
                    LOGGER.error("Unsupported Request Type Found !");
                    return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
                }
            }
        } catch (Throwable e) {
            LOGGER.error("Error while getting user details for User MSISDN [ " + msisdn +" ] ", e);
            return createErrorResponse(INTERNAL_ERROR);
        }
    }

    private BasicUserResponseMessage createSuccessBasicUserResponse(User user) throws DataManipulationException {

        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setUserId(user.getUserId());
        response.setType(UserType.getEnum(user.getUserType().name()));
        response.setStatus(getEnum(user.getUserStatus().name()));
        response.setGroups(userService.getUserGroups(user));
        if (user.getCorperateParentId() != null) {
            response.setCorporateUserId(user.getCorperateParentId().getUserId());
        } else {
            response.setCorporateUserId(user.getUserId());
        }
        addBasicDataToMap(response, user);
        response.setStatusCode(SUCCESS);
        return response;
    }

    private void addBasicDataToMap(BasicUserResponseMessage response, User user) {

        if (user.getCorperateParentId() != null) {
            response.setAdditionalData(CORPORATE_USER_NAME, user.getCorperateParentId().getUsername());
            response.setAdditionalData(CORPORATE_USER_EMAIL, user.getCorperateParentId().getEmail());
        } else {
            response.setAdditionalData(CORPORATE_USER_NAME, user.getUsername());
            response.setAdditionalData(CORPORATE_USER_EMAIL, user.getEmail());
        }
        response.setAdditionalData(USERNAME, user.getUsername());
        response.setAdditionalData(FIRST_NAME, user.getFirstName());
        response.setAdditionalData(LAST_NAME, user.getLastName());
        response.setAdditionalData(EMAIL, user.getEmail());
        response.setAdditionalData(MPIN, user.getMpin());
        response.setAdditionalData(MSISDN, user.getMobileNo());
        response.setAdditionalData(BIRTHDAY, String.valueOf(user.getBirthday()));
        response.setAdditionalData(GENDER, user.getGender());
        response.setAdditionalData(PROFESSION, user.getProfession());
        if (user.getLastLogin() != null) response.setAdditionalData(LAST_LOGIN_TIME, user.getLastLogin().toString());

    }

    private BasicUserResponseMessage setAdditionalData(User user, BasicUserResponseMessage response, String moduleName) {

        response.setAdditionalData(ADDRESS, user.getAddress());
        response.setAdditionalData(COUNTRY, user.getCountry());
        response.setAdditionalData(CITY, user.getCity());
        response.setAdditionalData(PROVINCE, user.getProvince());
        response.setAdditionalData(POST_CODE, user.getPostCode());
        response.setAdditionalData(OPERATOR, user.getOperator());
        response.setAdditionalData(PHONE_MODEL, user.getPhoneModel());
        response.setAdditionalData(PHONE_TYPE, user.getPhoneType());
        if (moduleName != null) {
            try {
                response.setRoles(userService.getUserRoles(user, moduleName));
                response.setGroups(userService.getUserGroups(user));
            } catch (DataManipulationException e) {
                LOGGER.error("Error while getting User Permissions", e);
            }
        }
        return response;
    }

    private BasicUserResponseMessage createErrorResponse(StatusCodes statusCode) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setStatusCode(statusCode);
        return response;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}