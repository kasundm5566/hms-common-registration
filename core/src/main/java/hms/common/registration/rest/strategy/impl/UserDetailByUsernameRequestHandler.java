/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest.strategy.impl;

import hms.common.registration.api.common.RequestType;
import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.common.UserType;
import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.request.UserDetailByUsernameRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;
import hms.common.registration.model.*;
import hms.common.registration.service.UserService;
import hms.common.registration.rest.strategy.RequestProcessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;

import static hms.common.registration.api.common.RequestType.USER_ADDITIONAL_DETAILS;
import static hms.common.registration.api.common.RequestType.USER_BASIC_DETAILS;
import static hms.common.registration.api.common.StatusCodes.*;
import static hms.common.registration.api.common.UserStatus.getEnum;
import static hms.common.registration.api.util.RestApiKeys.*;

/**
 * Handles the Get User Detail Requests by user name
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserDetailByUsernameRequestHandler implements RequestProcessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailByUsernameRequestHandler.class);
    private UserService userService;

    @Override
    public RegistrationResponseMessage createResponse(RegistrationRequestMessage registrationRequestMessage) {
        LOGGER.info("User Get Detail by user name Request Received [{}] ", registrationRequestMessage);
        UserDetailByUsernameRequestMessage reqMessage = null;
        if (registrationRequestMessage instanceof UserDetailByUsernameRequestMessage) {
            reqMessage = (UserDetailByUsernameRequestMessage) registrationRequestMessage;
        } else {
            LOGGER.error("Unsupported Request Message Type Found [{}]" + registrationRequestMessage);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
        }
        String userName = reqMessage.getUserName();
        try {
            RequestType requestType = reqMessage.getRequestType();
            User user = userService.findUserByName(userName);
            if (user == null) {
                LOGGER.error("No User Found with the given useranme [{}]", userName);
                return createErrorResponse(USER_NOT_FOUND);
            }  else if (user.getUserStatus() != UserStatus.ACTIVE) {
                LOGGER.error("No User Active with the given name [{}], He is [{}]", userName, user.getUserStatus());
                return createErrorResponse(StatusCodes.USER_NOT_ACTIVE);
            }
//            else if(UserType.INDIVIDUAL.equals(UserType.getEnum(user.getUserType().name())) && !user.isMsisdnVerified()){
//                LOGGER.error("MSISDN not validated for user [ " + userName + " ]");
//                throw new BadCredentialsException("MSISDN not validated");
//            } else if(UserType.CORPORATE.equals(UserType.getEnum(user.getUserType().name())) && !user.isEmailVerified()){
//                LOGGER.error("email not validated for user [ " + userName + " ]");
//                throw new BadCredentialsException("email not validated");
//            }
            else {
                return generateUserDetailResponse(userName, reqMessage.getModuleName(), requestType, user);
            }
        } catch (Throwable e) {
            LOGGER.error("Error while getting user details for User  [ " + userName + " ] ", e);
            return createErrorResponse(INTERNAL_ERROR);
        }
    }

    private RegistrationResponseMessage generateUserDetailResponse(String userName, String moduleName,
                                                                   RequestType requestType, User user) {
        if (requestType == USER_BASIC_DETAILS) {
            LOGGER.debug("Requesting User Basic Details for User  [{}]", userName);
            return createSuccessBasicUserResponse(user, moduleName);
        } else if (requestType == USER_ADDITIONAL_DETAILS) {
            LOGGER.debug("Requesting User Additional Details for User  [{}]", userName);
            BasicUserResponseMessage basicUserResponseMessage = createSuccessBasicUserResponse(user, moduleName);
            return setAdditionalData(user, basicUserResponseMessage);
        } else {
            LOGGER.error("Unsupported Request Type Found !");
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
        }
    }

    private BasicUserResponseMessage createSuccessBasicUserResponse(final User user, final String moduleName) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setUserId(user.getUserId());
        response.setType(UserType.getEnum(user.getUserType().name()));
        response.setStatus(getEnum(user.getUserStatus().name()));
        if (user.getCorperateParentId() != null) {
            response.setCorporateUserId(user.getCorperateParentId().getUserId());
        } else {
            response.setCorporateUserId(user.getUserId());
        }

        if (user.getUserType() == hms.common.registration.model.UserType.SUB_CORPORATE) {
            final SubCorporateUser subCorporateUser = (SubCorporateUser) user;
            response.setRoles(subCorporateUser.getRolesAsStrings());
        } else {
            response.setRoles(user.getUserGroup().getRolesAsStrings(moduleName));
        }

        response.setGroups(new HashSet<String>() {{
            if (user.getUserGroup() != null) {
                add(user.getUserGroup().getName());
            }
        }});
        addBasicDataToMap(response, user);
        response.setStatusCode(SUCCESS);
        LOGGER.debug("Created response is {}", response);
        return response;
    }

    private void addBasicDataToMap(BasicUserResponseMessage response, User user) {
        if (user.getCorperateParentId() != null) {
            response.setAdditionalData(CORPORATE_USER_NAME, user.getCorperateParentId().getUsername());
            response.setAdditionalData(CORPORATE_USER_EMAIL, user.getCorperateParentId().getEmail());
        } else {
            response.setAdditionalData(CORPORATE_USER_NAME, user.getUsername());
            response.setAdditionalData(CORPORATE_USER_EMAIL, user.getEmail());
        }
        response.setAdditionalData(USERNAME, user.getUsername());
        response.setAdditionalData(FIRST_NAME, user.getFirstName());
        response.setAdditionalData(LAST_NAME, user.getLastName());
        response.setAdditionalData(EMAIL, user.getEmail());
        response.setAdditionalData(MPIN, user.getMpin());
        response.setAdditionalData(MSISDN, user.getMobileNo());
        response.setAdditionalData(MSISDN_VERIFIED, String.valueOf(user.isMsisdnVerified()));
        response.setAdditionalData(BIRTHDAY, String.valueOf(user.getBirthday()));
        response.setAdditionalData(GENDER, user.getGender());
        response.setAdditionalData(PROFESSION, user.getProfession());
        response.setAdditionalData(CREATED_DATE, String.valueOf(user.getCreatedTime()));
        if (user.getLastLogin() != null) {
            response.setAdditionalData(LAST_LOGIN_TIME, user.getLastLogin().toString());
        }
        if (user.isCorporateUser()) {
            response.setAdditionalData(ORGANIZATION_NAME, ((CorporateUser) user).getOrgName());
        }
    }

    private BasicUserResponseMessage setAdditionalData(User user, BasicUserResponseMessage response) {
        //need to have corporate user id and corporate-user-name in the request
        //for corporte user it will be his own user-id and user-name
        //for sub corporate user it will be his parent user's user-id and user-name

        response.setAdditionalData(ADDRESS, user.getAddress());
        response.setAdditionalData(COUNTRY, user.getCountry());
        response.setAdditionalData(CITY, user.getCity());
        response.setAdditionalData(PROVINCE, user.getProvince());
        response.setAdditionalData(POST_CODE, user.getPostCode());
        response.setAdditionalData(OPERATOR, user.getOperator());
        response.setAdditionalData(PHONE_MODEL, user.getPhoneModel());
        response.setAdditionalData(PHONE_TYPE, user.getPhoneType());
        if (user.getUserType().equals(UserType.CORPORATE)) {
            if (user.isCorporateUser()) {
                CorporateUser corporateUser = (CorporateUser) user;
                response.setAdditionalData(DISPLAY_NAME, corporateUser.getContactPersonName());
            }
        } else if(user.getUserType().equals(UserType.SUB_CORPORATE)) {
            response.setAdditionalData(DISPLAY_NAME, user.getUsername());
        } else {
            response.setAdditionalData(DISPLAY_NAME, user.getFirstName());
        }

        if (user.getLastLogin() != null) response.setAdditionalData(LAST_LOGIN_TIME, user.getLastLogin().toString());
        response.setStatusCode(SUCCESS);
        return response;
    }

    private BasicUserResponseMessage createErrorResponse(StatusCodes statusCode) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setStatusCode(statusCode);
        return response;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}