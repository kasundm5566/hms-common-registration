/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest;

import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;

/**
 * This used to coordinates the REST API Request to the corresponding Request Handler
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface RestRequestHandler {

    RegistrationResponseMessage execute(RegistrationRequestMessage registrationRequestMessage);
}
