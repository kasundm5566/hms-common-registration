/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.rest.strategy.impl;

import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.common.UserType;
import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.request.UserAuthenticationRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.api.response.RegistrationResponseMessage;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserStatus;
import hms.common.registration.service.UserService;
import hms.common.registration.rest.strategy.RequestProcessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.api.common.StatusCodes.*;
import static hms.common.registration.api.common.UserStatus.getEnum;
import static hms.common.registration.api.util.RestApiKeys.*;

/**
 * Handles the User Authentication Requests
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserAuthenticationRequestHandler implements RequestProcessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthenticationRequestHandler.class);
    private UserService userService;

    @Override
    public RegistrationResponseMessage createResponse(RegistrationRequestMessage registrationRequestMessage) {
        LOGGER.debug("User Authentication Request Received [{}]", registrationRequestMessage);
        UserAuthenticationRequestMessage reqMessage = null;
        if (registrationRequestMessage instanceof UserAuthenticationRequestMessage) {
            reqMessage = (UserAuthenticationRequestMessage)registrationRequestMessage;
        } else {
            LOGGER.error("Unsupported Request Message Type Found [{}]" + registrationRequestMessage);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE);
        }
        try {
            User user = userService.findUserByNamAndPassword(reqMessage.getUserName(), reqMessage.getPassword());
            if(user == null) {
                LOGGER.error("No User Found with the given msisdn [{}]", reqMessage.getUserName());
                return createErrorResponse(USER_NOT_FOUND);
            }  else if (user.getUserStatus() != UserStatus.ACTIVE) {
                LOGGER.error("No User Active with the given username [{}], He is [{}]",
                        reqMessage.getUserName(),
                        user.getUserStatus());
                return createErrorResponse(StatusCodes.USER_NOT_ACTIVE,user);
            }else {
                return createSuccessBasicUserResponse(user);
            }
        } catch (DataManipulationException e) {
            LOGGER.error("Error while authenticating User for User name [{}]", reqMessage.getUserName());
            return createErrorResponse(INTERNAL_ERROR);
        }
    }

    private BasicUserResponseMessage createSuccessBasicUserResponse(User user) throws DataManipulationException {

        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setUserId(user.getUserId());
        response.setStatus(getEnum(user.getUserStatus().name()));
        response.setType(UserType.getEnum(user.getUserType().name()));
        if (user.getCorperateParentId() != null) {
            response.setCorporateUserId(user.getCorperateParentId().getUserId());
        } else {
            response.setCorporateUserId(user.getUserId());
        }
        response.setGroups(userService.getUserGroups(user));
        addToMap(response, user);
        response.setStatusCode(SUCCESS);
        response.setMobileNo(user.getMobileNo());
        return response;
    }

    private void addToMap(BasicUserResponseMessage response, User user) {

        if (user.getCorperateParentId() != null) {
            response.setAdditionalData(CORPORATE_USER_NAME, user.getCorperateParentId().getUsername());
            response.setAdditionalData(CORPORATE_USER_EMAIL, user.getCorperateParentId().getEmail());
        } else {
            response.setAdditionalData(CORPORATE_USER_NAME, user.getUsername());
            response.setAdditionalData(CORPORATE_USER_EMAIL, user.getEmail());
        }
        response.setAdditionalData(USERNAME, user.getUsername());
        response.setAdditionalData(FIRST_NAME, user.getFirstName());
        response.setAdditionalData(LAST_NAME, user.getLastName());
        response.setAdditionalData(EMAIL, user.getEmail());
        response.setAdditionalData(MPIN, user.getMpin());
        response.setAdditionalData(MSISDN, user.getMobileNo());
        response.setAdditionalData(BIRTHDAY, String.valueOf(user.getBirthday()));
        response.setAdditionalData(GENDER, user.getGender());
        response.setAdditionalData(PROFESSION, user.getProfession());
        if (user.getLastLogin() != null) response.setAdditionalData(LAST_LOGIN_TIME,user.getLastLogin().toString());
    }

    private BasicUserResponseMessage createErrorResponse(StatusCodes statusCode) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setStatusCode(statusCode);
        return response;
    }

    private BasicUserResponseMessage createErrorResponse(StatusCodes statusCode,User user) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setMobileNo(user.getMobileNo());
        response.setStatusCode(statusCode);
        return response;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}