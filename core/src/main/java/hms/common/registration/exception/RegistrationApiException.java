/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.exception;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class RegistrationApiException extends Exception {

    private static final long serialVersionUID = 463719948503671754L;
    private String errorCode;
    private String errorDescription;

    public RegistrationApiException(String message) {
        super(message);
    }

    public RegistrationApiException(String message, String errorCode, String errorDescription) {
        super(message);
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public RegistrationApiException(String message, Throwable cause) {
        super(message, cause);
    }
}