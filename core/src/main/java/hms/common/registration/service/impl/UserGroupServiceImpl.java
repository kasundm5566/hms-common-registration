/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service.impl;

import hms.common.registration.dao.UserGroupDao;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.ModuleGroup;
import hms.common.registration.model.Role;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.service.UserGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */

@Repository
public class UserGroupServiceImpl implements UserGroupService {

    private static final Logger logger = LoggerFactory.getLogger(UserGroupServiceImpl.class);

    @Autowired
    private UserGroupDao userGroupDao;
    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "UserGroup";
    private String roleEntityName = "hsenidmobile.registration.model.Role";


    @Transactional
    public UserGroup merge(UserGroup userGroup) {
        logger.debug("Updating UserGroup {}", userGroup);
        UserGroup userGroupMerged = entityManager.merge(userGroup);
        entityManager.flush();
        return userGroupMerged;

    }

   @Transactional
    public void persist(UserGroup userGroup)  throws DataManipulationException{
        if (logger.isInfoEnabled()) {
            logger.info("Creating new User Group");
        }
        try {
            entityManager.persist(userGroup);
            //entityManager.flush();
        } catch (Exception t) {

            throw new DataManipulationException("Error on persisting UserGroup ", t);
        }

    }

    @Transactional
    public void remove(UserGroup userGroup)  throws DataManipulationException{
        if (logger.isInfoEnabled()) {
            logger.info("Deleting User Group");
        }
        try {
            if (entityManager.contains(userGroup)) {
                entityManager.remove(userGroup);
            } else {
                UserGroup attached = entityManager.find(UserGroup.class,
                        userGroup.getId());
                entityManager.remove(attached);
            }
            entityManager.flush();
        } catch (Exception t) {
            throw new DataManipulationException("Error on removing UserGroup", t);
        }
    }


    @Override
    public List<User> findAllUsersByModuleGroup(ModuleGroup moduleGroup, Map<String, Object> parameterMap) throws DataManipulationException {
        if (logger.isInfoEnabled()) {
            logger.info("Finding All users by ModuleGroup");
        }
        try {
            Query query = entityManager.createQuery(getQueryString(parameterMap,
                    "SELECT userGroup.user FROM " + entityName
                            + " userGroup WHERE userGroup.group=:group")).setParameter("group", moduleGroup);
            return query.getResultList();
        } catch (Exception t) {
            throw new DataManipulationException("Error on finding all users by Module Group", t);
        }
    }


    @Override
    public List<User> findAllUsersByGroupWithPaging(UserGroup moduleGroup, int indexStart, int recordsPerPage, Map<String, Object> parameterMap) throws DataManipulationException {
        try {
            Query query = entityManager.createQuery(getQueryString(parameterMap,
                    "SELECT userGroup.users FROM " + entityName
                            + " userGroup WHERE userGroup=:group")).setParameter("group", moduleGroup);
            query.setFirstResult(indexStart);
            query.setMaxResults(recordsPerPage);
            return query.getResultList();
        } catch (Exception t) {
            throw new DataManipulationException("Error on finding all users by Module Group with paging", t);
        }
    }

    @Override
    public ModuleGroup getModuleGroupForSubcoporateUser(User user) throws DataManipulationException {
        if (logger.isInfoEnabled()) {
            logger.info("Finding UserGroup for Subcoporate user");
        }
        try {
            Query query = entityManager.createQuery("SELECT userGroup.group FROM " + entityName
                    + " userGroup WHERE userGroup.user=:user").setParameter("user", user);
            return (ModuleGroup) query.getSingleResult();
        } catch (Exception t) {
            throw new DataManipulationException("Error on Finding UserGroup for Subcoporate user", t);
        }
    }


    @Override
    public UserGroup findUserGroupWithAllUsers(long groupId) {
        Query query = entityManager.createQuery("FROM " + entityName
                + " userGroup left join fetch userGroup.users WHERE userGroup.id = :groupId").setParameter("groupId", groupId);
        return (UserGroup) query.getSingleResult();

    }




    @Override
    @Transactional
    public UserGroup findUserGroupByGroupName(String userGroupName) {

        logger.debug("Finding UserGroup using Group Name [{}]", userGroupName);
        try {
            Query query = entityManager.createQuery("FROM " + entityName
                    + " userGroup WHERE userGroup.name=:groupName").setParameter("groupName", userGroupName);

            UserGroup userGroup = (UserGroup) query.getSingleResult();
            if (userGroup != null) {
                userGroup.getUsers().isEmpty();
            }
            logger.debug(" retrieved user group [{}]" , userGroup);
            return userGroup;
        } catch (Exception e) {
            logger.debug("Error on Finding UserGroup using Group Name", e);
            // throw new DataManipulationException("Error on Finding UserGroup using Group Name",e);
        }

        return null;

    }

    @Override
    public List<UserGroup> findUserGroupByRole(Role role) {
        Query query = entityManager.createQuery("Select distinct userGroup FROM " + entityName
                    + " userGroup join fetch userGroup.roles role where role = :role").setParameter("role", role);
            return query.getResultList();
    }

    @Override
    public List<UserGroup> findUserGroupByRoleHavingName(Role role, String groupName) {
        Query query = entityManager.createQuery("SELECT userGroup FROM " + entityName + " userGroup left join "
                + roleEntityName +" role WHERE userGroup.name LIKE :name and role = :role")
                .setParameter("name", "%" + groupName + "%")
                .setParameter("role", role);
        return query.getResultList();
    }

    @Override
    public List<UserGroup> findUserGroupByName(String groupName) {
        Query query = entityManager.createQuery("SELECT userGroup FROM " + entityName + " userGroup WHERE " +
                "userGroup.name LIKE :name")
                .setParameter("name", "%" + groupName + "%");
        return query.getResultList();
    }

    @Override
    public List<String> findAllUserGroupNames() {
        logger.debug(" Finding All UserGroup Names ");
        Query query = entityManager.createQuery("select ug.name from " + entityName + " as ug ");
        return query.getResultList();
    }

    @Override
    @Transactional
    public List<UserGroup> findAllUserGroups() {

        logger.debug(" Finding All UserGroups ");
        Query query = entityManager.createQuery("FROM " + entityName
                + "");
        List<UserGroup> resultList = query.getResultList();
        for (UserGroup userGroup : resultList) {
            userGroup.getUsers().size();
        }
        return resultList;

    }

    private String getQueryString(Map<String, Object> parameters, String baseString) {

        StringBuilder stringBuilder = new StringBuilder(baseString);
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            String key = entry.getKey();
            String sqlSegment = searchByParamsQueryMap.get(key);
            if (sqlSegment != null) {
                stringBuilder.append(" AND ");
                stringBuilder.append(sqlSegment);
                stringBuilder.append(parameters.get(key) + "%'");
            }
        }
        return stringBuilder.toString();
    }


    /**
     * Map containing relevant fields of the User table
     */
    private static Map<String, String> searchByParamsQueryMap = new HashMap<String, String>();
    static {
        searchByParamsQueryMap.put("username", "userGroup.user.username like '");
        searchByParamsQueryMap.put("firstName", "userGroup.user.firstName like '");
        searchByParamsQueryMap.put("status", "userGroup.user.userStatus like '");
        searchByParamsQueryMap.put("type", "userGroup.user.userType like '");
    }
}
