/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.ModuleGroup;

import java.util.List;
import java.io.Serializable;

/**
 * Service class which defines the Module Group related functionality
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface ModuleGroupService extends Serializable {
    static final String GROUP_NAME = "name";
    static final String GROUP_DESC = "description";
    /**
     * Adds new module group
     * @param moduleGroup
     * @throws DataManipulationException
     */
    void persist(ModuleGroup moduleGroup) throws DataManipulationException ;

    /**
     * Updates module group
     * @param moduleGroup
     * @return
     * @throws DataManipulationException
     */
    ModuleGroup merge(ModuleGroup  moduleGroup) throws DataManipulationException;

    /**
     * Removes Module Group
     * @param moduleGroup
     * @throws DataManipulationException
     */
    void remove(ModuleGroup moduleGroup) throws DataManipulationException;

    /**
     * Returns all available Groups names
     * @return
     * @throws DataManipulationException
     */
    List<String> findAllModuleGroupNames() throws DataManipulationException;

    /**
     * Returns module group for a given group name
     * @param groupName
     * @return
     * @throws DataManipulationException
     */
    ModuleGroup findModuleGroupByName(String groupName) throws DataManipulationException;

    /**
     * Returns all available ModuleGroups
     * @return
     * @throws DataManipulationException
     */
    List<ModuleGroup> findAllModuleGroups() throws DataManipulationException;

    /**
     * Return all available Module Groups starting with the given String
     * @param groupName
     * @return
     * @throws DataManipulationException
     */
    List<ModuleGroup> findModuleGroupListByName(String groupName) throws DataManipulationException;

    /**
     * Return a list of all available module group names except virtual groups
     * @param sub_corporate_virtual_group_begin_keyword
     * @return
     * @throws DataManipulationException
     */
    List<String> findAllModuleGroupNamesExceptVirtualGroups(String sub_corporate_virtual_group_begin_keyword) throws DataManipulationException;
}