/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service.impl;

import hms.common.registration.dao.ModuleDao;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import hms.common.registration.service.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Repository
public class ModuleServiceImpl implements ModuleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleServiceImpl.class);

    @Autowired
    private ModuleDao moduleDao;

    @PersistenceContext
    private EntityManager entityManager;
    private String entityNameRole = "Role";

    @Transactional
    public Module update(Module module) throws DataManipulationException {
        LOGGER.info("Updating Module");
        try {
            return moduleDao.merge(module);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on updating Module", t);
        }
    }

    @Transactional
    public void persist(Module module) throws DataManipulationException {
        LOGGER.info("Creating new Module");
        try {
            moduleDao.persist(module);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on persisting Module ", t);
        }
    }

    @Transactional
    public void remove(Module module) throws DataManipulationException {
        LOGGER.info("Deleting Module");
        try {
            moduleDao.remove(module);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on removing Module", t);
        }
    }

    @Override
    public List<String> findAllModuleNames() throws DataManipulationException {
        LOGGER.debug("Finding All Available Modules");
        try {
            return moduleDao.findAllModuleNames();
        } catch (Throwable e) {
            throw new DataManipulationException("Error while finding all available modules", e);
        }
    }

    @Override
    public Module findModuleByName(String moduleName) throws DataManipulationException {
        LOGGER.debug("Finding Module for name [{}]", moduleName);
        try {
            return moduleDao.findModuleByName(moduleName);
        } catch (Throwable e) {
            throw new DataManipulationException("Error while finding module", e);
        }
    }

    @Override
    public List<Module> searchModules(String searchKey) throws DataManipulationException {
        LOGGER.debug("Searching matching modules for search key [{}]", searchKey);
        try {
            return moduleDao.searchModules(searchKey.trim());
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable e) {
            throw new DataManipulationException("Error while searching modules", e);
        }
    }

    @Override
    public List<Role> findAllRoles() {
        return entityManager.createQuery("SELECT role FROM Role role").getResultList();
    }

    @Override
    public List<Long> findAllRoleIdsByModule(Module module) {
        return entityManager.createQuery("SELECT role.id FROM Role role WHERE role.module = :module")
                .setParameter("module", module)
                .getResultList();
    }

    @Override
    public List<Long> findAllRoleIds() {
        return entityManager.createQuery("SELECT role.id FROM Role role")
                .getResultList();
    }

    @Override
    public List<Role> findAllRolesByModuleWithPaging(Module module, int indexStart, int recordsPerPage) {
        Query query = entityManager.createQuery("SELECT role FROM Role role WHERE " +
                "role.module=:module").setParameter("module", module);
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);
        return query.getResultList();
    }

    @Override
    public List<Role> findAllRolesWithPaging(int indexStart, int recordsPerPage) {
        Query query = entityManager.createQuery("SELECT role FROM Role role");
        query.setFirstResult(indexStart);
        query.setMaxResults(recordsPerPage);
        return query.getResultList();
    }

    @Override
    public boolean isModuleExist(String moduleName) {
        Query query = entityManager.createQuery("SELECT module FROM Module module where " +
                "module.moduleName=:moduleName").setParameter("moduleName", moduleName);
        return query.getResultList().size() != 0;
    }

    @Override
    public boolean isRolePrefixAlreadyDefined(String rolePrefix) {
        Query query = entityManager.createQuery("SELECT module FROM Module module where " +
                "module.rolePrefix=:rolePrefix").setParameter("rolePrefix", rolePrefix);
        return query.getResultList().size() != 0;
    }

    @Override
    public boolean isRoleExist(String roleName) {
        Query query = entityManager.createQuery("SELECT role FROM Role role where " +
                "role.name=:roleName").setParameter("roleName", roleName);
        return query.getResultList().size() != 0;
    }

    @Override
    public void remove(Role role) {
        entityManager.remove(role);
    }

    @Override
    public List<Module> findAllModules() throws DataManipulationException {
        LOGGER.debug("Finding all available modules");
        try {
            return moduleDao.findAll();
        } catch (Throwable e) {
            throw new DataManipulationException("Error while finding all available modules", e);
        }
    }


    //Role
    @Transactional
    public Role mergeRole(Role role) {
        Role mergedRole = this.entityManager.merge(role);
        entityManager.flush();
        return mergedRole;
    }

    @Transactional
    public void persistRole(Role role) {
        entityManager.persist(role);
        entityManager.flush();
    }



    @Transactional
    public Role findRoleByName(String roleName) {

        Query query = entityManager.createQuery("FROM " + entityNameRole
                + " module WHERE module.name=:givenName").setParameter("givenName", roleName);
        Role role = (Role) query.getSingleResult();
        return role;

    }

    @Override
    @Transactional(readOnly = true)
    public List<String> findAllRoleNames() throws DataManipulationException{

      return entityManager.createQuery("SELECT role.name FROM Role role")
                .getResultList();

    }

}
