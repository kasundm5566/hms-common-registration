/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service;

import hms.common.registration.model.Module;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Role;

import java.util.List;
import java.io.Serializable;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public interface ModuleService extends Serializable {

    static final String MODULE_NAME = "moduleName";
    static final String MODULE_DESCRIPTION = "description";
    static final String ROLE_NAME = "name";
    static final String ROLE_DESCRIPTION = "description";
    static final String MODULE = "module";
    static final String LIST_ALL_ROLES = "listAllRoles";
    static final String ROLE_PREFIX = "rolePrefix";

    Module update(Module entity) throws DataManipulationException ;

    void persist(Module module) throws DataManipulationException;

    void persistRole(Role role) throws DataManipulationException;

    Role mergeRole(Role role) throws DataManipulationException;

    void remove(Module module) throws DataManipulationException;

    Module findModuleByName(String moduleName) throws DataManipulationException;

    List<String> findAllModuleNames() throws DataManipulationException;

    List<Module> findAllModules() throws DataManipulationException;

    List<Module> searchModules(String searchKey) throws DataManipulationException;
    
    List<Role> findAllRoles();

    List<Long> findAllRoleIdsByModule(Module module);

    List<Long> findAllRoleIds();

    List<Role> findAllRolesByModuleWithPaging(Module module, int indexStart, int recordsPerPage);

    List<Role> findAllRolesWithPaging(int indexStart, int recordsPerPage);

    boolean isModuleExist(String moduleName);

    boolean isRolePrefixAlreadyDefined(String rolePrefix);

    boolean isRoleExist(String roleName);

    void remove(Role role);

    public Role findRoleByName(String roleName);

    List<String> findAllRoleNames() throws DataManipulationException;
}
