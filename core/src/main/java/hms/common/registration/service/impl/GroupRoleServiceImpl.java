/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service.impl;

import hms.common.registration.dao.GroupRoleDao;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.GroupRole;
import hms.common.registration.model.ModuleGroup;
import hms.common.registration.model.Role;
import hms.common.registration.service.GroupRoleService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Deprecated
public class GroupRoleServiceImpl implements GroupRoleService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GroupRoleServiceImpl.class);

    @Autowired
    private GroupRoleDao groupRoleDao;

    public GroupRole merge(GroupRole groupRole) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Updating GroupRole");
        }
        try {
            return groupRoleDao.merge(groupRole);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on updating GroupRole", t);
        }
    }

    public void persist(GroupRole groupRole) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Creating new Group Role");
        }
        try {
            groupRoleDao.persist(groupRole);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on persisting GroupRole ", t);
        }
    }

    public void remove(GroupRole groupRole) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Deleting Group Role");
        }
        try {
            groupRoleDao.remove(groupRole);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on removing GroupRole", t);
        }
    }

    @Override
    public List<ModuleGroup> findModuleGroupByRole(Role role) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Finding ModuleGroups by Role");
        }
        try {
            return groupRoleDao.findModuleGroupByRole(role);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on Finding ModuleGroups by Role", t);
        }
    }

    @Override
    public List<ModuleGroup> findModuleGroupByRoleHavingName(Role role, String groupName) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Finding ModuleGroups by Role With name");
        }
        try {
            return groupRoleDao.findModuleGroupByRoleHavingName(role, groupName);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on Finding ModuleGroups by Role with name", t);
        }
    }

    @Override
    public List<Role> findRolesAssignedToGroup(ModuleGroup group) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Finding Roles by Module Group");
        }
        try {
            return groupRoleDao.findRolesAssignedToGroup(group);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on Finding Roles by Module Group", t);
        }
    }

    @Override
    public void removeSubUsersGroupRole(ModuleGroup moduleGroup, Role role) throws DataManipulationException{
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Removing sub user GroupRole");
        }
        try {
            groupRoleDao.removeSubUsersGroupRole(moduleGroup, role);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on Removing sub user GroupRole", t);
        }
    }

    @Override
    public GroupRole findGroupRoleHavingModuleGroupAndName(ModuleGroup moduleGroup, String roleName) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Finding group role having module and role name");
        }
        try {
            return groupRoleDao.findGroupRoleHavingModuleGroupAndName(moduleGroup, roleName);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on Finding group role having module and role name", t);
        }
    }


}
