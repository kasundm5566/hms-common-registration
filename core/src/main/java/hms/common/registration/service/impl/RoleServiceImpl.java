/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service.impl;

import hms.common.registration.dao.RoleDao;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import hms.common.registration.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Deprecated
public class RoleServiceImpl implements RoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);
    @Autowired
    private RoleDao roleDao;

    public Role update(Role role) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Updating Role");
        }
        try {
            return roleDao.merge(role);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on updating Role", t);
        }
    }

    public void persist(Role role)  throws DataManipulationException{
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Creating new Role");
        }
        try {
            roleDao.persist(role);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on persisting Role ", t);
        }

    }

    public void remove(Role role)  throws DataManipulationException{
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Deleting Role");
        }
        try {
            roleDao.remove(role);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on removing Role", t);
        }
    }

    @Override
    public List<Role> findAllRoles() throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Searching all roles");
        }
        try {
            return roleDao.findAllRoles();
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding all roles");
        }
    }

    @Override
    public List<Role> findAllRolesByModule(Module module) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Find Role by Module Name");
        }
        try {
            return roleDao.findAllRolesByModule(module);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding Role by Module Name");
        }
    }

    @Override
    public List<String> findAllRoleNames() throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Searching all Role names");
        } try {
            return roleDao.findAllRoleNames();
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding all available Role names");
        }
    }

    @Override
    public Role findRoleByName(String roleName) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Searching Role by Name");
        } try {
            return roleDao.findAllRoleByName(roleName);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding Role by Name");
        }
    }

    @Override
    public List<Long> findAllRoleIdsByModule(Module module) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Find All Role Ids by Module Name");
        }
        try {
            return roleDao.findAllRoleIdsByModule(module);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding All Role Ids by Module Nam");
        }
    }

    @Override
    public List<Long> findAllRoleIds() throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Searching all role ids");
        }
        try {
            return roleDao.findAllRoleIds();
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding all role ids");
        }
    }

    @Override
    public List<Role> findAllRolesByModuleWithPaging(Module module, int indexStart, int recordsPerPage) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Find All Roles by Module Name with Paging");
        }
        try {
            return roleDao.findAllRolesByModuleWithPaging(module,indexStart,recordsPerPage);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding All Roles by Module Name with Paging");
        }
    }

    @Override
    public List<Role> findAllRolesWithPaging(int indexStart, int recordsPerPage) throws DataManipulationException {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Find All Roles with Paging");
        }
        try {
            return roleDao.findAllRolesWithPaging(indexStart,recordsPerPage);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding All Roles with Paging");
        }
    }
}
