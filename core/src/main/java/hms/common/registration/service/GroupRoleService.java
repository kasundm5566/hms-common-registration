/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.GroupRole;
import hms.common.registration.model.ModuleGroup;
import hms.common.registration.model.Role;

import java.io.Serializable;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public interface GroupRoleService extends Serializable{

    GroupRole merge(GroupRole groupRole) throws DataManipulationException ;

    void persist(GroupRole groupRole) throws DataManipulationException ;

    void remove(GroupRole groupRole) throws DataManipulationException ;

    List<ModuleGroup> findModuleGroupByRole(Role role) throws DataManipulationException;

    List<ModuleGroup> findModuleGroupByRoleHavingName(Role role, String groupName) throws DataManipulationException;

    List<Role> findRolesAssignedToGroup(ModuleGroup group) throws DataManipulationException;

    void removeSubUsersGroupRole(ModuleGroup moduleGroup, Role role) throws DataManipulationException;

    GroupRole findGroupRoleHavingModuleGroupAndName(ModuleGroup moduleGroup, String roleName) throws DataManipulationException;
}
