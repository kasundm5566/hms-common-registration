/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;

import java.io.Serializable;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public interface RoleService extends Serializable {

    public static final String ROLE_NAME = "name";
    public static final String ROLE_DESCRIPTION = "description";
    public static final String MODULE = "module";

    Role update(Role role) throws DataManipulationException ;

    void persist(Role role) throws DataManipulationException ;

    void remove(Role role) throws DataManipulationException;

    List<Role> findAllRoles() throws DataManipulationException;

    List<Role> findAllRolesByModule(Module module) throws DataManipulationException;

    List<String> findAllRoleNames() throws DataManipulationException;

    Role findRoleByName(String selectedRole) throws DataManipulationException;

    List<Long> findAllRoleIdsByModule(Module module) throws DataManipulationException;

    List<Long> findAllRoleIds() throws DataManipulationException;

    List<Role> findAllRolesByModuleWithPaging(Module module, int indexStart, int recordsPerPage) throws DataManipulationException;

    List<Role> findAllRolesWithPaging(int indexStart, int recordsPerPage) throws DataManipulationException;
}
