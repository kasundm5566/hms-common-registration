/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service.impl;

import hms.common.registration.dao.ModuleGroupDao;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.ModuleGroup;
import hms.common.registration.service.ModuleGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Implementation for the ModuleService
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Deprecated
public class ModuleGroupServiceImpl implements ModuleGroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleGroupServiceImpl.class);
    @Autowired
    private ModuleGroupDao moduleGroupDao;

    /**
     * Saving Module Group Entity
     * @param moduleGroup
     * @throws DataManipulationException
     */
    @Override
    public void persist(ModuleGroup moduleGroup) throws DataManipulationException {
        LOGGER.debug("Adding new Module Group");
        try {
            moduleGroupDao.persist(moduleGroup);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on persisting Module Group ", t);
        }
    }

    /**
     * Updating Module Group Entity
     * @param moduleGroup
     * @return
     * @throws DataManipulationException
     */
    @Override
    public ModuleGroup merge(ModuleGroup moduleGroup) throws DataManipulationException {
        LOGGER.debug("Updating new Module Group");
        try {
            return moduleGroupDao.merge(moduleGroup);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on updating Module Group", t);
        }
    }

    /**
     * Removing Module Group Entity
     * @param moduleGroup
     * @throws DataManipulationException
     */
    @Override
    public void remove(ModuleGroup moduleGroup) throws DataManipulationException {
        LOGGER.debug("Deleting Module Group ");
        try {
            moduleGroupDao.remove(moduleGroup);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on deleting Module Group ", t);
        }
    }

    /**
     * Finding all available module group names
     * @return
     * @throws DataManipulationException
     */
    @Override
    public List<String> findAllModuleGroupNames() throws DataManipulationException {
        LOGGER.debug("Finding available Module Group names ");
        try {
            return moduleGroupDao.findAllModuleGroupNames();
        } catch (Throwable t) {
            throw new DataManipulationException("Error on finding available Module Group names", t);
        }
    }

    /**
     * Finding group for a given group name
     * @param groupName
     * @return
     * @throws DataManipulationException
     */
    @Override
    public ModuleGroup findModuleGroupByName(String groupName) throws DataManipulationException {
        LOGGER.debug("Finding available Module Group for name [{}]", groupName);
        try {
            return moduleGroupDao.findModuleGroupByName(groupName);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on finding available Module Group", t);
        }
    }

    @Override
    public List<ModuleGroup> findAllModuleGroups() throws DataManipulationException {
        LOGGER.debug("Finding all available Module Groups");
        try {
            return moduleGroupDao.findAllModuleGroups();
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding all available Module Groups", t);
        }
    }

    @Override
    public List<ModuleGroup> findModuleGroupListByName(String groupName) throws DataManipulationException {
        LOGGER.debug("Finding all available Module Groups By Name");
        try {
            return moduleGroupDao.findModuleGroupListByName(groupName);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding all available Module Groups by name", t);
        }
    }

    @Override
    public List<String> findAllModuleGroupNamesExceptVirtualGroups(String sub_corporate_virtual_group_begin_keyword) throws DataManipulationException {
        LOGGER.debug("Finding all available Module Groups Except Virtual Groups");
        try {
            return moduleGroupDao.findAllModuleGroupNamesExceptVirtualGroups(sub_corporate_virtual_group_begin_keyword);
        } catch (Throwable t) {
            throw new DataManipulationException("Error while finding all available Module Groups Except Virtual Groups", t);
        }
    }
}