/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service;

import com.google.common.base.Optional;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.*;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Service interface definition for User entity management
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */

public interface UserService extends Serializable {

    static final String PROVINCE = "province";
    static final String LAST_NAME = "lastName";
    static final String BIRTHDAY = "birthday";
    static final String COUNTRY = "country";
    static final String CITY = "city";
    static final String ADDRESS = "address";
    static final String POST_CODE = "postCode";
    static final String PHONE_NUMBER = "mobileNo";
    static final String EMAIL = "email";
    static final String PROFESSION = "profession";
    static final String MPIN = "mpin";
    static final String PHONE_TYPE = "phoneType";
    static final String PHONE_MODEL = "phoneModel";
    static final String GENDER = "gender";
    static final String OPERATOR = "operator";
    static final String USER_NAME = "username";
    static final String PASSWORD = "password";
    static final String FIRST_NAME = "firstName";
    static final String DISPLAY_NAME = "displayName";
    static final String ROLES = "roles";

    /**
     * Finds user for given user name
     *
     * @param userName
     * @return
     * @throws DataManipulationException
     */
    User findUserByName(String userName) throws DataManipulationException;

    /**
     * Finds user for a given persistence ID
     *
     * @param id
     * @return
     * @throws DataManipulationException
     */
    User findUserById(long id) throws DataManipulationException;

    /**
     * Finds user for a given user ID
     *
     * @param id
     * @return
     * @throws DataManipulationException
     */
    User findUserByUserId(String id) throws DataManipulationException;

    /**
     * Finds user for a given user ID
     *
     * @param domain
     * @param domainId
     * @return
     * @throws DataManipulationException
     */
    User findUserByDomainId(String domain, String domainId) throws DataManipulationException;

    /**
     * @param msisdn
     * @return
     * @throws DataManipulationException
     */
    User findUserByMsisdnAndStatus(String msisdn, UserStatus userStatus) throws DataManipulationException;

    /**
     * @param msisdn
     * @return
     * @throws DataManipulationException
     */
    User findUserByMsisdn(String msisdn) throws DataManipulationException;

    /**
     * Updates user entity
     *
     * @param user
     * @return
     * @throws DataManipulationException
     */
    User merge(User user) throws DataManipulationException;

    /**
     * Persists user entity
     *
     * @param user
     * @throws DataManipulationException
     */
    void persist(User user) throws DataManipulationException;

    /**
     * Persists user entity with status
     *
     * @param user
     * @throws DataManipulationException
     */
    void persist(User user, UserStatus userStatus) throws DataManipulationException;

    /**
     * Remove the existing user and create a new one
     * @param previousUser
     * @param newUser
     * @return
     * @throws DataManipulationException
     */
    User convertUser(User previousUser, User newUser) throws DataManipulationException;

    /**
     * Removes user entity
     *
     * @param user
     * @throws DataManipulationException
     */
    void remove(User user) throws DataManipulationException;

    /**
     * Activated
     *
     * @param emailVerificationCode
     * @throws DataManipulationException
     */
    User activateEmail(String emailVerificationCode) throws DataManipulationException;

    /**
     * Activating user for a given user ID
     *
     * @param id
     * @throws DataManipulationException
     */
    void activateUserByUserId(String id) throws DataManipulationException;

    /**
     * Suspending user for a given user ID
     *
     * @param id
     * @throws DataManipulationException
     */
    void suspendUserByUserId(String id) throws DataManipulationException;

    /**
     * Returns permissions attached to a given user bby the username
     *
     * @param user
     * @param moduleName
     * @return
     */
    List<String> getUserRoles(User user, String moduleName) throws DataManipulationException;

    /**
     * Returns permissions attached to a given user by the user name
     *
     * @param user
     * @return
     */
    Hashtable<Module, List<Role>> getUserRolesAndMoudles(User user) throws DataManipulationException;

    /**
     * Returns set containing user group names for a given user
     *
     * @param user
     * @return
     * @throws DataManipulationException
     */
    Set<String> getUserGroups(User user) throws DataManipulationException;

    /**
     * Returns all available users accounts in the system
     *
     * @return
     */
    List<User> getAllAvailableUsers() throws DataManipulationException;

    /**
     * Returns list of users for given parameter values
     *
     * @param parameters
     * @return
     * @throws DataManipulationException
     */
    List<User> getUserList(Map<String, Object> parameters) throws DataManipulationException;

    /**
     * @param indexStart
     * @param recordsPerPage
     * @param parameterMap
     * @return
     */
    List<User> getUserListWithPaging(int indexStart, int recordsPerPage, Map<String, Object> parameterMap)
            throws DataManipulationException;

    /**
     * @param userType
     * @return
     * @throws DataManipulationException
     */
    List<String> getAllUsernamesByType(UserType userType) throws DataManipulationException;

    /**
     * remove all the expired msisdns
     * expired criteria : not validated within 30 minutes
     *
     * @param interval
     * @throws DataManipulationException
     */
    void removeAllExpiredMsisdns(TimeUnit timeUnit, long interval) throws DataManipulationException;

    /**
     * @param timeUnit
     * @param checkingInterval
     * @throws DataManipulationException
     */
    void removeAllExpiredEmailAddress(TimeUnit timeUnit, long checkingInterval) throws DataManipulationException;

    /**
     * returns id list of user table
     *
     * @param parameterMap
     * @return
     * @throws DataManipulationException
     */
    List<Long> getUserIdList(Map<String, Object> parameterMap) throws DataManipulationException;

    /**
     * @param msisdn
     * @return
     * @throws DataManipulationException
     */
    boolean isMsisdnAlreadyTaken(String msisdn) throws DataManipulationException;

    User findByEmailVerificationCode(String emailVerification);

    User findUserByEmailVerificationCodeAndUserID(String userID, String emailVerification);

    User updateUserAccountActivationStatus(String userID, String emailVerification);

    int userAccountActivateUsingEmail(String emailVerification, String userID);

    void createDefaultSpForUser(User user);

    List<User> getUsersOfTheGivenUserGroup(UserGroup userGroup);

    List<Long> findUserIdListByConditionList(List<Condition> condition);

    List<User> findUserListByConditionList(int indexStart, int recordsPerPage, List<Condition> condition);

    boolean isEmailAvailableForUserId(String email, String userName);

    User findUserByNamAndPassword(String userName, String password);

    Optional<User> findUserByEmail(String email);

    List<User> getAllAvailableUsersWithPaging(int start, int limit) throws DataManipulationException;

    int getAllUsersCount() throws DataManipulationException;
}
