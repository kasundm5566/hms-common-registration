/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.ModuleGroup;
import hms.common.registration.model.Role;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public interface UserGroupService extends Serializable {

    UserGroup merge(UserGroup userGroup) throws DataManipulationException ;

    void persist(UserGroup userGroup) throws DataManipulationException;

    void remove(UserGroup userGroup) throws DataManipulationException ;

    List<User> findAllUsersByModuleGroup(ModuleGroup moduleGroup, Map<String, Object> parameterMap) throws DataManipulationException;

    List<User> findAllUsersByGroupWithPaging(UserGroup moduleGroup, int indexStart, int recordsPerPage, Map<String, Object> parameterMap) throws DataManipulationException;

    ModuleGroup getModuleGroupForSubcoporateUser(User user) throws DataManipulationException;

   // List<Long> findAllUserIdsByUserGroupAndParamMap(ModuleGroup moduleGroup, Map<String,Object> parameterMap) throws DataManipulationException;

    //UserGroup findUserGroupHavingUserAndGroup(String username, ModuleGroup moduleGroup) throws DataManipulationException;

   // List<UserGroup> findAllGroupsByModuleGroup(ModuleGroup moduleGroup) throws DataManipulationException;

    UserGroup findUserGroupWithAllUsers(long groupId);

    UserGroup findUserGroupByGroupName(String userGroupName);

    List<UserGroup> findAllUserGroups();

    List<UserGroup> findUserGroupByRole(Role role);

    List<UserGroup> findUserGroupByRoleHavingName(Role role, String groupName);

    List<UserGroup> findUserGroupByName(String groupName);

    List<String> findAllUserGroupNames();
}
