/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.service.impl;

import com.google.common.base.Optional;
import hms.common.registration.common.Condition;
import hms.common.registration.dao.UserDao;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.*;
import hms.common.registration.service.UserService;
import hms.common.registration.util.RegistrationFeatureRegistry;
import hms.common.registration.util.ServiceProvider;
import hms.common.registration.util.ValidateUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static hms.common.registration.model.UserStatus.INITIAL;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private ServiceProvider serviceProvider;

    @Autowired
    private UserDao userDao;

    @PersistenceContext
    private EntityManager entityManager;
    private String entityName = "User";


    @Override
    public User findUserByName(String userName) throws DataManipulationException {
        LOGGER.info("Finding User by user name : " + userName);
        try {
            return userDao.findUserByName(userName);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            throw new DataManipulationException("Error occurred on finding User for user name : " + userName, t);
        }
    }


    @Override
    public User findUserById(long id) throws DataManipulationException {
        LOGGER.info("Finding User by user id : " + id);
        try {
            return userDao.findUserById(id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            throw new DataManipulationException("Error occured on finding User for user Id : " + id, t);
        }
    }


    @Override
    public User findUserByUserId(String userId) throws DataManipulationException {
        LOGGER.info("Finding User by user userId : " + userId);
        try {
            return userDao.findUserByUserId(userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            throw new DataManipulationException("Error occured on finding User for userId : " + userId, t);
        }
    }


    @Override
    public User findUserByDomainId(String domain, String domainId) throws DataManipulationException {
        LOGGER.info("Finding User by domainId : {}/{}", domain, domainId);
        try {
            return userDao.findUserByDomainId(domain, domainId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            throw new DataManipulationException("Error occurred on finding User for domainId : "
                    + domain + "/" + domainId, t);
        }
    }


    public User merge(User user) throws DataManipulationException {
        LOGGER.info("Updating User");
        try {
            return userDao.merge(user);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on updating User", t);
        }
    }

    public void persist(User user) throws DataManipulationException {
        LOGGER.info("Creating new User");
        try {
            //todo handle the user activation depending on the users configurations
            if ((user.isEmailVerified() && user.isMsisdnVerified()) || user.getUserType().equals(UserType.SUB_CORPORATE)) {
                user.activate();
            } else if (user.getUserType() == UserType.SUB_CORPORATE) {
                user.activate();
            } else {
                user.setUserStatus(INITIAL);
            }
            userDao.persist(user);
        } catch (Throwable e) {
            throw new DataManipulationException("Data access error in creating User " + "[ " + user.getUsername() + " ]", e);
        }
    }

    public void persist(User user, UserStatus userStatus) throws DataManipulationException {
        LOGGER.info("Creating new User");
        try {
            //todo handle the user activation depending on the users configurations
            if (user.isEmailVerified() && user.isMsisdnVerified()) {
                user.activate();
            } else {
                user.setUserStatus(userStatus);
            }
            userDao.persist(user);
        } catch (Throwable e) {
            throw new DataManipulationException("Data access error in creating User " + "[ " + user.getUsername() + " ]", e);
        }
    }

    public User convertUser(User previousUser, User newUser) throws DataManipulationException {
        LOGGER.info("Converting user [{}] into [{}]", previousUser, newUser);
        User result;
        try {
            result = userDao.convertUser(previousUser, newUser);
        } catch (Throwable e) {
            throw new DataManipulationException("Data access error in converting user " + "[ " + previousUser.getUsername() + " ]", e);
        }
        return result;
    }

    public void remove(User user) throws DataManipulationException {
        LOGGER.info("Deleting User");
        try {
            userDao.remove(user);
        } catch (Throwable t) {
            throw new DataManipulationException("Error on removing User", t);
        }
    }


    @Override
    public User activateEmail(String emailVerificationCode) throws DataManipulationException {
        LOGGER.debug("Activating Email account ");
        User user = null;
        try {
            user = userDao.findByEmailVerificationCode(emailVerificationCode);
            if (user != null) {
                user.setEmailVerified(true);

                //TODO check feature verify
                if (ValidateUser.isValidUser(user)) {
                    user.activate();
                }
                userDao.merge(user);
            } else {
                throw new DataManipulationException("No User Found with the given email" +
                        "verification code " + "[ " + emailVerificationCode + " ]");
            }
        } catch (Throwable e) {
            throw new DataManipulationException("Data access error in updating user with the email" +
                    "verification details " + "[ " + user.getUsername() + " ]", e);
        }

        return user;
    }

    @Override
    public void activateUserByUserId(String userId) throws DataManipulationException {
        LOGGER.debug("Activating User with the ID [{}]", userId);
        User user = findUserByUserId(userId);
        if (user != null) {
            user.activate();
            userDao.merge(user);
            LOGGER.debug("User account was activated for user userId [{}]", userId);
        }
    }

    @Override
    public void suspendUserByUserId(String userId) throws DataManipulationException {
        LOGGER.debug("Suspending User with the ID [{}]", userId);
        User user = findUserByUserId(userId);
        if (user != null) {
            user.suspend();
            userDao.merge(user);
            LOGGER.debug("User account was suspended for user userId [{}]", userId);
        }
    }

    @Override
    public User findUserByMsisdnAndStatus(String msisdn, UserStatus userStatus) throws DataManipulationException {
        LOGGER.info("Finding User by MSISDN {} And Status {} ", msisdn, userStatus);
        try {
            User user = userDao.findByMsisdnAndStatus(msisdn, userStatus);
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            throw new DataManipulationException("Error occurred on finding User for MSISDN : [ " + msisdn
                    + " ]", t);
        }
    }

    @Override
    public User findUserByMsisdn(String msisdn) throws DataManipulationException {
        LOGGER.info("Finding User by MSISDN {} ", msisdn);
        try {
            User user = userDao.findByMsisdn(msisdn);
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            throw new DataManipulationException("Error occurred on finding User for MSISDN : [ " + msisdn
                    + " ]", t);
        }
    }

    /**
     * Returns the Roles assigned to a particular User
     *
     * @param user
     * @param moduleName
     * @return
     */
    @Override
    public List<String> getUserRoles(User user, String moduleName) throws DataManipulationException {
        try {
            List<String> userRoles = new ArrayList<String>();
            if (moduleName == null) {
                LOGGER.error("Module name was not specified when requesting user roles");
            } else {
                /*if(user.getUserGroupList() != null) {
                    for (UserGroup userGroup : user.getUserGroupList()) {
                        for (GroupRole groupRole : userGroup.getGroup().getGroupRoleList()) {
                            String assignedModuleName = groupRole.getRole().getModule().getModuleName();
                            if (assignedModuleName.equals(moduleName.trim())) {
                                userRoles.add(groupRole.getRole().getName());
                            }
                        }
                    }
                } else {
                    LOGGER.debug("No user roles assigned for user [ " + user.getUsername() + " ]");
                }
                */

            }
            return userRoles;
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding Permissions " +
                    " for the user [ " + user + " ] ", t);
        }
    }


    public Hashtable<Module, List<Role>> getUserRolesAndMoudles(User user) throws DataManipulationException {

        LOGGER.debug("getting user roles and modules ");
        try {
            Hashtable<Module, List<Role>> userRoles = new Hashtable<Module, List<Role>>();
//            if (user.getUserGroupList() != null) {
//                for (UserGroup userGroup : user.getUserGroupList()) {
            /* ModuleGroup moduleGroup = userGroup.getGroup();
          for (GroupRole groupRole : moduleGroup.getGroupRoleList()) {
              Role role = groupRole.getRole();
              List<Role> roleList = userRoles.get(role.getModule());
              if(roleList == null){
                  roleList = new ArrayList<Role>();
                  roleList.add(role);
                  userRoles.put(role.getModule(), roleList);
              }else{
                  roleList.add(role);
              }
          }  */
//                }
//            } else {
//                LOGGER.debug("No user roles assigned for user [ " + user.getUsername() + " ]");
//            }
            return userRoles;
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding Permissions " +
                    " for the user [ " + user + " ] ", t);
        }
    }

    /**
     * Returns all available user accounts
     *
     * @return
     */
    @Override
    public List<User> getAllAvailableUsers() throws DataManipulationException {
        LOGGER.info("Finding All Available Users");
        try {
            return userDao.findAllUsers();
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding all Users ", t);
        }
    }

    @Override
    public Set<String> getUserGroups(User user) throws DataManipulationException {
        try {
            Set<String> userGroups = new HashSet<String>();
//            if(user.getUserGroupList() != null) {
//                LOGGER.debug("[{}] user groups found for user [ " + user.getUsername() + " ]",
//                        user.getUserGroupList().size());
//                for (UserGroup userGroup : user.getUserGroupList()) {
//                    //userGroups.add(userGroup.getGroup().getName());
//                }
//            } else {
//                LOGGER.debug("No user groups assigned for user [ " + user.getUsername() + " ]");
//            }
            return userGroups;
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding user gropus " +
                    " for the user [ " + user + " ] ", t);
        }
    }

    @Override
    public List<User> getUserList(Map<String, Object> parameters) throws DataManipulationException {
        LOGGER.info("Finding users for given parameters map");
        try {
            return userDao.getUserList(parameters);
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding Users for a given parameters map", t);
        }
    }

    @Override
    public List<User> getUserListWithPaging(int indexStart, int recordsPerPage, Map<String, Object> parameters)
            throws DataManipulationException {
        LOGGER.info("Finding users for given parameters map with paging");
        try {
            return userDao.getUserListWithPaging(indexStart, recordsPerPage, parameters);
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding Users for a given parameters map with Paging", t);
        }
    }

    @Override
    public List<String> getAllUsernamesByType(UserType userType) throws DataManipulationException {
        LOGGER.info("Finding users by UserType");
        try {
            return userDao.findAllUserNamesByType(userType);
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding users by UserType");
        }
    }

    @Override
    public List<Long> getUserIdList(Map<String, Object> parameterMap) {
        LOGGER.info("Finding List of User Ids");
        return userDao.getUserIdList(parameterMap);
    }

    @Override
    public boolean isMsisdnAlreadyTaken(String msisdn) throws DataManipulationException {
        LOGGER.info("Checking for msisdn validity");
        try {
            return userDao.isMsisdnAlreadyTaken(msisdn); //todo remove dao
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in Checking for msisdn validity");
        }
    }

    @Override
    public void removeAllExpiredMsisdns(TimeUnit timeUnit, long interval) throws DataManipulationException {
        LOGGER.info("Removing All Expired Msisdns");
        try {
            userDao.removeAllExpiredMsisdns(timeUnit, interval);
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in Removing All Expired Msisdns");
        }
    }

    @Override
    public void removeAllExpiredEmailAddress(TimeUnit timeUnit, long checkingInterval) throws DataManipulationException {
        LOGGER.info("Removing All Expired Email Addresses");
        try {
            userDao.removeAllExpiredEmailAddresses(timeUnit, checkingInterval);
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in Removing All Email Addresses");
        }
    }


    @Override
    public User findByEmailVerificationCode(String email_verification) {
        LOGGER.info("Finding User Email Verification : " + email_verification);
        try {
            User user = userDao.findByEmailVerificationCode(email_verification);
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            LOGGER.debug("Error occured on finding User for Email Verification : [ " + email_verification
                    + " ]", t);
            return null;
        }
    }


    @Override
    public User findUserByEmailVerificationCodeAndUserID(String userID, String emailVerificationCode) {


        LOGGER.info("Finding the User by User ID [" + userID + "] and Email Verification code [" + emailVerificationCode + "]");
        try {
            User user = userDao.findUserByEmailVerificationCodeAndUserID(userID, emailVerificationCode);
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Throwable t) {
            LOGGER.debug("Error occurred the User by User ID [" + userID + "] and Email Verification code [" + emailVerificationCode + "]", t);
            return null;
        }


    }


    @Override
    public User updateUserAccountActivationStatus(String userID, String emailVerificationCode) {

        LOGGER.info("Updating the User by User ID [" + userID + "] and Email Verification code [" + emailVerificationCode + "] for Email Accoutn Verification");

        try {

            return userDao.updateUserAccountActivationStatus(userID, emailVerificationCode);

        } catch (EmptyResultDataAccessException e) {

            LOGGER.debug("Error occurred the User account update by User ID [" + userID + "] and Email Verification code [" + emailVerificationCode + "]", e);
            return null;
        } catch (Throwable t) {
            LOGGER.debug("Error occurred the User account update by User ID [" + userID + "] and Email Verification code [" + emailVerificationCode + "]", t);
            return null;
        }


    }


    public int userAccountActivateUsingEmail(String emailVerification, String userID) {
        LOGGER.debug("Activating Email account ");
        User user = null;
        try {
            user = userDao.findUserByEmailVerificationCodeAndUserID(userID, emailVerification);

            boolean isUserAlreadyActivated = userDao.findUserActivationStatusByEmailVerificationCodeAndUserId(userID, emailVerification);

            if (user != null) {

                if (isUserAlreadyActivated) {
                    LOGGER.debug(" user is already activated ");
                    return 2;
                }

                user.setEmailVerified(true);

                //TODO check feature verify
                if(user.isEmailVerified() && isCorporateUserMsisdnVerifyEnabled()
                        && isCorporateUserMsisdnVerifySkippable()) {
                    user.activate();
                }

                if (ValidateUser.isValidUser(user)) {
                    user.activate();
                }

                if (user.isCorporateUser()
                        && RegistrationFeatureRegistry.isAutoSpCreationEnabled()
                        && user.isEnabled()) {
                    createDefaultSpForUser(user);
                }

                userDao.merge(user);
                LOGGER.debug("Verified User with the given email verification code " + "[ " + emailVerification + " ] and user id [" + userID + "]");
                return 1;

            } else {
                LOGGER.debug("No User Found with the given email verification code " + "[ " + emailVerification + " ] and user id [" + userID + "]");
                return 0;
            }
        } catch (Throwable e) {
            LOGGER.debug("Data access error in updating user with the user id " + userID +
                    "verification details " + "[ " + emailVerification + " ]", e);

            LOGGER.debug(" error message is " + e.getMessage());

            LOGGER.debug(" user object is " + user);

            return -1;
        }

    }

    @Override
    public void createDefaultSpForUser(User user) {
        LOGGER.debug("Automatic SP creation about to begin for user [{}]", user.getUsername());
        serviceProvider.createDefaultSpUser(user);
    }

    public List<User> getUsersOfTheGivenUserGroup(UserGroup userGroup) {

        /*
      long userGroup= userGroup.getId();

      Query query = entityManager.createQuery("FROM " + entityName + " as user WHERE " +
                      "user.userGroup=" +userGroupId+"");

      List<User> retrievedUsers= query.getResultList();

            return retrievedUsers;
        */
        //TODO

        return new ArrayList<User>();

    }//getUsersOfTheGivenUserGroup

    @Override
    public List<Long> findUserIdListByConditionList(List<Condition> conditionList) {
        return userDao.findUserIdListByConditionList(conditionList);
    }

    @Override
    public List<User> findUserListByConditionList(int indexStart, int recordsPerPage, List<Condition> conditionList) {
        return userDao.getUserListByConditionList(indexStart, recordsPerPage, conditionList);
    }


    @Override
    public boolean isEmailAvailableForUserId(String email, String userId) {
        return userDao.isEmailAvailableForUserId(email, userId);
    }

    @Override
    public User findUserByNamAndPassword(String userName, String password) {
        LOGGER.info("Finding User by user name [{}] and password [{}]", userName, password);
        try {
            User user = userDao.findUserByName(userName, password);
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * Create the query string containing the given parameters
     *
     * @param parameters
     * @return query string
     */
    private String getQueryString(Map<String, Object> parameters, String baseString) {
        StringBuilder stringBuilder = new StringBuilder(baseString);
        if (parameters.isEmpty()) {
            stringBuilder.replace(stringBuilder.length() - " WHERE ".length(), stringBuilder.length(), "");
        } else {
            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                String key = entry.getKey();
                String sqlSegment = searchByParamsQueryMap.get(key);
                if (sqlSegment != null) {
                    stringBuilder.append(sqlSegment);
                    if (sqlSegment.contains("like")) {
                        stringBuilder.append(parameters.get(key) + "%'");
                    } else {
                        stringBuilder.append(parameters.get(key) + "'");
                    }
                    stringBuilder.append(" AND ");
                }
            }
            stringBuilder.replace(stringBuilder.length() - " AND ".length(), stringBuilder.length(), "");
        }
        return stringBuilder.toString();
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        try {
            final User userByEmail = userDao.findUserByEmail(email);
            return Optional.fromNullable(userByEmail);
        } catch (Exception e) {
            return Optional.absent();
        }
    }

    @Override
    public List<User> getAllAvailableUsersWithPaging(int start, int limit) throws DataManipulationException {
        LOGGER.info("Finding All Available Users");
        try {
            return userDao.findAllUsersWithPaging(start, limit);
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding all Users ", t);
        }
    }

    @Override
    public int getAllUsersCount() throws DataManipulationException {
        LOGGER.info("Finding All Available Users Count");
        try {
            return userDao.findAllUsersCount();
        } catch (Throwable t) {
            throw new DataManipulationException("Data access error in finding all Users ", t);
        }
    }

    /**
     * Map containing relevant fields of the User table
     */
    private static Map<String, String> searchByParamsQueryMap = new HashMap<String, String>();

    static {
        searchByParamsQueryMap.put("username", "user.username like '");
        searchByParamsQueryMap.put("firstName", "user.firstName like '");
        searchByParamsQueryMap.put("status", "user.userStatus='");
        searchByParamsQueryMap.put("type", "user.userType='");
        searchByParamsQueryMap.put("coporateParent", "user.corperateParentId.username='");
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }
}
