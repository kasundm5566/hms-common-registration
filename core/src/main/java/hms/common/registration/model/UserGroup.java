/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "user_group")
public class UserGroup extends PersistentObject {

    private static final long serialVersionUID = 1506806885690915498L;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userGroup", cascade = {CascadeType.MERGE})
    private Set<User> users;
    @Column(unique = true)
    private String name;
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_group_roles")
    private Set<Role> roles;

    public UserGroup() {
    }

    public UserGroup(String name) {
        this(name, "");
    }

    public UserGroup(String name, String description) {
        this.name = name;
        this.description = description;
        this.users = new HashSet<User>();
    }

    //    public UserGroup(ModuleGroup group, User users) {
//        this.group = group;
//        this.users = users;
//    }
//
//    public UserGroup(ModuleGroup group, User users, Date userLastLogin) {
//        this.group = group;
//        this.users = users;
//        this.userLastLogin = userLastLogin;
//    }
//
//    public ModuleGroup getGroup() {
//        return this.group;
//    }
//
//    public void setGroup(ModuleGroup group) {
//        this.group = group;
//    }


    public Set<User> getUsers() {
        return Collections.unmodifiableSet(users);
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("UserGroup{")
                .append(", name='").append(name).append('\'')
                .append(", description='").append(description).append('\'')
                .append(", roles=").append(roles != null ? roles.size() : null)
                .append('}').toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public Set<String> getRolesAsStrings() {
        if (roles == null) return null;
        Set<String> roles = new HashSet<String>();
        for (Role role : this.roles) {
            roles.add(role.getName());
        }
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<String> getRolesAsStrings(String moduleName) {
        List<String> result = new ArrayList<String>();
        for (Role role : roles) {
            if (role.getModule().getModuleName().equals(moduleName)) {
                result.add(role.getName());
            }
        }
        return result;
    }

    public void add(User user) {
        this.users.add(user);
    }
}
