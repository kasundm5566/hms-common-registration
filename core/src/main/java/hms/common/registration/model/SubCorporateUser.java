/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Entity
public class SubCorporateUser extends User {

    @ManyToMany(cascade = {}, fetch = FetchType.EAGER)
    private Set<Role> roles;

    public SubCorporateUser() {
        super();
        this.roles = new HashSet<Role>();
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<String> getRolesAsStrings() {
        Set<Role> roles = getRoles();
        List<String> rolesAsStringArray = new ArrayList<String>();
        for (Role role : roles) {
            rolesAsStringArray.add(role.getName());
        }
        return rolesAsStringArray;
    }

    @Override
    public List<String> getRolesAsStrings(String moduleName) {
        List<String> result = new ArrayList<String>();
        for (Role role : roles) {
            if (role.getModule().getModuleName().equals(moduleName)) {
                result.add(role.getName());
            }
        }
        return result;
    }
}
