/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role extends PersistentObject {

    private static final long serialVersionUID = 1504706885690915477L;
    private String name;
    private String description;

    @ManyToOne
    private Module module;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }


    @Column(name = "name", unique = true, nullable = false, insertable = true, updatable = true, length = 55)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", unique = false, nullable = true, insertable = true, updatable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public String toString() {
        return new StringBuilder()
                .append("\n\t Role/Permission Name: ").append(name)
                .append("\n\t Role/Permission Description: ").append(description)
                .append("\n\t Module Name: ").append(module).toString();
    }
}