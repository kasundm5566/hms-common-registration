/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

/**
 * Defines available status for Users
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public enum UserStatus {

    INITIAL,

    ACTIVE {
        @Override
        public boolean isActive() {
            return true;
        }},
    SUSPEND,

    PENDING_FOR_APPROVAL;

    public boolean isActive(){
        return false;
    }
}