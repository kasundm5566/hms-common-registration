/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

//@Entity
//@Table(name = "group_role")
public class GroupRole extends PersistentObject {

    private static final long serialVersionUID = 1506806885630915498L;
//    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
//    @Cascade({CascadeType.LOCK})
//    @JoinColumn(name = "group_id", unique = false, nullable = false, insertable = true, updatable = true)
    private ModuleGroup group;
//    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
//    @Cascade({CascadeType.LOCK})
//    @JoinColumn(name = "role_id", unique = false, nullable = false, insertable = true, updatable = true)
    private Role role;

    public GroupRole() {
    }

    public GroupRole(ModuleGroup group, Role role) {
        this.group = group;
        this.role = role;
    }

    public ModuleGroup getGroup() {
        return this.group;
    }

    public void setGroup(ModuleGroup group) {
        this.group = group;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}


