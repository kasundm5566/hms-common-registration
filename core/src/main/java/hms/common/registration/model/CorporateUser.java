/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Entity class for corpoarte Users
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@Entity
public class CorporateUser extends User {

    private static final long serialVersionUID = 1504406885690975498L;

    @Column(name = "org_name", unique = false, insertable = true, updatable = true, length = 255)
    private String orgName;

    @Column(name = "industry", unique = false, insertable = true, updatable = true, length = 255)
    private String industry;

    @Column(name = "org_size", unique = false, insertable = true, updatable = true, length = 255)
    private String orgSize;

    @Column(name = "phone_numbers", unique = false, insertable = true, updatable = true, length = 255)
    private String orgPhoneNumbers;

    @Column(name = "fax_no", unique = false, insertable = true, updatable = true, length = 255)
    private String orgFaxNo;

    @Column(name = "contact_person_name", unique = false, insertable = true, updatable = true, length = 255)
    private String contactPersonName;

    @Column(name = "contact_person_phone", unique = false, insertable = true, updatable = true, length = 255)
    private String contactPersonPhone;

    @Column(name = "contact_person_email", unique = false, insertable = true, updatable = true, length = 255)
    private String contactPersonEmail;

    @Column(name = "developer_type", unique = false, nullable = true)
    @Enumerated(EnumType.STRING)
    private DeveloperType developerType;

    public CorporateUser() {
    }

    public CorporateUser(User user, UserGroup userGroup) {
        setUserId(user.getUserId());
        setUserGroup(userGroup);
        setAddress(user.getAddress());
        setBirthday(user.getBirthday());
        setCity(user.getCity());
        setCorporateUser(user.isCorporateUser());
        setLdapUser(user.isLdapUser());
        setCountry(user.getCountry());
        setCreatedBy(user.getCreatedBy());
        setEmail(user.getEmail());
        setEmailVerified(user.isEmailVerified());
        setEmailVerificationCode(user.getEmailVerificationCode());
        setEnabled(user.isEnabled());
        setFirstName(user.getFirstName());
        setGender(user.getGender());
        setLandNo(user.getLandNo());
        setLastLogin(user.getLastLogin());
        setCurrentLogin(user.getCurrentLogin());
        setLastName(user.getLastName());
        setLastPasswordUpdatedDate(user.getLastPasswordUpdatedDate());
        setLastSmsSendDate(user.getLastSmsSendDate());
        setLastEmailSendDate(user.getLastEmailSendDate());
        setLoginAttemptCount(user.getLoginAttemptCount());
        setMobileNo(user.getMobileNo());
        setMpin(user.getMpin());
        setMsisdnVerified(user.isMsisdnVerified());
        setMsisdnVerificationCode(user.getMsisdnVerificationCode());
        setOperator(user.getOperator());
        setPassword(user.getPassword());
        setPhoneModel(user.getPhoneModel());
        setPhoneType(user.getPhoneType());
        setPostCode(user.getPostCode());
        setPreviousPassword(user.getPreviousPassword());
        setProfession(user.getProfession());
        setDomain(user.getDomain());
        setDomainId(user.getDomainId());
        setProvince(user.getProvince());
        setUsername(user.getUsername());
        setBeneficiaryName(user.getBeneficiaryName());
        setBankCode(user.getBankCode());
        setBankBranchCode(user.getBankBranchCode());
        setBankBranchName(user.getBankBranchName());
        setBankAccountNumber(user.getBankAccountNumber());
        setUserStatus(user.getUserStatus());
        setUserType(user.getUserType());
        setContactPersonName(user.getFirstName());
        setContactPersonPhone(user.getMobileNo());
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getOrgSize() {
        return orgSize;
    }

    public void setOrgSize(String orgSize) {
        this.orgSize = orgSize;
    }

    public String getOrgPhoneNumbers() {
        return orgPhoneNumbers;
    }

    public void setOrgPhoneNumbers(String orgPhoneNumbers) {
        this.orgPhoneNumbers = orgPhoneNumbers;
    }

    public String getOrgFaxNo() {
        return orgFaxNo;
    }

    public void setOrgFaxNo(String orgFaxNo) {
        this.orgFaxNo = orgFaxNo;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonPhone() {
        return contactPersonPhone;
    }

    public void setContactPersonPhone(String contactPersonPhone) {
        this.contactPersonPhone = contactPersonPhone;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public DeveloperType getDeveloperType() {
        return developerType;
    }

    public void setDeveloperType(DeveloperType developerType) {
        this.developerType = developerType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(", orgName='").append(orgName).append('\'');
        sb.append(", orgSize='").append(orgSize).append('\'');
        sb.append(", industry='").append(industry).append('\'');
        sb.append(", orgPhoneNumbers='").append(orgPhoneNumbers).append('\'');
        sb.append(", orgFaxNo=").append(orgFaxNo).append('\'');
        sb.append(", contactPersonName=").append(contactPersonName).append('\'');
        sb.append(", contactPersonPhone=").append(contactPersonPhone).append('\'');
        sb.append(", contactPersonEmail=").append(contactPersonEmail).append('\'');
        sb.append(", developerType=").append(developerType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}