/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "module")
public class Module extends PersistentObject {

    private static final long serialVersionUID = 1504706225690975498L;
    private String moduleName;
    private String description;
    private String rolePrefix;

    @OneToMany(fetch = FetchType.EAGER)
    @Cascade({CascadeType.ALL, CascadeType.DELETE_ORPHAN})
    @JoinColumn(name="module") //todo make this column module_id
    private Set<Role> roleList;

    public Module() {
    }

    public Module(String moduleName) {
        this.moduleName = moduleName;
    }

    public Module(String moduleName, String description, Set<Role> roleList) {
        this.moduleName = moduleName;
        this.description = description;
        this.roleList = roleList;
    }

    @Column(name = "module_name", unique = true, nullable = false, insertable = true, updatable = true, length = 55)
    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Column(name = "description", unique = false, nullable = false, insertable = true, updatable = true)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Role> getRoleList() {
        return this.roleList;
    }

    public void setRoleList(Set<Role> roleList) {
        this.roleList = roleList;
    }

    public void addRole(Role role) {
        this.roleList.add(role);
    }

    public boolean remove(Role o) {
        return roleList.remove(o);
    }

    public boolean contains(Role o) {
        return roleList.contains(o);
    }

    public void clear() {
        roleList.clear();
    }

    @Column(name = "role_prefix", unique = true, nullable = false, insertable = true, updatable = true)
    public String getRolePrefix() {
        return rolePrefix;
    }

    public void setRolePrefix(String rolePrefix) {
        this.rolePrefix = rolePrefix;
    }

    @Override
    public String toString() {
        return String.format("[ Module-Name %s, Description %s, Role-Prefix %s]",this.getModuleName(),
                this.description, this.rolePrefix);
    }
}


