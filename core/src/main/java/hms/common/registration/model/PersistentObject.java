/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@MappedSuperclass
public abstract class PersistentObject implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    Long id;
    @Version
    private int version;
    private Date createdTime;
    private Date lastModifiedTime;

    public PersistentObject() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final PersistentObject persistentObject = (PersistentObject) o;

        if (id == null || persistentObject.getId() == null) {
            return false;
        }

        return id.equals(persistentObject.getId());
    }

    public int hashCode() {
        if (getId() == null)
            return 0;
        else
            return getId().intValue();
    }

    @Column(updatable = false, name = "created_time")
    public Date getCreatedTime() {
        return createdTime;
    }

    @Column(name = "last_modified_time")
    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public void setLastModifiedTime(Date lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    @PrePersist
    public void setCreatedTime() {
        createdTime = new Date();
    }


    @PreUpdate
    public void setLastModifiedTime() {
        lastModifiedTime = new Date();
    }
}
