/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static hms.common.registration.model.UserStatus.ACTIVE;
import static hms.common.registration.model.UserStatus.SUSPEND;

@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class User extends PersistentObject {

    private static final long serialVersionUID = 1506809885690915498L;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "corperate_parent_id", unique = false, nullable = true, insertable = true, updatable = true)
    private User corperateParentId;

    @Column(name = "user_id", unique = true, nullable = false, length = 100)
    private String userId;

    private String address;
    private Date birthday;
    private String city;
    private boolean corporateUser;
    private boolean ldapUser = false;
    private String country;
    private String createdBy;
    private String email = "";
    private boolean emailVerified = false;
    private boolean enabled;
    private String firstName;
    private String gender;
    private String landNo;
    private Date lastLogin;
    private Date currentLogin;
    private String lastName;
    private Date lastPasswordUpdatedDate;
    private Date lastSmsSendDate;
    private Date lastEmailSendDate;
    private Integer loginAttemptCount;
    private String mobileNo;
    private String mpin;
    private boolean msisdnVerified = false;
    private String operator;
    private String password = "";
    private String phoneModel;
    private String phoneType;
    private String postCode;
    private String previousPassword;
    private String profession;
    private String province;
    private String username = "";
    private String emailVerificationCode;
    private String msisdnVerificationCode;
    private String domain;
    private String domainId;
    private String beneficiaryName;
    private String bankCode;
    private String bankBranchCode;
    private String bankBranchName;
    private String bankAccountNumber;
    private String tin;
    private String businessId;
    private String businessLicenseNumber;
    private String securityQuestion;
    private String securityQuestionAnswer;
    private String image;

    @Column(name = "msisdn_verification_code_requests_count", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer msisdnVerificationCodeReqCount = 0;

    @Column(name = "msisdn_verification_attempts", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer msisdnVerificationAttempts = 0;

    @Column(name = "msisdn_change_attempts", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer msisdnChangeAttempts = 0;

    @Column(name = "first_time_login", unique = false, nullable = true, insertable = true, updatable = true)
    private Boolean firstTimeLogin = Boolean.FALSE;

    @Column(name = "msisdn_verify_skip", unique = false, nullable = true, insertable = true, updatable = true)
    private Boolean msisdnVerifySkip = Boolean.FALSE;

    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    private UserGroup userGroup;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private UserType userType;

    public User() {
        this.userId = getGeneratedUserId("");
        this.domain = "internal";
    }

    public User(String username) {
        this.username = username.toLowerCase();
        this.userId = getGeneratedUserId(username);
    }

    public User(String address, Date birthday, String city, boolean corporateUser, boolean isLdapUser, String country, String createdBy, String email, boolean emailVerified, boolean enabled, String firstName, String gender, String landNo, Date lastLogin, String lastName, Date lastPasswordUpdatedDate, Integer loginAttemptCount, String mobileNo, String mpin, boolean msisdnVerified, String operator, String password, String phoneModel, String phoneType, String postCode, String previousPassword, String profession, String province, String username, String emailVerificationCode, String msisdnVerificationCode, Set<UserGroup> userGroupList, String beneficiaryName, String bankCode, String bankBranchCode, String bankBranchName, String bankAccountNumber, String tin, String businessId) {
        this(address, birthday, city, corporateUser, isLdapUser, country, createdBy, email, emailVerified, enabled, firstName, gender, landNo, lastLogin, lastName, lastPasswordUpdatedDate, loginAttemptCount, mobileNo, mpin, msisdnVerified, operator, password, phoneModel, phoneType, postCode, previousPassword, profession, province, username, emailVerificationCode, msisdnVerificationCode, userGroupList, beneficiaryName, bankCode, bankBranchCode, bankBranchName, bankAccountNumber);
        this.tin = tin;
        this.businessId = businessId;
    }

    public User(String address, Date birthday, String city, boolean corporateUser, boolean isLdapUser, String country, String createdBy, String email, boolean emailVerified, boolean enabled, String firstName, String gender, String landNo, Date lastLogin, String lastName, Date lastPasswordUpdatedDate, Integer loginAttemptCount, String mobileNo, String mpin, boolean msisdnVerified, String operator, String password, String phoneModel, String phoneType, String postCode, String previousPassword, String profession, String province, String username, String emailVerificationCode, String msisdnVerificationCode, Set<UserGroup> userGroupList, String beneficiaryName, String bankCode, String bankBranchCode, String bankBranchName, String bankAccountNumber) {
        this.address = address;
        this.birthday = birthday;
        this.city = city;
        this.corporateUser = corporateUser;
        this.ldapUser = isLdapUser;
        this.country = country;
        this.createdBy = createdBy;
        this.email = email;
        this.emailVerified = emailVerified;
        this.enabled = enabled;
        this.firstName = firstName;
        this.gender = gender;
        this.landNo = landNo;
        this.lastLogin = lastLogin;
        this.lastName = lastName;
        this.lastPasswordUpdatedDate = lastPasswordUpdatedDate;
        this.loginAttemptCount = loginAttemptCount;
        this.mobileNo = mobileNo;
        this.mpin = mpin;
        this.msisdnVerified = msisdnVerified;
        this.operator = operator;
        this.password = password;
        this.phoneModel = phoneModel;
        this.phoneType = phoneType;
        this.postCode = postCode;
        this.previousPassword = previousPassword;
        this.profession = profession;
        this.province = province;
        this.username = username;
        this.emailVerificationCode = emailVerificationCode;
        this.msisdnVerificationCode = msisdnVerificationCode;
        this.userId = getGeneratedUserId(username);
        this.beneficiaryName = beneficiaryName;
        this.bankCode = bankCode;
        this.bankBranchCode = bankBranchCode;
        this.bankBranchName = bankBranchName;
        this.bankAccountNumber = bankAccountNumber;
    }

    @Column(name = "image", unique = false, nullable = true, insertable = true, updatable = true)
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getCorperateParentId() {
        return corperateParentId;
    }

    public void setCorperateParentId(User corperateParentId) {
        this.corperateParentId = corperateParentId;
    }

    @Column(name = "address", unique = false, nullable = true, insertable = true, updatable = true, length = 50)
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "birth_day", unique = false, nullable = true, insertable = true, updatable = true)
    public Date getBirthday() {
        return this.birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Column(name = "city", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "corporate_user", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    public boolean isCorporateUser() {
        return this.corporateUser;
    }

    public void setCorporateUser(boolean corporateUser) {
        this.corporateUser = corporateUser;
    }

    @Column(name = "ldap_user", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    public boolean isLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(boolean ldapUser) {
        this.ldapUser = ldapUser;
    }

    @Column(name = "country", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "created_by", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "email", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "email_verified", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    public boolean isEmailVerified() {
        return this.emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    @Column(name = "enabled", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "first_name", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "gender", unique = false, nullable = true, insertable = true, updatable = true, length = 50)
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "land_no", unique = false, nullable = true, insertable = true, updatable = true, length = 12)
    public String getLandNo() {
        return this.landNo;
    }

    public void setLandNo(String landNo) {
        this.landNo = landNo;
    }

    @Column(name = "last_login", unique = false, nullable = false, insertable = true, updatable = true)
    public Date getLastLogin() {
        return this.lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Column(name = "current_login", unique = false, nullable = false, insertable = true, updatable = true)
    public Date getCurrentLogin() {
        return this.currentLogin;
    }

    public void setCurrentLogin(Date currentLogin) {
        this.currentLogin = currentLogin;
    }

    @Column(name = "last_name", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "last_password_updated_date", unique = false, nullable = true, insertable = true, updatable = true)
    public Date getLastPasswordUpdatedDate() {
        return this.lastPasswordUpdatedDate;
    }

    public void setLastPasswordUpdatedDate(Date lastPasswordUpdatedDate) {
        this.lastPasswordUpdatedDate = lastPasswordUpdatedDate;
    }

    @Column(name = "last_sms_send_date", unique = false, nullable = true, insertable = true, updatable = true)
    public Date getLastSmsSendDate() {
        return this.lastSmsSendDate;
    }

    public void setLastSmsSendDate(Date lastSmsSendDate) {
        this.lastSmsSendDate = lastSmsSendDate;
    }

    @Column(name = "last_email_send_date", unique = false, nullable = true, insertable = true, updatable = true)
    public Date getLastEmailSendDate() {
        return this.lastEmailSendDate;
    }

    public void setLastEmailSendDate(Date lastEmailSendDate) {
        this.lastEmailSendDate = lastEmailSendDate;
    }

    @Column(name = "login_attempt_count", unique = false, nullable = true, insertable = true, updatable = true, length = 10)
    public Integer getLoginAttemptCount() {
        return this.loginAttemptCount;
    }

    public void setLoginAttemptCount(Integer loginAttemptCount) {
        this.loginAttemptCount = loginAttemptCount;
    }

    @Column(name = "mobile_no", unique = true, nullable = true, insertable = true, updatable = true, length = 12)
    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Column(name = "mpin", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getMpin() {
        return this.mpin;
    }

    public void setMpin(String mpin) {
        this.mpin = mpin;
    }

    @Column(name = "msisdn_verified", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    public boolean isMsisdnVerified() {
        return this.msisdnVerified;
    }

    public void setMsisdnVerified(boolean msisdnVerified) {
        this.msisdnVerified = msisdnVerified;
    }

    @Column(name = "operator", unique = false, nullable = true, insertable = true, updatable = true)
    public String getOperator() {
        return this.operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Column(name = "password", unique = false, nullable = true, insertable = true, updatable = true, length = 1000)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "phone_model", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getPhoneModel() {
        return this.phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    @Column(name = "phone_type", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getPhoneType() {
        return this.phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    @Column(name = "post_code", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getPostCode() {
        return this.postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "previous_password", unique = false, nullable = true, insertable = true, updatable = true, length = 2000)
    public String getPreviousPassword() {
        return this.previousPassword;
    }

    public void setPreviousPassword(String previousPassword) {
        this.previousPassword = previousPassword;
    }

    @Column(name = "profession", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getProfession() {
        return this.profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    //    @Id
    @Column(name = "domain", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Column(name = "domain_id", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    @Column(name = "province", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getProvince() {
        return this.province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    //    @Id
    @Column(name = "username", unique = false, nullable = false, insertable = true, updatable = true, length = 100)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username.toLowerCase();
    }

    @Column(name = "emailVerificationCode", unique = false, nullable = true, insertable = true, updatable = true)
    public String getEmailVerificationCode() {
        return this.emailVerificationCode;
    }

    public void setEmailVerificationCode(String emailVerificationCode) {
        this.emailVerificationCode = emailVerificationCode;
    }

    @Column(name = "msisdnVerificationCode", unique = false, nullable = true, insertable = true, updatable = true)
    public String getMsisdnVerificationCode() {
        return this.msisdnVerificationCode;
    }

    public void setMsisdnVerificationCode(String msisdnVerificationCode) {
        this.msisdnVerificationCode = msisdnVerificationCode;
    }

    @Column(name = "beneficiary_name", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getBeneficiaryName() {
        return this.beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    @Column(name = "bank_code", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getBankCode() {
        return this.bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }


    @Column(name = "bank_branch_code", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getBankBranchCode() {
        return this.bankBranchCode;
    }

    public void setBankBranchCode(String bankBranchCode) {
        this.bankBranchCode = bankBranchCode;
    }

    @Column(name = "bank_branch_name", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getBankBranchName() {
        return this.bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    @Column(name = "bank_account_number", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    public String getBankAccountNumber() {
        return this.bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Column(name = "tin", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    @Column(name = "business_id", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    public String getBusinessId() {
        return businessId;
    }

    @Column(name = "business_license_number", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    public String getBusinessLicenseNumber() {
        return businessLicenseNumber;
    }

    @Column(name = "security_question", unique = false, nullable = true, insertable = true, updatable = true, length = 256)
    public String getSecurityQuestion() {
        return securityQuestion;
    }

    @Column(name = "security_question_answer", unique = false, nullable = true, insertable = true, updatable = true, length = 256)
    public String getSecurityQuestionAnswer() {
        return securityQuestionAnswer;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public void setBusinessLicenseNumber(String businessLicenseNumber) {
        this.businessLicenseNumber = businessLicenseNumber;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public void setSecurityQuestionAnswer(String securityQuestionAnswer) {
        this.securityQuestionAnswer = securityQuestionAnswer;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public void activate() {
        enabled = true;
        userStatus = ACTIVE;
    }

    public void suspend() {
        enabled = false;
        userStatus = SUSPEND;
    }

    private String getGeneratedUserId(String userName) {
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + userName;
    }

    public Integer getMsisdnVerificationCodeReqCount() {
        return msisdnVerificationCodeReqCount;
    }

    public void setMsisdnVerificationCodeReqCount(Integer msisdnVerificationCodeReqCount) {
        this.msisdnVerificationCodeReqCount = msisdnVerificationCodeReqCount;
    }

    public Integer getMsisdnVerificationAttempts() {
        return msisdnVerificationAttempts;
    }

    public void setMsisdnVerificationAttempts(Integer msisdnVerificationAttempts) {
        this.msisdnVerificationAttempts = msisdnVerificationAttempts;
    }

    public Integer getMsisdnChangeAttempts() {
        return msisdnChangeAttempts;
    }

    public void setMsisdnChangeAttempts(Integer msisdnChangeAttempts) {
        this.msisdnChangeAttempts = msisdnChangeAttempts;
    }

    public Boolean isFirstTimeLogin() {
        return firstTimeLogin;
    }

    public void setFirstTimeLogin(Boolean firstTimeLogin) {
        this.firstTimeLogin = firstTimeLogin;
    }

    public Boolean isMsisdnVerifySkip() {
        return msisdnVerifySkip;
    }

    public void setMsisdnVerifySkip(Boolean msisdnVerifySkip) {
        this.msisdnVerifySkip = msisdnVerifySkip;
    }

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
        userGroup.add(this);
    }

    public List<String> getRolesAsStrings(String moduleName) {
        List<String> result = new ArrayList<String>();
        Set<Role> roles = this.getUserGroup().getRoles();
        for (Role role : roles) {
            if (role.getModule().getModuleName().equals(moduleName)) {
                result.add(role.getName());
            }
        }
        return result;
    }

    public List<String> getRolesAsStrings() {
        List<String> roles = new ArrayList<String>();
        roles.addAll(getUserGroup().getRolesAsStrings());
        return roles;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", birthday=").append(birthday);
        sb.append(", city='").append(city).append('\'');
        sb.append(", corporateUser=").append(corporateUser);
        sb.append(", ldapUser=").append(ldapUser);
        sb.append(", country='").append(country).append('\'');
        sb.append(", image='").append(image).append('\'');
        sb.append(", createdBy='").append(createdBy).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", emailVerified=").append(emailVerified);
        sb.append(", enabled=").append(enabled);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", landNo='").append(landNo).append('\'');
        sb.append(", lastLogin=").append(lastLogin);
        sb.append(", currentLogin=").append(currentLogin);
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", lastPasswordUpdatedDate=").append(lastPasswordUpdatedDate);
        sb.append(", lastSmsSendDate=").append(lastSmsSendDate);
        sb.append(", lastEmailSendDate=").append(lastEmailSendDate);
        sb.append(", loginAttemptCount=").append(loginAttemptCount);
        sb.append(", mobileNo='").append(mobileNo).append('\'');
        sb.append(", mpin='").append(mpin).append('\'');
        sb.append(", msisdnVerified=").append(msisdnVerified);
        sb.append(", operator='").append(operator).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", phoneModel='").append(phoneModel).append('\'');
        sb.append(", phoneType='").append(phoneType).append('\'');
        sb.append(", postCode='").append(postCode).append('\'');
        sb.append(", previousPassword='").append(previousPassword).append('\'');
        sb.append(", profession='").append(profession).append('\'');
        sb.append(", province='").append(province).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", emailVerificationCode='").append(emailVerificationCode).append('\'');
        sb.append(", msisdnVerificationCode='").append(msisdnVerificationCode).append('\'');
        sb.append(", domain='").append(domain).append('\'');
        sb.append(", domainId='").append(domainId).append('\'');
        sb.append(", beneficiaryName='").append(beneficiaryName).append('\'');
        sb.append(", bankCode='").append(bankCode).append('\'');
        sb.append(", bankBranchCode='").append(bankBranchCode).append('\'');
        sb.append(", bankBranchName='").append(bankBranchName).append('\'');
        sb.append(", bankAccountNumber='").append(bankAccountNumber).append('\'');
        sb.append(", tin='").append(tin).append('\'');
        sb.append(", businessId='").append(businessId).append('\'');
        sb.append(", msisdnVerificationCodeReqCount=").append(msisdnVerificationCodeReqCount);
        sb.append(", msisdnVerificationAttempts=").append(msisdnVerificationAttempts);
        sb.append(", msisdnChangeAttempts=").append(msisdnChangeAttempts);
        sb.append(", firstTimeLogin=").append(firstTimeLogin);
        sb.append(", msisdnVerifySkip=").append(msisdnVerifySkip);
        sb.append(", userGroup=").append(userGroup);
        sb.append(", userStatus=").append(userStatus);
        sb.append(", userType=").append(userType);
        sb.append('}');
        return sb.toString();
    }
}