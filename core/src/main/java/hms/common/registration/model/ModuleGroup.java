/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.model;

//@Entity
//@Table(name = "module_group")
public class ModuleGroup extends PersistentObject {

    private static final long serialVersionUID = 1504706885690975498L;
    private String name;
    private String description;
//    @OneToMany(cascade = {}, fetch = FetchType.EAGER, mappedBy = "group")
//    @Cascade({CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DELETE_ORPHAN})
//    private Set<GroupRole> groupRoleList = new HashSet<GroupRole>(0);
//    @OneToMany(cascade = {}, fetch = FetchType.LAZY, mappedBy = "group")
//    @Cascade({CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DELETE_ORPHAN})
//    private Set<UserGroup> userGroupList = new HashSet<UserGroup>(0);

    public ModuleGroup() {
    }

    public ModuleGroup(String name) {
        this.name = name;
    }

//    public ModuleGroup(String name, String description, Set<GroupRole> groupRoleList, Set<UserGroup> userGroupList) {
    public ModuleGroup(String name, String description) {
        this.name = name;
        this.description = description;
  //        this.groupRoleList = groupRoleList;
  //        this.userGroupList = userGroupList;
    }

//    @Column(name = "name", unique = false, nullable = false, insertable = true, updatable = true, length = 55)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @Column(name = "description", unique = false, nullable = true, insertable = true, updatable = true)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public Set<GroupRole> getGroupRoleList() {
//        return this.groupRoleList;
//    }
//
//    public void setGroupRoleList(Set<GroupRole> groupRoleList) {
//        this.groupRoleList = groupRoleList;
//    }
//
//    public Set<UserGroup> getUserGroupList() {
//        return this.userGroupList;
//    }
//
//    public void setUserGroupList(Set<UserGroup> userGroupList) {
//        this.userGroupList = userGroupList;
//    }

//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder();
//        sb.append("ModuleGroup");
//        sb.append(", name='").append(name).append('\'');
//        sb.append(", description='").append(description).append('\'');
//        sb.append(", groupRoleList=").append(groupRoleList);
//        sb.append(", userGroupList=").append(userGroupList);
//        sb.append('}');
//        return sb.toString();
//    }
}


