package hms.common.registration.simulator.dialog.connect;

import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DialogConnectServlet extends HttpServlet{

    private static final Logger logger = Logger.getLogger("dialog-connect");

    private static final ImmutableMap<String, String> response;
    private static final ImmutableMap<String, String> users;

    static {
        users = ImmutableMap.of("hsenid2", "test123#");
        response = ImmutableMap.of("hsenid2", successResponse1());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String username = req.getParameter("username");
        final String password = req.getParameter("password");

        logger.log(Level.INFO, String.format("username = %s, password = %s", username, password));

        if(users.containsKey(username) && users.get(username).equals(password)) {
            resp.getWriter().append(response.get(username));
        } else {
            resp.getWriter().append(errorResponse());
        }
        resp.setStatus(200);
        resp.setContentType("text/xml; charset=UTF-8");
    }

    private static String successResponse1() {
        return "<?xml version=\"1.0\"?><res>\n" +
                "<status>OK</status>\n" +
                "<code>0</code>\n" +
                "<desc>User validated</desc>\n" +
                ".<user>\n" +
                "..<phoneNo>770808373</phoneNo>\n" +
                "..<userID>hsenid2</userID>..\n" +
                "..<email>anjulaw@hsenidmobile.com</email>\n" +
                "..<type>prepaid</type>\n" +
                "..<status>C</status>\n" +
                "..<firstName><![CDATA[-]]></firstName>\n" +
                "..<lastName><![CDATA[HSENID MOBILE SOLUTIONS PVT LTD]]></lastName>..\n" +
                "..<dob></dob>\n" +
                "..<contactNo></contactNo>..\n" +
                "..<gender></gender>..\n" +
                "..<idType>TIN</idType>\n" +
                "..<idNumber>PV62579</idNumber>\n" +
                "..<moreInfo></moreInfo>\n" +
                "..<title><![CDATA[MS]]></title>\n" +
                "..\n" +
                "...<services>\n" +
                "...\n" +
                "....<service>\n" +
                ".....<type>GSM</type>\n" +
                ".....<contractid>15062826</contractid>\n" +
                ".....<number>770808373</number>\n" +
                ".....<cxtype>prepaid</cxtype>\n" +
                ".....<connStatus>C</connStatus>\n" +
                ".....<theme>prepaid_blue</theme>\n" +
                ".....<default>true</default>\n" +
                "....</service>\n" +
                "......\n" +
                "...</services>\n" +
                "..  \n" +
                ".</user>\n" +
                "</res>";
    }

    private static String errorResponse() {
        return "<?xml version=\"1.0\"?><res>\n" +
                "        <code>505</code>\n" +
                "        <status>ERR</status>\n" +
                "        <desc>User not found</desc>\n" +
                "        </res>";
    }
}
