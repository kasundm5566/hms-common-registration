#!/bin/sh

source ./migrate-user-functions.sh

existing_username="";
dialog_connect_username="";

function migration_using_file(){
    if [ -f $1 ]; then
    	for line in $(cat $1); do

            ex_username=`echo ${line}|cut -d"|" -f1`;
            dc_username=`echo ${line}|cut -d"|" -f2`;

            migrate_existing_user ${ex_username} ${dc_username};
        done
    else
        echo "The file '$1' doesn't exists";
    fi

    exit 1;
}

function migration_using_name(){
    if [ "${existing_username}" != "" -a "${dialog_connect_username}" != "" ]
    then
        migrate_existing_user ${existing_username} ${dialog_connect_username};

        exit 1;
    fi
}

function usage(){
    echo "Usage";
    echo "==========";
    echo "$1 -f [user-list-file]";
    echo "$1 -e [existing-username] -d [dialog-connect-username]";
    echo "$1 -h [help]";
    echo "";

    echo "Format of user-list-file contents";
    echo "======================================";
    echo "existing-username|dialog-connect-username";

    echo "";
    echo "Sample user-list-file";
    echo "===========================";
    echo "sanjeewa|sanjeewarc";
    echo "hsenid|beyondm";
    echo "";

    exit 1;
}

if [ $# -eq 0 ]
then
    usage $0;
    exit 1;
fi

while getopts "f:e:d:h" flag
do
   case $flag in
   f)
        migration_using_file $OPTARG
   ;;
   e)
        existing_username=$OPTARG;
        migration_using_name
   ;;
   d)
        dialog_connect_username=$OPTARG;
        migration_using_name
   ;;
   h)
        usage $0
   ;;
   *)
        usage $0
   ;;
   esac
done

usage $0;