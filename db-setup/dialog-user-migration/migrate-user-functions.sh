#!/bin/sh

mysql_server="db.mysql.cur";
mysql_username="user";
mysql_password="password";

mysql_common_admin_db="common_admin";
mysql_reporting_db="reporting";

mongo_server="db.mongo.sdp";
mongo_port="27017";

mongo_kite_db="kite";
mongo_appstore_db="appstore";

log_file=./migration.log

function log(){
    printf "$1" >> ${log_file};
}

function print_details(){
   log "$1\n";
   log ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";

   if [ "$2" != "" -a "$2" != "null" ]
   then
       log "$2\n";
   else
       log "no entry found\n";
   fi

   log "\n";
}

function execute_mongo_evaluation(){
   mongo ${mongo_server}:${mongo_port}/$1 --quiet --eval "$2";
}

function print_mongo_details(){
   result=`execute_mongo_evaluation $1 "$2"`;

   print_details "mongo $3 entry(s) $4 updating" "${result}";
}

function update_mongo_details(){
   print_mongo_details $1 "$2" "$5" "before"

   execute_mongo_evaluation $1 "$4";

   print_mongo_details $1 "$3" "$5" "after"
}

function update_mongo_db(){
   log "Updating mongo details of [$1]\n";
   log "=============================================================================\n";

   `update_mongo_details ${mongo_kite_db} 'printjson(db.sp.findOne({"coop-user-name" : "'$1'"},{"coop-user-id" : 1,"coop-user-name" : 1, "created-by" : 1}))' 'printjson(db.sp.findOne({"coop-user-name" : "'$2'"},{"coop-user-id" : 1,"coop-user-name" : 1, "created-by" : 1}))' 'db.sp.update({"coop-user-name" : "'$1'"}, {$set:{"created-by" : "'$2'", "coop-user-name" : "'$2'"}})' "${mongo_kite_db}.sp"`;

   `update_mongo_details ${mongo_kite_db} 'var cursor = db.app.find({"created-by" : "'$1'"},{"app-id" : 1, "created-by" : 1}); while (cursor.hasNext()) printjson(cursor.next());' 'var cursor = db.app.find({"created-by" : "'$2'"},{"app-id" : 1, "created-by" : 1}); while (cursor.hasNext()) printjson(cursor.next());' 'db.app.update({"created-by" : "'$1'"}, {$set:{"created-by" : "'$2'"}}, false, true)' "${mongo_kite_db}.app"`;

   `update_mongo_details ${mongo_kite_db} 'var cursor = db.ncs_sla.find({"created-by" : "'$1'"},{"app-id" : 1, "created-by" : 1, "ncs-type" : 1}); while (cursor.hasNext()) printjson(cursor.next());' 'var cursor = db.ncs_sla.find({"created-by" : "'$2'"},{"app-id" : 1, "created-by" : 1, "ncs-type" : 1}); while (cursor.hasNext()) printjson(cursor.next());' 'db.ncs_sla.update({"created-by" : "'$1'"}, {$set:{"created-by" : "'$2'"}}, false, true)' "${mongo_kite_db}.ncs"`;

   `update_mongo_details ${mongo_appstore_db} 'var cursor = db.apps.find({"developer" : "'$1'"},{"developer" : 1}); while (cursor.hasNext()) printjson(cursor.next());' 'var cursor = db.apps.find({"developer" : "'$2'"},{"developer" : 1}); while (cursor.hasNext()) printjson(cursor.next());' 'db.apps.update({"developer" : "'$1'"}, {$set:{"developer" : "'$2'"}}, false, true)' "${mongo_appstore_db}.apps"`;
}

function execute_mysql_query(){
   mysql -u${mysql_username} -p${mysql_password} -h${mysql_server} $1 -e"$2";
}

function mysql_print_details(){
   result=`execute_mysql_query $1 "$2"`;

   print_details "mysql $1.$3 entry $4 updating" "${result}";
}

function update_mysql_details(){
   mysql_print_details $1 "$2" $5 "before";

   execute_mysql_query $1 "$4";

   mysql_print_details $1 "$3" $5 "after";
}

function update_mysql_db(){
   log "Updating mysql details of [$1]\n";
   log "=============================================================================\n";

   `update_mysql_details ${mysql_common_admin_db} "select id, user_id, username, domain, domain_id from user where username='$1'\G" "select id, user_id, username, domain, domain_id from user where username='$2'\G" "update user set domain='dialog', domain_id='$2', username='$2' where username='$1'" "user"`;

   log "Start updating mysql reporting.sdp_transaction of [$1] at `date`\n\n";

   execute_mysql_query ${mysql_reporting_db} "update sdp_transaction set sp_name='$2' where sp_name='$1'";

   log "Stop updating mysql reporting.sdp_transaction of [$1] at `date`\n\n";

   `update_mysql_details ${mysql_reporting_db} "select sp_id, sp_name from service_provider_info where sp_name='$1' group by sp_id, sp_name\G" "select sp_id, sp_name from service_provider_info where sp_name='$2' group by sp_id, sp_name\G" "update service_provider_info set sp_name='$2' where sp_name='$1'" "service_provider_info"`;

   `update_mysql_details ${mysql_reporting_db} "select sp_id, sp_name from sdp_transaction_summary where sp_name='$1' group by sp_id, sp_name\G" "select sp_id, sp_name from sdp_transaction_summary where sp_name='$2' group by sp_id, sp_name\G" "update sdp_transaction_summary set sp_name='$2' where sp_name='$1'" "sdp_transaction_summary"`;
}

function migrate_existing_user(){
   log "###################################  `date`  ################################\n\n";
   log "Migrating user [$1] into [$2]\n";
   log "=============================================================================\n\n";

   update_mongo_db $1 $2;
   update_mysql_db $1 $2;

   log "###################################  `date`  ################################\n\n";
}