INSERT INTO `module` VALUES (6,NULL,NULL,0,'The Appstore module','appstore','ROLE_APP_');


INSERT INTO `role` VALUES (134,NULL,NULL,0,'Appstore login','ROLE_APP_LOGIN',6),
(135,NULL,NULL,0,'Publish approve','ROLE_APP_APPROVE_PUBLISH',6),
(136,NULL,NULL,0,'Publish CR List view','ROLE_APP_VIEW_PUBLISH_CR',6),
(137,NULL,NULL,0,'Publish CR approve','ROLE_APP_APPROVE_PUBLISH_CR',6),
(138,NULL,NULL,0,'Publish CR reject','ROLE_APP_REJECT_PUBLISH_CR',6),
(139,NULL,NULL,0,'Configure panels','ROLE_APP_CONFIG_PANNELS',6),
(140,NULL,NULL,0,'Select featured apps','ROLE_APP_SELECT_FEATURED_APPS',6),
(141,NULL,NULL,0,'Define categories','ROLE_APP_DEFINE_CATEGORIES',6),
(142,NULL,NULL,0,'Remove Wishlist','ROLE_APP_REMOVE_WISHLIST',6),
(143,NULL,NULL,0,'Remove Comments','ROLE_APP_REMOVE_COMMENTS',6),
(144,NULL,NULL,0,'Blacklist Comments','ROLE_APP_BLACKLIST_COMMENTS',6),
(145,NULL,NULL,0,'Blacklist Wishlist','ROLE_APP_BLACKLIST_WHISHLIST',6),
(165,NULL,NULL,0,'Appstore – Administrative Role','ROLE_APP_ADMIN',6),
(166,NULL,NULL,0,'Appstore –  Corporate User Role','ROLE_APP_SP',6),
(167,NULL,NULL,0,'Appstore – Consumer Role','ROLE_APP_USER',6),
(345,NULL,NULL,0,'Common appstore url view permission','ROLE_PROV_COMMON_APPSTORE_URL',5),
(381,NULL,NULL,0,'Display appstore in header menu','ROLE_SDP_RPT_HEADER_MENU_APPSTORE',8);



INSERT INTO `user_group_roles` VALUES (1,134),
(1,135),
(1,136),
(1,137),
(1,138),
(1,139),
(1,140),
(1,141),
(1,142),
(1,143),
(1,144),
(1,145),
(1,165),
(1,166),
(1,381),
(1,345),
(3,134),
(3,166),
(3,167),
(3,345),
(3,381),
(4,134),
(5,134),
(7,165),
(7,166),
(7,167),
(10,136);

