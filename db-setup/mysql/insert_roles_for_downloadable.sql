INSERT INTO `role` VALUES
(183,NULL,NULL,0,'Build File Approval Permission','ROLE_PROV_APPROVE_BUILD',5),
(184,NULL,NULL,0,'Build File Edit Permission','ROLE_PROV_EDIT_BUILD',5),
(185,NULL,NULL,0,'Build File Suspend Permission','ROLE_PROV_SUSPEND_BUILD',5),
(186,NULL,NULL,0,'Build File Reject Permission','ROLE_PROV_REJECT_BUILD',5),
(187,NULL,NULL,0,'Build File Restore Permission','ROLE_PROV_RESTORE_BUILD',5),
(188,NULL,NULL,0,'Build File Delete Permission','ROLE_PROV_DELETE_BUILD',5),
(189,NULL,NULL,0,'Build file Delete permission for the SP','ROLE_PROV_SP_DELETE_BUILD',5),
(190,NULL,NULL,0,'Build file Edit permission for the SP','ROLE_PROV_SP_EDIT_BUILD',5),
(191,NULL,NULL,0,'Build file Approve permission for the Admin','ROLE_PROV_ADMIN_APPROVE_BUILD',5),
(192,NULL,NULL,0,'Build file Reject permission for the Admin','ROLE_PROV_ADMIN_REJECT_BUILD',5),
(193,NULL,NULL,0,'Build file Suspend permission for the Admin','ROLE_PROV_ADMIN_SUSPEND_BUILD',5),
(194,NULL,NULL,0,'Build file Delete permission for the Admin','ROLE_PROV_ADMIN_DELETE_BUILD',5),
(195,NULL,NULL,0,'Build file Restore permission for the Admin','ROLE_PROV_ADMIN_RESTORE_BUILD',5),
(196,NULL,NULL,0,'Build file Upload permission for the SP','ROLE_PROV_SP_UPLOAD_BUILD',5);


INSERT INTO `user_group_roles` VALUES (1,191),(1,192),(1,193),(1,194),(1,195), (3,189),(3,190),(3,196);