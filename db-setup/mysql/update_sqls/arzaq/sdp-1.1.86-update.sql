INSERT INTO `role` VALUES
(611,NULL,NULL,0,'Caas offline transaction required read permission','ROLE_PROV_CAAS_READ_OFFLINE_TRANSACTION_REQUIRED',5),
(612,NULL,NULL,0,'Caas offline transaction required write permission','ROLE_PROV_CAAS_WRITE_OFFLINE_TRANSACTION_REQUIRED',5),
(613,NULL,NULL,0,'Caas nfc payment required read permission','ROLE_PROV_CAAS_READ_NFC_PAYMENT_REQUIRED',5),
(614,NULL,NULL,0,'Caas nfc payment required write permission','ROLE_PROV_CAAS_WRITE_NFC_PAYMENT_REQUIRED',5);

INSERT INTO `user_group_roles` VALUES
(3,611),
(3,612),
(3,613),
(3,614),
(1,611),
(1,612),
(1,613),
(1,614),
(3,209),
(3,210),
(3,211),
(3,212),
(3,213),
(3,214),
(1,209),
(1,210),
(1,211),
(1,212),
(1,213),
(1,214);