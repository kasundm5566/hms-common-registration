USE common_admin;

INSERT INTO `role` VALUES (208,NULL,NULL,0,'Caas allow query balance write permission','ROLE_PROV_CAAS_WRITE_ALLOW_QUERY_BALANCE',5);

INSERT INTO `user_group_roles`(`user_group`, `roles`) VALUES (1,207), (3,207), (1,208), (3,208);
