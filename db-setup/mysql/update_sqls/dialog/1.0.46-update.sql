INSERT INTO `role` VALUES

(201,NULL,NULL,0,'Caas asynchronous charging notification read permission','ROLE_PROV_CAAS_READ_ASYNC_CHARGING_NOTIFICATION',5),
(202,NULL,NULL,0,'Caas asynchronous charging notification write permission','ROLE_PROV_CAAS_WRITE_ASYNC_CHARGING_NOTIFICATION',5),
(207,NULL,NULL,0,'Caas allow query balance read permission','ROLE_PROV_CAAS_READ_ALLOW_QUERY_BALANCE',5),
(208,NULL,NULL,0,'Caas allow query balance write permission','ROLE_PROV_CAAS_WRITE_ALLOW_QUERY_BALANCE',5),
(501,NULL,NULL,0,'Authorization Medium read permission','ROLE_PROV_CAAS_READ_ALLOW_AUTH_MEDIUM',5),
(502,NULL,NULL,0,'Authorization Medium  write permission','ROLE_PROV_CAAS_WRITE_ALLOW_AUTH_MEDIUM',5),
(503,NULL,NULL,0,'Authorization Threshold read permission','ROLE_PROV_CAAS_READ_ALLOW_AUTH_THRESHOLD',5),
(504,NULL,NULL,0,'Authorization Threshold write permission','ROLE_PROV_CAAS_WRITE_ALLOW_AUTH_THRESHOLD',5)
(505,NULL,NULL,0,'Downloadable charging notification read permission','ROLE_PROV_DOWNLOADABLE_READ_CHARGING_NOTIFICATION_ENABLE',5),
(506,NULL,NULL,0,'Downloadable charging notification write permission','ROLE_PROV_DOWNLOADABLE_WRITE_CHARGING_NOTIFICATION_ENABLE',5);

INSERT INTO `user_group_roles` VALUES
(1,201),
(1,202),
(3,201),
(3,202),
(1,207),
(1,208),
(3,207),
(3,208),
(1,501),
(1,502),
(3,501),
(3,502),
(1,503),
(1,504),
(1,505),
(1,506),
(3,505),
(3,506);

DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 458;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 460;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 468;