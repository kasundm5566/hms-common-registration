--
-- common admin update script for adding pgw reports
--

USE `common_admin`;

-- adding roles

INSERT INTO `role` (`id`, `created_time`, `last_modified_time`, `version`, `description`, `name`, `module`) VALUES
(470,NULL,NULL,0,'PGW Payment Instruments Revenue report','ROLE_PGW_RPT_PAYMENT_INSTRUMENT_REVENUE',8),
(471,NULL,NULL,0,'PGW Detail Transactions report (Customer Care)','ROLE_PGW_RPT_DETAIL__TRANS',8);

-- adding new reporting roles to admin users

INSERT INTO `user_group_roles` (`user_group`, `roles`) VALUES (1, 470);
INSERT INTO `user_group_roles` (`user_group`, `roles`) VALUES (1, 471);