DELETE FROM `user_group_roles` WHERE roles = 201;
DELETE FROM `user_group_roles` WHERE roles = 202;
DELETE FROM `user_group_roles` WHERE roles = 211;
DELETE FROM `user_group_roles` WHERE roles = 212;
DELETE FROM `user_group_roles` WHERE roles = 213;
DELETE FROM `user_group_roles` WHERE roles = 214;
DELETE FROM `user_group_roles` WHERE roles = 209;
DELETE FROM `user_group_roles` WHERE roles = 210;
DELETE FROM `user_group_roles` WHERE roles = 211;
DELETE FROM `user_group_roles` WHERE roles = 212;
DELETE FROM `user_group_roles` WHERE roles = 213;
DELETE FROM `user_group_roles` WHERE roles = 214;
DELETE FROM `user_group_roles` WHERE roles = 215;
DELETE FROM `user_group_roles` WHERE roles = 216;
DELETE FROM `user_group_roles` WHERE roles = 217;
DELETE FROM `user_group_roles` WHERE roles = 218;
DELETE FROM `user_group_roles` WHERE roles = 219;
DELETE FROM `user_group_roles` WHERE roles = 220;
DELETE FROM `user_group_roles` WHERE roles = 227;
DELETE FROM `user_group_roles` WHERE roles = 228;
DELETE FROM `user_group_roles` WHERE roles = 229;
DELETE FROM `user_group_roles` WHERE roles = 230;
DELETE FROM `user_group_roles` WHERE roles = 231;
DELETE FROM `user_group_roles` WHERE roles = 232;
DELETE FROM `user_group_roles` WHERE roles = 235;
DELETE FROM `user_group_roles` WHERE roles = 236;
DELETE FROM `user_group_roles` WHERE roles = 269;
DELETE FROM `user_group_roles` WHERE roles = 270;
DELETE FROM `user_group_roles` WHERE roles = 285;
DELETE FROM `user_group_roles` WHERE roles = 286;

DELETE FROM `role` WHERE id = 201;
DELETE FROM `role` WHERE id = 202;
DELETE FROM `role` WHERE id = 211;
DELETE FROM `role` WHERE id = 212;
DELETE FROM `role` WHERE id = 213;
DELETE FROM `role` WHERE id = 214;
DELETE FROM `role` WHERE id = 209;
DELETE FROM `role` WHERE id = 210;
DELETE FROM `role` WHERE id = 211;
DELETE FROM `role` WHERE id = 212;
DELETE FROM `role` WHERE id = 213;
DELETE FROM `role` WHERE id = 214;
DELETE FROM `role` WHERE id = 215;
DELETE FROM `role` WHERE id = 216;
DELETE FROM `role` WHERE id = 217;
DELETE FROM `role` WHERE id = 218;
DELETE FROM `role` WHERE id = 219;
DELETE FROM `role` WHERE id = 220;
DELETE FROM `role` WHERE id = 227;
DELETE FROM `role` WHERE id = 228;
DELETE FROM `role` WHERE id = 229;
DELETE FROM `role` WHERE id = 230;
DELETE FROM `role` WHERE id = 231;
DELETE FROM `role` WHERE id = 232;
DELETE FROM `role` WHERE id = 235;
DELETE FROM `role` WHERE id = 236;
DELETE FROM `role` WHERE id = 269;
DELETE FROM `role` WHERE id = 270;
DELETE FROM `role` WHERE id = 285;
DELETE FROM `role` WHERE id = 286;

DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 300;

INSERT INTO `role` VALUES (388,NULL,NULL,0,'Display provisioning in soltura header menu','ROLE_SOL_HEADER_MENU_PROVISIONING',8);
INSERT INTO `role` VALUES (389,NULL,NULL,0,'Display appstore in soltura header menu','ROLE_SOL_HEADER_MENU_APPSTORE',8);
INSERT INTO `role` VALUES (390,NULL,NULL,0,'Display reporting in soltura header menu','ROLE_SOL_HEADER_MENU_REPORTING',8);

INSERT INTO `user_group_roles` VALUES (3,372);
INSERT INTO `user_group_roles` VALUES (3,388);
INSERT INTO `user_group_roles` VALUES (3,389);
INSERT INTO `user_group_roles` VALUES (3,390);
