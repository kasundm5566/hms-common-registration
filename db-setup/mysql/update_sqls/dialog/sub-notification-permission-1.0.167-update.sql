INSERT INTO role (id, created_time, last_modified_time, version, description, name, module) VALUES (500, null, null, 0, 'Send NotificationPermission', 'ROLE_PROV_SUBSCRIPTION_READ_ALLOW_SEND_NOTIFICATION', 5);
INSERT INTO user_group_roles (user_group, roles) VALUES (1, 500);
INSERT INTO user_group_roles (user_group, roles) VALUES (3, 500);