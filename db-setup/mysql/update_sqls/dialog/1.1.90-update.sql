use common_admin;

DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 505;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 506;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 505;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 506;