DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 360;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 361;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 362;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 363;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 381;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 382;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 383;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 360;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 361;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 362;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 363;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 381;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 382;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 383;

DELETE FROM `role` WHERE id = 360;
DELETE FROM `role` WHERE id = 361;
DELETE FROM `role` WHERE id = 362;
DELETE FROM `role` WHERE id = 363;
DELETE FROM `role` WHERE id = 381;
DELETE FROM `role` WHERE id = 382;
DELETE FROM `role` WHERE id = 383;

INSERT INTO `role` VALUES (360,NULL,NULL,0,'Display provisioning in header menu','ROLE_SDP_RPT_HEADER_MENU_PROVISIONING',8);
INSERT INTO `role` VALUES (361,NULL,NULL,0,'Display admin in header menu','ROLE_SDP_RPT_HEADER_MENU_ADMIN',8);
INSERT INTO `role` VALUES (362,NULL,NULL,0,'Display registration in header menu','ROLE_SDP_RPT_HEADER_MENU_REGISTRATION',8);
INSERT INTO `role` VALUES (363,NULL,NULL,0,'Display soltura in header menu','ROLE_SDP_RPT_HEADER_MENU_SOLTURA',8);
INSERT INTO `role` VALUES (381,NULL,NULL,0,'Display appstore in header menu','ROLE_SDP_RPT_HEADER_MENU_APPSTORE',8);

INSERT INTO `user_group_roles` VALUES (1,345);
INSERT INTO `user_group_roles` VALUES (1,360);
INSERT INTO `user_group_roles` VALUES (1,361);
INSERT INTO `user_group_roles` VALUES (1,362);
INSERT INTO `user_group_roles` VALUES (1,381);
INSERT INTO `user_group_roles` VALUES (3,167);
INSERT INTO `user_group_roles` VALUES (3,345);
INSERT INTO `user_group_roles` VALUES (3,360);
INSERT INTO `user_group_roles` VALUES (3,363);
INSERT INTO `user_group_roles` VALUES (3,381);
