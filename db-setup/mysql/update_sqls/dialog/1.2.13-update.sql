use common_admin;

INSERT INTO `role` VALUES (419,NULL,NULL,0,'Caas allowed payment instrument read permission','ROLE_PROV_CAAS_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (420,NULL,NULL,0,'Caas allowed payment instrument write permission','ROLE_PROV_CAAS_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);

INSERT INTO `user_group_roles` VALUES (1,419);
INSERT INTO `user_group_roles` VALUES (1,420);

INSERT INTO `user_group_roles` VALUES (3,419);
INSERT INTO `user_group_roles` VALUES (3,420);