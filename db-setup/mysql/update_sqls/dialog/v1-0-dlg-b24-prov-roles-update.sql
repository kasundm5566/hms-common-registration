INSERT INTO `role` VALUES
(187,NULL,NULL,0,'Build file Delete permission for the SP','ROLE_PROV_SP_DELETE_BUILD',5),
(188,NULL,NULL,0,'Build file Edit permission for the SP','ROLE_PROV_SP_EDIT_BUILD',5),
(189,NULL,NULL,0,'Build file Upload permission for the SP','ROLE_PROV_SP_UPLOAD_BUILD',5),
(190,NULL,NULL,0,'Build file Approve permission for the Admin','ROLE_PROV_ADMIN_APPROVE_BUILD',5),
(191,NULL,NULL,0,'Build file Reject permission for the Admin','ROLE_PROV_ADMIN_REJECT_BUILD',5),
(192,NULL,NULL,0,'Build file Suspend permission for the Admin','ROLE_PROV_ADMIN_SUSPEND_BUILD',5),
(193,NULL,NULL,0,'Build file Delete permission for the Admin','ROLE_PROV_ADMIN_DELETE_BUILD',5),
(194,NULL,NULL,0,'Build file Restore permission for the Admin','ROLE_PROV_ADMIN_RESTORE_BUILD',5);

INSERT INTO `user_group_roles` VALUES
(1,190),
(1,191),
(1,192),
(1,193),
(1,194),
(3,61),
(3,70),
(3,79),
(3,88),
(3,95),
(3,112),
(3,113),
(3,116),
(3,187),
(3,188),
(3,189);

DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 97;