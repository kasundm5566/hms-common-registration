USE common_admin;

INSERT INTO `role` VALUES (510,NULL,NULL,0,'sdp app report sort option revenue','ROLE_SDP_RPT_APP_REVENUE_TOTAL',8),
                          (511,NULL,NULL,0,'sdp app report sort option charged msg count','ROLE_SDP_RPT_APP_REVENUE_CHARGED_MSG',8),
                          (512,NULL,NULL,0,'sdp app report sort option traffic','ROLE_SDP_RPT_APP_REVENUE_TRAFFIC',8);

INSERT INTO `user_group_roles`(`user_group`, `roles`) VALUES (1,510), (3,510), (5,510), (1,511), (3,511), (5,511), (1,512), (3,512), (5,512);
