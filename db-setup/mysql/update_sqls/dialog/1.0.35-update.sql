DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 285;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 286;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 285;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 286;

INSERT INTO `role` VALUES 
(374,NULL,NULL,0,'Provisioning side menu manage sp item permission','ROLE_PROV_SIDE_MENU_MANAGE_SP',5),
(375,NULL,NULL,0,'Provisioning side menu manage application item permission','ROLE_PROV_SIDE_MENU_MANAGE_APP',5),
(376,NULL,NULL,0,'Provisioning side menu manage build files item permission','ROLE_PROV_SIDE_MENU_MANAGE_BUILD_FILES',5),
(377,NULL,NULL,0,'Provisioning side menu upload build files item permission','ROLE_PROV_SIDE_MENU_UPLOAD_BUILD_FILES',5);

INSERT INTO `user_group_roles` VALUES
(1,374),
(1,375),
(1,376),
(3,377);

UPDATE role SET description='Provisioning side menu register sp item permission', name='ROLE_PROV_SIDE_MENU_REGISTER_SP' WHERE id = 96 AND name='ROLE_PROV_REGISTER_SP';
UPDATE role SET description='Provisioning side menu new sp requests item permission', name='ROLE_PROV_SIDE_MENU_NEW_SP_REQUESTS' WHERE id = 97 AND name='ROLE_PROV_APPROVE_SP';
UPDATE role SET description='Provisioning side menu create application item permission', name='ROLE_PROV_SIDE_MENU_CREATE_APP' WHERE id = 112 AND name='ROLE_PROV_CREATE_APP';
UPDATE role SET description='Provisioning side menu view application item permission', name='ROLE_PROV_SIDE_MENU_VIEW_APP' WHERE id = 113 AND name='ROLE_PROV_VIEW_APP';
UPDATE role SET description='Provisioning side menu new application requests item permission', name='ROLE_PROV_SIDE_MENU_NEW_APP_REQUESTS' WHERE id = 115 AND name='ROLE_PROV_APPROVE_APP';