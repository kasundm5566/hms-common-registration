DELETE FROM `user_group_roles` WHERE roles = 388;
DELETE FROM `user_group_roles` WHERE roles = 389;
DELETE FROM `user_group_roles` WHERE roles = 390;

DELETE FROM `role` WHERE id = 388;
DELETE FROM `role` WHERE id = 389;
DELETE FROM `role` WHERE id = 390;

INSERT INTO `role` VALUES (388,NULL,NULL,0,'Display provisioning in soltura header menu','ROLE_SOL_HEADER_MENU_PROVISIONING',4);
INSERT INTO `role` VALUES (389,NULL,NULL,0,'Display appstore in soltura header menu','ROLE_SOL_HEADER_MENU_APPSTORE',4);
INSERT INTO `role` VALUES (390,NULL,NULL,0,'Display reporting in soltura header menu','ROLE_SOL_HEADER_MENU_REPORTING',4);

INSERT INTO `user_group_roles` VALUES (3,388);
INSERT INTO `user_group_roles` VALUES (3,389);
INSERT INTO `user_group_roles` VALUES (3,390);
