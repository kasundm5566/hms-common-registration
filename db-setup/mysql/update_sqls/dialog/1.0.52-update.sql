use common_admin;
INSERT INTO `role` (`id`, `created_time`, `last_modified_time`, `version`, `description`, `name`, `module`) VALUES
(507, NULL, NULL, 0, 'Appstore admin module login permission.', 'ROLE_APP_ADMIN_LOGIN',6);

INSERT INTO `user_group_roles` (`user_group`, `roles`) VALUES (1, 507);
