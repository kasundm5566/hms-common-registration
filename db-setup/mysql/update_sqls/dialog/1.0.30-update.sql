INSERT INTO `role` VALUES
(307,NULL,NULL,0,'Sms short code select read permission','ROLE_PROV_SMS_READ_SHORT_CODE_SELECT',5),
(308,NULL,NULL,0,'Sms short code select write permission','ROLE_PROV_SMS_WRITE_SHORT_CODE_SELECT',5),
(309,NULL,NULL,0,'Sms keyword read permission','ROLE_PROV_SMS_READ_KEYWORD',5),
(310,NULL,NULL,0,'Sms keyword write permission','ROLE_PROV_SMS_WRITE_KEYWORD',5),
(311,NULL,NULL,0,'Sms connection url read permission','ROLE_PROV_SMS_READ_CONNECTION_URL',5),
(312,NULL,NULL,0,'Sms connection url write permission','ROLE_PROV_SMS_WRITE_CONNECTION_URL',5),
(313,NULL,NULL,0,'Sms mo tps read permission','ROLE_PROV_SMS_READ_MO_TPS',5),
(314,NULL,NULL,0,'Sms mo tps write permission','ROLE_PROV_SMS_WRITE_MO_TPS',5),
(315,NULL,NULL,0,'Sms mo tpd read permission','ROLE_PROV_SMS_READ_MO_TPD',5),
(316,NULL,NULL,0,'Sms mo tpd write permission','ROLE_PROV_SMS_WRITE_MO_TPD',5),
(317,NULL,NULL,0,'Sms mo charging read permission','ROLE_PROV_SMS_READ_MO_CHARGING',5),
(318,NULL,NULL,0,'Sms mo charging write permission','ROLE_PROV_SMS_WRITE_MO_CHARGING',5),
(319,NULL,NULL,0,'Sms default sender address read permission','ROLE_PROV_SMS_READ_DEFAULT_SENDER_ADDRESS',5),
(320,NULL,NULL,0,'Sms default sender address write permission','ROLE_PROV_SMS_WRITE_DEFAULT_SENDER_ADDRESS',5),
(321,NULL,NULL,0,'Sms aliasing read permission','ROLE_PROV_SMS_READ_ALIASING',5),
(322,NULL,NULL,0,'Sms aliasing write permission','ROLE_PROV_SMS_WRITE_ALIASING',5),
(323,NULL,NULL,0,'Sms mt tps read permission','ROLE_PROV_SMS_READ_MT_TPS',5),
(324,NULL,NULL,0,'Sms mt tps write permission','ROLE_PROV_SMS_WRITE_MT_TPS',5),
(325,NULL,NULL,0,'Sms mt tpd read permission','ROLE_PROV_SMS_READ_MT_TPD',5),
(326,NULL,NULL,0,'Sms mt tpd write permission','ROLE_PROV_SMS_WRITE_MT_TPD',5),
(327,NULL,NULL,0,'Sms delivery report read permission','ROLE_PROV_SMS_READ_DELIVERY_REPORT',5),
(328,NULL,NULL,0,'Sms delivery report write permission','ROLE_PROV_SMS_WRITE_DELIVERY_REPORT',5),
(329,NULL,NULL,0,'Sms delivery report url read permission','ROLE_PROV_SMS_READ_DELIVERY_REPORT_URL',5),
(330,NULL,NULL,0,'Sms delivery report url write permission','ROLE_PROV_SMS_WRITE_DELIVERY_REPORT_URL',5),
(331,NULL,NULL,0,'Sms mt charging read permission','ROLE_PROV_SMS_READ_MT_CHARGING',5),
(332,NULL,NULL,0,'Sms mt charging write permission','ROLE_PROV_SMS_WRITE_MT_CHARGING',5),
(333,NULL,NULL,0,'Sms subscription required read permission','ROLE_PROV_SMS_READ_SUBSCRIPTION_REQUIRED',5),
(334,NULL,NULL,0,'Sms subscription required write permission','ROLE_PROV_SMS_WRITE_SUBSCRIPTION_REQUIRED',5),
(335,NULL,NULL,0,'Common sdp short code view permission','ROLE_PROV_COMMON_SDP_SHORT_CODE',5),
(336,NULL,NULL,0,'Common soltura short code view permission','ROLE_PROV_COMMON_SOLTURA_SHORT_CODE',5),
(337,NULL,NULL,0,'Common premium short code view permission','ROLE_PROV_COMMON_PREMIUM_SHORT_CODE',5),
(338,NULL,NULL,0,'Common home url view permission','ROLE_PROV_COMMON_HOME_URL',5),
(339,NULL,NULL,0,'Common registration url view permission','ROLE_PROV_COMMON_REGISTRATION_URL',5),
(340,NULL,NULL,0,'Common soltura url view permission','ROLE_PROV_COMMON_SOLTURA_URL',5),
(341,NULL,NULL,0,'Common reporting url view permission','ROLE_PROV_COMMON_REPORTING_URL',5),
(342,NULL,NULL,0,'Common admin url view permission','ROLE_PROV_COMMON_ADMIN_URL',5),
(343,NULL,NULL,0,'Common consumer self care url view permission','ROLE_PROV_COMMON_CONSUMER_SELF_CARE_URL',5),
(344,NULL,NULL,0,'Common admin self care url view permission','ROLE_PROV_COMMON_ADMIN_SELF_CARE_URL',5),
(345,NULL,NULL,0,'Common appstore url view permission','ROLE_PROV_COMMON_APPSTORE_URL',5),
(346,NULL,NULL,0,'Ussd short code select read permission','ROLE_PROV_USSD_READ_SHORT_CODE_SELECT',5),
(347,NULL,NULL,0,'Ussd short code select write permission','ROLE_PROV_USSD_WRITE_SHORT_CODE_SELECT',5),
(348,NULL,NULL,0,'Ussd keyword read permission','ROLE_PROV_USSD_READ_KEYWORD',5),
(349,NULL,NULL,0,'Ussd keyword write permission','ROLE_PROV_USSD_WRITE_KEYWORD',5),
(350,NULL,NULL,0,'Ussd connection url read permission','ROLE_PROV_USSD_READ_CONNECTION_URL',5),
(351,NULL,NULL,0,'Ussd connection url write permission','ROLE_PROV_USSD_WRITE_CONNECTION_URL',5),
(352,NULL,NULL,0,'Ussd mo tps read permission','ROLE_PROV_USSD_READ_MO_TPS',5),
(353,NULL,NULL,0,'Ussd mo tps write permission','ROLE_PROV_USSD_WRITE_MO_TPS',5),
(354,NULL,NULL,0,'Ussd mo tpd read permission','ROLE_PROV_USSD_READ_MO_TPD',5),
(355,NULL,NULL,0,'Ussd mo tpd write permission','ROLE_PROV_USSD_WRITE_MO_TPD',5),
(356,NULL,NULL,0,'Ussd mo charging read permission','ROLE_PROV_USSD_READ_MO_CHARGING',5),
(357,NULL,NULL,0,'Ussd mo charging write permission','ROLE_PROV_USSD_WRITE_MO_CHARGING',5),
(358,NULL,NULL,0,'Ussd subscription required read permission','ROLE_PROV_USSD_READ_SUBSCRIPTION_REQUIRED',5),
(359,NULL,NULL,0,'Ussd subscription required write permission','ROLE_PROV_USSD_WRITE_SUBSCRIPTION_REQUIRED',5);

INSERT INTO `user_group_roles` VALUES
(1,307),
(1,308),
(1,309),
(1,310),
(1,311),
(1,312),
(1,313),
(1,314),
(1,315),
(1,316),
(1,317),
(1,318),
(1,319),
(1,320),
(1,321),
(1,322),
(1,323),
(1,324),
(1,325),
(1,326),
(1,327),
(1,328),
(1,329),
(1,330),
(1,331),
(1,332),
(1,333),
(1,334),
(1,335),
(1,336),
(1,337),
(1,338),
(1,339),
(1,341),
(1,342),
(1,346),
(1,347),
(1,348),
(1,349),
(1,350),
(1,351),
(1,352),
(1,353),
(1,354),
(1,355),
(1,356),
(1,357),
(1,358),
(1,359),
(3,307),
(3,308),
(3,309),
(3,310),
(3,311),
(3,312),
(3,313),
(3,315),
(3,317),
(3,318),
(3,319),
(3,320),
(3,321),
(3,322),
(3,323),
(3,325),
(3,327),
(3,328),
(3,329),
(3,330),
(3,331),
(3,332),
(3,333),
(3,334),
(3,335),
(3,338),
(3,341),
(3,346),
(3,347),
(3,348),
(3,349),
(3,350),
(3,351),
(3,352),
(3,354),
(3,356),
(3,357),
(3,358),
(3,359);

/*Reporting related roles*/
INSERT INTO `role` VALUES (173,NULL,NULL,0,'ncs wise report viewer','ROLE_SDP_RPT_NCS_REV_TRAFF',8);
INSERT INTO `role` VALUES (175,NULL,NULL,0,'add wise report viewer','ROLE_SDP_RPT_ADV_AD_WISE',8);
INSERT INTO `role` VALUES (176,NULL,NULL,0,'app wise add report viewer','ROLE_SDP_RPT_APP_WISE',8);
INSERT INTO `role` VALUES (183,NULL,NULL,0,'sdp reconciliation report viewer','ROLE_SDP_RPT_REC_REP',8);
INSERT INTO `role` VALUES (184,NULL,NULL,0,'sdp governance report viewer','ROLE_SDP_RPT_GOV_REP',8);

INSERT INTO `user_group_roles` VALUES (1,173);
INSERT INTO `user_group_roles` VALUES (1,175);
INSERT INTO `user_group_roles` VALUES (1,176);
INSERT INTO `user_group_roles` VALUES (1,183);
INSERT INTO `user_group_roles` VALUES (1,184);
