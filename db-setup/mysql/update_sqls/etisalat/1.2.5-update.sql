use common_admin;
-- insert roles
INSERT INTO `role` VALUES
(267,NULL,NULL,0,'Prov main masking read permission','ROLE_PROV_APP_READ_MASKING',5),
(268,NULL,NULL,0,'Prov main masking write permission','ROLE_PROV_APP_WRITE_MASKING',5);

INSERT INTO `user_group_roles` VALUES
(1,267),
(1,268),
(3,267);

