USE common_admin;

INSERT INTO `role` VALUES (510,NULL,NULL,0,'sdp app report sort option traffic','ROLE_SDP_RPT_APP_REVENUE_TRAFFIC',8),
                          (511,NULL,NULL,0,'sdp app wise report viewer','ROLE_SDP_RPT_APP_REV_TRAFF',8);

INSERT INTO `user_group_roles`(`user_group`, `roles`) VALUES (1,510), (3,510), (1,511), (3,511);
