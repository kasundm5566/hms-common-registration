
/**  Update data */

USE common_admin;


/*remove un-necessary role for sdp-admin */
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((1,183),(1,184),(1,175),(1,171),(1,176),(1,363),(1,381),(1,361),(1,469)) ;

/*remove un-necessary role for customer-care */
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((9,469),(9,361)) ;

/*add necessary role for customer-care */
INSERT INTO `user_group_roles` VALUES (9,174);

/*remove un-necessary role for Corporate */
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((3,175),(3,171),(3,176),(3,469),(3,180),(3,181),(3,170),(3,174),(3,184),(3,361),(3,381),(3,360),(3,362),(3,363),(3,168),(3,173),(3,169),(3,183)) ;




/*remove modules tab*/
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((1,21),(2,21));

/*remove un-necessary user group roles*/
DELETE FROM `user_group_roles` WHERE user_group in (4,5,6,7,8,10);

/*remove unnecessary user groups */
DELETE FROM `user_group` WHERE id IN (4,5,6,7,8,10);

/*remove add user groups*/
DELETE FROM user_group_roles WHERE (user_group,roles) IN ((1,13),(2,13));

/*add separate role for Add user functionality*/
INSERT INTO `role` VALUES (509,NULL,NULL,0,'Add users','ROLE_REG_ADD_USER',1);

/*add new role to add user to user groups*/
INSERT INTO `user_group_roles` VALUES (1,509),(2,509);




/*remove un-necessary role for sdp-admin */
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((1,183),(1,184),(1,175),(1,171),(1,176),(1,363),(1,381),(1,361),(1,469),(1,169),(1,174),(1,181));

/*remove un-necessary role for customer-care */
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((9,469),(9,361),(9,174),(9,181));

/*remove un-necessary role for Corporate */
DELETE FROM `user_group_roles` WHERE (user_group,roles) IN ((3,175),(3,171),(3,176),(3,469),(3,180),(3,181),(3,170),(3,174),(3,184),(3,361),(3,381),(3,360),(3,362),(3,363),(3,168),(3,173),(3,169),(3,183));




/*remove and re-establish rptuser as a customer care user*/
DELETE FROM `user` where username='rptuser';

INSERT INTO `user` VALUES ('User', 135, '2013-10-25 11:19:52', '2013-10-25 11:33:37', 7, 'sri lanka', Null, Null, false, Null, Null, Null, 'internal', Null, 'ReportUser@hnb.lk', Null, false, true, 'ReportUser', NULL, Null, Null, Null, 'hnb', Null, Null, false, Null, Null, Null, Null, false, Null, '44bc7b417faef5c8125581ebaf8b8e1e', Null, Null, Null, Null, 'Employed', Null, '20131025111900609', 'ACTIVE', 'ADMIN_USER', 'rptuser', Null, Null, Null, Null, Null,  Null, Null, Null, Null, 9, Null, Null, null, Null, Null, false, 0, 0, 0, false);

