/* give prov permission to sp */
insert into user_group_roles (user_group,roles) values (3,95);

/* remove top sdp reporting link for sp from soltura*/
delete from user_group_roles where user_group=3 and roles=390;

/* give sp prov revenue share read permission*/
insert into user_group_roles (user_group,roles) values (3,249);

/* give sp edit permission for ussd resource tps tpd */
insert into user_group_roles (user_group,roles) values (3,353),(3,355);
