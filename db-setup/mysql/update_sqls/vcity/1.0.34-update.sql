DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 285;
DELETE FROM `user_group_roles` WHERE user_group = 1 AND roles = 286;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 285;
DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 286;

INSERT INTO `role` VALUES
(364,NULL,NULL,0,'Provisioning edit application action permission','ROLE_PROV_APP_ACTION_EDIT',5),
(365,NULL,NULL,0,'Provisioning delete application action permission','ROLE_PROV_APP_ACTION_DELETE',5),
(366,NULL,NULL,0,'Provisioning move to draft application action permission','ROLE_PROV_APP_ACTION_MOVE_TO_DRAFT',5),
(367,NULL,NULL,0,'Provisioning suspend application action permission','ROLE_PROV_APP_ACTION_SUSPEND',5),
(368,NULL,NULL,0,'Provisioning approve application action permission','ROLE_PROV_APP_ACTION_APPROVE',5),
(369,NULL,NULL,0,'Provisioning reject application action permission','ROLE_PROV_APP_ACTION_REJECT',5),
(370,NULL,NULL,0,'Provisioning restore application action permission','ROLE_PROV_APP_ACTION_RESTORE',5),
(371,NULL,NULL,0,'Provisioning terminate application action permission','ROLE_PROV_APP_ACTION_TERMINATE',5),
(372,NULL,NULL,0,'Provisioning request active production application action permission','ROLE_PROV_APP_ACTION_REQUEST_ACTIVE_PRODUCTION',5),
(373,NULL,NULL,0,'Provisioning request limited production application action permission','ROLE_PROV_APP_ACTION_REQUEST_REQUEST_LIMITED_PRODUCTION',5);

INSERT INTO `user_group_roles` VALUES
(1,364),
(1,365),
(1,367),
(1,368),
(1,369),
(1,370),
(1,371),
(3,364),
(3,365);