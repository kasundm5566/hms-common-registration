DELETE FROM `user_group_roles` WHERE roles = 195;
DELETE FROM `user_group_roles` WHERE roles = 196;
DELETE FROM `user_group_roles` WHERE roles = 197;
DELETE FROM `user_group_roles` WHERE roles = 198;
DELETE FROM `user_group_roles` WHERE roles = 285;
DELETE FROM `user_group_roles` WHERE roles = 286;

DELETE FROM `role` WHERE id = 195;
DELETE FROM `role` WHERE id = 196;
DELETE FROM `role` WHERE id = 197;
DELETE FROM `role` WHERE id = 198;
DELETE FROM `role` WHERE id = 285;
DELETE FROM `role` WHERE id = 286;

DELETE FROM `user_group_roles` WHERE user_group = 3 AND roles = 300;

INSERT INTO `role` VALUES (388,NULL,NULL,0,'Display provisioning in soltura header menu','ROLE_SOL_HEADER_MENU_PROVISIONING',8);
INSERT INTO `role` VALUES (389,NULL,NULL,0,'Display appstore in soltura header menu','ROLE_SOL_HEADER_MENU_APPSTORE',8);
INSERT INTO `role` VALUES (390,NULL,NULL,0,'Display reporting in soltura header menu','ROLE_SOL_HEADER_MENU_REPORTING',8);

INSERT INTO `user_group_roles` VALUES (3,372);
INSERT INTO `user_group_roles` VALUES (3,388);
INSERT INTO `user_group_roles` VALUES (3,389);
INSERT INTO `user_group_roles` VALUES (3,390);
