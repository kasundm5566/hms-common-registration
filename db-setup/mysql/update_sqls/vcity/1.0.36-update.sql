INSERT INTO `role` VALUES
(380,NULL,NULL,0,'Provisioning side menu new application requests item permission','ROLE_PROV_SIDE_MENU_NEW_APP_REQUESTS',5);

INSERT INTO `user_group_roles` VALUES
(1,380);

UPDATE role SET description='Approve app', name='ROLE_PROV_APPROVE_APP' WHERE id = 115 AND name='ROLE_PROV_SIDE_MENU_NEW_APP_REQUESTS';