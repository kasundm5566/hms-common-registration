use common_admin;

DELETE FROM `user_group_roles` WHERE roles = 101;

UPDATE role SET description='Provisioning Approve SP', name='ROLE_PROV_SP_ACTION_APPROVE' WHERE id = 107 AND name='ROLE_PROV_ACTIVATE_SP';
UPDATE role SET description='Provisioning Suspend SP', name='ROLE_PROV_SP_ACTION_SUSPEND' WHERE id = 106 AND name='ROLE_PROV_SUSPEND_SP';
UPDATE role SET description='Provisioning Reject SP', name='ROLE_PROV_SP_ACTION_REJECT' WHERE id = 104 AND name='ROLE_PROV_REJECT_SP_REQ';
UPDATE role SET description='Provisioning Edit SP', name='ROLE_PROV_SP_ACTION_EDIT' WHERE id = 101 AND name='ROLE_PROV_EDIT_SP';
UPDATE role SET description='Provisioning Enable SP', name='ROLE_PROV_SP_ACTION_ENABLE' WHERE id = 98 AND name='ROLE_PROV_VIEW_SP';

INSERT INTO `user_group_roles` VALUES (1,101);


