USE common_admin;

DELETE FROM `user_group_roles` WHERE roles = 400;
DELETE FROM `user_group_roles` WHERE roles = 401;
DELETE FROM `user_group_roles` WHERE roles = 402;
DELETE FROM `user_group_roles` WHERE roles = 403;
DELETE FROM `user_group_roles` WHERE roles = 404;
DELETE FROM `user_group_roles` WHERE roles = 405;

DELETE FROM `role` WHERE id = 400;
DELETE FROM `role` WHERE id = 401;
DELETE FROM `role` WHERE id = 402;
DELETE FROM `role` WHERE id = 403;
DELETE FROM `role` WHERE id = 404;
DELETE FROM `role` WHERE id = 405;

DELETE FROM `user_group_roles` WHERE roles = 356;
DELETE FROM `user_group_roles` WHERE roles = 357;

DELETE FROM `role` WHERE id = 356;
DELETE FROM `role` WHERE id = 357;

INSERT INTO `role` VALUES (356,NULL,NULL,0,'Ussd mo charging allowed read permission','ROLE_PROV_USSD_READ_MO_CHARGING_ALLOWED',5);
INSERT INTO `role` VALUES (357,NULL,NULL,0,'Ussd mo charging allowed write permission','ROLE_PROV_USSD_WRITE_MO_CHARGING_ALLOWED',5);

INSERT INTO `user_group_roles` VALUES (1,356);
INSERT INTO `user_group_roles` VALUES (1,357);

INSERT INTO `user_group_roles` VALUES (3,356);
INSERT INTO `user_group_roles` VALUES (3,357);

INSERT INTO `role` VALUES (391,NULL,NULL,0,'Ussd mt charging allowed read permission','ROLE_PROV_USSD_READ_MT_CHARGING_ALLOWED',5);
INSERT INTO `role` VALUES (392,NULL,NULL,0,'Ussd mt charging allowed write permission','ROLE_PROV_USSD_WRITE_MT_CHARGING_ALLOWED',5);
INSERT INTO `role` VALUES (393,NULL,NULL,0,'Ussd session charging allowed read permission','ROLE_PROV_USSD_READ_SESSION_CHARGING_ALLOWED',5);
INSERT INTO `role` VALUES (394,NULL,NULL,0,'Ussd session charging allowed write permission','ROLE_PROV_USSD_WRITE_SESSION_CHARGING_ALLOWED',5);

INSERT INTO `user_group_roles` VALUES (1,391);
INSERT INTO `user_group_roles` VALUES (1,392);
INSERT INTO `user_group_roles` VALUES (1,393);
INSERT INTO `user_group_roles` VALUES (1,394);

INSERT INTO `user_group_roles` VALUES (3,391);
INSERT INTO `user_group_roles` VALUES (3,392);
INSERT INTO `user_group_roles` VALUES (3,393);
INSERT INTO `user_group_roles` VALUES (3,394);

INSERT INTO `role` VALUES (395,NULL,NULL,0,'Ussd mo charging type read permission','ROLE_PROV_USSD_MO_CHARGING_READ_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (396,NULL,NULL,0,'Ussd mo charging type write permission','ROLE_PROV_USSD_MO_CHARGING_WRITE_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (397,NULL,NULL,0,'Ussd mo charging party read permission','ROLE_PROV_USSD_MO_CHARGING_READ_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (398,NULL,NULL,0,'Ussd mo charging party write permission','ROLE_PROV_USSD_MO_CHARGING_WRITE_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (399,NULL,NULL,0,'Ussd mo allowed payment instrument read permission','ROLE_PROV_USSD_MO_CHARGING_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (400,NULL,NULL,0,'Ussd mo allowed payment instrument write permission','ROLE_PROV_USSD_MO_CHARGING_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (401,NULL,NULL,0,'Ussd mo charging amount read permission','ROLE_PROV_USSD_MO_CHARGING_READ_CHARGING_AMOUNT',5);
INSERT INTO `role` VALUES (402,NULL,NULL,0,'Ussd mo charging amount write permission','ROLE_PROV_USSD_MO_CHARGING_WRITE_CHARGING_AMOUNT',5);

INSERT INTO `user_group_roles` VALUES (1,395);
INSERT INTO `user_group_roles` VALUES (1,396);
INSERT INTO `user_group_roles` VALUES (1,397);
INSERT INTO `user_group_roles` VALUES (1,398);
INSERT INTO `user_group_roles` VALUES (1,399);
INSERT INTO `user_group_roles` VALUES (1,400);
INSERT INTO `user_group_roles` VALUES (1,401);
INSERT INTO `user_group_roles` VALUES (1,402);

INSERT INTO `user_group_roles` VALUES (3,395);
INSERT INTO `user_group_roles` VALUES (3,396);
INSERT INTO `user_group_roles` VALUES (3,397);
INSERT INTO `user_group_roles` VALUES (3,398);
INSERT INTO `user_group_roles` VALUES (3,399);
INSERT INTO `user_group_roles` VALUES (3,400);
INSERT INTO `user_group_roles` VALUES (3,401);
INSERT INTO `user_group_roles` VALUES (3,402);

INSERT INTO `role` VALUES (403,NULL,NULL,0,'Ussd mt charging type read permission','ROLE_PROV_USSD_MT_CHARGING_READ_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (404,NULL,NULL,0,'Ussd mt charging type write permission','ROLE_PROV_USSD_MT_CHARGING_WRITE_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (405,NULL,NULL,0,'Ussd mt charging party read permission','ROLE_PROV_USSD_MT_CHARGING_READ_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (406,NULL,NULL,0,'Ussd mt charging party write permission','ROLE_PROV_USSD_MT_CHARGING_WRITE_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (407,NULL,NULL,0,'Ussd mt allowed payment instrument read permission','ROLE_PROV_USSD_MT_CHARGING_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (408,NULL,NULL,0,'Ussd mt allowed payment instrument write permission','ROLE_PROV_USSD_MT_CHARGING_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (409,NULL,NULL,0,'Ussd mt charging amount read permission','ROLE_PROV_USSD_MT_CHARGING_READ_CHARGING_AMOUNT',5);
INSERT INTO `role` VALUES (410,NULL,NULL,0,'Ussd mt charging amount write permission','ROLE_PROV_USSD_MT_CHARGING_WRITE_CHARGING_AMOUNT',5);

INSERT INTO `user_group_roles` VALUES (1,403);
INSERT INTO `user_group_roles` VALUES (1,404);
INSERT INTO `user_group_roles` VALUES (1,405);
INSERT INTO `user_group_roles` VALUES (1,406);
INSERT INTO `user_group_roles` VALUES (1,407);
INSERT INTO `user_group_roles` VALUES (1,408);
INSERT INTO `user_group_roles` VALUES (1,409);
INSERT INTO `user_group_roles` VALUES (1,410);

INSERT INTO `user_group_roles` VALUES (3,403);
INSERT INTO `user_group_roles` VALUES (3,404);
INSERT INTO `user_group_roles` VALUES (3,405);
INSERT INTO `user_group_roles` VALUES (3,406);
INSERT INTO `user_group_roles` VALUES (3,407);
INSERT INTO `user_group_roles` VALUES (3,408);
INSERT INTO `user_group_roles` VALUES (3,409);
INSERT INTO `user_group_roles` VALUES (3,410);

INSERT INTO `role` VALUES (411,NULL,NULL,0,'Ussd session charging type read permission','ROLE_PROV_USSD_SESSION_CHARGING_READ_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (412,NULL,NULL,0,'Ussd session charging type write permission','ROLE_PROV_USSD_SESSION_CHARGING_WRITE_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (413,NULL,NULL,0,'Ussd session charging party read permission','ROLE_PROV_USSD_SESSION_CHARGING_READ_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (414,NULL,NULL,0,'Ussd session charging party write permission','ROLE_PROV_USSD_SESSION_CHARGING_WRITE_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (415,NULL,NULL,0,'Ussd session allowed payment instrument read permission','ROLE_PROV_USSD_SESSION_CHARGING_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (416,NULL,NULL,0,'Ussd session allowed payment instrument write permission','ROLE_PROV_USSD_SESSION_CHARGING_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (417,NULL,NULL,0,'Ussd session charging amount read permission','ROLE_PROV_USSD_SESSION_CHARGING_READ_CHARGING_AMOUNT',5);
INSERT INTO `role` VALUES (418,NULL,NULL,0,'Ussd session charging amount write permission','ROLE_PROV_USSD_SESSION_CHARGING_WRITE_CHARGING_AMOUNT',5);

INSERT INTO `user_group_roles` VALUES (1,411);
INSERT INTO `user_group_roles` VALUES (1,412);
INSERT INTO `user_group_roles` VALUES (1,413);
INSERT INTO `user_group_roles` VALUES (1,414);
INSERT INTO `user_group_roles` VALUES (1,415);
INSERT INTO `user_group_roles` VALUES (1,416);
INSERT INTO `user_group_roles` VALUES (1,417);
INSERT INTO `user_group_roles` VALUES (1,418);

INSERT INTO `user_group_roles` VALUES (3,411);
INSERT INTO `user_group_roles` VALUES (3,412);
INSERT INTO `user_group_roles` VALUES (3,413);
INSERT INTO `user_group_roles` VALUES (3,414);
INSERT INTO `user_group_roles` VALUES (3,415);
INSERT INTO `user_group_roles` VALUES (3,416);
INSERT INTO `user_group_roles` VALUES (3,417);
INSERT INTO `user_group_roles` VALUES (3,418);

INSERT INTO `role` VALUES (419,NULL,NULL,0,'Caas allowed payment instrument read permission','ROLE_PROV_CAAS_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (420,NULL,NULL,0,'Caas allowed payment instrument write permission','ROLE_PROV_CAAS_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);

INSERT INTO `user_group_roles` VALUES (1,419);
INSERT INTO `user_group_roles` VALUES (1,420);

INSERT INTO `user_group_roles` VALUES (3,419);
INSERT INTO `user_group_roles` VALUES (3,420);

INSERT INTO `role` VALUES (421,NULL,NULL,0,'Downloadable allowed payment instrument download write permission','ROLE_PROV_DOWNLOADABLE_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (422,NULL,NULL,0,'Downloadable allowed payment instrument download read permission','ROLE_PROV_DOWNLOADABLE_READ_ALLOWED_PAYMENT_INSTRUMENT',5);

INSERT INTO `user_group_roles` VALUES (1,421);
INSERT INTO `user_group_roles` VALUES (1,422);

INSERT INTO `user_group_roles` VALUES (3,421);
INSERT INTO `user_group_roles` VALUES (3,422);

INSERT INTO `role` VALUES (423,NULL,NULL,0,'Subscription allowed payment instrument read permission','ROLE_PROV_SUBSCRIPTION_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (424,NULL,NULL,0,'Subscription allowed payment instrument write permission','ROLE_PROV_SUBSCRIPTION_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);

INSERT INTO `user_group_roles` VALUES (1,423);
INSERT INTO `user_group_roles` VALUES (1,424);

INSERT INTO `user_group_roles` VALUES (3,423);
INSERT INTO `user_group_roles` VALUES (3,424);

DELETE FROM `user_group_roles` WHERE roles = 317;
DELETE FROM `user_group_roles` WHERE roles = 318;
DELETE FROM `user_group_roles` WHERE roles = 331;
DELETE FROM `user_group_roles` WHERE roles = 332;

DELETE FROM `role` WHERE id = 317;
DELETE FROM `role` WHERE id = 318;
DELETE FROM `role` WHERE id = 331;
DELETE FROM `role` WHERE id = 332;

INSERT INTO `role` VALUES (317,NULL,NULL,0,'Sms mo charging type read permission','ROLE_PROV_SMS_MO_CHARGING_READ_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (318,NULL,NULL,0,'Sms mo charging type write permission','ROLE_PROV_SMS_MO_CHARGING_WRITE_CHARGING_TYPE',5);

INSERT INTO `role` VALUES (425,NULL,NULL,0,'Sms mo charging party read permission','ROLE_PROV_SMS_MO_CHARGING_READ_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (426,NULL,NULL,0,'Sms mo charging party write permission','ROLE_PROV_SMS_MO_CHARGING_WRITE_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (427,NULL,NULL,0,'Sms mo allowed payment instrument read permission','ROLE_PROV_SMS_MO_CHARGING_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (428,NULL,NULL,0,'Sms mo allowed payment instrument write permission','ROLE_PROV_SMS_MO_CHARGING_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (429,NULL,NULL,0,'Sms mo charging amount read permission','ROLE_PROV_SMS_MO_CHARGING_READ_CHARGING_AMOUNT',5);
INSERT INTO `role` VALUES (430,NULL,NULL,0,'Sms mo charging amount write permission','ROLE_PROV_SMS_MO_CHARGING_WRITE_CHARGING_AMOUNT',5);

INSERT INTO `user_group_roles` VALUES (1,317);
INSERT INTO `user_group_roles` VALUES (1,318);
INSERT INTO `user_group_roles` VALUES (1,425);
INSERT INTO `user_group_roles` VALUES (1,426);
INSERT INTO `user_group_roles` VALUES (1,427);
INSERT INTO `user_group_roles` VALUES (1,428);
INSERT INTO `user_group_roles` VALUES (1,429);
INSERT INTO `user_group_roles` VALUES (1,430);

INSERT INTO `user_group_roles` VALUES (3,317);
INSERT INTO `user_group_roles` VALUES (3,318);
INSERT INTO `user_group_roles` VALUES (3,425);
INSERT INTO `user_group_roles` VALUES (3,426);
INSERT INTO `user_group_roles` VALUES (3,427);
INSERT INTO `user_group_roles` VALUES (3,428);
INSERT INTO `user_group_roles` VALUES (3,429);
INSERT INTO `user_group_roles` VALUES (3,430);

INSERT INTO `role` VALUES (331,NULL,NULL,0,'Sms mt charging type read permission','ROLE_PROV_SMS_MT_CHARGING_READ_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (332,NULL,NULL,0,'Sms mt charging type write permission','ROLE_PROV_SMS_MT_CHARGING_WRITE_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (431,NULL,NULL,0,'Sms mt charging party read permission','ROLE_PROV_SMS_MT_CHARGING_READ_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (432,NULL,NULL,0,'Sms mt charging party write permission','ROLE_PROV_SMS_MT_CHARGING_WRITE_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (433,NULL,NULL,0,'Sms mt allowed payment instrument read permission','ROLE_PROV_SMS_MT_CHARGING_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (434,NULL,NULL,0,'Sms mt allowed payment instrument write permission','ROLE_PROV_SMS_MT_CHARGING_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (435,NULL,NULL,0,'Sms mt charging amount read permission','ROLE_PROV_SMS_MT_CHARGING_READ_CHARGING_AMOUNT',5);
INSERT INTO `role` VALUES (436,NULL,NULL,0,'Sms mt charging amount write permission','ROLE_PROV_SMS_MT_CHARGING_WRITE_CHARGING_AMOUNT',5);

INSERT INTO `user_group_roles` VALUES (1,331);
INSERT INTO `user_group_roles` VALUES (1,332);
INSERT INTO `user_group_roles` VALUES (1,431);
INSERT INTO `user_group_roles` VALUES (1,432);
INSERT INTO `user_group_roles` VALUES (1,433);
INSERT INTO `user_group_roles` VALUES (1,434);
INSERT INTO `user_group_roles` VALUES (1,435);
INSERT INTO `user_group_roles` VALUES (1,436);

INSERT INTO `user_group_roles` VALUES (3,331);
INSERT INTO `user_group_roles` VALUES (3,332);
INSERT INTO `user_group_roles` VALUES (3,431);
INSERT INTO `user_group_roles` VALUES (3,432);
INSERT INTO `user_group_roles` VALUES (3,433);
INSERT INTO `user_group_roles` VALUES (3,434);
INSERT INTO `user_group_roles` VALUES (3,435);
INSERT INTO `user_group_roles` VALUES (3,436);

INSERT INTO `role` VALUES (437,NULL,NULL,0,'Wap push default sender address read permission','ROLE_PROV_WAP_PUSH_READ_DEFAULT_SENDER_ADDRESS',5);
INSERT INTO `role` VALUES (438,NULL,NULL,0,'Wap push default sender address write permission','ROLE_PROV_WAP_PUSH_WRITE_DEFAULT_SENDER_ADDRESS',5);
INSERT INTO `role` VALUES (439,NULL,NULL,0,'Wap push aliasing read permission','ROLE_PROV_WAP_PUSH_READ_ALIASING',5);
INSERT INTO `role` VALUES (440,NULL,NULL,0,'Wap push aliasing write permission','ROLE_PROV_WAP_PUSH_WRITE_ALIASING',5);
INSERT INTO `role` VALUES (441,NULL,NULL,0,'Wap push mt tps read permission','ROLE_PROV_WAP_PUSH_READ_MT_TPS',5);
INSERT INTO `role` VALUES (442,NULL,NULL,0,'Wap push mt tps write permission','ROLE_PROV_WAP_PUSH_WRITE_MT_TPS',5);
INSERT INTO `role` VALUES (443,NULL,NULL,0,'Wap push mt tpd read permission','ROLE_PROV_WAP_PUSH_READ_MT_TPD',5);
INSERT INTO `role` VALUES (444,NULL,NULL,0,'Wap push mt tpd write permission','ROLE_PROV_WAP_PUSH_WRITE_MT_TPD',5);
INSERT INTO `role` VALUES (445,NULL,NULL,0,'Wap push delivery report read permission','ROLE_PROV_WAP_PUSH_READ_DELIVERY_REPORT',5);
INSERT INTO `role` VALUES (446,NULL,NULL,0,'Wap push delivery report write permission','ROLE_PROV_WAP_PUSH_WRITE_DELIVERY_REPORT',5);
INSERT INTO `role` VALUES (447,NULL,NULL,0,'Wap push delivery report url read permission','ROLE_PROV_WAP_PUSH_READ_DELIVERY_REPORT_URL',5);
INSERT INTO `role` VALUES (448,NULL,NULL,0,'Wap push delivery report url write permission','ROLE_PROV_WAP_PUSH_WRITE_DELIVERY_REPORT_URL',5);

INSERT INTO `user_group_roles` VALUES (1,437);
INSERT INTO `user_group_roles` VALUES (1,438);
INSERT INTO `user_group_roles` VALUES (1,439);
INSERT INTO `user_group_roles` VALUES (1,440);
INSERT INTO `user_group_roles` VALUES (1,441);
INSERT INTO `user_group_roles` VALUES (1,442);
INSERT INTO `user_group_roles` VALUES (1,443);
INSERT INTO `user_group_roles` VALUES (1,444);
INSERT INTO `user_group_roles` VALUES (1,445);
INSERT INTO `user_group_roles` VALUES (1,446);
INSERT INTO `user_group_roles` VALUES (1,447);
INSERT INTO `user_group_roles` VALUES (1,448);

INSERT INTO `user_group_roles` VALUES (3,437);
INSERT INTO `user_group_roles` VALUES (3,438);
INSERT INTO `user_group_roles` VALUES (3,439);
INSERT INTO `user_group_roles` VALUES (3,440);
INSERT INTO `user_group_roles` VALUES (3,441);
INSERT INTO `user_group_roles` VALUES (3,442);
INSERT INTO `user_group_roles` VALUES (3,443);
INSERT INTO `user_group_roles` VALUES (3,444);
INSERT INTO `user_group_roles` VALUES (3,445);
INSERT INTO `user_group_roles` VALUES (3,446);
INSERT INTO `user_group_roles` VALUES (3,447);
INSERT INTO `user_group_roles` VALUES (3,448);

INSERT INTO `role` VALUES (449,NULL,NULL,0,'Wap push mt charging type read permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_READ_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (450,NULL,NULL,0,'Wap push mt charging type write permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_WRITE_CHARGING_TYPE',5);
INSERT INTO `role` VALUES (451,NULL,NULL,0,'Wap push mt charging party read permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_READ_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (452,NULL,NULL,0,'Wap push mt charging party write permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_WRITE_CHARGING_PARTY',5);
INSERT INTO `role` VALUES (453,NULL,NULL,0,'Wap push mt allowed payment instrument read permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_READ_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (454,NULL,NULL,0,'Wap push mt allowed payment instrument write permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_WRITE_ALLOWED_PAYMENT_INSTRUMENT',5);
INSERT INTO `role` VALUES (455,NULL,NULL,0,'Wap push mt charging amount read permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_READ_CHARGING_AMOUNT',5);
INSERT INTO `role` VALUES (456,NULL,NULL,0,'Wap push mt charging amount write permission','ROLE_PROV_WAP_PUSH_MT_CHARGING_WRITE_CHARGING_AMOUNT',5);

INSERT INTO `user_group_roles` VALUES (1,449);
INSERT INTO `user_group_roles` VALUES (1,450);
INSERT INTO `user_group_roles` VALUES (1,451);
INSERT INTO `user_group_roles` VALUES (1,452);
INSERT INTO `user_group_roles` VALUES (1,453);
INSERT INTO `user_group_roles` VALUES (1,454);
INSERT INTO `user_group_roles` VALUES (1,455);
INSERT INTO `user_group_roles` VALUES (1,456);

INSERT INTO `user_group_roles` VALUES (3,449);
INSERT INTO `user_group_roles` VALUES (3,450);
INSERT INTO `user_group_roles` VALUES (3,451);
INSERT INTO `user_group_roles` VALUES (3,452);
INSERT INTO `user_group_roles` VALUES (3,453);
INSERT INTO `user_group_roles` VALUES (3,454);
INSERT INTO `user_group_roles` VALUES (3,455);
INSERT INTO `user_group_roles` VALUES (3,456);