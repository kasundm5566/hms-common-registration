INSERT INTO `role` VALUES
(601,NULL,NULL,0,'Caas in app allowed read permission','ROLE_PROV_CAAS_READ_IN_APP_ALLOWED',5),
(602,NULL,NULL,0,'Caas in app allowed required write permission','ROLE_PROV_CAAS_WRITE_IN_APP_ALLOWED',5);

INSERT INTO `user_group_roles` VALUES
(3, 601),
(3, 602),
(1, 601),
(1, 602);