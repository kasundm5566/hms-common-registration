-- add admin roles to customer care user
insert into user_group_roles values(6,147),(6,148),(6,149);

-- removing roles from corporate user for reporting
delete from user_group_roles where user_group=3 and roles=180;
delete from user_group_roles where user_group=3 and roles=181;
