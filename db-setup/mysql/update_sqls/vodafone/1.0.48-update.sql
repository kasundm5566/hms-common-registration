/*
===================================
      Release Details
===================================

Date: 23/08/2013
SDP 1.0.176
Common Registration 1.0.48
CAS v1-0-vdf-b2

*/

USE common_admin;

INSERT INTO `role` VALUES (208,NULL,NULL,0,'Caas allow query balance write permission','ROLE_PROV_CAAS_WRITE_ALLOW_QUERY_BALANCE',5);

INSERT INTO `user_group_roles`(`user_group`, `roles`) VALUES (1,207), (3,207), (1,208), (3,208);

/*Bug 23532 - Configure CaaS NCS with Write Permissions for a new application*/
INSERT INTO `role` VALUES (201,NULL,NULL,0,'Caas asynchronous charging notification read permission','ROLE_PROV_CAAS_READ_ASYNC_CHARGING_NOTIFICATION',5);
INSERT INTO `role` VALUES (202,NULL,NULL,0,'Caas asynchronous charging notification write permission','ROLE_PROV_CAAS_WRITE_ASYNC_CHARGING_NOTIFICATION',5);
INSERT INTO `user_group_roles`(`user_group`, `roles`) VALUES (3, 224), (3, 226), (1,201), (3,201), (3,202), (1,202), (3, 204), (3, 206);
DELETE FROM `user_group_roles` where user_group=3 and roles=197;

/*Bug 23609 - Configure Downloadable NCS with Write Permissions for a new application*/
INSERT INTO `user_group_roles`(`user_group`, `roles`) VALUES (3,292), (3, 282), (3, 280), (3, 278), (3, 276), (3, 274);

