use common_admin;

update user set business_id = MID(mobile_no, 4, 7) where developer_type = 'INDIVIDUAL';

-- roles update
INSERT INTO `role` VALUES
(603,NULL,NULL,0, 'sdp app reporting ncs chart show', 'ROLE_SDP_NCS_REPORT_CHART_SHOW', 8);
INSERT INTO `user_group_roles` VALUES (1,603);