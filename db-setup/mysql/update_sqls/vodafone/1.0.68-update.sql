-- add new columns to the table
alter table user add column (`security_question` varchar(255) DEFAULT NULL,
  `security_question_answer` varchar(255) DEFAULT NULL,
  `developer_type` varchar(255) DEFAULT NULL);

-- update all existing developers to individual developers
update user set `developer_type` = 'INDIVIDUAL' where `type` = 'CORPORATE';