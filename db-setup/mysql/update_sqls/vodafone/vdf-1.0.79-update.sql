-- Adding new fields to store user profile picture (image) and contact no (contact_no)
ALTER TABLE `common_admin`.`user` 
ADD COLUMN `image` VARCHAR(255) NULL,
ADD COLUMN `contact_no` VARCHAR(255) NULL AFTER `image`;


-- Copy moble_no value to the contact_no
SET SQL_SAFE_UPDATES=0;
UPDATE `common_admin`.`user` SET `contact_no` = `mobile_no`;
SET SQL_SAFE_UPDATES=1;

