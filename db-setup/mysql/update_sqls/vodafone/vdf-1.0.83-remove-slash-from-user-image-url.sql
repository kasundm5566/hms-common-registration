-- Update image url column by removing / at the biginning.
SET SQL_SAFE_UPDATES=0;
UPDATE `common_admin`.`user` SET `image` = SUBSTRING(`image`,2,LENGTH(`image`)-2) WHERE LEFT(`image`,1) = '/';
SET SQL_SAFE_UPDATES=1;
