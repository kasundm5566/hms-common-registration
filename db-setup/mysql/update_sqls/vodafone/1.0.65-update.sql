-- use common admin database
use common_admin;

-- add tin , business_id columns
alter table user
  add column tin varchar(20),
  add column business_id varchar(20);

insert into role (id,name,version,description,module) values (600,"ROLE_APP_VIEW_REGISTRATION",0,"Appstore - view registration link",6);
insert into user_group_roles (user_group,roles) values (1,600),(3,600);