-- Adding new roles to handle Subscribe, Download and Reset Password actions under SDP-Admin module for the user groups
-- 'SDP admin', 'Customer care users' and 'SDP manager users'.
INSERT INTO `role` VALUES
(7000,NULL,NULL,0,'Subscription – Subscribe','ROLE_ADM_SUBSCRIBE_SUBSCRIPTION',7),
(7001,NULL,NULL,0,'User Management – Reset Password','ROLE_ADM_USER_RESET_PASSWORD',7);

INSERT INTO `user_group_roles` VALUES
(1,7000),
(1,7001),
(6,7000),
(6,7001),
(7,7000),
(7,7001);