/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.userservice.connector;

import hms.common.registration.api.request.UserDetailByUsernameRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.rest.util.client.AbstractWebClient;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import java.util.Date;

import static hms.common.registration.api.common.RequestType.USER_BASIC_DETAILS;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class RegistrationApiConnector extends AbstractWebClient {

    private static final Logger logger = Logger.getLogger(RegistrationApiConnector.class);
    private String baseUrl = "";

    public RegistrationApiConnector(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public BasicUserResponseMessage getDetailedUserResponseMessage(String userName, String moduleName) {
        try {
            logger.debug("Sending Request to get User Details to [ " + baseUrl +" ] for User name [ " + userName + " ]");

            WebClient webClient = createWebClient(baseUrl);

            UserDetailByUsernameRequestMessage reqMessage = new UserDetailByUsernameRequestMessage();
            reqMessage.setRequestType(USER_BASIC_DETAILS);
            reqMessage.setRequestedTimeStamp(new Date());
            reqMessage.setUserName(userName);
            reqMessage.setModuleName(moduleName);

            Response response = webClient.post(reqMessage.convertToMap());

            return BasicUserResponseMessage.convertFromMap(readJsonResponse(response));
        } catch (Exception e) {
            return null;
        }
    }

}
