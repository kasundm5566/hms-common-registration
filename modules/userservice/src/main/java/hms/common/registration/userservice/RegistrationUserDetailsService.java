/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.userservice;

import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.userservice.connector.RegistrationApiConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashSet;
import java.util.Set;

/**
 * Custom Implementation for CAS UserDetailsService to get the User Roles
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RegistrationUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationUserDetailsService.class);
    private String baseUrl = "";
    private String moduleName = "";

    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException, DataAccessException {
        logger.debug("Calling RegistrationUserDetailsService for getting User Permissions ");
        try {
            return getUserDetails(userName);

        } catch (UsernameNotFoundException e) {
            logger.debug("UserNotFoundExceptionOccurred", e);
            throw e;
        } catch (DataAccessException e) {
            logger.debug("DataAccessException occurred", e);
            throw e;
        } catch (RuntimeException e) {
            logger.error("Unexpected error occurred ", e);
            throw e;
        }
    }

    private UserDetails getUserDetails(String userName) {

        RegistrationApiConnector registrationApiConnector = new RegistrationApiConnector(baseUrl);
        BasicUserResponseMessage resMessage = registrationApiConnector.getDetailedUserResponseMessage(userName,
                moduleName);
        logger.debug("Response received from Registration API[{}] is [{}]", registrationApiConnector, resMessage);

        if (resMessage == null) {
            logger.debug("Detailed User Response Message has returned null");
            throw new BadCredentialsException(" [ " + userName + " ] Not Found");
        } else if (StatusCodes.USER_NOT_FOUND.equals(resMessage.getStatusCode())) {
            logger.debug("Status Code is [{}]", resMessage.getStatus());
            throw new UsernameNotFoundException(" [ " + userName + " ] Not Found");
        } else if (resMessage.getRoles() != null && resMessage.getRoles().size() > 0) {
            logger.debug("Adding roles for user [{}]", userName);
            RegistrationUser customUser = new RegistrationUser();
            customUser.setEnabled(true);
            customUser.setAccountNonExpired(true);
            customUser.setAccountNonLocked(true);
            customUser.setCredentialsNonExpired(true);

            customUser.setUsername(userName);
            customUser.setPassword("");
            Set<Authority> authorities = new HashSet<Authority>();
            for (String role : resMessage.getRoles()) {
                if (role != null && !role.isEmpty()) {
                    authorities.add(new Authority(role.trim()));
                }
            }
            customUser.setRoles(authorities);
            logger.debug("Received User Permissions [ " + resMessage.getRoles() + " ]");
            return customUser;
        } else {
            logger.debug("No Roles found for username [{}] in [{}]", userName, resMessage);
            throw new BadCredentialsException(" [ " + userName + " ] has no roles.");
        }
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
}