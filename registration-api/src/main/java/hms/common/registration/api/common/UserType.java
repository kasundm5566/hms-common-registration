/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.common;

import java.util.EnumSet;

/**
 * Defines available user types
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public enum UserType {

    INDIVIDUAL {
        @Override
        public boolean isCorporateUser() {
            return false;
        }},

    CORPORATE {
        @Override
        public boolean isCorporateUser() {
            return true;
        }},

    SUB_CORPORATE {
        @Override
        public boolean isCorporateUser() {
            return false;
        }},

    ADMIN_USER {
        @Override
        public boolean isCorporateUser() {
            return false;
        }};

    public abstract boolean isCorporateUser();

    /**
     * Create userType enum from given string type.
     * @param code
     * @return
     */
    public static UserType getEnum(String code) {
        if (code == null || code.trim().length() == 0) {
            throw new IllegalArgumentException("User Type cannot be empty or null.");
        }
        for (UserType tag : EnumSet.allOf(UserType.class)) {
            if (tag.name().equalsIgnoreCase(code)) {
                return tag;
            }
        }

        return null;
    }
    
}