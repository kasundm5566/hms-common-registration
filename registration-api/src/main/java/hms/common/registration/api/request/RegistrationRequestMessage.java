/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.request;

import hms.common.registration.api.common.RequestType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlEnumValue;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract Request which defines the common request parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class RegistrationRequestMessage {

    @XmlAttribute(name = "requested-timestamp")
    protected Date requestedTimeStamp = new Date();

    @XmlAttribute(name = "request-type")
    protected RequestType requestType;

    protected Map<String, String> additionalParams = new HashMap<String, String>();

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Date getRequestedTimeStamp() {
        return requestedTimeStamp;
    }

    public void setRequestedTimeStamp(Date requestedTimeStamp) {
        this.requestedTimeStamp = requestedTimeStamp;
    }

    public Map<String, String> getAdditionalParams() {
        return additionalParams;
    }

    public void setAdditionalParams(Map<String, String> additionalParams) {
        this.additionalParams = additionalParams;
    }
    
    public Map<String, Object> convertToMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        final SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        map.put("requested-timestamp", f.format(requestedTimeStamp));
        map.put("request-type", requestType.name());
        map.put("additional-params", additionalParams);
        return map;
    }
    
    public static void convertFromMap(RegistrationRequestMessage r, Map<String, Object> m) throws ParseException {
        r.setRequestType(RequestType.valueOf((String) m.get("request-type")));
        final SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        r.setRequestedTimeStamp(f.parse((String) m.get("requested-timestamp")));
        r.setAdditionalParams((Map<String, String>) m.get("additional-params"));
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RegistrationRequestMessage");
        sb.append("{requestedTimeStamp=").append(requestedTimeStamp);
        sb.append(", requestType=").append(requestType);
        sb.append(", additionalParams=").append(additionalParams);
        sb.append('}');
        return sb.toString();
    }
}