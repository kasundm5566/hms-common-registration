/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.ParseException;
import java.util.Map;

/**
 * Defines the get user basic details by msisdn request parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@XmlRootElement(name = "user-detail-by-msisdn")
@XmlAccessorType(XmlAccessType.NONE)
public class UserDetailByMsisdnRequestMessage extends RegistrationRequestMessage {

    @XmlAttribute(name = "msisdn")
    private String msisdn;

    @XmlAttribute(name = "modulename")
    private String moduleName = null;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Map<String, Object> convertToMap() {
        Map<String, Object> m = super.convertToMap();
        m.put("msisdn", msisdn);
        m.put("module-name", moduleName);
        return m;
    }

    public static UserDetailByMsisdnRequestMessage convertFromMap(Map<String, Object> m) throws ParseException {
        UserDetailByMsisdnRequestMessage r = new UserDetailByMsisdnRequestMessage();
        r.setMsisdn((String) m.get("msisdn"));
        r.setModuleName((String) m.get("module-name"));
        RegistrationRequestMessage.convertFromMap(r, m);

        return r;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UserDetailByMsisdnRequestMessage");
        sb.append("{msisdn='").append(msisdn).append('\'');
        sb.append('}');
        return sb.toString();
    }
}