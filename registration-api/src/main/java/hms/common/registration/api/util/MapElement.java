/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.util;

import javax.xml.bind.annotation.XmlTransient;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@XmlTransient
abstract class MapElement<K, V> {

    protected K key;
    protected V value;

    public abstract K getKey();

    public abstract V getValue();

    public abstract void setKey(K key);

    public abstract void setValue(V value);

}
