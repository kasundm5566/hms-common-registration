/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.response;

import hms.common.registration.api.common.StatusCodes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract response which defines the common response parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class RegistrationResponseMessage {

    @XmlAttribute(name = "status-code")
    private StatusCodes statusCode;

    @XmlAttribute(name = "mobile-no")
    private String mobileNo;

    public final StatusCodes getStatusCode() {
        return statusCode;
    }

    public final void setStatusCode(StatusCodes statusCode) {
        this.statusCode = statusCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Map<String, Object> convertToMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("status-code", statusCode.name());
        return map;
    }

    public Map<String, Object> convertToMapForDiscoveryAPI() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("statusCode", statusCode.getShortCode());
        map.put("statusDescription", statusCode.getDescription());
        map.put("mobileNo", this.mobileNo);
        return map;
    }

    public static void convertFromMap(RegistrationResponseMessage r, Map<String, Object> m) throws ParseException {
        r.setStatusCode(StatusCodes.valueOf((String) m.get("status-code")));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RegistrationResponseMessage");
        sb.append("{statusCode=").append(statusCode);
        sb.append('}');
        return sb.toString();
    }
}