/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.common;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public enum RequestType {
    USER_AUTHENTICATION,
    USER_BASIC_DETAILS,
    USER_ADDITIONAL_DETAILS,
    USER_ACTIVATE,
    USER_SUSPEND;
}