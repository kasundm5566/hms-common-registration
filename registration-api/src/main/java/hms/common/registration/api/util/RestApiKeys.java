/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.util;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public interface RestApiKeys {

    public static final String USERNAME = "username";
    public static final String FIRST_NAME = "first-name";
    public static final String CORPORATE_USER_NAME = "corporate-user-name";
    public static final String CORPORATE_USER_EMAIL = "corporate-user-email";

    public static final String EMAIL = "email";
    public static final String MPIN = "mpin";
    public static final String MSISDN = "msisdn";

    public static final String MSISDN_VERIFIED = "msisdn-verified";

    public static final String LAST_NAME = "last-name";
    public static final String BIRTHDAY = "birthday";
    public static final String PROFESSION = "profession";
    public static final String GENDER = "gender";

    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String PROVINCE = "province";
    public static final String COUNTRY = "country";
    public static final String POST_CODE = "post-code";

    public static final String PHONE_TYPE = "phone-type";
    public static final String PHONE_MODEL = "phone-model";
    public static final String OPERATOR = "operator";
    public static final String CREATED_DATE="created-date";
    public static final String ORGANIZATION_NAME="organization-name";
    public static final String LAST_LOGIN_TIME="last-login-time";

    public static final String CONTACT_PERSON_NAME = "contact-person-name";
    public static final String DISPLAY_NAME="display-name";

}