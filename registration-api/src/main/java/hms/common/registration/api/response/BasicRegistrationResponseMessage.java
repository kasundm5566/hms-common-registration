/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.response;

import hms.common.registration.api.common.StatusCodes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement(name = "basic-response")
@XmlAccessorType(XmlAccessType.NONE)
public class BasicRegistrationResponseMessage extends RegistrationResponseMessage {

    public BasicRegistrationResponseMessage() {
    }

    public static BasicRegistrationResponseMessage convertFromMap(Map<String, Object> m) throws ParseException {
        BasicRegistrationResponseMessage r = new BasicRegistrationResponseMessage();
        RegistrationResponseMessage.convertFromMap(r, m);
        return r;
    }
}