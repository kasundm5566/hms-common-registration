/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.common;

import java.util.EnumSet;

/**
 * Defines status for users
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public enum UserStatus {

    INITIAL("INITIAL"),

    ACTIVE("ACTIVE") {
        @Override
        public boolean isActive() {
            return true;
        }},

    SUSPEND("SUSPEND"),

    PENDING_FOR_APPROVAL("PENDING_FOR_APPROVAL");

    private String matchingStateName ="";

    private UserStatus(String matchingStateName) {
        this.matchingStateName = matchingStateName;
    }

    public String getMatchingStateName() {
        return matchingStateName;
    }

    /**
     * Create User Status enum from given string state code.
     * @param code
     * @return
     */
    public static UserStatus getEnum(String code) {
        if (code == null || code.trim().length() == 0) {
            throw new IllegalArgumentException("User Status  code cannot be empty or null.");
        }

        for (UserStatus tag : EnumSet.allOf(UserStatus.class)) {
            if (tag.getMatchingStateName().equalsIgnoreCase(code)) {
                return tag;
            }
        }
        return null;
    }

    public boolean isActive() {
        return false;
    }

}
