/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Defines the SDP - SMS MT send Request parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlRootElement(name = "sdp-message")
@XmlAccessorType(XmlAccessType.NONE)
public class SdpRestApiRequestMessage {

    @XmlAttribute(name = "message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> convert2ToMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("message", message);
        return map;
    }
    
    public static SdpRestApiRequestMessage convertFromMap(Map<String, Object> m) {
        SdpRestApiRequestMessage r = new SdpRestApiRequestMessage();
        r.setMessage((String) m.get("message"));
        return r;
    } 

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SdpRestApiRequestMessage");
        sb.append("{message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}