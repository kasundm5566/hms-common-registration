/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.response;

import hms.common.registration.api.common.UserStatus;
import hms.common.registration.api.common.UserType;
import hms.common.registration.api.request.RegistrationRequestMessage;
import hms.common.registration.api.util.MapStringStringAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.text.ParseException;
import java.util.*;

/**
 * Defines the basic user detail response parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlRootElement(name = "user-basic-response")
@XmlAccessorType(XmlAccessType.NONE)
public class BasicUserResponseMessage extends RegistrationResponseMessage {

    @XmlAttribute(name = "user-id")
    private String userId;

    @XmlAttribute(name = "corporate-user-id")
    private String corporateUserId;

    @XmlAttribute(name = "type")
    private UserType type;

    @XmlAttribute(name = "status")
    private UserStatus status;

    @XmlAttribute(name = "roles")
    private List<String> roles = new ArrayList<String>();

    @XmlAttribute(name = "groups")
    private Set<String> groups = new HashSet<String>();

    @XmlElement(name = "additional-parameters")
    @XmlJavaTypeAdapter(MapStringStringAdapter.class)
    private Map<String, String> additionalDataMap = new HashMap<String, String>();

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getCorporateUserId() {
        return corporateUserId;
    }

    public void setCorporateUserId(String corporateUserId) {
        this.corporateUserId = corporateUserId;
    }

    public Set<String> getGroups() {
        return groups;
    }

    public void setGroups(Set<String> groups) {
        this.groups = groups;
    }

    public Map<String, String> getAdditionalDataMap() {
        return additionalDataMap;
    }

    public void setAdditionalDataMap(Map<String, String> additionalDataMap) {
        this.additionalDataMap = additionalDataMap;
    }

    /**
     * returns the value for the given RestApiKey
     * @param parameterName RestApiKey
     * @return
     */
    public String getAdditionalData(String parameterName) {
        return additionalDataMap.get(parameterName);
    }

    /**
     * put tne key value pair to the map containing additional data
     * @param parameterName RestApiKey
     * @param value value
     */
    public void setAdditionalData(String parameterName, String value) {
        if(this.additionalDataMap == null) {
            this.additionalDataMap = new HashMap<String, String>();
        }
        if (parameterName != null && value != null) {
            additionalDataMap.put(parameterName, value);
        }
    }

    public Map<String, Object> convertToMap() {
        Map<String, Object> m = super.convertToMap();
        m.put("user-id", userId);
        m.put("corporate-user-id", corporateUserId);
        if (null != type) {
            m.put("user-type", type.name());
        }

        if (null != status) {
            m.put("status", status.name());
        }
        m.put("roles", roles);
        m.put("groups", Arrays.asList(groups)); 
        m.put("additional-parameters", additionalDataMap);
        return m;
    }

    public static BasicUserResponseMessage convertFromMap(Map<String, Object> m) throws ParseException {
        BasicUserResponseMessage r = new BasicUserResponseMessage();
        if (m.containsKey("user-id")) {
            r.setUserId((String) m.get("user-id"));
        }
        if (m.containsKey("corporate-user-id")) {
            r.setCorporateUserId((String) m.get("corporate-user-id"));
        }
        if (m.containsKey("user-type")) {
            r.setType(UserType.valueOf((String) m.get("user-type")));
        }
        if (m.containsKey("status")) {
            r.setStatus(UserStatus.valueOf((String) m.get("status")));
        }
        if (m.containsKey("roles")) {
            r.setRoles((List<String>) m.get("roles"));
        }
        if (m.containsKey("groups")) {
            r.setGroups(new HashSet<String>((List<String>) m.get("groups")));
        }
        if (m.containsKey("additional-parameters")) {
            r.setAdditionalDataMap((Map<String, String>) m.get("additional-parameters"));
        }
        RegistrationResponseMessage.convertFromMap(r, m);

        return r;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("BasicUserResponseMessage");
        sb.append("{userId='").append(userId).append('\'');
        sb.append(", corporateUserId='").append(corporateUserId).append('\'');
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append(", roles=").append(roles);
        sb.append(", groups=").append(groups);
        sb.append(", additionalDataMap=").append(additionalDataMap);
        sb.append('}');
        return sb.toString();
    }
}