/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.ParseException;
import java.util.Map;

/**
 * Defines the get user basic details by user name request parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlRootElement(name = "user-detail-by-username")
@XmlAccessorType(XmlAccessType.NONE)
public class UserDetailByUsernameRequestMessage extends RegistrationRequestMessage {

    @XmlAttribute(name = "username")
    private String userName;

    @XmlAttribute(name = "modulename")
    private String moduleName = null;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Map<String, Object> convertToMap() {
        Map<String, Object> m = super.convertToMap();
        m.put("user-name", userName);
        m.put("module-name", moduleName);
        return m;
    }

    public static UserDetailByUsernameRequestMessage convertFromMap(Map<String, Object> m) throws ParseException {
        UserDetailByUsernameRequestMessage r = new UserDetailByUsernameRequestMessage();
        r.setUserName((String) m.get("user-name"));
        r.setModuleName((String) m.get("module-name"));
        RegistrationRequestMessage.convertFromMap(r, m);

        return r;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UserDetailByUsernameRequestMessage");
        sb.append("{userName='").append(userName).append('\'');
        sb.append(", moduleName='").append(moduleName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}