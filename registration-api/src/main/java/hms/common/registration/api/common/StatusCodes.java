/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.common;

public enum StatusCodes {

    SUCCESS("Success", "S1000"),
    INVALID_CREDENTIALS("Invalid credentials", "E1010"),
    USER_NOT_FOUND("User does not exist", "E1015"),
    USER_NOT_ACTIVE("User not active", "E1025"),
    USER_NOT_ALLOWED("User not allowed", "E1035"),
    USER_EXISTS("User already exists", "E1045"),
    INTERNAL_ERROR("Internal error", "E1030"),
    UNSUPPORTED_REQUEST_TYPE("Unsupported request type", "E1055");

    private String description;
    private String shortCode;

    StatusCodes(String description, String shortCode) {
        this.description = description;
        this.shortCode = shortCode;
    }

    public String getDescription() {
        return description;
    }

    public String getShortCode(){
        return shortCode;
    }
}