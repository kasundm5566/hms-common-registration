/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.request;

import hms.common.registration.api.common.RequestType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.ParseException;
import java.util.Map;

/**
 * Defines the user authentication request parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlRootElement(name = "user-authentication")
@XmlAccessorType(XmlAccessType.NONE)
public class UserAuthenticationRequestMessage extends RegistrationRequestMessage {

    @XmlAttribute(name = "user-name")
    private String userName;
    @XmlAttribute(name = "password")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Map<String, Object> convertToMap() {
        Map<String, Object> m = super.convertToMap();
        m.put("user-name", userName);
        m.put("password", password);
        return m;
    }

    public static UserAuthenticationRequestMessage convertFromMap(Map<String, Object> m) throws ParseException {
        UserAuthenticationRequestMessage r = new UserAuthenticationRequestMessage();
        r.setUserName((String) m.get("user-name"));
        r.setPassword((String) m.get("password"));
        RegistrationRequestMessage.convertFromMap(r, m);
        
        return r;
    }

    public static UserAuthenticationRequestMessage convertFromMapForDiscoveryAPI(Map<String, Object> m) throws ParseException {
        UserAuthenticationRequestMessage r = new UserAuthenticationRequestMessage();
        r.setUserName((String) m.get("user-name"));
        r.setPassword((String) m.get("password"));
        return r;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UserAuthenticationRequestMessage");
        sb.append("{userName='").append(userName).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}