/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
abstract class MapAdapter<T extends MapElement<K, V>, K, V> extends XmlAdapter<T[], Map<K, V>> {


    public abstract T[] newMapElement(int size);

    public abstract T newMapElement(K key, V value);

    @Override
    public final T[] marshal(final Map<K, V> map) throws Exception {
        if (null == map) {
            return null;
        }
        final T[] mapElements = newMapElement(map.size());
        int i = 0;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            mapElements[i++] = newMapElement(entry.getKey(), entry.getValue());
        }
        return mapElements;
    }

    @Override
    public final Map<K, V> unmarshal(final T[] elements) throws Exception {
        if (null == elements) {
            return null;
        }
        final Map<K, V> r = new HashMap<K, V>();
        for (MapElement<K, V> mapelement : elements) {
            r.put(mapelement.getKey(), mapelement.getValue());
        }
        return r;
    }
}

