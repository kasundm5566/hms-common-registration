/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api;

import java.util.Map;


/**
 *todo add class level comment
 */
public interface RegistrationRestApi {

    /**
     *
     * @param request {"first-name" : "Darshana" ,
     *                 "last-name" : "Don" ,
     *                 "birth-date" : "25/12/2011",
     *                 "username" : "darshana",
     *                 "password" : "md5encodedpassword",
     *                 "msisdn" : "phone-number-in-international-format : 25470234234234",
     *                 "mpin" : "2341"
     *                 "operator" : "safaricom",
     *                 "verify-msisdn", true/false,
     *                 "domain" : "internal|facebook|google|twitter"
     *                            }
     * @return
     * 1. if successfully created
     *                         {"status-code" : "S1000"}
     * 2. in case if failure
     *                        {
     *                        "status-code" : "E1300"
     *                        "common-errors", ["username.is.already.registered"]
     *                        }
     * 3. internal errors    {
     *                        "status-code" : "E1300"
     *                        "common-errors", ["system.error"]
     *                        }
     *
     */
    Map<String, Object> createUser(Map<String, Object> request);

    /**
     *
     * @param request           { "first-name" : "Darshana" ,
     *                            "last-name" : "Don" ,
     *                            "birth-date" : "25/12/2011",
     *                            "username" : "darshana",
     *                            "password" : "md5encodedpassword",
     *                            "msisdn" : "phone-number-in-international-format : 25470234234234",
     *                            "mpin" : "2341",
     *                            "operator" : "safaricom",
     *                            "verify-msisdn", true/false
     *
     *                            }
     * @return
     * 1. successfully edited
     *                         {"status-code" : "S1000"}
     * 2. invalid values
     *                        {
     *                        "status-code" : "E1300"
     *                        "common-errors", ["username.is.already.registered", "system.error"]
     *                        }
     * 3. internal errors
     */
    Map<String, Object> editUser(Map<String, Object> request);



    /**
     *
     * @param request {"username" : "Darshana" }
     * @return
     * 1. user_details
     *                         {"status-code" : "S1000"}
     * 2. invalid values
     *                        {
     *                        "status-code" : "E1300"
     *                        "first-name" : ["invalid.first.name", "first.name.is.larger]
     *                        "common-errors", ["user.already.exist", "system.is.busy.try.again"]
     *                        }
     * 3. internal errors
     */
    Map<String, Object> retrieveUser(Map<String, Object> request);


    /**
     *
     * @param request {"user-id" : "202020202020" }
     * @return
     * 1. user_details
     *                         {"status-code" : "S1000"}
     * 2. invalid values
     *                        {
     *                        "status-code" : "E1300"
     *                        "first-name" : ["invalid.first.name", "first.name.is.larger]
     *                        "common-errors", ["user.already.exist", "system.is.busy.try.again"]
     *                        }
     * 3. internal errors
     */
    public Map<String, Object> retrieveUserByUserId(Map<String, Object> request);



    public Map<String, Object> retrieveUserByUserMsisdn(Map<String, Object> request);

    /**
     *
     * @param map- {user-id , verification-code}
     * @return
     */
    Map<String, Object> verifyMsisdn(Map<String, Object> request);



    /**
     *
     * @param request {"username" : "Darshana" ,"old-password": "test123#", "new-password": "test123#new"}
     * @return
     * 1. password changed
     *                         {"status-code" : "S1000"}
     * 2. given old password does not match with the database
     *                        {
     *                        "status-code" : "E1300"
     *                        "common-errors", ["old.password.is.wrong"]
     *                        }
     * 3. internal errors
     */
    Map<String, Object> changeUserPassword(Map<String, Object> request);


    Map<String, Object> changeUserMpin(Map<String, Object> request);


    Map<String, Object> checkUsernameAvailability(Map<String, Object> request);


    Map<String, Object> checkUserVerifiedStatus(Map<String, Object> request);

    Map<String, Object> authenticateMpin(Map<String, Object> authenticateRequest);

    Map<String, Object> changeUserMpinByMsisdn(Map<String, Object> request);

    Map<String, Object> authenticateUsingProvider(Map<String, Object> request);

    Map<String, Object> sessionValidation(Map<String, Object> request);

    Map<String, Object> logout(Map<String, Object> request);

    Map<String,Object> requestVerificationCode(Map<String, Object> request);


    Map<String,Object> requestNewVerificationCode(Map<String, Object> request);

    /**
     *
     * @param request {'email':'user@mailclient.com'}
     * @return response
     *
     * 1. success message
     *              { "status-code" : "S100" }
     *
     * 2. failure message
     *             {
     *                 "status-code" : "E1300",
     *                 "common-errors" : "user does not exist" | "system failure"
     *             }
     */
    Map<String, Object> recoverAccount(Map<String, Object> request);

    Map<String, Object> UserProfileUpdate(Map<String, Object> request);

    /**
     * This method
     *
     * @param request {
     *                 "msisdn" : "phone-number-in-international-format : 6799900001",
     *                 }
     * @return
     * 1. if successfully created
     *                         {"status-code" : "S1000"}
     * 2. in case if failure
     *                        {
     *                        "status-code" : "E1300"
     *                        "common-errors", ["username.is.already.registered"]
     *                        }
     * 3. internal errors    {
     *                        "status-code" : "E1300"
     *                        "common-errors", ["system.error"]
     *                        }
     *
     */
    Map<String, Object> createAutoLoginUser(Map<String, Object> request);

    Map<String, Object> resetUserPassword(Map<String, Object> request);

    Map<String, Object> findAllUsers(int start, int limit);

    Map<String, Object> allUsersCount();

    Map<String, Object> findUsersByField(int start, int limit, String field, String value);
}
