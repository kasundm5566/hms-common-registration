/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.api.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.ParseException;
import java.util.Map;

/**
 * Defines the get user basic details by user id request parameters
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlRootElement(name = "user-detail")
@XmlAccessorType(XmlAccessType.NONE)
public class UserDetailRequestMessage extends RegistrationRequestMessage {

    @XmlAttribute(name = "user-id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, Object> convertToMap() {
        Map<String, Object> m = super.convertToMap();
        m.put("user-id", userId);
        return m;
    }

    public static UserDetailRequestMessage convertFromMap(Map<String, Object> m) throws ParseException {
        UserDetailRequestMessage r = new UserDetailRequestMessage();
        r.setUserId((String) m.get("user-id"));
        RegistrationRequestMessage.convertFromMap(r, m);

        return r;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UserDetailRequestMessage");
        sb.append("{userId=").append(userId);
        sb.append('}');
        return sb.toString();
    }
}