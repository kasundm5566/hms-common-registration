1.The following DNS should be placed in the /etc/hosts file of your server

ldap.cur  - this doamin name should be mapped to the LDAP server (if you have done this when implementing the CAS server, you can ignore this step)

2.dev.sdp.hsenidmobile.com DNS has been hardcoded in the following files of the registration module. please Change them as required.

webapps/registration/WEB-INF/classes/properties/cas.properties
webapps/registration/WEB-INF/classes/properties/registration-ui.properties

3.the system.default.sender.address property of the registration/WEB-INF/classes/registration_messages_en.properties should be changed as required. the default value is 383.


