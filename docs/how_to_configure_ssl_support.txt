###################################################
#											      #
#				CAS SSL Configuration			  #
#											      #
###################################################

Login as super users

1) Generating the key casserver : keytool -keystore $JAVA_HOME/jre/lib/security/cacerts -genkey -alias tomcat -keyalg RSA
What is your first and last name?
[Unknown]: localhost
What is the name of your organizational unit?
[Unknown]: SSO Services
What is the name of your organization?
[Unknown]: Cool solutions !
What is the name of your City or Locality?
[Unknown]: Melbourne
What is the name of your State or Province?
[Unknown]: Victoria
What is the two-letter country code for this unit?
[Unknown]: AU
Is CN=localhost, OU=SSO Services, O=Cool solutions !, L=Melbourne, ST=Victoria, C=AU correct?
[no]: yes

Note : Remember to put localhost [or the host name of your machine] as your first and last name. It is misleading. But that's the way life goes !. IPAddresses will not work as SSL does only support host name. You can either add a hostname for the ipaddress in your hosts files if you want.

2) export the certificate into server.crt : keytool -keystore $JAVA_HOME/jre/lib/security/cacerts -export -alias tomcat -file server.crt

3) Now import the server.crt into your cacerts : keytool -import -file server.crt -keystore $JAVA_HOME/jre/lib/security/cacerts

Now the last setup is to configure your tomcat for ssl. Go to $CATALINA_HOME/conf/server.xml file and uncomment the following setting [there are few modification].

    <Connector port="8443" maxHttpHeaderSize="8192"
    maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
    enableLookups="false" disableUploadTimeout="true"
    acceptCount="100" scheme="https" secure="true"
    clientAuth="false" sslProtocol="TLS" keystoreFile="/path/to/java/home/jre/lib/security/cacerts"
    keystorePass="changeit" truststoreFile="/path/to/java/home/jre/lib/security/cacerts" truststorePass="changeit"/>
