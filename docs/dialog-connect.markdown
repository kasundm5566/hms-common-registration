# Login Success

## Request

curl -X POST -H"Content-Type:application/json" 'http://127.0.0.1:4287/registration/registrationservice/authenticate/using-provider' -d'{"username":"hsenid2","password": "test123#"}'

## Response

{
  "statusCode": "S1000",
  "statusDescription": "Success.",
  "mobileNo": "94770808373",
  "username": "hsenid2",
  "userId": "20131124232526860",
  "sessionId": "81bc9e45ab0be86d1ec0aa95b182a554"
}


# Session Validataion


## Request

curl -X POST -H"Content-Type:application/json" 'http://127.0.0.1:4287/registration/registrationservice/user/session-validate' -d'{ "sessionId":"81bc9e45ab0be86d1ec0aa95b182a554"}'

## Response

{
  "statusCode": "S1000",
  "statusDescription": "Success.",
  "username": "hsenid2",
  "userId": "20131124232526860",
  "mobileNo": "94770808373"
}

# Logout

## Request

curl -X POST -H"Content-Type:application/json" 'http://127.0.0.1:4287/registration/registrationservice/user/logout' -d'{ "sessionId":"81bc9e45ab0be86d1ec0aa95b182a554"}'

## Response

{
  "statusCode": "S1000",
  "statusDescription": "Success."
}