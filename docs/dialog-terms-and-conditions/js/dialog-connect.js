/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

var DLG;
if (!DLG) {
    DLG = {};
    DLG._proxy_url = "http://login.dialog.lk/CommonLogin/md_proxy.jsp";
    DLG._login_popup = null;
    DLG._appId = null;
    DLG._callBack = null;
    DLG._logoutCallback = null;
    DLG.info = {};
    DLG.inject = function (a, b) {
        if (b === undefined) b = "";
        var c = document.createElement("script");
        c.setAttribute("type", "text/javascript");
        c.setAttribute("src", a + "?" + b);
        if (typeof c !== undefined) document.getElementsByTagName("head")[0].appendChild(c)
    };
    DLG.init = function (a) {
        DLG._appId = a.appId;
        DLG._callBack = a.callBack;
        DLG._logoutCallback = a.logoutCallback;
        var b = setInterval(function () {
            if (DLG._appId) {
                clearInterval(b);
                DLG.callapi()
            }
        }, 100)
    };
    DLG.callapi = function () {
        if (DLG.cookie("DLG_" + DLG._appId)) {
            DLG.inject(DLG._proxy_url, "info=true&t=" + DLG.cookie("DLG_" + DLG._appId))
        } else if (DLG._appId) {
            DLG.info = {
                status: false
            };
            var a = setInterval(function () {
                if (DLG._callBack) {
                    clearInterval(a);
                    DLG._callBack(DLG.info)
                }
            }, 100)
        }
        DLG.createLoginButton("/dialogWconnectWbutton/", "<input type='image' src='http://login.dialog.lk/CommonLogin/scripts/img/dialog-connect-button.gif' border='0' width='109' height='25' alt='Login with Dialog Connect' onclick='DLG.login()'/>")
    };
    DLG.login = function () {
        DLG._login_popup = window.open(DLG._proxy_url + "?login=true&appid=" + DLG._appId, "mdlogin", "width=800,height=400,top=" + (screen.height ? (screen.height - 400) / 2 : 100) + ",left=" + (screen.width ? (screen.width - 800) / 2 : 100) + ",location=no,directories=no,menubar=no,toolbar=no,resizable=yes,scrollbars=yes,status=yes");
        DLG._login_popup.focus();
        var a = setInterval(function () {
            if (!DLG._login_popup || DLG._login_popup.closed) {
                clearInterval(a);
                DLG.callapi()
            }
        }, 100)
    };
    DLG.logout = function () {
        DLG.inject(DLG._proxy_url, "off=true&appId=" + DLG._appId);
        DLG._logoutCallback()
    };
    DLG.cookie = function (a, b, c) {
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(b)) || b === null || b === undefined)) {
            if (c === undefined) {
                c = {}
            }
            if (b === undefined || b === null) {
                c.expires = -1
            }
            if (typeof c.expires === "number") {
                var d = c.expires,
                    e = c.expires = new Date;
                e.setDate(e.getDate() + d)
            }
            b = String(b);
            return document.cookie = [encodeURIComponent(a), "=", c.raw ? b : encodeURIComponent(b), c.expires ? "; expires=" + c.expires.toUTCString() : "", c.path ? "; path=" + c.path : "", c.domain ? "; domain=" + c.domain : "", c.secure ? "; secure" : ""].join("")
        }
        c = b || {};
        var f = c.raw ? function (a) {
            return a
        } : decodeURIComponent;
        var g = document.cookie.split("; ");
        for (var h = 0, i; i = g[h] && g[h].split("="); h++) {
            if (f(i[0]) === a) return f(i[1] || "")
        }
    };
    DLG._main = function () {
        var a = window.location.search.replace("?", "");
        var b = window.location.hash.replace("#", "");
        if (/^(.{128})+\.(.*)$/.test(b) && a != null) {
            var c = b.split(".");
            DLG.cookie("DLG_" + a, c[0]);
            DLG.cookie("DLG_h", c[1]);
            window.close()
        }
    };
    DLG.api = function (a) {
        DLG.info = a;
        if (!a.status) {
            DLG.cookie("DLG_" + DLG._appId, null)
        }
        var b = setInterval(function () {
            if (DLG._callBack) {
                clearInterval(b);
                DLG._callBack(DLG.info)
            }
        }, 100)
    };
    DLG.createLoginButton = function (a, b) {
        var c = document.getElementsByTagName("body")[0];
        var d = c.getElementsByTagName("*");
        for (var e = 0, f = d.length; e < f; e++) {
            var g = d[e].className;
            if (/dialog\Wconnect\Wbutton/.test(g)) {
                d[e].innerHTML = b
            }
        }
    };
    DLG._main()
}