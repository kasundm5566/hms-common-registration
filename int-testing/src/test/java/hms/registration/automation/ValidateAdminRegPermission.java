/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;
import java.util.ResourceBundle;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

public class ValidateAdminRegPermission extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";

    private static String permissionLinkLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VAccordion[0]/domChild[2]/domChild[0]/domChild[0]/domChild[0]";
    private static String addPermissionLinkLoc = "vaadin=registration::PID_SaddRoleLink/domChild[0]/domChild[0]";
    private static String addPermissionHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String addPermissionModuleLink = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VFilterSelect[0]/domChild[1]";
    private static String addPermissionPGWModuleLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item2";
    private static String addPermissionNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[0]";
    private static String addPermissionDescLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextArea[0]";
    private static String addPermissionSDPADMINGroupLeftLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#leftSelect";
    private static String addPermissionGroupAddBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#add";
    private static String addPermissionSDPADMINGroupRightLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#rightSelect";
    private static String addPermissionGroupRemoveBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#remove";
    private static String addPermissionSubmitBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String addPermissionErrorLblLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";

    private static String managePermissionHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String managePermissionLinkLoc = "vaadin=registration::PID_SmanagePermissionLink/domChild[0]/domChild[0]";
    private static String managePermissionModuleNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VFilterSelect[0]/domChild[1]";
    private static String managePermissionPGWModuleLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item2";
    private static String managePermissionSearchBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[2]/VButton[0]/domChild[0]/domChild[0]";

    private static String managePermissionViewInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[14]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String managePermissionInfoGroupNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String managePermissionCloseInfoLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String managePermissionEditInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[14]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String managePermissionEditIndfoModuleNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String managePermissionEditInfoPermissionNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String managePermissionEditInfoPermissionDescLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[2]/domChild[0]/domChild[0]/domChild[0]";
    private static String managePermissionEditInfoGroupLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[3]/domChild[0]/domChild[0]/domChild[0]";
    private static String managePermissionCloseEditInfoLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String managePermissionDeleteLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[14]/VHorizontalLayout[0]/ChildComponentContainer[2]/VButton[0]/domChild[0]/domChild[0]";
    private static String managePermissionDeleteConfirmYesLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String managePermissionDeleteConfirmNoLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for SDP Admin - Manage permission");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_SDPAdminLoginTest();
    }

    private void internal_SDPAdminLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_SDPAdminLoginTest");
        setTestName("internal_SDPAdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "sdpadmin");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "sdpadmin"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            internal_comm_reg_test_sdp_admin_add_permission_module_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_permission_module_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {
            cmd.setCommand("mouseClick", permissionLinkLoc, "33,-134");
            doMouseClick(permissionLinkLoc, "33,-134");

            cmd.setCommand("click", addPermissionLinkLoc, "");
            doCommand("click", new String[]{addPermissionLinkLoc});

            doCommand("assertText", new String[]{addPermissionHeaderLoc, resourceBundle.getObject("registration.admin.permission.add.heading").toString()});
            System.out.println(i + ". SDP admin - Add Permission - Header Passed");

            cmd.setCommand("click", addPermissionSubmitBtnLoc, "");
            doCommand("click", new String[]{addPermissionSubmitBtnLoc});

            doCommand("assertText", new String[]{addPermissionErrorLblLoc, resourceBundle.getObject("registration.admin.role.invalid.role.name").toString()});
            System.out.println(++i + ". SDP admin - Add Permission - Module name required validation passed");

            internal_comm_reg_test_sdp_admin_add_permission_permission_name_format1_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_permission_permission_name_format1_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addPermissionLinkLoc, "");
            doCommand("click", new String[]{addPermissionLinkLoc});

            cmd.setCommand("mouseClick", addPermissionModuleLink, "11,-133");
            doMouseClick(addPermissionModuleLink, "11,-133");

            cmd.setCommand("mouseClick", addPermissionPGWModuleLoc, "49,5");
            doMouseClick(addPermissionPGWModuleLoc, "49,5");

            cmd.setCommand("click", addPermissionSubmitBtnLoc, "");
            doCommand("click", new String[]{addPermissionSubmitBtnLoc});

            doCommand("assertText", new String[]{addPermissionErrorLblLoc, resourceBundle.getObject("registration.admin.module.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add Permission - Permission name format validation passed");

            internal_comm_reg_test_sdp_admin_add_permission_permission_name_format2_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_permission_permission_name_format2_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addPermissionLinkLoc, "");
            doCommand("click", new String[]{addPermissionLinkLoc});

            cmd.setCommand("mouseClick", addPermissionModuleLink, "11,-133");
            doMouseClick(addPermissionModuleLink, "11,-133");

            cmd.setCommand("mouseClick", addPermissionPGWModuleLoc, "49,5");
            doMouseClick(addPermissionPGWModuleLoc, "49,5");

            cmd.setCommand("enterCharacter", addPermissionNameLoc, "testrole");
            doCommand("enterCharacter", new String[]{addPermissionNameLoc, "testrole"});

            cmd.setCommand("click", addPermissionSubmitBtnLoc, "");
            doCommand("click", new String[]{addPermissionSubmitBtnLoc});

            doCommand("assertText", new String[]{addPermissionErrorLblLoc, resourceBundle.getObject("registration.admin.module.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add Permission - Permission name format validation passed");

            internal_comm_reg_test_sdp_admin_add_permission_permission_create_permission();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

//    private void internal_comm_reg_test_sdp_admin_add_permission_permission_description_validate() throws Throwable {
//        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
//        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");
//
//        try {
//
//            cmd.setCommand("click", addPermissionLinkLoc, "");
//            doCommand("click", new String[]{addPermissionLinkLoc});
//
//            cmd.setCommand("mouseClick", addPermissionModuleLink, "11,-133");
//            doMouseClick(addPermissionModuleLink, "11,-133");
//
//            cmd.setCommand("mouseClick", addPermissionPGWModuleLoc, "49,5");
//            doMouseClick(addPermissionPGWModuleLoc, "49,5");
//
//            cmd.setCommand("enterCharacter", addPermissionNameLoc, "ROLE_TEST");
//            doCommand("enterCharacter", new String[]{addPermissionNameLoc, "ROLE_TEST"});
//
//            cmd.setCommand("enterCharacter", addPermissionDescLoc, "Test Role");
//            doCommand("enterCharacter", new String[]{addPermissionDescLoc, "Test Role"});
//
//            cmd.setCommand("click", addPermissionSubmitBtnLoc, "");
//            doCommand("click", new String[]{addPermissionSubmitBtnLoc});
//
//            doCommand("assertText", new String[]{addPermissionErrorLblLoc, resourceBundle.getObject("registration.admin.module.name.required").toString()});
//            System.out.println(++i + ". SDP admin - Add Permission - Permission name format validation passed");
//
//        } catch (Throwable e) {
//            if (writeScreenshots) {
//                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
//            }
//            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
//        }
//        handleSoftErrors();
//    }

    private void internal_comm_reg_test_sdp_admin_add_permission_permission_create_permission() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addPermissionLinkLoc, "");
            doCommand("click", new String[]{addPermissionLinkLoc});

            cmd.setCommand("mouseClick", addPermissionModuleLink, "11,-133");
            doMouseClick(addPermissionModuleLink, "11,-133");

            cmd.setCommand("mouseClick", addPermissionPGWModuleLoc, "49,5");
            doMouseClick(addPermissionPGWModuleLoc, "49,5");

            cmd.setCommand("enterCharacter", addPermissionNameLoc, "ROLE_TEST");
            doCommand("enterCharacter", new String[]{addPermissionNameLoc, "ROLE_TEST"});

            cmd.setCommand("enterCharacter", addPermissionDescLoc, "Test Role");
            doCommand("enterCharacter", new String[]{addPermissionDescLoc, "Test Role"});

            cmd.setCommand("addSelection", addPermissionSDPADMINGroupLeftLoc, "label=SDP_ADMIN");
            doCommand("addSelection", new String[]{addPermissionSDPADMINGroupLeftLoc, "label=SDP_ADMIN"});

            cmd.setCommand("click", addPermissionGroupAddBtnLoc, "");
            doCommand("click", new String[]{addPermissionGroupAddBtnLoc});

            cmd.setCommand("click", addPermissionGroupAddBtnLoc, "");
            doCommand("click", new String[]{addPermissionGroupAddBtnLoc});

            cmd.setCommand("removeSelection", addPermissionSDPADMINGroupRightLoc, "label=SDP_ADMIN");
            doCommand("removeSelection", new String[]{addPermissionSDPADMINGroupRightLoc, "label=SDP_ADMIN"});

            cmd.setCommand("click", addPermissionGroupRemoveBtnLoc, "");
            doCommand("click", new String[]{addPermissionGroupRemoveBtnLoc});

            cmd.setCommand("click", addPermissionSubmitBtnLoc, "");
            doCommand("click", new String[]{addPermissionSubmitBtnLoc});

            doCommand("assertText", new String[]{addPermissionErrorLblLoc, resourceBundle.getObject("registration.admin.module.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add Permission - Permission creation passed");

            internal_comm_reg_test_sdp_admin_manage_permission_view_info_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_permission_view_info_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", managePermissionLinkLoc, "");
            doCommand("click", new String[]{managePermissionLinkLoc});

            doCommand("assertText", new String[]{addPermissionHeaderLoc, resourceBundle.getObject("registration.admin.permission.add.heading").toString()});
            System.out.println("\n\n"+ ++i + ". SDP admin - Manage Permission - Header Passed");

            cmd.setCommand("mouseClick", managePermissionModuleNameLoc, "9,-158");
            doMouseClick(managePermissionModuleNameLoc, "9,-158");
             System.out.println(++i + ". SDP admin - Manage Permission Module name Clicked");

            cmd.setCommand("mouseClick", managePermissionPGWModuleLoc, "17,10");
            doMouseClick(managePermissionPGWModuleLoc, "17,10");
            System.out.println(++i + ". SDP admin - Manage Permission Module name Selected");

            cmd.setCommand("click", managePermissionSearchBtnLoc, "");
            doCommand("click", new String[]{managePermissionSearchBtnLoc});
            System.out.println(++i + ". SDP admin - Manage Permission - Found Permissions");

            cmd.setCommand("click", managePermissionViewInfoLoc, "");
            doCommand("click", new String[]{managePermissionViewInfoLoc});

            doCommand("assertText", new String[]{managePermissionInfoGroupNameLoc, resourceBundle.getObject("registration.admin.group.name").toString()});
            System.out.println(++i + ". SDP admin - Manage Permission - Viewing Permission Info");

            cmd.setCommand("mouseClick", managePermissionCloseInfoLoc, "8,10");
            doMouseClick(managePermissionCloseInfoLoc, "8,10");

            internal_comm_reg_test_sdp_admin_manage_permission_edit_info_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_permission_edit_info_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", managePermissionLinkLoc, "");
            doCommand("click", new String[]{managePermissionLinkLoc});

            cmd.setCommand("mouseClick", managePermissionModuleNameLoc, "9,-158");
            doMouseClick(managePermissionModuleNameLoc, "9,-158");
            System.out.println(++i + ". SDP admin - Manage Permission Module name Clicked");

            cmd.setCommand("mouseClick", managePermissionPGWModuleLoc, "17,10");
            doMouseClick(managePermissionPGWModuleLoc, "17,10");
            System.out.println(++i + ". SDP admin - Manage Permission Module name Selected");

            cmd.setCommand("click", managePermissionSearchBtnLoc, "");
            doCommand("click", new String[]{managePermissionSearchBtnLoc});

            cmd.setCommand("click", managePermissionEditInfoLoc, "");
            doCommand("click", new String[]{managePermissionEditInfoLoc});
            System.out.println(++i + ". SDP admin - Manage Permission - Found Permissions");

            doCommand("assertText", new String[]{managePermissionEditIndfoModuleNameLoc, resourceBundle.getObject("registration.admin.group.module").toString()});
            System.out.println(++i + ". SDP admin - Manage Permission - Edit Info - Module Name Viewing");
            doCommand("assertText", new String[]{managePermissionEditInfoPermissionNameLoc, resourceBundle.getObject("registration.admin.role.name").toString()});
            System.out.println(++i + ". SDP admin - Manage Permission - Edit Info - Permission Name Viewing");
            doCommand("assertText", new String[]{managePermissionEditInfoPermissionDescLoc, resourceBundle.getObject("registration.admin.role.description").toString()});
            System.out.println(++i + ". SDP admin - Manage Permission - Edit Info - Permission Description Viewing");
            doCommand("assertText", new String[]{managePermissionEditInfoGroupLoc, resourceBundle.getObject("registration.admin.role.groups").toString()});
            System.out.println(++i + ". SDP admin - Manage Permission - Edit Info - Module Groups Viewing");

            cmd.setCommand("mouseClick", managePermissionCloseEditInfoLoc, "9,5");
            doMouseClick(managePermissionCloseEditInfoLoc, "9,5");

            internal_comm_reg_test_sdp_admin_manage_permission_delete_permission_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_permission_delete_permission_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", managePermissionLinkLoc, "");
            doCommand("click", new String[]{managePermissionLinkLoc});

            cmd.setCommand("mouseClick", managePermissionModuleNameLoc, "9,-158");
            doMouseClick(managePermissionModuleNameLoc, "9,-158");

            cmd.setCommand("mouseClick", managePermissionPGWModuleLoc, "17,10");
            doMouseClick(managePermissionPGWModuleLoc, "17,10");

            cmd.setCommand("click", managePermissionSearchBtnLoc, "");
            doCommand("click", new String[]{managePermissionSearchBtnLoc});

            cmd.setCommand("click", managePermissionDeleteLoc, "");
            doCommand("click", new String[]{managePermissionDeleteLoc});
            System.out.println(++i + ". SDP admin - Manage Permission - Delete Permission Link Clicked");

            cmd.setCommand("click", managePermissionDeleteConfirmNoLoc, "");
            doCommand("click", new String[]{managePermissionDeleteConfirmNoLoc});
            System.out.println(++i + ". SDP admin - Manage Permission - Delete Permission Confirm No Clicked");

            cmd.setCommand("click", managePermissionDeleteLoc, "");
            doCommand("click", new String[]{managePermissionDeleteLoc});
            System.out.println(++i + ". SDP admin - Manage Permission - Delete Permission Link Clicked");

            cmd.setCommand("click", managePermissionDeleteConfirmYesLoc, "");
            doCommand("click", new String[]{managePermissionDeleteConfirmYesLoc});
            System.out.println(++i + ". SDP admin - Manage Permission - Delete Permission Confirm Yes Clicked");

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

}
