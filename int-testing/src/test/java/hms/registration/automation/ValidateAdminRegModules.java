/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

public class ValidateAdminRegModules extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";

    private static String modulesLinkLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VAccordion[0]/domChild[3]/domChild[0]/domChild[0]/domChild[0]";
    private static String addModulesLinkLoc = "vaadin=registration::PID_SaddModuleLink/domChild[0]/domChild[0]";
    private static String addModulesHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String addModulemoduleNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[0]";
    private static String addModuleModuleDescLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextArea[0]";
    private static String addModuleErrorLblLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";
    private static String addModuleSubmitBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";

    private static String manageModuleLinkLoc = "vaadin=registration::PID_SmanageModuleLink/domChild[0]/domChild[0]";
    private static String manageModuleSearchFieldLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VTextField[0]";
    private static String manageModuleSearchBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VEmbedded[0]/domChild[0]";

    private static String manageModuleViewInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageModuleViewInfoPermNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[2]";
    private static String manageModuleViewInfoPermDescLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[1]/domChild[2]";
    private static String manageModuleViewInfoModuleNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]/domChild[2]/domChild[2]";
    private static String manageModuleCloseInfoLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String manageModuleEditInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageModuleEditModuleNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageModuleEditModuleDescLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageModuleCloseEditLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String manageModuleDeleteLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[2]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageModuleDeleteConfirmYesLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageModuleDeleteConfirmNoLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for SDP Admin - Manage Modules");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_SDPAdminLoginTest();
    }

    private void internal_SDPAdminLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_SDPAdminLoginTest");
        setTestName("internal_SDPAdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "sdpadmin");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "sdpadmin"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            internal_comm_reg_test_sdp_admin_add_module_module_name_validate();


        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_module_module_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("mouseClick", modulesLinkLoc, "31,-159");
            doMouseClick(modulesLinkLoc, "31,-159");

            cmd.setCommand("click", addModulesLinkLoc, "");
            doCommand("click", new String[]{addModulesLinkLoc});

            doCommand("assertText", new String[]{addModulesHeaderLoc, resourceBundle.getObject("registration.admin.modules.add.heading").toString()});
            System.out.println(i + ". SDP Admin - Add Module - Header Passed");

            cmd.setCommand("click", addModuleSubmitBtnLoc, "");
            doCommand("click", new String[]{addModuleSubmitBtnLoc});

            doCommand("assertText", new String[]{addModuleErrorLblLoc, resourceBundle.getObject("registration.admin.module.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add Module - Module name required validation passed");

            internal_comm_reg_test_sdp_admin_add_module_module_desc_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_module_module_desc_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addModulesLinkLoc, "");
            doCommand("click", new String[]{addModulesLinkLoc});

            cmd.setCommand("enterCharacter", addModulemoduleNameLoc, "Test_Module");
            doCommand("enterCharacter", new String[]{addModulemoduleNameLoc, "Test_Module"});

            cmd.setCommand("click", addModuleSubmitBtnLoc, "");
            doCommand("click", new String[]{addModuleSubmitBtnLoc});

            doCommand("assertText", new String[]{addModuleErrorLblLoc, resourceBundle.getObject("registration.admin.module.description.required").toString()});
            System.out.println(++i + ". SDP admin - Add Module - Module description required validation passed");

            internal_comm_reg_test_sdp_admin_add_module_create_module();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_module_create_module() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addModulesLinkLoc, "");
            doCommand("click", new String[]{addModulesLinkLoc});



            cmd.setCommand("enterCharacter", addModulemoduleNameLoc, "Test_Module");
            doCommand("enterCharacter", new String[]{addModulemoduleNameLoc, "Test_Module"});

            cmd.setCommand("enterCharacter", addModuleModuleDescLoc, "Test Module");
            doCommand("enterCharacter", new String[]{addModuleModuleDescLoc, "Test Module"});

            cmd.setCommand("click", addModuleSubmitBtnLoc, "");
            doCommand("click", new String[]{addModuleSubmitBtnLoc});

            doCommand("assertText", new String[]{addModuleErrorLblLoc, resourceBundle.getObject("registration.admin.module.description.required").toString()});
            System.out.println(++i + ". SDP admin - Add Module - Module created");

            internal_comm_reg_test_sdp_admin_manage_module_view_module_info();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_module_view_module_info() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", manageModuleLinkLoc, "");
            doCommand("click", new String[]{manageModuleLinkLoc});

            cmd.setCommand("enterCharacter", manageModuleSearchFieldLoc, "Test_Module");
            doCommand("enterCharacter", new String[]{manageModuleSearchFieldLoc, "Test_Module"});

            cmd.setCommand("mouseClick", manageModuleSearchBtnLoc, "10,2");
            doMouseClick(manageModuleSearchBtnLoc, "10,2");

            cmd.setCommand("click", manageModuleViewInfoLoc, "");
            doCommand("click", new String[]{manageModuleViewInfoLoc});

            cmd.setCommand("mouseClick", manageModuleViewInfoPermNameLoc, "99,12");
            doMouseClick(manageModuleViewInfoPermNameLoc, "99,12");
            System.out.println(++i+". SDP Admin - manage Module Module - Sorting according to permission name");

            cmd.setCommand("mouseClick", manageModuleViewInfoPermDescLoc, "145,7");
            doMouseClick(manageModuleViewInfoPermNameLoc, "145,7");
            System.out.println(++i+". SDP Admin - manage Module Module - Sorting according to permission description");

            cmd.setCommand("mouseClick", manageModuleViewInfoModuleNameLoc, "90,17");
            doMouseClick(manageModuleViewInfoModuleNameLoc, "90,17");
            System.out.println(++i+". SDP Admin - manage Module Module - Sorting according to module name");

            cmd.setCommand("mouseClick", manageModuleCloseInfoLoc, "12,6");
            doMouseClick(manageModuleCloseInfoLoc, "12,6");

            System.out.println(++i + ". SDP admin - Manage Module - Module Info Viewed");

            internal_comm_reg_test_sdp_admin_manage_module_edit_module_info();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_module_edit_module_info() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", manageModuleLinkLoc, "");
            doCommand("click", new String[]{manageModuleLinkLoc});

            cmd.setCommand("enterCharacter", manageModuleSearchFieldLoc, "Test_Module");
            doCommand("enterCharacter", new String[]{manageModuleSearchFieldLoc, "Test_Module"});

            cmd.setCommand("mouseClick", manageModuleSearchBtnLoc, "10,2");
            doMouseClick(manageModuleSearchBtnLoc, "10,2");

            cmd.setCommand("click", manageModuleEditInfoLoc, "");
            doCommand("click", new String[]{manageModuleEditInfoLoc});

            doCommand("assertText", new String[]{manageModuleEditModuleNameLoc, resourceBundle.getObject("registration.admin.module.moduleName").toString()});
            System.out.println(++i+". SDP admin - Manage Module - Edit module name");

            doCommand("assertText", new String[]{manageModuleEditModuleDescLoc, resourceBundle.getObject("registration.admin.module.description").toString()});
            System.out.println(++i+". SDP admin - Manage Module - Edit module description");

            cmd.setCommand("mouseClick", manageModuleCloseEditLoc, "9,6");
            doMouseClick(manageModuleCloseEditLoc, "9,6");

            System.out.println(++i + ". SDP admin - Manage Module - Module Info Edited");

            internal_comm_reg_test_sdp_admin_manage_module_delete_module();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_module_delete_module() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", manageModuleLinkLoc, "");
            doCommand("click", new String[]{manageModuleLinkLoc});

            cmd.setCommand("enterCharacter", manageModuleSearchFieldLoc, "Test_Module");
            doCommand("enterCharacter", new String[]{manageModuleSearchFieldLoc, "Test_Module"});

            cmd.setCommand("mouseClick", manageModuleSearchBtnLoc, "10,2");
            doMouseClick(manageModuleSearchBtnLoc, "10,2");

            cmd.setCommand("click", manageModuleDeleteLoc, "");
            doCommand("click", new String[]{manageModuleDeleteLoc});
            System.out.println(++i+". SDP admin - Manage Module - Delete module clicked");

            cmd.setCommand("click", manageModuleDeleteConfirmNoLoc, "");
            doCommand("click", new String[]{manageModuleDeleteConfirmNoLoc});
            System.out.println(++i+". SDP admin - Manage Module - Delete module not confirmed");

            cmd.setCommand("click", manageModuleDeleteLoc, "");
            doCommand("click", new String[]{manageModuleDeleteLoc});
            System.out.println(++i+". SDP admin - Manage Module - Delete module clicked");

            cmd.setCommand("click", manageModuleDeleteConfirmYesLoc, "");
            doCommand("click", new String[]{manageModuleDeleteConfirmYesLoc});
            System.out.println(++i+". SDP admin - Manage Module - Delete module confirmed");

            cmd.setCommand("mouseClick", manageModuleCloseEditLoc, "9,6");
            doMouseClick(manageModuleCloseEditLoc, "9,6");

            System.out.println(++i + ". SDP admin - Manage Module - Module deleted");

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }
}
