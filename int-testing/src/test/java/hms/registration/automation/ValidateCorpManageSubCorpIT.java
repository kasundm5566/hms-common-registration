/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

public class ValidateCorpManageSubCorpIT extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";

    private static String subCorpUserNameLoc = "vaadin=registration::PID_SUsernameTextField";
    private static String subCorpFirstNameLoc = "vaadin=registration::PID_SFirstNameTextField";
    private static String subCorpStatusComboLoc = "vaadin=registration::PID_SStatusCombo/domChild[1]";
    private static String subCorpActiveStateLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item1";
    private static String subCorpInitialStateLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item2";
    private static String subCorpSearchBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String subCorpViewInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String subCorpEditInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String subCorpViewInfoCloseBtnLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String subCorpBasicDeatilsHeaderLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String subCorpBasicDeatilsUserNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[2]/VLabel[0]";
    private static String subCorpBasicDetailsEmailLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[4]/VLabel[0]";
    private static String subCorpBsicDetailsParentLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[6]/VLabel[0]";
    private static String manageSubCorpHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String manageSubCorpLinkLoc = "vaadin=registration::PID_SmanageSubUserLink/domChild[0]/domChild[0]";

    private static String editSubCorpUserNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String editSubCorpEmailLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String editSubCorpAppStoreLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String editSubCorpProvLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VLabel[0]";
    private static String editSubCorpSolturaLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[24]/VLabel[0]";
    private static String editSubCorpPGWLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[28]/VLabel[0]";
    private static String editSubCorpRegLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[43]/VLabel[0]";
    private static String editSubCorpCloseBtnLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String disableSubCorpUserBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[3]/VButton[0]/domChild[0]/domChild[0]";
    private static String disableConfirmYesBtnLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String disableConfirmNoBtnLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for Corporate User - Manage Corporate User");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_CorporateLoginTest();
    }

    private void internal_CorporateLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("AdminLoginTest");
        setTestName("AdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "corpuser");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "corpuser"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            internal_comm_reg_test_Corp_search_Sub_Corp_user_name();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_Corp_search_Sub_Corp_user_name() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            doCommand("assertText", new String[]{manageSubCorpHeaderLoc, resourceBundle.getObject("registration.subcorporate.users.manage.heading").toString()});
            System.out.println(i + ". Manage SubCorporate Users Heading Passed");

            cmd.setCommand("enterCharacter", subCorpUserNameLoc, "sub");
            doCommand("enterCharacter", new String[]{subCorpUserNameLoc, "sub"});

            cmd.setCommand("click", subCorpSearchBtnLoc, "");
            doCommand("click", new String[]{subCorpSearchBtnLoc});

            cmd.setCommand("click", subCorpViewInfoLoc, "");
            doCommand("click", new String[]{subCorpViewInfoLoc});

            System.out.println(++i + ". Manage SubCorp - Found Users for user name \"sub\"");

            checkUserInfo(cmd);

            internal_comm_reg_test_Corp_search_Sub_Corp_first_name();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_Corp_search_Sub_Corp_first_name() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_first_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_first_name");

        try {
            cmd.setCommand("click", manageSubCorpLinkLoc, "");
            doCommand("click", new String[]{manageSubCorpLinkLoc});

            cmd.setCommand("enterCharacter", subCorpFirstNameLoc, "subcorp");
            doCommand("enterCharacter", new String[]{subCorpFirstNameLoc, "subcorp"});

            cmd.setCommand("click", subCorpSearchBtnLoc, "");
            doCommand("click", new String[]{subCorpSearchBtnLoc});

            cmd.setCommand("click", subCorpViewInfoLoc, "");
            doCommand("click", new String[]{subCorpViewInfoLoc});

            System.out.println("\n\n" + ++i + ". Manage SubCorp - Found Users for first name \"subcorp\"");

            checkUserInfo(cmd);

            internal_comm_reg_test_Corp_search_Sub_Corp_active_state();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_Corp_search_Sub_Corp_active_state() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_state");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_state");

        try {
            cmd.setCommand("click", manageSubCorpLinkLoc, "");
            doCommand("click", new String[]{manageSubCorpLinkLoc});

            cmd.setCommand("mouseClick", subCorpStatusComboLoc, "11,-207");
            doMouseClick(subCorpStatusComboLoc, "11,-207");

            cmd.setCommand("mouseClick", subCorpActiveStateLoc, "36,10");
            doMouseClick(subCorpActiveStateLoc, "36,10");

            cmd.setCommand("click", subCorpSearchBtnLoc, "");
            doCommand("click", new String[]{subCorpSearchBtnLoc});

            cmd.setCommand("click", subCorpViewInfoLoc, "");
            doCommand("click", new String[]{subCorpViewInfoLoc});

            System.out.println("\n\n" + ++i + ". Manage SubCorp - Found Users for state \"ACTIVE\"");

            checkUserInfo(cmd);

            internal_comm_reg_test_Corp_search_Sub_Corp_initial_state();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_Corp_search_Sub_Corp_initial_state() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_state");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_state");

        try {
            cmd.setCommand("click", manageSubCorpLinkLoc, "");
            doCommand("click", new String[]{manageSubCorpLinkLoc});

            cmd.setCommand("mouseClick", subCorpStatusComboLoc, "11,-207");
            doMouseClick(subCorpStatusComboLoc, "11,-207");

            cmd.setCommand("mouseClick", subCorpInitialStateLoc, "29,12");
            doMouseClick(subCorpInitialStateLoc, "29,12");

            cmd.setCommand("click", subCorpSearchBtnLoc, "");
            doCommand("click", new String[]{subCorpSearchBtnLoc});

            cmd.setCommand("click", subCorpViewInfoLoc, "");
            doCommand("click", new String[]{subCorpViewInfoLoc});

            System.out.println("\n\n" + ++i + ". Manage SubCorp - Found Users for state \"INITIAL\"");

            checkUserInfo(cmd);

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void checkUserInfo(CurrentCommand cmd) {

        doCommand("assertText", new String[]{subCorpBasicDeatilsHeaderLoc, resourceBundle.getObject("registration.user.basic.details.heading").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - Header viewing");
        doCommand("assertText", new String[]{subCorpBasicDeatilsUserNameLoc, resourceBundle.getObject("registration.user.username").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - User name viewing");
        doCommand("assertText", new String[]{subCorpBasicDetailsEmailLoc, resourceBundle.getObject("registration.user.email").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - email viewing");
        doCommand("assertText", new String[]{subCorpBsicDetailsParentLoc, resourceBundle.getObject("registration.admin.user.corporate.parent").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - Corporate Parent viewing");

        cmd.setCommand("mouseClick", subCorpViewInfoCloseBtnLoc, "13,10");
        doMouseClick(subCorpViewInfoCloseBtnLoc, "13,10");

        cmd.setCommand("click", subCorpEditInfoLoc, "");
        doCommand("click", new String[]{subCorpEditInfoLoc});

        System.out.println("\n\n" + ++i + ". Manage SubCorp - Edit user");

        doCommand("assertText", new String[]{editSubCorpUserNameLoc, resourceBundle.getObject("registration.user.username").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - User name Viewing");
        doCommand("assertText", new String[]{editSubCorpEmailLoc, resourceBundle.getObject("registration.user.email").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - email Viewing");
        doCommand("assertText", new String[]{editSubCorpAppStoreLoc, "appstore"});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - App Store Roles Viewing");
        doCommand("assertText", new String[]{editSubCorpProvLoc, "provisioning"});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - Provisioning  Roles Viewing");
        doCommand("assertText", new String[]{editSubCorpSolturaLoc, "soltura"});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - Soltura Roles Viewing");
        doCommand("assertText", new String[]{editSubCorpPGWLoc, "pgw-selfcare"});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - PGW Roles Viewing");
        doCommand("assertText", new String[]{editSubCorpRegLoc, "registration"});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User edit details - Registration Roles Viewing");

        cmd.setCommand("mouseClick", editSubCorpCloseBtnLoc, "11,10");
        doMouseClick(editSubCorpCloseBtnLoc, "11,10");

        System.out.println("\n\n" + ++i + ". Manage SubCorp - Disable/Enable user");

        cmd.setCommand("click", disableSubCorpUserBtnLoc, "");
        doCommand("click", new String[]{disableSubCorpUserBtnLoc});

        System.out.println(++i + ". Manage SubCorp - Disable user");

        cmd.setCommand("click", disableConfirmYesBtnLoc, "");
        doCommand("click", new String[]{disableConfirmYesBtnLoc});

        System.out.println(++i + ". Manage SubCorp - User disabling confirmed");

        cmd.setCommand("click", disableSubCorpUserBtnLoc, "");
        doCommand("click", new String[]{disableSubCorpUserBtnLoc});

        System.out.println(++i + ". Manage SubCorp - Enable user");

        cmd.setCommand("click", disableConfirmYesBtnLoc, "");
        doCommand("click", new String[]{disableConfirmYesBtnLoc});

        System.out.println(++i + ". Manage SubCorp - User enabling confirmed");

        cmd.setCommand("click", disableSubCorpUserBtnLoc, "");
        doCommand("click", new String[]{disableSubCorpUserBtnLoc});

        System.out.println(++i + ". Manage SubCorp - Disable user");

        cmd.setCommand("click", disableConfirmNoBtnLoc, "");
        doCommand("click", new String[]{disableConfirmNoBtnLoc});

        System.out.println(++i + ". Manage SubCorp - User disabling not confirmed");
    }

}
