/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

public class ValidateAdminRegAddAdmin extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";

    private static String addAdminLinkLoc = "vaadin=registration::PID_SAddUserLink/domChild[0]/domChild[0]";
    private static String addAdminHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String submitBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String cancelBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String firstNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[0]";
    private static String lastNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[1]";
    private static String emailLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[2]";
    private static String userNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[3]";
    private static String passwordLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VPasswordField[0]";
    private static String confirmPasswordLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VPasswordField[1]";
    private static String msisdnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[4]";
    private static String consumerModuleGroupLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VListSelect[0]/domChild[0]";
    private static String errorLblLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";

    private static String basicDetailsHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String confirmUserNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[2]/VLabel[0]";
    private static String confirmEmailLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[4]/VLabel[0]";
    private static String personalDetailsHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[7]/VLabel[0]";
    private static String confirmFirstNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[9]/VLabel[0]";
    private static String confirmLastNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[11]/VLabel[0]";
    private static String moduleGroupHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[16]/VLabel[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for SDP Admin - Add administrator");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_SDPAdminLoginTest();
    }

    private void internal_SDPAdminLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_SDPAdminLoginTest");
        setTestName("internal_SDPAdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "sdpadmin");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "sdpadmin"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            internal_comm_reg_test_sdp_admin_add_admin_first_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_first_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            doCommand("assertText", new String[]{addAdminHeaderLoc, resourceBundle.getObject("registration.admin.users.add.heading").toString()});
            System.out.println(i + ". SDP admin - Add administrator - Header Passed");

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.first.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - first name required validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_last_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_last_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.last.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Last name required validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_email_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_email_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.email.required").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Email required validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_email_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_email_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email");
            doCommand("enterCharacter", new String[]{emailLoc, "email"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.invalid.email").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Email format validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_user_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_user_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.invalid.email").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Email format validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_user_name_not_available_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_user_name_not_available_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "corpuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "corpuser"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.username.not.available").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - User name not available validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_password_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_password_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.password.required").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Password required validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_password_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_password_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("enterCharacter", passwordLoc, "123456789");
            doCommand("enterCharacter", new String[]{passwordLoc, "123456789"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.invalid.password").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Password format validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_password_confirm_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_password_confirm_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.retype.password.required").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Password confirm required validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_passwords_not_matching_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_passwords_not_matching_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswordLoc, "test12345#");
            doCommand("enterCharacter", new String[]{confirmPasswordLoc, "test12345#"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.password.does.not.match").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Passwords not matching validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_msisdn_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_msisdn_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", msisdnLoc, "abcdefgh");
            doCommand("enterCharacter", new String[]{msisdnLoc, "abcdefgh"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.invalid.msisdn").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - MSISDN Format validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_module_group_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_module_group_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswordLoc, "test123#"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            doCommand("assertText", new String[]{errorLblLoc, resourceBundle.getObject("registration.user.groups.required").toString()});
            System.out.println(++i + ". SDP admin - Add administrator - Module Group required validation passed");

            internal_comm_reg_test_sdp_admin_add_admin_success();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_admin_success() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {
            cmd.setCommand("click", addAdminLinkLoc, "");
            doCommand("click", new String[]{addAdminLinkLoc});

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "admin");
            doCommand("enterCharacter", new String[]{lastNameLoc, "admin"});

            cmd.setCommand("enterCharacter", emailLoc, "email@email.com");
            doCommand("enterCharacter", new String[]{emailLoc, "email@email.com"});

            cmd.setCommand("enterCharacter", userNameLoc, "testadmin");
            doCommand("enterCharacter", new String[]{userNameLoc, "testadmin"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswordLoc, "test123#"});

            cmd.setCommand("addSelection", consumerModuleGroupLoc, "label=CONSUMER_USER");
            doCommand("addSelection", new String[]{consumerModuleGroupLoc, "label=CONSUMER_USER"});

            cmd.setCommand("click", submitBtnLoc, "");
            doCommand("click", new String[]{submitBtnLoc});

            validateConfirmation();

            System.out.println(++i + ". SDP admin - Add administrator - Admin added Successfully");

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void validateConfirmation() {

        doCommand("assertText",new String[]{basicDetailsHeaderLoc, resourceBundle.getObject("registration.user.basic.details.heading").toString()});
        doCommand("assertText",new String[]{confirmUserNameLoc,resourceBundle.getObject("registration.user.username").toString()});
        doCommand("assertText",new String[]{confirmEmailLoc,resourceBundle.getObject("registration.user.email").toString()});
        doCommand("assertText",new String[]{personalDetailsHeaderLoc,resourceBundle.getObject("registration.settings.personal.details.heading").toString()});
        doCommand("assertText",new String[]{confirmFirstNameLoc,resourceBundle.getObject("registration.user.firstName").toString()});
        doCommand("assertText",new String[]{confirmLastNameLoc,resourceBundle.getObject("registration.user.lastName").toString()});
        doCommand("assertText",new String[]{moduleGroupHeaderLoc,resourceBundle.getObject("registration.admin.role.groups").toString()});
    }
}
