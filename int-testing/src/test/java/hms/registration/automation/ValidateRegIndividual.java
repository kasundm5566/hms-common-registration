/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;

import java.util.ResourceBundle;

import com.vaadin.testbench.util.CurrentCommand;

public class ValidateRegIndividual extends AbstractVaadinTestCase{

    private boolean writeScreenshots = false;
    private static String headerLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String firstNameLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VTextField[0]";
    private static String lastNameLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[1]/VTextField[0]";
    private static String birthdayMonthLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[4]/VHorizontalLayout[0]/ChildComponentContainer[0]/VFilterSelect[0]/domChild[0]";
    private static String birthdayDayLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[4]/VHorizontalLayout[0]/ChildComponentContainer[1]/VTextField[0]";
    private static String birthdaYearLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[4]/VHorizontalLayout[0]/ChildComponentContainer[2]/VTextField[0]";
    private static String operatorLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[11]/VFilterSelect[0]/domChild[0]";
    private static String operatorMenuLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[11]/VFilterSelect[0]/domChild[1]";
    private static String safaricomLoc = "vaadin=registrationdefault::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item1";
    private static String phoneNoLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[12]/VTextField[0]";
    private static String mpinLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[15]/VPasswordField[0]";
    private static String mpinCnfirmLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[16]/VPasswordField[0]";
    private static String usrNameLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[17]/VTextField[0]";
    private static String passwordLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[18]/VPasswordField[0]";
    private static String passwordConfirmLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[19]/VPasswordField[0]";
    private static String termsCheckLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[22]/VCheckBox[0]/domChild[0]";
    private static String submitBtnLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String resetBtnLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String errorLabelLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for individual User");
//        setBrowserIdentifier("linux-firefox3");
//        startBrowser("linux-firefox3");
        internal_comm_reg_test_first_name_validate();
    }

    private void submitForm(CurrentCommand cmd) {
        cmd.setCommand("mouseClick", termsCheckLoc, "7,-1028");
        doMouseClick(termsCheckLoc, "7,-1028");

        cmd.setCommand("click", submitBtnLoc, "");
        doCommand("click", new String[]{submitBtnLoc});
    }

    private void clearForm(CurrentCommand cmd) {
        cmd.setCommand("click", resetBtnLoc, "");
        doCommand("click", new String[]{resetBtnLoc});
    }

    private void internal_comm_reg_test_first_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            System.out.println("Validations for Individual User");

            open("/registration/default/#common/individual");

            doCommand("assertText", new String[]{headerLoc, (resourceBundle.getObject("registration.user.create.account").toString())});
            System.out.println("Header Validation Passed");

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.first.name.required").toString())});
            System.out.println(++i + ". First name required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_last_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_last_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.last.name.required").toString())});
            System.out.println(++i + ". Last name required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_user_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_user_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.username.required").toString())});
            System.out.println(++i + ". User name required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_user_name_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_user_name_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "abc");
            doCommand("enterCharacter", new String[]{usrNameLoc, "abc"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.invalid.username").toString())});
            System.out.println(++i + ". User name required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_password_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_password_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.password.required").toString())});
            System.out.println(++i + ". Password required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_password_confirm_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_password_confirm_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.retype.password.required").toString())});
            System.out.println(++i + ". Password confirm required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_passwords_not_match_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_passwords_not_match_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test12345#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test12345#"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.password.does.not.match").toString())});
            System.out.println(++i + ". Passwords not matching validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_password_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_password_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test1234");
            doCommand("enterCharacter", new String[]{passwordLoc, "test1234"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test1234");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test1234"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.invalid.password").toString())});
            System.out.println(++i + ". Passwords format validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_operator_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_operator_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.operator.required").toString())});
            System.out.println(++i + ". Operator validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_msisdn_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_msisdn_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.msisdn.required").toString())});
            System.out.println(++i + ". MSISDN required validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_msisdn_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_msisdn_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            cmd.setCommand("enterCharacter", phoneNoLoc, "123456789");
            doCommand("enterCharacter", new String[]{phoneNoLoc, "123456789"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.invalid.msisdn").toString())});
            System.out.println(++i + ". MSISDN format validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_msisdn_not_available_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_msisdn_not_available_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            cmd.setCommand("enterCharacter", phoneNoLoc, "254240200200");
            doCommand("enterCharacter", new String[]{phoneNoLoc, "254240200200"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.msisdn.not.available").toString())});
            System.out.println(++i + ". MSISDN not available validation passed");

            clearForm(cmd);

            internal_comm_reg_test_indiv_mpin_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_mpin_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            cmd.setCommand("enterCharacter", phoneNoLoc, "254211123456");
            doCommand("enterCharacter", new String[]{phoneNoLoc, "254211123456"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.invalid.msisdn").toString())});
            System.out.println(++i + ". MSISDN/MPin issue Here<<");

            clearForm(cmd);

            internal_comm_reg_test_indiv_mpin_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_mpin_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            cmd.setCommand("enterCharacter", phoneNoLoc, "254211123456");
            doCommand("enterCharacter", new String[]{phoneNoLoc, "25411123456"});

            cmd.setCommand("enterCharacter", mpinLoc, "abcdef");
            doCommand("enterCharacter", new String[]{mpinLoc, "abcdef"});

            cmd.setCommand("enterCharacter", mpinCnfirmLoc, "abcdef");
            doCommand("enterCharacter", new String[]{mpinCnfirmLoc, "abcdef"});

            submitForm(cmd);

            internal_comm_reg_test_indiv_mpin_confirm_validate();

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.invalid.msisdn").toString())});
            System.out.println(++i + ". MSISDN/MPin issue Here<<");

            clearForm(cmd);

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_mpin_confirm_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            cmd.setCommand("enterCharacter", phoneNoLoc, "2542411123456");
            doCommand("enterCharacter", new String[]{phoneNoLoc, "254211123456"});

            cmd.setCommand("enterCharacter", mpinLoc, "1234");
            doCommand("enterCharacter", new String[]{mpinLoc, "1234"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.invalid.msisdn").toString())});
            System.out.println(++i + ". MSISDN/MPin issue Here<<");

            clearForm(cmd);

            internal_comm_reg_test_indiv_captcha_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_indiv_captcha_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_validate");
        setTestName("comm_reg_test_user_name_validate");
        try {

            cmd.setCommand("enterCharacter", firstNameLoc, "test");
            doCommand("enterCharacter", new String[]{firstNameLoc, "test"});

            cmd.setCommand("enterCharacter", lastNameLoc, "user");
            doCommand("enterCharacter", new String[]{lastNameLoc, "user"});

            cmd.setCommand("enterCharacter", usrNameLoc, "testIndiviUser");
            doCommand("enterCharacter", new String[]{usrNameLoc, "testIndiviUser"});

            cmd.setCommand("enterCharacter", passwordLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordLoc, "test123#"});

            cmd.setCommand("enterCharacter", passwordConfirmLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwordConfirmLoc, "test123#"});

            cmd.setCommand("mouseClick", operatorMenuLoc, "12,-486");
            doMouseClick(operatorMenuLoc, "12,-486");

            cmd.setCommand("mouseClick", safaricomLoc, "30,9");
            doMouseClick(safaricomLoc, "30,9");

            cmd.setCommand("enterCharacter", phoneNoLoc, "2542411123456");
            doCommand("enterCharacter", new String[]{phoneNoLoc, "2542411123456"});

            cmd.setCommand("enterCharacter", mpinLoc, "1234");
            doCommand("enterCharacter", new String[]{mpinLoc, "1234"});

            cmd.setCommand("enterCharacter", mpinCnfirmLoc, "1234");
            doCommand("enterCharacter", new String[]{mpinCnfirmLoc, "1234"});

            submitForm(cmd);

            doCommand("assertText", new String[]{errorLabelLoc, (resourceBundle.getObject("registration.user.captcha.code.invalid").toString())});
            System.out.println(++i + ". captcha validation Passed ");

            clearForm(cmd);

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

}
