/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

public class ValidateAdminRegManageUsers extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";

    private static String manageUsersHeadingLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String manageUsersLinkLoc = "vaadin=registration::PID_SmanageUserLink/domChild[0]/domChild[0]";
    private static String manageUsersUserNameSearchLoc = "vaadin=registration::PID_SUsernameTextField";
    private static String manageUsersFirstNameSearchLoc = "vaadin=registration::PID_SFirstNameTextField";
    private static String manageUsersStatusSearchLoc = "vaadin=registration::PID_SStatusCombo/domChild[1]";
    private static String manageUsersTypeSearchLoc = "vaadin=registration::PID_SUserSearchForm/ChildComponentContainer[3]/VFilterSelect[0]/domChild[1]";
    private static String manageUsersSearchBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";

    private static String manageUsersStatusActiveLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item1";
    private static String manageUsersTypeCorporateLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item2";
    private static String manageUsersTypeSubCorpLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item4";
    private static String manageUsersTypeIndividualLoc = "vaadin=registration::Root/VFilterSelect$SuggestionPopup[0]/VFilterSelect$SuggestionMenu[0]#item1";

    private static String manageUsersViewInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageUsersViewInfoHeaderLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String manageUsersViewInfoUserNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[2]/VLabel[0]";
    private static String manageUsersViewInfoEmailLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[4]/VLabel[0]";
    private static String manageUsersViewInfoCloseBtnLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String manageUsersEditInfoBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageUsersEditInfoUserName = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageUsersEditInfoEmailLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageUsersEditInfoAppStoreRolesLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String manageUsersEditInfoProvRolesLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[15]/VLabel[0]";
    private static String manageUsersEditInfoRegistrationRolesLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[42]/VLabel[0]";
    private static String manageUsersEditInfoCloseBtnLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String manageUsersDisbleBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[3]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageUsersDisbleConfirmYesBtnLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageUsersDisbleConfirmNoBtnLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";

    private static String manageUsersIndivUserEditUserNameLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageUsersIndivUserEditModuleGroupLoc = "vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageUsersIndivUserEditInfoCloseBtnLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

       private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for SDP Admin - Manage Users");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_SDPAdminLoginTest();
    }

    private void internal_SDPAdminLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_SDPAdminLoginTest");
        setTestName("internal_SDPAdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "sdpadmin");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "sdpadmin"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            internal_comm_reg_test_sdp_admin_search_users_user_name();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_search_users_user_name() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {

            doCommand("assertText", new String[]{manageUsersHeadingLoc, resourceBundle.getObject("registration.admin.users.manage.heading").toString()});
            System.out.println(i + ". Manage Users Heading Passed");

            cmd.setCommand("enterCharacter", manageUsersUserNameSearchLoc, "sub");
            doCommand("enterCharacter", new String[]{manageUsersUserNameSearchLoc, "sub"});

            cmd.setCommand("click", manageUsersSearchBtnLoc, "");
            doCommand("click", new String[]{manageUsersSearchBtnLoc});

            cmd.setCommand("click", manageUsersViewInfoLoc, "");
            doCommand("click", new String[]{manageUsersViewInfoLoc});

            System.out.println(++i + ". Manage Users - Found Users for user name \"sub\"");

            checkUserInfo(cmd);

            internal_comm_reg_test_sdp_admin_search_users_first_name();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_search_users_first_name() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {

            cmd.setCommand("click", manageUsersLinkLoc, "");
            doCommand("click", new String[]{manageUsersLinkLoc});

            cmd.setCommand("enterCharacter", manageUsersFirstNameSearchLoc, "subcorp");
            doCommand("enterCharacter", new String[]{manageUsersFirstNameSearchLoc, "subcorp"});

            cmd.setCommand("click", manageUsersSearchBtnLoc, "");
            doCommand("click", new String[]{manageUsersSearchBtnLoc});

            cmd.setCommand("click", manageUsersViewInfoLoc, "");
            doCommand("click", new String[]{manageUsersViewInfoLoc});

            System.out.println(++i + ". Manage Users - Found Users for first name \"subcorp\"");

            checkUserInfo(cmd);

            internal_comm_reg_test_sdp_admin_search_users_active_status();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_search_users_active_status() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {

            cmd.setCommand("click", manageUsersLinkLoc, "");
            doCommand("click", new String[]{manageUsersLinkLoc});

            cmd.setCommand("mouseClick", manageUsersStatusSearchLoc, "8,-252");
            doMouseClick(manageUsersStatusSearchLoc, "8,-252");

            cmd.setCommand("mouseClick", manageUsersStatusActiveLoc, "21,10");
            doMouseClick(manageUsersStatusActiveLoc, "21,10");

            cmd.setCommand("click", manageUsersSearchBtnLoc, "");
            doCommand("click", new String[]{manageUsersSearchBtnLoc});

            cmd.setCommand("click", manageUsersViewInfoLoc, "");
            doCommand("click", new String[]{manageUsersViewInfoLoc});

            System.out.println(++i + ". Manage Users - Found Users for status \"ACTIVE\"");

            checkUserInfo(cmd);

            internal_comm_reg_test_sdp_admin_search_users_individual_type();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_search_users_individual_type() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {

            cmd.setCommand("click", manageUsersLinkLoc, "");
            doCommand("click", new String[]{manageUsersLinkLoc});

            cmd.setCommand("mouseClick", manageUsersTypeSearchLoc, "10,-226");
            doMouseClick(manageUsersTypeSearchLoc, "10,-226");

            cmd.setCommand("mouseClick", manageUsersTypeIndividualLoc, "13,9");
            doMouseClick(manageUsersTypeIndividualLoc, "13,9");

            cmd.setCommand("click", manageUsersSearchBtnLoc, "");
            doCommand("click", new String[]{manageUsersSearchBtnLoc});

            cmd.setCommand("click", manageUsersViewInfoLoc, "");
            doCommand("click", new String[]{manageUsersViewInfoLoc});

            System.out.println(++i + ". Manage Users - Found Users for status \"ACTIVE\"");

            checkUserInfo(cmd);

            editInfoIndividual(cmd);

            internal_comm_reg_test_sdp_admin_search_users_sub_corp_type();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_search_users_sub_corp_type() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");
        setTestName("internal_comm_reg_test_Corp_search_Sub_Corp_user_name");

        try {

            cmd.setCommand("click", manageUsersLinkLoc, "");
            doCommand("click", new String[]{manageUsersLinkLoc});

            cmd.setCommand("mouseClick", manageUsersTypeSearchLoc, "9,-231");
            doMouseClick(manageUsersTypeSearchLoc, "9,-231");

            cmd.setCommand("mouseClick", manageUsersTypeSubCorpLoc, "57,10");
            doMouseClick(manageUsersTypeSubCorpLoc, "57,10");

            cmd.setCommand("click", manageUsersSearchBtnLoc, "");
            doCommand("click", new String[]{manageUsersSearchBtnLoc});

            cmd.setCommand("click", manageUsersViewInfoLoc, "");
            doCommand("click", new String[]{manageUsersViewInfoLoc});

            System.out.println(++i + ". Manage Users - Found Users for status \"ACTIVE\"");

            checkUserInfo(cmd);

            editInfoSubCorp(cmd);

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void checkUserInfo(CurrentCommand cmd) {

        doCommand("assertText", new String[]{manageUsersViewInfoHeaderLoc, resourceBundle.getObject("registration.user.basic.details.heading").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - Header viewing");
        doCommand("assertText", new String[]{manageUsersViewInfoUserNameLoc, resourceBundle.getObject("registration.user.username").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - User name viewing");
        doCommand("assertText", new String[]{manageUsersViewInfoEmailLoc, resourceBundle.getObject("registration.user.email").toString()});
        System.out.println(++i + ". Manage SubCorp - SubCorporate User view details - email viewing");

        cmd.setCommand("mouseClick", manageUsersViewInfoCloseBtnLoc, "13,10");
        doMouseClick(manageUsersViewInfoCloseBtnLoc, "13,10");

        System.out.println("\n\n" + ++i + ". Manage SubCorp - Disable/Enable user");

        cmd.setCommand("click", manageUsersDisbleBtnLoc, "");
        doCommand("click", new String[]{manageUsersDisbleBtnLoc});

        System.out.println(++i + ". Manage SubCorp - Disable user");

        cmd.setCommand("click", manageUsersDisbleConfirmYesBtnLoc, "");
        doCommand("click", new String[]{manageUsersDisbleConfirmYesBtnLoc});

        System.out.println(++i + ". Manage SubCorp - User disabling confirmed");

        cmd.setCommand("click", manageUsersDisbleBtnLoc, "");
        doCommand("click", new String[]{manageUsersDisbleBtnLoc});

        System.out.println(++i + ". Manage SubCorp - Enable user");

        cmd.setCommand("click", manageUsersDisbleConfirmYesBtnLoc, "");
        doCommand("click", new String[]{manageUsersDisbleConfirmYesBtnLoc});

        System.out.println(++i + ". Manage SubCorp - User enabling confirmed");

        cmd.setCommand("click", manageUsersDisbleBtnLoc, "");
        doCommand("click", new String[]{manageUsersDisbleBtnLoc});

        System.out.println(++i + ". Manage SubCorp - Disable user");

        cmd.setCommand("click", manageUsersDisbleConfirmNoBtnLoc, "");
        doCommand("click", new String[]{manageUsersDisbleConfirmNoBtnLoc});

        System.out.println(++i + ". Manage SubCorp - User disabling not confirmed");
    }

    private void editInfoSubCorp(CurrentCommand cmd) {

        cmd.setCommand("click", manageUsersEditInfoBtnLoc, "");
        doCommand("click", new String[]{manageUsersEditInfoBtnLoc});

        System.out.println("\n\n" + ++i + ". Manage SubCorp - Edit user");

        doCommand("assertText", new String[]{manageUsersEditInfoUserName, resourceBundle.getObject("registration.user.username").toString()});
        System.out.println(++i + ". Manage users -  User edit details - User name Viewing");
        doCommand("assertText", new String[]{manageUsersEditInfoEmailLoc, resourceBundle.getObject("registration.user.email").toString()});
        System.out.println(++i + ". Manage user - User edit details - email Viewing");
        doCommand("assertText", new String[]{manageUsersEditInfoAppStoreRolesLoc, "appstore"});
        System.out.println(++i + ". Manage user - User edit details - App Store Roles Viewing");
        doCommand("assertText", new String[]{manageUsersEditInfoProvRolesLoc, "provisioning"});
        System.out.println(++i + ". Manage user - User edit details - Provisioning  Roles Viewing");
        doCommand("assertText", new String[]{manageUsersEditInfoRegistrationRolesLoc, "registration"});
        System.out.println(++i + ". Manage user - User edit details - Registration Roles Viewing");

        cmd.setCommand("mouseClick", manageUsersEditInfoCloseBtnLoc, "11,10");
        doMouseClick(manageUsersEditInfoCloseBtnLoc, "11,10");
    }

    private void editInfoIndividual(CurrentCommand cmd) {

        cmd.setCommand("click", manageUsersEditInfoBtnLoc, "");
        doCommand("click", new String[]{manageUsersEditInfoBtnLoc});

        System.out.println("\n\n" + ++i + ". Manage SubCorp - Edit user");

        doCommand("assertText", new String[]{manageUsersIndivUserEditUserNameLoc, resourceBundle.getObject("registration.user.username").toString()});
        System.out.println(++i + ". Manage users -  Individual user edit details - User name Viewing");
        doCommand("assertText", new String[]{manageUsersIndivUserEditModuleGroupLoc, resourceBundle.getObject("registration.user.groups").toString()});
        System.out.println(++i + ". Manage user - Individual user edit details - Module groups Viewing");

        cmd.setCommand("mouseClick", manageUsersIndivUserEditInfoCloseBtnLoc, "11,10");
        doMouseClick(manageUsersIndivUserEditInfoCloseBtnLoc, "11,10");
    }
}
