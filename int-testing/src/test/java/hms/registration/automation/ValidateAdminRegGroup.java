/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;


/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

public class ValidateAdminRegGroup extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";

    private static String groupsLinkLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VAccordion[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupLinkLoc = "vaadin=registration::PID_SmanageGroupLink/domChild[0]/domChild[0]";
    private static String manageGroupHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String manageGroupSearchBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[2]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageGroupSearchFieldLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VTextField[0]";
    private static String manageGroupViewInfoLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageGroupInfoUsrNameLoc = "vaadin=registration::PID_SUserSearchForm/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupInfofstNameLoc = "vaadin=registration::PID_SUserSearchForm/domChild[0]/domChild[1]/domChild[0]/domChild[0]";
    private static String manageGroupInfoStatusLoc = "vaadin=registration::PID_SUserSearchForm/domChild[0]/domChild[2]/domChild[0]/domChild[0]";
    private static String manageGroupInfoTypeLoc = "vaadin=registration::PID_SUserSearchForm/domChild[0]/domChild[3]/domChild[0]/domChild[0]";
    private static String manageGroupCloseInfoBtnLoc = "vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";

    private static String manageGroupEditInfoLoc="vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String manaegGroupInfoGrpNameLoc="vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupInfoGrpDescLoc ="vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[1]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupInfoGrpPermissionLoc ="vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[2]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupInfoGrpUsersLoc ="vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/domChild[0]/domChild[1]/domChild[3]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupCloseEditInfoBtnLoc ="vaadin=registration::/VWindow[0]/domChild[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String manageGroupDeleteGroupLoc="vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VScrollTable[0]/FocusableScrollPanel[0]/VScrollTable$VScrollTableBody[0]/VScrollTable$VScrollTableBody$VScrollTableRow[0]/VHorizontalLayout[0]/ChildComponentContainer[2]/VButton[0]/domChild[0]/domChild[0]";
    private static String manageGroupDeleteGroupYesBtnLoc="vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]";
    private static String manageGroupDeleteGroupNoBtnLoc="vaadin=registration::/VWindow[0]/FocusableScrollPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";


    private static String addGroupLinkLoc = "vaadin=registration::PID_SaddGroupLink/domChild[0]/domChild[0]";
    private static String addGroupHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";
    private static String addGroupNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[0]";
    private static String addGroupDescriptionLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextArea[0]";
    private static String addGroupRegLoginRoleLeftLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#leftSelect";
    private static String addGroupAddRoleBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#add";
    private static String addGroupRegLoginRoleRightLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#rightSelect";
    private static String addGroupRemoveRoleBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTwinColSelect[0]#remove";
    private static String addGroupAddUserLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VListSelect[0]/domChild[0]";
    private static String addGroupSubmitBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String addGroupErrorLblLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for SDP Admin - Manage Groups");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_SDPAdminLoginTest();
    }

    private void internal_SDPAdminLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_SDPAdminLoginTest");
        setTestName("internal_SDPAdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "sdpadmin");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "sdpadmin"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            internal_comm_reg_test_sdp_admin_add_group_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_group_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {
            cmd.setCommand("mouseClick", groupsLinkLoc, "33,-168");
            doMouseClick(groupsLinkLoc, "33,-168");

            cmd.setCommand("click", addGroupLinkLoc, "");
            doCommand("click", new String[]{addGroupLinkLoc});

            doCommand("assertText", new String[]{addGroupHeaderLoc, resourceBundle.getObject("registration.admin.groups.add.heading").toString()});
            System.out.println(i + ". SDP admin - Add Group - Header Passed");

            cmd.setCommand("click", addGroupSubmitBtnLoc, "");
            doCommand("click", new String[]{addGroupSubmitBtnLoc});

            doCommand("assertText", new String[]{addGroupErrorLblLoc, resourceBundle.getObject("registration.admin.group.name.required").toString()});
            System.out.println(++i + ". SDP admin - Add Group - Group Name required validation passed");

            internal_comm_reg_test_sdp_admin_add_group_description_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_group_description_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addGroupLinkLoc, "");
            doCommand("click", new String[]{addGroupLinkLoc});

            cmd.setCommand("enterCharacter", addGroupNameLoc, "TEST_GROUP");
            doCommand("enterCharacter", new String[]{addGroupNameLoc, "TEST_GROUP"});

            cmd.setCommand("click", addGroupSubmitBtnLoc, "");
            doCommand("click", new String[]{addGroupSubmitBtnLoc});

            doCommand("assertText", new String[]{addGroupErrorLblLoc, resourceBundle.getObject("registration.admin.group.description.required").toString()});
            System.out.println(++i + ". SDP admin - Add Group - Group description required validation passed");

            internal_comm_reg_test_sdp_admin_add_group_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_add_group_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", addGroupLinkLoc, "");
            doCommand("click", new String[]{addGroupLinkLoc});

            cmd.setCommand("enterCharacter", addGroupNameLoc, "TEST_GROUP");
            doCommand("enterCharacter", new String[]{addGroupNameLoc, "TEST_GROUP"});

            cmd.setCommand("enterCharacter", addGroupDescriptionLoc, "Test group description");
            doCommand("enterCharacter", new String[]{addGroupDescriptionLoc, "Test group description"});

            cmd.setCommand("addSelection", addGroupRegLoginRoleLeftLoc, "label=CONSUMER_USER");
            doCommand("addSelection", new String[]{addGroupRegLoginRoleLeftLoc, "label=CONSUMER_USER"});

            cmd.setCommand("click", addGroupAddRoleBtnLoc, "");
            doCommand("click", new String[]{addGroupAddRoleBtnLoc});

            cmd.setCommand("click", addGroupAddRoleBtnLoc, "");
            doCommand("click", new String[]{addGroupAddRoleBtnLoc});

            cmd.setCommand("removeSelection", addGroupRegLoginRoleRightLoc, "label=CONSUMER_USER");
            doCommand("removeSelection", new String[]{addGroupRegLoginRoleRightLoc, "label=CONSUMER_USER"});

            cmd.setCommand("click", addGroupRemoveRoleBtnLoc, "");
            doCommand("click", new String[]{addGroupRemoveRoleBtnLoc});

            cmd.setCommand("click", addGroupSubmitBtnLoc, "");
            doCommand("click", new String[]{addGroupSubmitBtnLoc});

            cmd.setCommand("addSelection", addGroupAddUserLoc, "label=admin");
            doCommand("addSelection", new String[]{addGroupAddUserLoc, "label=admin"});

            doCommand("assertText", new String[]{addGroupErrorLblLoc, resourceBundle.getObject("registration.admin.group.description.required").toString()});
            System.out.println(++i + ". SDP admin - Add Group - Group description required validation passed");

            internal_comm_reg_test_sdp_admin_manage_group_info_view_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_group_info_view_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", manageGroupLinkLoc, "");
            doCommand("click", new String[]{manageGroupLinkLoc});

            doCommand("assertText", new String[]{manageGroupHeaderLoc, resourceBundle.getObject("registration.admin.groups.add.heading").toString()});
            System.out.println(i + ". SDP admin - Manager Group - Header Passed");

            cmd.setCommand("enterCharacter", manageGroupSearchFieldLoc, "TEST_GROUP");
            doCommand("enterCharacter", new String[]{manageGroupSearchFieldLoc, "TEST_GROUP"});

            cmd.setCommand("click", manageGroupSearchBtnLoc, "");
            doCommand("click", new String[]{manageGroupSearchBtnLoc});

            cmd.setCommand("click", manageGroupViewInfoLoc, "");
            doCommand("click", new String[]{manageGroupViewInfoLoc});

            doCommand("assertText", new String[]{manageGroupInfoUsrNameLoc, resourceBundle.getObject("registration.admin.user.username").toString()});
            doCommand("assertText", new String[]{manageGroupInfofstNameLoc, resourceBundle.getObject("registration.admin.user.first.name").toString()});
            doCommand("assertText", new String[]{manageGroupInfoStatusLoc, resourceBundle.getObject("registration.admin.user.status").toString()});
            doCommand("assertText", new String[]{manageGroupInfoTypeLoc, resourceBundle.getObject("registration.admin.user.type").toString()});

            cmd.setCommand("mouseClick", manageGroupCloseInfoBtnLoc, "7,7");
            doMouseClick(manageGroupCloseInfoBtnLoc, "7,7");

            internal_comm_reg_test_sdp_admin_manage_group_info_edit_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_group_info_edit_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", manageGroupLinkLoc, "");
            doCommand("click", new String[]{manageGroupLinkLoc});

            cmd.setCommand("enterCharacter", manageGroupSearchFieldLoc, "TEST_GROUP");
            doCommand("enterCharacter", new String[]{manageGroupSearchFieldLoc, "TEST_GROUP"});

            cmd.setCommand("click", manageGroupSearchBtnLoc, "");
            doCommand("click", new String[]{manageGroupSearchBtnLoc});

            cmd.setCommand("click", manageGroupEditInfoLoc, "");
            doCommand("click", new String[]{manageGroupEditInfoLoc});

            doCommand("assertText", new String[]{manaegGroupInfoGrpNameLoc, resourceBundle.getObject("registration.admin.group.name").toString()});
            doCommand("assertText", new String[]{manageGroupInfoGrpDescLoc, resourceBundle.getObject("registration.admin.group.description").toString()});
            doCommand("assertText", new String[]{manageGroupInfoGrpPermissionLoc, resourceBundle.getObject("registration.admin.group.roles").toString()});
            doCommand("assertText", new String[]{manageGroupInfoGrpUsersLoc, resourceBundle.getObject("registration.admin.group.users").toString()});

            cmd.setCommand("mouseClick", manageGroupCloseEditInfoBtnLoc, "13,10");
            doMouseClick(manageGroupCloseEditInfoBtnLoc, "13,10");

            internal_comm_reg_test_sdp_admin_manage_group_delete_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_sdp_admin_manage_group_delete_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("internal_comm_reg_test_sdp_admin_add_group_name_validate");
        setTestName("internal_comm_reg_test_sdp_admin_add_group_name_validate");

        try {

            cmd.setCommand("click", manageGroupLinkLoc, "");
            doCommand("click", new String[]{manageGroupLinkLoc});

            cmd.setCommand("enterCharacter", manageGroupSearchFieldLoc, "TEST_GROUP");
            doCommand("enterCharacter", new String[]{manageGroupSearchFieldLoc, "TEST_GROUP"});

            cmd.setCommand("click", manageGroupSearchBtnLoc, "");
            doCommand("click", new String[]{manageGroupSearchBtnLoc});

            cmd.setCommand("click", manageGroupDeleteGroupLoc, "");
            doCommand("click", new String[]{manageGroupDeleteGroupLoc});

            cmd.setCommand("click", manageGroupDeleteGroupNoBtnLoc, "");
            doCommand("click", new String[]{manageGroupDeleteGroupNoBtnLoc});

            cmd.setCommand("click", manageGroupDeleteGroupYesBtnLoc, "");
            doCommand("click", new String[]{manageGroupDeleteGroupYesBtnLoc});


        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }
}
