/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */

import com.vaadin.testbench.testcase.AbstractVaadinTestCase;
import com.vaadin.testbench.util.CurrentCommand;
import org.testng.annotations.Test;
import java.util.ResourceBundle;

public class ValidateCorpAddSubCorp extends AbstractVaadinTestCase {

    private boolean writeScreenshots = false;
    private static String URL = "/cas/login?service=http%3A%2F%2Fweb.cur%3A4287%2Fregistration%2Fj_spring_cas_security_check";
    private static String loginUserNameLoc = "username";
    private static String loginPasswordLoc = "password";
    private static String loginSubmitBtnLoc = "submit";
    private static String registrationBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VMenuBar[0]#item1";
    private static String addSubCorpLinkLoc = "vaadin=registration::PID_SaddSubUserLink/domChild[0]/domChild[0]";
    private static String addSubCorpSubmitBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String addSubCorpCancelBtnLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]";
    private static String addSubCorpUserNameLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[0]";
    private static String addSubCorpPasswordLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VPasswordField[0]";
    private static String addSubCorpConfirmPasswordLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VPasswordField[1]";
    private static String addSubCorpEmailLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VFormLayout[0]/VFormLayout$VFormLayoutTable[0]/VTextField[1]";
    private static String addSubCorpAppStoreRoleLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpProvEditNCSSLALoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[3]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpProvEditAppSLALoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[4]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpSolturaAlertEditLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[25]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpSolturaLoginLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[26]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpPgwViewAppLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[29]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpPgwLoginLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[31]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpRegViewSubLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[47]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpRegLoginLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[46]/VCheckBox[0]/domChild[0]";
    private static String addSubCorpErrorLblLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";
    private static String addSubCorpHeaderLoc = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VLabel[0]";


    private static String roleLblLoc1 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[19]/VLabel[0]";
    private static String roleLblLoc2 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[16]/VLabel[0]";
    private static String roleLblLoc3 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[17]/VLabel[0]";
    private static String roleLblLoc4 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[12]/VLabel[0]";
    private static String roleLblLoc5 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[14]/VLabel[0]";
    private static String roleLblLoc6 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[11]/VLabel[0]";
    private static String roleLblLoc7 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[13]/VLabel[0]";
    private static String roleLblLoc8 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[15]/VLabel[0]";
    private static String roleLblLoc9 = "vaadin=registration::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[1]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[2]/VVerticalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[18]/VLabel[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @Test
    public void testIndivReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for Corporate User - Add Corporate User");
        setBrowserIdentifier("linux-firefox3");
        startBrowser("linux-firefox3");
        internal_AdminLoginTest();
    }

    private void corp_Add_subCorp_SubmitForm(CurrentCommand cmd) {

        cmd.setCommand("click", addSubCorpSubmitBtnLoc, "");
        doCommand("click", new String[]{addSubCorpSubmitBtnLoc});
    }

    private void corpClearForm(CurrentCommand cmd) {
        cmd.setCommand("click", addSubCorpCancelBtnLoc, "");
        doCommand("click", new String[]{addSubCorpCancelBtnLoc});
    }

    private void internal_AdminLoginTest() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("AdminLoginTest");
        setTestName("AdminLoginTest");
        try {
            cmd.setCommand("open", URL, "");
            open(URL);

            cmd.setCommand("enterCharacter", loginUserNameLoc, "corpuser");
            doCommand("enterCharacter", new String[]{loginUserNameLoc, "corpuser"});

            cmd.setCommand("enterCharacter", loginPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{loginPasswordLoc, "test123#"});

            cmd.setCommand("mouseClickAndWait", loginSubmitBtnLoc, "35,15");
            doCommand("mouseClickAndWait", new String[]{loginSubmitBtnLoc, "35,15"});

            cmd.setCommand("mouseClick", registrationBtnLoc, "19,2");
            doMouseClick(registrationBtnLoc, "19,2");

            cmd.setCommand("click", addSubCorpLinkLoc, "");
            doCommand("click", new String[]{addSubCorpLinkLoc});

            internal_comm_reg_test_corp_add_subCorp_user_name_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("AdminLoginTest");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_user_name_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            doCommand("assertText", new String[]{addSubCorpHeaderLoc, (resourceBundle.getObject("registration.subcorporate.users.add.heading").toString())});
            System.out.println(i + ". Add SubCorp - Header validation Passed");
            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.username.required").toString())});
            System.out.println(++i + ". Add SubCorp - User name required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_user_name_not_available_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_user_name_not_available_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "subcorp");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "subcorp"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.username.not.available").toString())});
            System.out.println(++i + ". Add SubCorp - User name not available validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.password.required").toString())});
            System.out.println(++i + ". Add SubCorp - Password required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_format_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            cmd.setCommand("enterCharacter", addSubCorpPasswordLoc, "12345678");
            doCommand("enterCharacter", new String[]{addSubCorpPasswordLoc, "12345678"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.invalid.password").toString())});
            System.out.println(++i + ". Add SubCorp - Password format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_confirm_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_confirm_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            cmd.setCommand("enterCharacter", addSubCorpPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpPasswordLoc, "test123#"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.retype.password.required").toString())});
            System.out.println(++i + ". Add SubCorp - Confirm Password validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_not_matching_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_not_matching_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            cmd.setCommand("enterCharacter", addSubCorpPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", addSubCorpConfirmPasswordLoc, "test12345#");
            doCommand("enterCharacter", new String[]{addSubCorpConfirmPasswordLoc, "test12345#"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.password.does.not.match").toString())});
            System.out.println(++i + ". Add SubCorp - Passwords not matching validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_email_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_email_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            cmd.setCommand("enterCharacter", addSubCorpPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", addSubCorpConfirmPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpConfirmPasswordLoc, "test123#"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.email.required").toString())});
            System.out.println(++i + ". Add SubCorp - Email required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_email_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_email_format_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            cmd.setCommand("enterCharacter", addSubCorpPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", addSubCorpConfirmPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpConfirmPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", addSubCorpEmailLoc, "email");
            doCommand("enterCharacter", new String[]{addSubCorpEmailLoc, "email"});

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{addSubCorpErrorLblLoc, (resourceBundle.getObject("registration.user.invalid.email").toString())});
            System.out.println(++i + ". Add SubCorp - Email format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_add_subCorp_password_roles_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_add_subCorp_password_roles_validate() throws Throwable {

        CurrentCommand cmd = new CurrentCommand("comm_reg_test_user_name_available_validate");
        setTestName("internal_comm_reg_test_corp_add_subCorp_user_name_validate");

        try {

            cmd.setCommand("enterCharacter", addSubCorpUserNameLoc, "test");
            doCommand("enterCharacter", new String[]{addSubCorpUserNameLoc, "test"});

            cmd.setCommand("enterCharacter", addSubCorpPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", addSubCorpConfirmPasswordLoc, "test123#");
            doCommand("enterCharacter", new String[]{addSubCorpConfirmPasswordLoc, "test123#"});

            cmd.setCommand("enterCharacter", addSubCorpEmailLoc, "email@gmail.com");
            doCommand("enterCharacter", new String[]{addSubCorpEmailLoc, "email@gmail.com"});

            cmd.setCommand("mouseClick", addSubCorpAppStoreRoleLoc, "11,-308");
            doMouseClick(addSubCorpAppStoreRoleLoc, "11,-308");

            cmd.setCommand("mouseClick", addSubCorpProvEditNCSSLALoc, "8,-312");
            doMouseClick(addSubCorpProvEditNCSSLALoc, "8,-312");

            cmd.setCommand("mouseClick", addSubCorpProvEditAppSLALoc, "7,-314");
            doMouseClick(addSubCorpProvEditAppSLALoc, "7,-314");

            cmd.setCommand("mouseClick", addSubCorpSolturaAlertEditLoc, "5,-668");
            doMouseClick(addSubCorpSolturaAlertEditLoc, "5,-668");

            cmd.setCommand("mouseClick", addSubCorpSolturaLoginLoc, "6,-666");
            doMouseClick(addSubCorpSolturaLoginLoc, "6,-666");

            cmd.setCommand("mouseClick", addSubCorpPgwViewAppLoc, "4,-803");
            doMouseClick(addSubCorpPgwViewAppLoc, "4,-803");

            cmd.setCommand("mouseClick", addSubCorpPgwLoginLoc, "3,-799");
            doMouseClick(addSubCorpPgwLoginLoc, "3,-799");

            cmd.setCommand("mouseClick", addSubCorpRegViewSubLoc, "4,-1168");
            doMouseClick(addSubCorpRegViewSubLoc, "4,-1168");

            cmd.setCommand("mouseClick", addSubCorpRegLoginLoc, "13,-1169");
            doMouseClick(addSubCorpRegLoginLoc, "13,-1169");

            corp_Add_subCorp_SubmitForm(cmd);

            doCommand("assertText", new String[]{roleLblLoc1,"ROLE_APP_SP" });
            System.out.println(++i + ". Add SubCorp - App Store role added");

            doCommand("assertText", new String[]{roleLblLoc2,"ROLE_PROV_EDIT_NCS_SLA" });
            System.out.println(++i + ". Add SubCorp - Prov edit NCS SLA role added");

            doCommand("assertText", new String[]{roleLblLoc3,"ROLE_PROV_EDIT_APP_SLA" });
            System.out.println(++i + ". Add SubCorp - Prov edit app SLA role added");

            doCommand("assertText", new String[]{roleLblLoc4,"ROLE_SOL_LOGIN" });
            System.out.println(++i + ". Add SubCorp - Soltura login role added");

            doCommand("assertText", new String[]{roleLblLoc5,"ROLE_SOL_EDIT_ALERT" });
            System.out.println(++i + ". Add SubCorp - Soltura edit alert role added");

            doCommand("assertText", new String[]{roleLblLoc6,"ROLE_REG_LOGIN" });
            System.out.println(++i + ". Add SubCorp - Registration login role added");

            doCommand("assertText", new String[]{roleLblLoc7,"ROLE_REG_VIEW_SUB_COOPERATE_USER" });
            System.out.println(++i + ". Add SubCorp - Registration view sub-corporate useres role added");

            doCommand("assertText", new String[]{roleLblLoc8,"ROLE_PGW_CSC_LOGIN" });
            System.out.println(++i + ". Add SubCorp - PGW login role added");

            doCommand("assertText", new String[]{roleLblLoc9,"ROLE_PGW_CSC_VIEW_APP_REQ" });
            System.out.println(++i + ". Add SubCorp - PGW view application requests role added");

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("internal_comm_reg_test_corp_add_subCorp_user_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

}
