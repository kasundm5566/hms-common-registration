/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.automation;

/**
 * Defines the API request types supported via Common Registration REST API
 * $LastChangedDate: 2011-06-03 16:51:56 +0530 (Fri, 03 Jun 2011) $
 * $LastChangedBy: nithyak $
 * $LastChangedRevision: 73790 $
 */
import com.vaadin.testbench.testcase.AbstractVaadinTestCase;

import java.util.ResourceBundle;

import com.vaadin.testbench.util.CurrentCommand;

public class ValidateRegCorporate extends AbstractVaadinTestCase {
    private boolean writeScreenshots = false;
    private static String corpHeaderLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/domChild[0]/domChild[0]/domChild[0]";
    private static String userNameLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[15]/VTextField[0]";
    private static String passwrdLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[16]/VPasswordField[0]";
    private static String confirmPasswrdLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[17]/VPasswordField[0]";
    private static String MpinLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[18]/VPasswordField[0]";
    private static String confirmMpinLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[19]/VPasswordField[0]";
    private static String emailLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[10]/VTextField[0]";
    private static String emailConfirmLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[11]/VTextField[0]";
    private static String contactPersonLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[12]/VTextField[0]";
    private static String contactPersonEmailLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[13]/VTextField[0]";
    private static String contactPersonPhoneNoLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[14]/VTextField[0]";
    private static String corpTermsCheckLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VGridLayout[0]/AbsolutePanel[0]/ChildComponentContainer[22]/VCheckBox[0]/domChild[0]";
    private static String corpSubmitBtnLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[1]/VButton[0]/domChild[0]/domChild[0]";
    private static String corpResetBtnLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VHorizontalLayout[0]/ChildComponentContainer[0]/VButton[0]/domChild[0]/domChild[0]";
    private static String corpErrorLabelLoc = "vaadin=registrationdefault::/VVerticalLayout[0]/ChildComponentContainer[1]/VVerticalLayout[0]/ChildComponentContainer[0]/VVerticalLayout[0]/ChildComponentContainer[2]/VHorizontalLayout[0]/ChildComponentContainer[0]/VPanel[0]/VVerticalLayout[0]/ChildComponentContainer[0]/VForm[0]/domChild[0]/domChild[3]/domChild[0]";

    private ResourceBundle resourceBundle;
    private int i = 1;

    @org.testng.annotations.Test
    public void testCorpReg() throws Throwable {
        resourceBundle = ResourceBundle.getBundle("registration_messages");
        System.out.println("Calling  Validation Methods for Corporate User");
        this.setBrowserIdentifier("linux-firefox36");
        startBrowser("linux-firefox3");
        internal_comm_reg_test_user_name_validate();
    }

    private void corpSubmitForm(CurrentCommand cmd) {
        cmd.setCommand("mouseClick", corpTermsCheckLoc, "7,-1028");
        doMouseClick(corpTermsCheckLoc, "7,-1028");

        cmd.setCommand("click", corpSubmitBtnLoc, "");
        doCommand("click", new String[]{corpSubmitBtnLoc});
    }

    private void corpClearForm(CurrentCommand cmd) {
        cmd.setCommand("click", corpResetBtnLoc, "");
        doCommand("click", new String[]{corpResetBtnLoc});
    }

    private void internal_comm_reg_test_user_name_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_testuser_name_validate");
        setTestName("comm_reg_test_testuser_name_validate");
        try {

            cmd.setCommand("open", "/registration/default/#common/corporate", "");
            open("/registration/default/#common/corporate");

            doCommand("assertText", new String[]{corpHeaderLoc, (resourceBundle.getObject("registration.corporate.create.account").toString())});
            System.out.println("Header Validation Passed");

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.username.required").toString())});
            System.out.println(i + ". User Name required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_user_name_available_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_testuser_name_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_user_name_available_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_testuser_name_available_validate");
        setTestName("comm_reg_test_testuser_name_available_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "corpuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "corpuser"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.username.not.available").toString())});
            System.out.println(++i + ". User Name not available validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_user_name_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_testuser_name_available_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_user_name_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_testuser_name_format_validate()");
        setTestName("comm_reg_test_testuser_name_format_validate()");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "abc");
            doCommand("enterCharacter", new String[]{userNameLoc, "abc"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.username").toString())});
            System.out.println(++i + ". User Name format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_psswrd_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_testuser_name_available_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_psswrd_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_psswrd_validate");
        setTestName("comm_reg_test_corp_psswrd_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.password.required").toString())});
            System.out.println(++i + ". Password required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_psswrd_not_match_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_psswrd_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_psswrd_not_match_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_psswrd_not_match_validate");
        setTestName("comm_reg_test_corp_psswrd_not_match_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test12345#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test12345#"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.password.does.not.match").toString())});
            System.out.println(++i + ". Passwords not matching validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_psswrd_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_psswrd_not_match_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_psswrd_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_psswrd_format_validate");
        setTestName("comm_reg_test_corp_psswrd_format_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.password").toString())});
            System.out.println(++i + ". Passwords not valid validation 1 passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_psswrd_format2_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_psswrd_format_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_psswrd_format2_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_psswrd_format_validate");
        setTestName("comm_reg_test_corp_psswrd_format_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test12345");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test12345"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test12345");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test12345"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.password").toString())});
            System.out.println(++i + ". Passwords not valid validation 2 passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_MPIN_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_psswrd_format_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_MPIN_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_MPIN_validate");
        setTestName("comm_reg_test_corp_MPIN_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonLoc, "contactname");
            doCommand("enterCharacter", new String[]{contactPersonLoc, "contactname"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.mpin.required").toString())});
            System.out.println(++i + ". MPin Required validation Passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_MPIN_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_MPIN_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_MPIN_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_MPIN_format_validate");
        setTestName("comm_reg_test_corp_MPIN_format_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", MpinLoc, "123456");
            doCommand("enterCharacter", new String[]{MpinLoc, "123456"});

            cmd.setCommand("enterCharacter", confirmMpinLoc, "123456");
            doCommand("enterCharacter", new String[]{confirmMpinLoc, "123456"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonLoc, "contactname");
            doCommand("enterCharacter", new String[]{contactPersonLoc, "contactname"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.mpin").toString())});
            System.out.println(++i + ". MPin Format validation 1 passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_MPIN_format2_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_MPIN_format_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_MPIN_format2_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_MPIN_format2_validate");
        setTestName("comm_reg_test_corp_MPIN_format2_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", MpinLoc, "abcdef");
            doCommand("enterCharacter", new String[]{MpinLoc, "abcdef"});

            cmd.setCommand("enterCharacter", confirmMpinLoc, "abcdef");
            doCommand("enterCharacter", new String[]{confirmMpinLoc, "abcdef"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonLoc, "contactname");
            doCommand("enterCharacter", new String[]{contactPersonLoc, "contactname"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.mpin").toString())});
            System.out.println(++i + ". MPin Format validation 2 passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_email_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_MPIN_format2_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_email_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_email_validate");
        setTestName("comm_reg_test_corp_email_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});


            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.email.required").toString())});
            System.out.println(++i + ". Email required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_email_format_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_email_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_email_format_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_email_format_validate");
        setTestName("comm_reg_test_corp_email_format_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});


            cmd.setCommand("enterCharacter", emailLoc, "corp");
            doCommand("enterCharacter", new String[]{emailLoc, "corp"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.email").toString())});
            System.out.println(++i + ". Email format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_email_not_match_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_email_format_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_email_not_match_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_email_format_validate");
        setTestName("comm_reg_test_corp_email_format_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});


            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "orp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "abc@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "abc@gmail.com"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.email.does.not.match").toString())});
            System.out.println(++i + ". Email format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_contact_person_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_email_format_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_contact_person_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_contact_person_validate");
        setTestName("comm_reg_test_corp_contact_person_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});


            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.organization.contact.person.name.required").toString())});
            System.out.println(++i + ". Contact Person Required validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_contact_person_email_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_contact_person_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_contact_person_email_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_contact_person_validate");
        setTestName("comm_reg_test_corp_contact_person_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});


            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonLoc, "contactname");
            doCommand("enterCharacter", new String[]{contactPersonLoc, "contactname"});

            cmd.setCommand("enterCharacter", contactPersonEmailLoc, "abc");
            doCommand("enterCharacter", new String[]{contactPersonEmailLoc, "abc"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.email").toString())});
            System.out.println(++i + ". Contact Person's  email Format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_contact_person_phone_no_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_contact_person_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void internal_comm_reg_test_corp_contact_person_phone_no_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_contact_person_validate");
        setTestName("comm_reg_test_corp_contact_person_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonLoc, "contactname");
            doCommand("enterCharacter", new String[]{contactPersonLoc, "contactname"});

            cmd.setCommand("enterCharacter", contactPersonEmailLoc, "abc@gmail.com");
            doCommand("enterCharacter", new String[]{contactPersonEmailLoc, "abc@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonPhoneNoLoc, "abcdef");
            doCommand("enterCharacter", new String[]{contactPersonPhoneNoLoc, "abcdef"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.invalid.msisdn").toString())});
            System.out.println(++i + ". Contact Person's Phone no format validation passed");

            corpClearForm(cmd);

            internal_comm_reg_test_corp_captcha_validate();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_contact_person_validate");
            }
            throw new java.lang.AssertionError(cmd.getInfo() + "\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }


    private void internal_comm_reg_test_corp_captcha_validate() throws Throwable {
        CurrentCommand cmd = new CurrentCommand("comm_reg_test_corp_captcha_validate");
        setTestName("comm_reg_test_corp_captcha_validate");
        try {

            cmd.setCommand("enterCharacter", userNameLoc, "testuser");
            doCommand("enterCharacter", new String[]{userNameLoc, "testuser"});

            cmd.setCommand("enterCharacter", passwrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{passwrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", confirmPasswrdLoc, "test123#");
            doCommand("enterCharacter", new String[]{confirmPasswrdLoc, "test123#"});

            cmd.setCommand("enterCharacter", MpinLoc, "1234");
            doCommand("enterCharacter", new String[]{MpinLoc, "1234"});

            cmd.setCommand("enterCharacter", confirmMpinLoc, "1234");
            doCommand("enterCharacter", new String[]{confirmMpinLoc, "1234"});

            cmd.setCommand("enterCharacter", emailLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", emailConfirmLoc, "corp@gmail.com");
            doCommand("enterCharacter", new String[]{emailConfirmLoc, "corp@gmail.com"});

            cmd.setCommand("enterCharacter", contactPersonLoc, "contactname");
            doCommand("enterCharacter", new String[]{contactPersonLoc, "contactname"});

            corpSubmitForm(cmd);

            doCommand("assertText", new String[]{corpErrorLabelLoc, (resourceBundle.getObject("registration.user.captcha.code.invalid").toString())});
            System.out.println(++i + ". Captcha Required validation passed");

            corpClearForm(cmd);

            individualValidations();

        } catch (Throwable e) {
            if (writeScreenshots) {
                createFailureScreenshot("comm_reg_test_corp_captcha_validate");
            }
            throw new java.lang.AssertionError("\n Message: " + e.getMessage() + "\nRemote control: " + getRemoteControlName());
        }
        handleSoftErrors();
    }

    private void individualValidations() {
        ValidateRegIndividual validateIndivRegistration = new ValidateRegIndividual();

        try {
            validateIndivRegistration.testIndivReg();
        } catch (Throwable e) {

        }

    }

}

