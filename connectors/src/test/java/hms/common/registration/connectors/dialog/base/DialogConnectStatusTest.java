package hms.common.registration.connectors.dialog.base;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DialogConnectStatusTest {
    @Test
    public void testGetStatusByCode() throws Exception {
        DialogConnectStatus statusByCode = DialogConnectStatus.getStatusByCode("500");
        assertEquals(statusByCode, DialogConnectStatus.UNAUTHORIZED_REQ);
    }
}
