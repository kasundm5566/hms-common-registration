/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.util;

import hms.common.registration.connectors.dialog.base.DialogAuthResponse;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class JaxbUtilTest {

    @Test
    public void testUnmarshall() throws Exception {
        String xmlString = "<?xml version=\"1.0\"?><res>\n" +
                "<status>OK</status>\n" +
                "<code>0</code>\n" +
                "<desc>User validated</desc>\n" +
                ".<user>\n" +
                "..<phoneNo>770808373</phoneNo>\n" +
                "..<userID>hsenid2</userID>..\n" +
                "..<email>anjulaw@hsenidmobile.com</email>\n" +
                "..<type>prepaid</type>\n" +
                "..<status>C</status>\n" +
                "..<firstName><![CDATA[-]]></firstName>\n" +
                "..<lastName><![CDATA[HSENID MOBILE SOLUTIONS PVT LTD]]></lastName>..\n" +
                "..<dob></dob>\n" +
                "..<contactNo></contactNo>..\n" +
                "..<gender></gender>..\n" +
                "..<idType>TIN</idType>\n" +
                "..<idNumber>PV62579</idNumber>\n" +
                "..<moreInfo></moreInfo>\n" +
                "..<title><![CDATA[MS]]></title>\n" +
                "..\n" +
                "...<services>\n" +
                "...\n" +
                "....<service>\n" +
                ".....<type>GSM</type>\n" +
                ".....<contractid>15062826</contractid>\n" +
                ".....<number>770808373</number>\n" +
                ".....<cxtype>prepaid</cxtype>\n" +
                ".....<connStatus>C</connStatus>\n" +
                ".....<theme>prepaid_blue</theme>\n" +
                ".....<default>true</default>\n" +
                "....</service>\n" +
                "......\n" +
                "...</services>\n" +
                "..  \n" +
                ".</user>\n" +
                "</res>";

        DialogAuthResponse response = JaxbUtil.unmarshall(DialogAuthResponse.class, xmlString);
        assertEquals(response.getUser().getServices().size(), 1);
        assertEquals(response.getUser().getPhoneNo(), "770808373");
        assertEquals(response.getUser().getLastName(), "HSENID MOBILE SOLUTIONS PVT LTD");
    }
}
