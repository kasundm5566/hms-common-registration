/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import hms.common.registration.connectors.api.AuthConnector;
import hms.common.registration.connectors.api.base.AuthRequest;
import hms.common.registration.connectors.api.base.AuthResponse;
import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;
import hms.common.registration.connectors.dialog.base.DialogAuthRequest;
import hms.common.registration.connectors.dialog.base.DialogAuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;

public class DialogAuthConnector implements AuthConnector {

    private static final ClientConfig clientConfig = new DefaultClientConfig();
    private static final Client client = Client.create(clientConfig);
    WebResource resource;
    private final String path;

    private static final Logger logger = LoggerFactory.getLogger(DialogAuthConnector.class);

    public DialogAuthConnector(String path) {
        this.path = path;
        resource = client.resource(path);
    }

    public AuthResponse authenticateUser(AuthRequest request) throws InvalidRequestTypeException {

        logger.debug("Connecting to dialog authentication service, authenticate request = [{}].", request);

        if(!(request instanceof DialogAuthRequest)) {
            logger.debug("Authentication request cannot be converted to Dialog authentication request.");
            throw new InvalidRequestTypeException();
        }

        /*
            Sample request
            POST /myauth/ctrl.jsp?username=hsenid2&password=test123%23&action=login&format=xml&full=false
        */

        DialogAuthRequest req = (DialogAuthRequest) request;
        AuthResponse response = resource.path(path).
                                    queryParam("username", req.getUsername()).
                                    queryParam("password", req.getPassword()).
                                    queryParam("action", req.getAction()).
                                    queryParam("format", req.getFormat()).
                                    queryParam("full", req.isFull()).
                                    queryParam("serviceCheck", req.getServiceCheck()).
                                    accept(MediaType.TEXT_XML_TYPE).
                                    post(DialogAuthResponse.class);

        logger.debug("Received response from dialog connect xml api = [{}]", response);

        return response;
    }

}
