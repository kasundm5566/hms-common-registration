/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public final class JaxbUtil {

    private static final Logger logger = LoggerFactory.getLogger(JaxbUtil.class);

    private JaxbUtil() {
        throw new AssertionError();
    }

    public static String marshall(Object obj) throws JAXBException {
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter reader = new StringWriter();
            marshaller.marshal(obj, reader);
            String objToString = reader.toString();
            logger.debug("Object [{}] was successfully marshall to string.");
            return objToString;
    }

    @SuppressWarnings("unchecked")
    public static <T> T unmarshall(Class<T> clazz, String xmlString) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(xmlString);
        T obj = (T) unmarshaller.unmarshal(reader);
        logger.debug("Xml string was successfully un-marshal to Object [{}].", obj);
        return obj;
    }
}
