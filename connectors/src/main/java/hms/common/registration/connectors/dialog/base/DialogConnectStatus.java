/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.base;

import java.util.HashMap;
import java.util.Map;

public enum DialogConnectStatus {
    SUCCESS("0"),
    UNAUTHORIZED_REQ("500"),
    INVALID_PARAMETER("501"),
    INVALID_USERNAME_PASSWORD("502"),
    ACCOUNT_LOCKED("503"),
    PASSWORD_RETRY_EXCEED("504"),
    USER_NOT_FOUND("505"),
    FULL_PROFILE_NOT_ALLOWED("506"),
    SYSTEM_FAULT("598");

    private String statusCode;

    private static final Map<String, DialogConnectStatus> statusMap = new HashMap<>();
    static {
        statusMap.put("0", SUCCESS);
        statusMap.put("500", UNAUTHORIZED_REQ);
        statusMap.put("501", INVALID_PARAMETER);
        statusMap.put("502", INVALID_USERNAME_PASSWORD);
        statusMap.put("503", ACCOUNT_LOCKED);
        statusMap.put("504", PASSWORD_RETRY_EXCEED);
        statusMap.put("505", USER_NOT_FOUND);
        statusMap.put("506", FULL_PROFILE_NOT_ALLOWED);
        statusMap.put("598", SYSTEM_FAULT);
    }

    DialogConnectStatus(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public static DialogConnectStatus getStatusByCode(String code) {
        return statusMap.get(code);
    }
}
