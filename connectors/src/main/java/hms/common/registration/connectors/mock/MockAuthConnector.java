/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.mock;

import hms.common.registration.connectors.api.AuthConnector;
import hms.common.registration.connectors.api.base.AuthRequest;
import hms.common.registration.connectors.api.base.AuthResponse;
import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;
import hms.common.registration.connectors.mock.base.MockAuthResponse;

public class MockAuthConnector implements AuthConnector {
    public AuthResponse authenticateUser(AuthRequest request) throws InvalidRequestTypeException {
        return new MockAuthResponse();
    }
}
