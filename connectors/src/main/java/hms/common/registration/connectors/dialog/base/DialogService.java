/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "service")
@XmlAccessorType(XmlAccessType.FIELD)
public class DialogService {

    @XmlElement(name = "type")
    private String type;

    @XmlElement(name = "contractid")
    private String contractId;

    @XmlElement(name = "number")
    private String number;

    @XmlElement(name = "cxtype")
    private String cxType;

    @XmlElement(name = "connStatus")
    private String connStatus;

    @XmlElement(name = "theme")
    private String theme;

    @XmlElement(name = "default")
    private String isDefault;

    public DialogService() {
    }

    public String getType() {
        return type;
    }

    public String getContractId() {
        return contractId;
    }

    public String getNumber() {
        return number;
    }

    public String getCxType() {
        return cxType;
    }

    public String getConnStatus() {
        return connStatus;
    }

    public String getTheme() {
        return theme;
    }

    public String getDefault() {
        return isDefault;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Service");
        sb.append("{type='").append(type).append('\'');
        sb.append(", contractId='").append(contractId).append('\'');
        sb.append(", number='").append(number).append('\'');
        sb.append(", cxType='").append(cxType).append('\'');
        sb.append(", connStatus='").append(connStatus).append('\'');
        sb.append(", theme='").append(theme).append('\'');
        sb.append(", isDefault='").append(isDefault).append('\'');
        sb.append('}');
        return sb.toString();
    }
}