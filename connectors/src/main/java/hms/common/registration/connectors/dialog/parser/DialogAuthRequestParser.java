/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.parser;

import hms.common.registration.connectors.api.base.AuthRequest;
import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;
import hms.common.registration.connectors.api.parser.AuthRequestParser;
import hms.common.registration.connectors.dialog.base.DialogAuthRequest;

import java.util.Map;

public class DialogAuthRequestParser implements AuthRequestParser {

    // request map keys
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";


    public DialogAuthRequestParser() {
    }

    public AuthRequest parse(Map<String, Object> request) throws InvalidRequestTypeException {
        String username = (String) request.get(USERNAME);
        String password = (String) request.get(PASSWORD);

        boolean isInvalidRequest =
                (username == null || username.trim().equals("")) ||
                (password == null || password.trim().equals(""));

        if(isInvalidRequest) throw new InvalidRequestTypeException();

        return new DialogAuthRequest.Builder().username(username).password(password).action("login").
                    format("xml").isFullDetail("true").serviceCheck("GSM").build();
    }
}
