/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.base;

public enum AuthenticationStatus {
    SUCCESS("S1000", "Success."),
    ERROR_INVALID_USERNAME("E1010", "Invalid username."),
    ERROR_DIALOG_CONNECT("E1020", "Error connecting to Dialog connect."),
    ERROR_INTERNAL("E1030", "Internal error occurred."),
    ERROR_SESSION_VALIDATION("E1040", "Session validation failed."),
    ERROR_SESSION_EXPIRED("E1050", "Session expired."),
    ERROR_CODE_RECEIVED_FROM_DIALOG("E1060", "Error code received from dialog connect."),
    ERROR_INVALID_REQUEST("E1070", "Invalid request received.");

    private final String statusCode;
    private final String statusDescription;

    private AuthenticationStatus(String statusCode, String statusDescription) {
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }
}
