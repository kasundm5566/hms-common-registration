/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.base;


import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class DialogUser {

    @XmlElement(name = "phoneNo")
    private String phoneNo;

    @XmlElement(name = "userID")
    private String userId;

    @XmlElement(name = "email")
    private String email;

    @XmlElement(name = "type")
    private String type;

    @XmlElement(name = "status")
    private String status;

    @XmlElement(name = "firstName")
    private String firstName;

    @XmlElement(name = "lastName")
    private String lastName;

    @XmlElement(name = "dob")
    private String dob;

    @XmlElement(name = "contactNo")
    private String contactNo;

    @XmlElement(name = "gender")
    private String gender;

    @XmlElement(name = "idType")
    private String idType;

    @XmlElement(name = "idNumber")
    private String idNumber;

    @XmlElement(name = "moreInfo")
    private String isMoreInfo;

    @XmlElement(name = "title")
    private String title;

    @XmlElementWrapper(name="services")
    @XmlElement(name = "service")
    private List<DialogService> services;


    public DialogUser() {
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDob() {
        return dob;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getGender() {
        return gender;
    }

    public String getIdType() {
        return idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public String getMoreInfo() {
        return isMoreInfo;
    }

    public String getTitle() {
        return title;
    }

    public List<DialogService> getServices() {
        return services;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public void setMoreInfo(String moreInfo) {
        isMoreInfo = moreInfo;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setServices(List<DialogService> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("DialogUser");
        sb.append("{phoneNo='").append(phoneNo).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", dob='").append(dob).append('\'');
        sb.append(", contactNo='").append(contactNo).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", idType='").append(idType).append('\'');
        sb.append(", idNumber='").append(idNumber).append('\'');
        sb.append(", isMoreInfo='").append(isMoreInfo).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", services=").append(services);
        sb.append('}');
        return sb.toString();
    }
}
