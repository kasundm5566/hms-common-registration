/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.api.parser;

import hms.common.registration.connectors.api.base.AuthRequest;
import hms.common.registration.connectors.api.exception.InvalidRequestTypeException;

import java.util.Map;

public interface AuthRequestParser {

    public AuthRequest parse(Map<String, Object> request) throws InvalidRequestTypeException;

}
