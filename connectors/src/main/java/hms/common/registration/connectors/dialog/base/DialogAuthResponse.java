/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.base;

import hms.common.registration.connectors.api.base.AuthResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "res")
@XmlAccessorType(XmlAccessType.FIELD)
public class DialogAuthResponse implements AuthResponse {

    @XmlElement(name = "status")
    private String status;

    @XmlElement(name = "code")
    private String code;

    @XmlElement(name = "desc")
    private String description;

    @XmlElement(name = "user")
    private DialogUser user;

    public DialogAuthResponse() {
    }

    public String getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public DialogUser getUser() {
        return user;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUser(DialogUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("AuthenticationResponse");
        sb.append("{status='").append(status).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }

    public static class DialogAuthResponseBuilder {
        DialogAuthResponse dialogAuthResponse = new DialogAuthResponse();

        public DialogAuthResponseBuilder withStatus(String status) {
            dialogAuthResponse.setStatus(status);
            return this;
        }

        public DialogAuthResponseBuilder withCode(String code) {
            dialogAuthResponse.setCode(code);
            return this;
        }

        public DialogAuthResponseBuilder withDescription(String description) {
            dialogAuthResponse.setDescription(description);
            return this;
        }

        public DialogAuthResponseBuilder withUser(DialogUser user) {
            dialogAuthResponse.setUser(user);
            return this;
        }

        public DialogAuthResponse build() {
            return this.dialogAuthResponse;
        }
    }
}