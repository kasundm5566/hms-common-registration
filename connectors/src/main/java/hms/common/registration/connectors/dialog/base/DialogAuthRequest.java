/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.connectors.dialog.base;

import hms.common.registration.connectors.api.base.AuthRequest;

public final class DialogAuthRequest implements AuthRequest {
    private final String username;
    private final String password;
    private final String action;
    private final String format;
    private final String full;
    private final String serviceCheck;

    private DialogAuthRequest(String username,
                             String password,
                             String action,
                             String format,
                             String full,
                             String serviceCheck) {
        this.username = username;
        this.password = password;
        this.action = action;
        this.format = format;
        this.full = full;
        this.serviceCheck = serviceCheck;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getAction() {
        return action;
    }

    public String getFormat() {
        return format;
    }

    public String isFull() {
        return full;
    }

    public String getServiceCheck() {
        return serviceCheck;
    }

    public static class Builder {
        private String username;
        private String password;
        private String action;
        private String format;
        private String full;
        private String serviceCheck;

        public Builder username(String username){
            this.username = username;
            return this;
        }

        public Builder password(String password){
            this.password = password;
            return this;
        }

        public Builder action(String action) {
            this.action = action;
            return this;
        }

        public Builder format(String format) {
            this.format = format;
            return this;
        }

        public Builder isFullDetail(String full) {
            this.full = full;
            return this;
        }

        public Builder serviceCheck(String serviceCheck) {
            this.serviceCheck = serviceCheck;
            return this;
        }

        public DialogAuthRequest build() {
            return new DialogAuthRequest(username, password, action, format, full, serviceCheck);
        }
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("AuthenticationRequest");
        sb.append("{username='").append(username).append('\'');
        sb.append(", password='").append("******").append('\'');
        sb.append(", action='").append(action).append('\'');
        sb.append(", format='").append(format).append('\'');
        sb.append(", full=").append(full);
        sb.append(", serviceCheck='").append(serviceCheck).append('\'');
        sb.append('}');
        return sb.toString();
    }
}