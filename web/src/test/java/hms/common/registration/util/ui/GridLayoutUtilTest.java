package hms.common.registration.util.ui;

import org.testng.annotations.Test;

import static hms.common.registration.util.ui.GridLayoutUtil.GridComponentCursor;
import static org.testng.Assert.assertEquals;


@Test
public class GridLayoutUtilTest {

    @Test
    public void testNextCell() {
        final GridComponentCursor gridComponentCursor = new GridComponentCursor(2, 3);
        gridComponentCursor.nextCell();
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 1);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 0);
        gridComponentCursor.nextCell();
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 0);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 1);
    }

    @Test
    public void testNextEntireRow() {
        final GridComponentCursor gridComponentCursor = new GridComponentCursor(2, 3);
        gridComponentCursor.nextEntireRow();
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 0);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 1);
    }

    @Test
    public void testNextCellWithNewLine() {
        final GridComponentCursor gridComponentCursor = new GridComponentCursor(2, 3);
        gridComponentCursor.nextCellWithNewLine();
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 0);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 1);
    }

    @Test
    public void testCursorMovement1() {
        final GridComponentCursor gridComponentCursor = new GridComponentCursor(2, 3);
        final GridLayoutUtil.GridArea gridArea = gridComponentCursor.nextEntireRow();
        System.out.println(gridArea);
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 0);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 1);

        final GridLayoutUtil.GridArea gridArea1 = gridComponentCursor.nextCell();
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 1);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 1);
        System.out.println(gridArea1);

        final GridLayoutUtil.GridArea gridArea3 = gridComponentCursor.nextCell();
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 0);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 2);
        System.out.println(gridArea3);

        final GridLayoutUtil.GridArea gridArea2 = gridComponentCursor.nextCellWithNewLine();
        System.out.println(gridArea2);
        assertEquals(gridComponentCursor.getCursorColumnIndex(), 0);
        assertEquals(gridComponentCursor.getCursorRowIndex(), 3);
    }
}
