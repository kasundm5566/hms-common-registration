package hms.common.registration.ui.content.user.individual;

import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Pattern;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: isuruanu
 * Date: 1/22/14
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
@Test
public class RegularExpressionTest {

    Properties properties = new Properties();

    @BeforeClass
    public void setUp() throws IOException {
        final InputStream resourceAsStream = RegularExpressionTest.class.getClassLoader().getResourceAsStream("validation-test.properties");
        properties.load(resourceAsStream);
    }

    @Test
    public void testIndividualUsernameValidationPattern() {
        final String property = properties.getProperty("individual.user.username.regex.pattern");
        final Pattern compile = Pattern.compile(property);
        assertTrue(compile.matcher("isuru_w").matches());
        assertTrue(compile.matcher("isur").matches());
        assertTrue(compile.matcher("isuru_a_w").matches());
        assertFalse(compile.matcher("isuru$a#w").matches());
        assertTrue(compile.matcher("isuru.w").matches());
        assertTrue(compile.matcher("isuru.a.w").matches());
        assertFalse(compile.matcher("isuru_w_").matches());
        assertFalse(compile.matcher("isuru__w").matches());
        assertFalse(compile.matcher("_isuru_w").matches());
    }

}
