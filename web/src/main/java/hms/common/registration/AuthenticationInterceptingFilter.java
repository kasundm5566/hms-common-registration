/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration;


import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ProxyReceivingTicketValidationFilter;
import org.jasig.cas.client.validation.TicketValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static hms.common.registration.ui.common.util.RegistrationKeys.*;
import static hms.common.registration.util.RegistrationCommonUtil.getDefaultBirthday;
import static hms.common.registration.util.RegistrationCommonUtil.getUserDetails;
import static hms.common.registration.model.UserType.CORPORATE;
import static hms.common.registration.model.UserType.INDIVIDUAL;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AuthenticationInterceptingFilter extends Cas20ProxyReceivingTicketValidationFilter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptingFilter.class);

    private UserService userService;
    private String internalUserDomain = "internal";
    private String casLogoutUrl = "https://dev.sdp.hsenidmobile.com/cas/logout";
    private String wapLoginPageUrl = "https://dev.sdp.hsenidmobile.com/wap/login/secure/login";
    private String webHomePageUrl = "https://dev.sdp.hsenidmobile.com/registration/";
    private String webHomePageUrlEditProfile = "https://dev.sdp.hsenidmobile.com/registration/#settings";
    private String landingPageUrl = "";

    private User user;

    private boolean isMobileBrowser(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        String accept = request.getHeader("accept");
        String wapProfile = request.getHeader("x-wap-profile");

        if (wapProfile != null) {
            return true;
        }

        if (accept != null && (accept.contains("wap") || accept.contains("wml"))) {
            return true;
        }

        Map<Pattern, String> mobileTypes = new HashMap<Pattern, String>();

        mobileTypes.put(Pattern.compile(".*iPhone.*"), "iphone");
        mobileTypes.put(Pattern.compile(".*Android.*"), "android");
        mobileTypes.put(Pattern.compile(".*Safari.*Pre.*"), "safari");
        mobileTypes.put(Pattern.compile(".*Nokia.*AppleWebKit.*"), "nokia");

        for (final Map.Entry<Pattern, String> entry : mobileTypes.entrySet()) {
            if (entry.getKey().matcher(userAgent).matches()) {
                return true;
            }
        }

        return false;
    }

    private void doHandleUserNotFoundScenario(HttpServletRequest request, HttpServletResponse response, boolean openIdUser, String userDomain) throws IOException {
        String failureUrl = failUrl(request, userDomain);
        if (!openIdUser) {
            failureUrl = casLogoutUrlWithError("internal.user.not.found");
        }

        logger.debug("User not found. Redirect to [" + failureUrl + "]");
        response.sendRedirect(failureUrl);
    }

    private String casLogoutUrlWithError(String errorKey) {
        if (casLogoutUrl.contains("?")) {
            return casLogoutUrl + "&error=" + errorKey;
        } else {
            return casLogoutUrl + "?error=" + errorKey;
        }
    }

    private String failUrl(HttpServletRequest request, String userDomain) {
        if (!("dialog").equals(userDomain) && isMobileBrowser(request)) {
            logger.debug("Mobile user agent found");

            String url = "/wap/login/registration-external/#common/openid?" + request.getQueryString();
            if (logger.isDebugEnabled()) {
                log.debug("Redirecting to WAP URL " + url);
            }
            return url;
        } else if (("dialog").equals(userDomain)) {
            String url = "/registration/default#common/dialog/?" + request.getQueryString();
            if (logger.isDebugEnabled()) {
                log.debug("Redirecting to WEB URL" + url);
            }
            return url;
        } else {
            String url = "/registration/default#common/openid?" + request.getQueryString();
            if (logger.isDebugEnabled()) {
                log.debug("Redirecting to WEB URL" + url);
            }
            return url;
        }
    }

    private String successUrl(String serviceURL, HttpServletRequest request) {
        if (serviceURL == null || serviceURL.trim().isEmpty()) {
            if (isMobileBrowser(request)) {
                log.debug("Redirecting to WAP Login URl " + wapLoginPageUrl);
                return wapLoginPageUrl;
            } else {
                return webHomePageUrl;
            }
        }

        if (serviceURL.contains("?")) {
            return serviceURL + "&ticket=" + request.getParameter("ticket");
        } else {
            return serviceURL + "?ticket=" + request.getParameter("ticket");
        }
    }

    private void fillUserData(User user, Map<String, String> userDetails) {
        String firstName = userDetails.get("first_name");
        String lastName = userDetails.get("last_name");
        String mobileNumber = userDetails.get("msisdn");
        String email = userDetails.get("email");
        String gender = userDetails.get("gender");
        String dob = userDetails.get("birth_day");
        String fullName = (firstName != null ? firstName : "") + (lastName != null ? (" " + lastName) : "");

        Date birthday = getDefaultBirthday();

        try {
            birthday = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
        } catch (ParseException e) {
            logger.error("Invalid birthday format detected [{}]", dob);
        } catch (Exception e) {
            logger.error("Error occurred while processing the birthday", e);
        }

        user.setEmail(email);
        if (!mobileNumber.startsWith("94")) {
            user.setMobileNo("94" + mobileNumber);
        } else {
            user.setMobileNo(mobileNumber);
        }
        user.setBirthday(birthday);
        user.setGender(gender);

        if (user.getUserType().equals(INDIVIDUAL)) {
            user.setFirstName(fullName);
        } else if (user.getUserType().equals(CORPORATE)) {
            ((CorporateUser) user).setContactPersonName(fullName);
        }
    }

    protected String constructServiceUrl(final HttpServletRequest request, final HttpServletResponse response) {
        return request.getParameter(LOCAL_SERVICE);
    }

    @Override
    protected void onSuccessfulValidation(HttpServletRequest request, HttpServletResponse response, Assertion assertion) {
        String username = assertion == null ? null : assertion.getPrincipal().getName();
        logger.debug("user from assertion [{}]", username);

        HttpSession session = request.getSession();

        String serviceUrl = request.getParameter(LOCAL_SERVICE);
        String userId = request.getParameter(USER_ID);
        String userDomain = request.getParameter(PROVIDER);

        if (serviceUrl != null && username != null) {
            logger.debug("Service Url is [{}], user id [{}], user domain [{}]", new Object[]{serviceUrl, userId, userDomain});

            session.setAttribute(LOCAL_SERVICE, serviceUrl);

            String successUrl;

            boolean isDialogUser = userId != null && userDomain != null && userDomain.equals("dialog");

            boolean isOpenIdUser = username.contains("#") || isDialogUser;

            String domainId;
            String domainName = "";

            try {
                try {
                    if (isOpenIdUser) {
                        if (isDialogUser) {
                            domainId = userId;
                            domainName = userDomain;
                        } else {
                            domainId = username.substring(username.indexOf('#') + 1); //TODO merge with dialog in future by removing '#'
                            domainName = username.substring(0, username.indexOf('#'));
                        }

                        user = userService.findUserByDomainId(domainName, domainId);
                        if (user == null) {
                            throw new DataManipulationException("User not found in the system fo domain/id : "
                                    + domainName + '/' + domainId);
                        }
                        successUrl = successUrl(serviceUrl, request);
                    } else {
                        user = userService.findUserByName(username);
                        if (user == null) {
                            throw new DataManipulationException("User not found in the system fo username : "
                                    + username);
                        }
                        successUrl = successUrl(serviceUrl, request);
                    }
                    logger.debug("Success Url is {}", successUrl);
                    logger.info("User found in the system. Redirect to [{}]", successUrl);
                    if (user.getDomain().equals("dialog")) {
                        Map<String, String> userDetails = getUserDetails(assertion);
                        fillUserData(user, userDetails);
                    }
                    user.setLastLogin(user.getCurrentLogin());
                    user.setCurrentLogin(new Date());
                    userService.merge(user);
                    logger.debug("Redirecting to {}", successUrl);
                    session.setAttribute(CUSTOM_AUTHENTICATION_INTERCEPTOR_EXECUTED, true);
                    session.setAttribute(USER_ID, username);
                    session.removeAttribute(CUSTOM_CAS_ASSERTION);
                    response.sendRedirect(successUrl);
                } catch (EmptyResultDataAccessException e) {
                    logger.error("Error occured while retrieving the user [{}] from internal repository. [{}]", username, e);
                    e.printStackTrace();
                    doHandleUserNotFoundScenario(request, response, isOpenIdUser, domainName);
                } catch (DataManipulationException e) {
                    logger.error("Error occured while retrieving the user [{}] from internal repository. [{}]", username, e);
                    e.printStackTrace();
                    doHandleUserNotFoundScenario(request, response, isOpenIdUser, domainName);
                }
            } catch (IOException e) {
                //todo provide a solution for this
                logger.error("Error occurred while redirecting", e);
            }
        } else {
            //todo provide a solution for this.
            logger.debug("Can not find necessary parameters, Filter {} will not execute", this);
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (!preFilter(servletRequest, servletResponse, filterChain)) {
            return;
        }

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession();

        Enumeration<String> attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String s = attributeNames.nextElement();
            log.debug("" + s + " : " + session.getAttribute(s));
        }

        final String ticket = CommonUtils.safeGetParameter(request, getArtifactParameterName());

        String userId = request.getParameter(USER_ID);
        logger.debug("Current user id [{}]", userId);

        Object userIdInSession = session.getAttribute(USER_ID);

        if (userId != null && userIdInSession != null && !userId.equals(userIdInSession)) {
            logger.debug("Unauthorized access detected, old user id [{}] and new user id [{}]. Removing information from session", userIdInSession, userId);
            session.removeAttribute(USER_ID);
            session.removeAttribute(CUSTOM_AUTHENTICATION_INTERCEPTOR_EXECUTED);
        }

        if (CommonUtils.isNotBlank(ticket) && session.getAttribute(CUSTOM_AUTHENTICATION_INTERCEPTOR_EXECUTED) == null) {
            if (log.isDebugEnabled()) {
                log.debug("Attempting to validate ticket: " + ticket + " for :" + request.getRequestURI());
            }

            try {
                final Assertion assertion = ticketValidator.validate(ticket, constructServiceUrl(request, response));

                if (log.isDebugEnabled()) {
                    log.debug("Successfully authenticated user: " + assertion.getPrincipal().getName());
                }

                request.setAttribute(CUSTOM_CAS_ASSERTION, assertion);
                session.setAttribute(CUSTOM_CAS_ASSERTION, assertion);

                onSuccessfulValidation(request, response, assertion);
                return;
            } catch (final TicketValidationException e) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                log.warn(e, e);

                onFailedValidation(request, response);

                if (exceptionOnValidationFailure) {
                    throw new ServletException(e);
                }
                return;
            }
        } else {
            log.debug("Authentication object already exists. No validation will take place for uri : " + request.getRequestURI());

            String service = request.getParameter(LOCAL_SERVICE);
            String url;

            if (service != null && service.contains("?")) {
                url = service + "&" + "ticket=" + request.getParameter("ticket");
            } else {
                url = service + "?" + "ticket=" + request.getParameter("ticket");
            }

            if (service != null && service.contains("settings")) {
                url = webHomePageUrlEditProfile;
            }

            if (service != null) {
                log.debug("Redirect to :" + url);
                response.sendRedirect(url);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

    public void setInternalUserDomain(String internalUserDomain) {
        this.internalUserDomain = internalUserDomain;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        this.landingPageUrl = landingPageUrl;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setCasLogoutUrl(String casLogoutUrl) {
        this.casLogoutUrl = casLogoutUrl;
    }
}