package hms.common.registration.ui.content.user.corporate;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.DeveloperType;
import hms.common.registration.model.UserGroup;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.user.success.UserCreationSuccessView;
import hms.common.registration.ui.content.user.verification.UserVerificationView;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.validator.EmailCheckValidator;
import hms.common.registration.ui.validator.MsisdnExistValidator;
import hms.common.registration.ui.validator.RetypeValidator;
import hms.common.registration.ui.validator.UserNameCheckValidator;
import hms.common.registration.util.Encrypter;
import hms.common.registration.util.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.ui.common.util.ExternalLinks.CAS_LOGIN_URL;
import static hms.common.registration.ui.common.util.ExternalLinks.CORPORATE_TERMS_URL;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;
import static hms.common.registration.util.WebUtils.generateVerificationCode;
import static hms.common.registration.util.ui.GridLayoutUtil.*;

public class CorporateRegistrationForm extends Form {

    private static final Logger logger = LoggerFactory.getLogger(CorporateRegistrationForm.class);

    private CommonRegistrationApp commonRegistrationApp;
    private static final int maxColumns = 2;
    private static final int maxRows = 40;
    private final GridComponentCursor gridComponentCursor = new GridComponentCursor(maxColumns, maxRows);
    private CorporateUser corporateUser = new CorporateUser();
    private BeanItem<CorporateUser> beanItem = new BeanItem<>(corporateUser);

    private final ImmutableMap<String, Field> fieldGeneratorMap;

    private GridLayout userFormGridLayout = new GridLayout(maxColumns, maxRows);

    private final Button saveButton;
    private final Button resetButton;
    private final Button cancelButton;


    private final TextField organizationName;
    private final ComboBox organizationSize;
    private final ComboBox industry;

    private final TextField contactPersonName;
    private final TextField mobileNoField;


    private final ComboBox  countryField;
    private final ComboBox  cityField;
    private final TextArea  addressField;
    private final TextArea  organizationPhoneNumbersField;
    private final TextArea  organizationFaxNumbersField;
    private final TextField emailField;
    private final TextField confirmEmailField;

    private final TextField usernameField;
    private final PasswordField passwordField;
    private final PasswordField retypePasswordField;
    /*private final ComboBox securityQuestionField;
    private final TextField securityQuestionAnswerField;*/

    private final TextField tinField;
    private final TextField businessId;
    private final TextField businessLicenseNumber;

    private final CheckBox termsAcceptedCheckBox;
    private final CaptchaField captchaField;
    private final TextField captchaText;

    private final UserService userService;
    private final UserGroupService userGroupService;
    private final MailSender mailSender;

    public CorporateRegistrationForm(CommonRegistrationApp commonRegistrationApp) {

        ContentPanel panel = commonRegistrationApp.getMainLayout().getContentLayout().getContentPanel();
        panel.setCaption(commonRegistrationApp.getMessage("registration.corporate.developer.heading"));

        this.commonRegistrationApp = commonRegistrationApp;

        this.userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        this.userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        this.mailSender = (MailSender) commonRegistrationApp.getBean("mailSender");

        saveButton = actionButton("registration.button.submit", commonRegistrationApp);
        resetButton = actionButton("registration.button.reset", commonRegistrationApp);
        cancelButton = actionButton("registration.button.cancel", commonRegistrationApp);
        termsAcceptedCheckBox = new CheckBox(commonRegistrationApp.getMessage("registration.button.individual.create.account.button"));
        captchaField = new CaptchaField(commonRegistrationApp);

        initForm();

        /* organization details */
        final Label heading1 = generateHeading("registration.user.organization.details.heading", commonRegistrationApp);
        render(heading1, new GridArea(0, 0, 1, 0), userFormGridLayout);

        organizationName = halfTextField(new GridArea(0, 1, 0, 1), "registration.user.orgName", Optional.of("registration.user.organization.name.required"), true);
        organizationSize = generateComboBox(new GridArea(0, 2, 0, 2), "registration.user.orgSize", Optional.<String>absent(), false, ORGANIZATION_SIZE);
        industry = generateComboBox(new GridArea(0, 3, 0, 3), "registration.user.industry", Optional.<String>absent(), false, INDUSTRIES);

        /* primary contact person details */

        render(getSpacer(), new GridArea(0, 4, 1, 4), userFormGridLayout);
        final Label primaryContactPersonDetailsHeading = generateHeading("registration.developer.individual.primary.contact.person.details", commonRegistrationApp);
        render(primaryContactPersonDetailsHeading, new GridArea(0, 5, 1, 5), userFormGridLayout);

        contactPersonName = halfTextField(new GridArea(0, 6, 0, 6), "registration.user.contactPersonName", Optional.of("registration.user.organization.contact.person.name.required"), true);
        mobileNoField = halfTextField(new GridArea(0, 7, 0, 7), "registration.developer.individual.mobile.no", Optional.of("registration.developer.individual.mobile.no.required.error"), true);

        /* contact details */

        render(getSpacer(), new GridArea(0, 8, 1, 8), userFormGridLayout);
        final Label heading3 = generateHeading("registration.developer.individual.contact.details", commonRegistrationApp);
        render(heading3, new GridArea(0, 9, 1, 9), userFormGridLayout);

        countryField = generateComboBox(new GridArea(0, 10, 0, 10), "registration.user.country", Optional.of("registration.user.country.required"), true, SUPPORTED_COUNTRIES);
        cityField = generateComboBox(new GridArea(0, 11, 0, 11), "registration.user.city", Optional.of("registration.user.city.required"), true, SUPPORTED_CITIES);
        addressField = generateTextArea(new GridArea(0, 12, 1, 12), "registration.user.address", Optional.<String>absent(), false, 80, 4);
        organizationPhoneNumbersField = generateTextArea(new GridArea(0, 13, 0, 13), "registration.user.orgPhoneNumbers", Optional.<String>absent(), false, 60, 2);
        organizationFaxNumbersField = generateTextArea(new GridArea(1, 13, 1, 13), "registration.user.orgFaxNo", Optional.<String>absent(), false, 60, 2);
        emailField = halfTextField(new GridArea(0, 14, 0, 14), "registration.user.email", Optional.of("registration.user.email.required"), true);
        confirmEmailField = halfTextField(new GridArea(1, 14, 1, 14), "registration.user.confirmEmail", Optional.of("registration.user.email.required"), true);

        /* select id and password */
        render(getSpacer(), new GridArea(0, 15, 1, 15), userFormGridLayout);
        final Label heading4 = generateHeading("registration.developer.individual.select.id.password", commonRegistrationApp);
        render(heading4, new GridArea(0, 16, 1, 16), userFormGridLayout);

        usernameField = halfTextField(new GridArea(0, 17, 0, 17), "registration.user.username", Optional.of("registration.user.username.required"), true);
        passwordField = passwordTextField(new GridArea(0, 18, 0, 18), "registration.user.password", Optional.of("registration.user.password.required"), true);
        retypePasswordField = passwordTextField(new GridArea(1, 18, 1, 18), "registration.user.retypePassword", Optional.of("registration.user.retype.password.required"), true);
        /*securityQuestionField = generateComboBox(new GridArea(0, 19, 0, 19), "registration.user.security.question", Optional.of("registration.user.security.question.required.error"), true, SECURITY_QUESTIONS);
        securityQuestionAnswerField = halfTextField(new GridArea(1, 19, 1, 19), "registration.user.security.question.answer", Optional.of("registration.user.security.question.answer.required.error"), true);*/

        /* reconciliation details */
        render(getSpacer(), new GridArea(0, 20, 1, 20), userFormGridLayout);
        final Label heading5 = generateHeading("registration.developer.individual.reconciliation.details", commonRegistrationApp);
        render(heading5, new GridArea(0, 21, 1, 21), userFormGridLayout);

        tinField = halfTextField(new GridArea(0, 22, 0, 22), "registration.user.tin", Optional.of("registration.user.tin.number.required"), true);
        businessId = halfTextField(new GridArea(1, 23, 1, 23), "registration.user.businessId", Optional.<String>absent(), false);
        businessLicenseNumber = halfTextField(new GridArea(0, 23, 0, 23), "registration.business.license.number", Optional.of("registration.user.businessId.required"), true);

        /* verification */
        render(getSpacer(), new GridArea(0, 24, 1, 24), userFormGridLayout);
        final Label heading6 = generateHeading("registration.user.word.verification.heading", commonRegistrationApp);
        render(heading6, new GridArea(0, 25, 1, 25), userFormGridLayout);

        render(captchaField, new GridArea(0, 26, 0, 26), userFormGridLayout);

        captchaText = halfTextField(new GridArea(0, 27, 0, 27), "registration.user.captcha.msg", Optional.<String>absent(), true);

        /* terms layout*/
        render(getSpacer(), new GridArea(0, 28, 1, 28), userFormGridLayout);
        final Label heading7 = generateHeading("registration.user.user.terms", commonRegistrationApp);
        render(heading7, new GridArea(0, 29, 1, 29), userFormGridLayout);
        render(generateUserTermsLayout(), new GridArea(0, 30, 1, 30), userFormGridLayout);

        render(getSpacer(), new GridArea(0, 31, 1, 31), userFormGridLayout);
        final Layout buttonLayout = generateButtonLayout();
        render(buttonLayout, new GridArea(0, 32, 1, 32), userFormGridLayout, 30, Alignment.MIDDLE_CENTER);

        addFieldValidators();

        fieldGeneratorMap = ImmutableMap.<String, Field>builder().
                put("orgName", organizationName).
                put("orgSize", organizationSize).
                put("industry", industry).
                put("contactPersonName", contactPersonName).
                put("mobileNo", mobileNoField).
                put("country", countryField).
                put("city", cityField).
                put("address", addressField).
                put("orgPhoneNumbers", organizationPhoneNumbersField).
                put("orgFaxNo", organizationFaxNumbersField).
                put("email", emailField).
                put("username", usernameField).
                put("password", passwordField).
                /*put("securityQuestion", securityQuestionField).
                put("securityQuestionAnswer", securityQuestionAnswerField).*/
                put("tin", tinField).
                put("businessId", businessId).
                put("businessLicenseNumber", businessLicenseNumber).
                build();

        this.setItemDataSource(beanItem, fieldGeneratorMap.keySet());

        addDefaultOrInitialValues();
    }

    private void addDefaultOrInitialValues() {
        mobileNoField.setValue(commonRegistrationApp.getBean("countryCode"));
        countryField.setValue(SUPPORTED_COUNTRIES.get(0));
    }

    private void initForm() {
        userFormGridLayout.setWidth(100, UNITS_PERCENTAGE);
        this.setLayout(userFormGridLayout);
    }

    private void addFieldValidators() {
        emailField.addValidator(new EmailValidator(commonRegistrationApp.getMessage("registration.user.invalid.email")));
        final EmailCheckValidator validator = new EmailCheckValidator(commonRegistrationApp);
        validator.setUserId(corporateUser.getUserId());
        emailField.addValidator(validator);

        mobileNoField.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN, commonRegistrationApp.getMessage("registration.corporate.user.invalid.msisdn")));
        mobileNoField.addValidator(new MsisdnExistValidator(commonRegistrationApp));

        addressField.addValidator(new RegexpValidator(ADDRESS_LENGTH_VALIDATION, commonRegistrationApp.getMessage("registration.user.address.length.validation")));

        usernameField.addValidator(new RegexpValidator(USER_NAME_REGEX_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.username")));
        usernameField.addValidator(new UserNameCheckValidator(commonRegistrationApp));

        passwordField.addValidator(new RegexpValidator(PASSWORD_REGEX_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.password")));

        businessId.addValidator(new RegexpValidator(BUSINESS_ID_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.business.id")));

        businessLicenseNumber.addValidator(new RegexpValidator(BUSINESS_LICENSE_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.business.license.number")));

        retypePasswordField.addValidator(new RetypeValidator(passwordField, commonRegistrationApp.getMessage("registration.user.password.does.not.match")));
        confirmEmailField.addValidator(new RetypeValidator(emailField, commonRegistrationApp.getMessage("registration.user.email.does.not.match")));
    }

    public void setItemDataSource(Item newDataSource, Collection<?> propertyIds) {
        // Removes all fields first from the form
        removeAllProperties();
        Item itemDataSource = newDataSource;
        if (itemDataSource == null) {
            return;
        }

        for (Object propertyId : propertyIds) {
            final Property itemProperty = itemDataSource.getItemProperty(propertyId);
            final Field field = fieldGeneratorMap.get(propertyId.toString());
            field.setPropertyDataSource(itemProperty);
        }
    }

    public TextField halfTextField(GridArea gridArea, String caption, Optional<String> requiredError, boolean mandatory) {
        final TextField textField = textField(caption, mandatory, requiredError.isPresent() ? requiredError.get() : "", commonRegistrationApp);
        textField.setImmediate(true);
        render(textField, gridArea, userFormGridLayout, 60, Alignment.TOP_LEFT);
        return textField;
    }

    public PasswordField passwordTextField(GridArea gridArea, String caption, Optional<String> requiredError, boolean mandatory) {
        final PasswordField textField = passwordField(caption, mandatory, requiredError.isPresent() ? requiredError.get() : "", commonRegistrationApp);
        textField.setImmediate(true);
        render(textField, gridArea, userFormGridLayout, 60, Alignment.TOP_LEFT);
        return textField;
    }

    public TextArea generateTextArea(GridArea gridArea, String captionId, Optional<String> requiredError, boolean isMandatory, int width, int maxRows) {
        TextArea textArea = textArea(captionId, isMandatory, requiredError.isPresent() ? requiredError.get() : "", commonRegistrationApp);
        textArea.setImmediate(true);
        textArea.setRows(maxRows);
        render(textArea, gridArea, userFormGridLayout, width, Alignment.TOP_LEFT);
        return textArea;
    }

    public ComboBox generateComboBox(GridArea gridArea, String captionId, Optional<String> requiredError, boolean mandatory, List<String> items) {
        final ComboBox comboBox = comboBox(captionId, mandatory, requiredError.isPresent() ? requiredError.get() : "", items, commonRegistrationApp);
        comboBox.setImmediate(true);
        render(comboBox, gridArea, userFormGridLayout, 60, Alignment.TOP_LEFT);
        return comboBox;
    }

    public DateField dateField(GridArea gridArea, String captionId, String requiredErrorId) {
        final DateField dateField = new DateField(commonRegistrationApp.getMessage(captionId));
        dateField.setDateFormat(COMMON_DATE_FORMAT);
        dateField.setRequired(true);
        dateField.setRequiredError(commonRegistrationApp.getMessage(requiredErrorId));
        dateField.setImmediate(true);
        render(dateField, gridArea, userFormGridLayout, 60, Alignment.TOP_LEFT);
        return dateField;
    }

    public VerticalLayout generateUserTermsLayout() {
        VerticalLayout verticalLayout = new VerticalLayout();

        if (isExternalTermsLinkEnabled()) {
            Link checkTermsLink = new Link(commonRegistrationApp.getMessage("registration.user.user.terms.link"),
                    new ExternalResource(CORPORATE_TERMS_URL), "_blank", -1, -1, Window.BORDER_DEFAULT);
            verticalLayout.addComponent(checkTermsLink);

        } else {
            Button checkTermsButton = new Button(commonRegistrationApp.getMessage("registration.user.user.terms.link"));
            checkTermsButton.setStyleName("link");
            checkTermsButton.addListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    getWindow().addWindow(CommonUiComponentGenerator.generateCorporateUserTermsSubWindow(commonRegistrationApp));
                }
            });
            verticalLayout.addComponent(checkTermsButton);

        }
        verticalLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.user.terms.last.message")));

        termsAcceptedCheckBox.setImmediate(true);
        termsAcceptedCheckBox.addListener(new ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                saveButton.setEnabled((Boolean) event.getProperty().getValue());
            }
        });
        verticalLayout.addComponent(termsAcceptedCheckBox);

        return verticalLayout;
    }


    private void reset() {
        if (isCaptchaValidationEnabled()) {
            captchaText.setValue(null);
        }
        termsAcceptedCheckBox.setValue(false);
        retypePasswordField.setValue(null);
        confirmEmailField.setValue(null);

        for (Field field : fieldGeneratorMap.values()) {
            if (!field.isReadOnly()) {
                field.setValue(null);
            }
        }
        this.setComponentError(null);
        this.discard();
    }

    public Layout generateButtonLayout() {
        final GridLayout gridLayout = new GridLayout(3, 1);

        saveButton.setEnabled(false);

        resetButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                reset();
                setComponentError(null);
            }
        });

        cancelButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                commonRegistrationApp.setLogoutURL(CAS_LOGIN_URL);
                commonRegistrationApp.close();
            }
        });

        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                formSubmissionFlow();
            }
        });

        gridLayout.addComponent(saveButton, 0, 0, 0, 0);
        gridLayout.addComponent(resetButton, 1, 0, 1, 0);
        gridLayout.addComponent(cancelButton, 2, 0, 2, 0);

        gridLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_CENTER);
        gridLayout.setComponentAlignment(resetButton, Alignment.MIDDLE_CENTER);
        gridLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_CENTER);

        return gridLayout;
    }

    private void formSubmissionFlow() {
        try {
            commit();
            validate();
            validateCaptchaText();
            goToVerificationFlow();
        } catch (Validator.InvalidValueException exception) {
            logger.debug("Invalid Form Entries [{}]", exception.getMessage());
            setComponentError(new UserError(exception.getMessage()));
        }

    }

    @Override
    public void validate() throws Validator.InvalidValueException {
        super.validate();    //To change body of overridden methods use File | Settings | File Templates.
        for (Field field : fieldGeneratorMap.values()) {
                field.validate();
        }
        retypePasswordField.validate();
        confirmEmailField.validate();
    }

    private void validateCaptchaText() {
        if (isCaptchaValidationEnabled()) {
            if (!captchaField.validateCaptcha((String) captchaText.getValue())) {
                captchaText.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.captcha.code.invalid")));
                setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.captcha.code.invalid")));
                throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.captcha.code.invalid"));
            } else {
                captchaText.setComponentError(null);
            }
        }
    }


    private void goToVerificationFlow() {
        try {
            setAdditionalUserDetails();
            createUserAccount();
            sendEmail();

            ContentPanel panel = commonRegistrationApp.getMainLayout().getContentLayout().getContentPanel();
            panel.removeAllComponents();
            panel.setCaption(commonRegistrationApp.getMessage("registration.user.complete.your.registration"));

            if (isCorporateUserMsisdnVerifyEnabled()) {
                panel.addComponent(new UserVerificationView(corporateUser, commonRegistrationApp, false));
            } else {
                panel.addComponent(new UserCreationSuccessView(corporateUser, commonRegistrationApp, UserCreationSuccessView.EMAIL));
            }
        } catch (Throwable e) {
            e.printStackTrace();
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.user.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
    }

    private void createUserAccount() throws DataManipulationException {
        userService.persist(corporateUser);
    }

    private void sendEmail() throws NoSuchAlgorithmException, UnsupportedEncodingException, DataManipulationException {
        if (isCorporateUserEmailVerifyEnabled()) {
            mailSender.sendUserVerificationEmail(corporateUser.getContactPersonName(), corporateUser.getUsername(), corporateUser.getEmail(),
                    corporateUser.getEmailVerificationCode(), corporateUser.getUserId());
        }
    }

    private void setAdditionalUserDetails() throws NoSuchAlgorithmException, UnsupportedEncodingException, DataManipulationException {
        corporateUser.setPassword(MD5(corporateUser.getPassword()));
        corporateUser.setOperator(getOperatorFromMsisdn(corporateUser.getMobileNo()));
        String emailVerificationCode = generateVerificationCode();
        corporateUser.setEmailVerificationCode(Encrypter.MD5(emailVerificationCode));
        UserGroup corporateUserGroup = userGroupService.findUserGroupByGroupName("CORPORATE_USER");
        corporateUser.setUserGroup(corporateUserGroup);
        corporateUser.setEnabled(true);
        corporateUser.setUserType(UserType.CORPORATE);
        corporateUser.setCorporateUser(true);
        corporateUser.setDeveloperType(DeveloperType.CORPORATE);
    }

    private String getOperatorFromMsisdn(String msisdn) {
        Set<String> supportedOperatorPrefixSet = OPERATOR_CODE_MAP.keySet();
        for (String operatorPrefix : supportedOperatorPrefixSet) {
            if (msisdn != null) {
                if (msisdn.startsWith(operatorPrefix)) {
                    return OPERATOR_CODE_MAP.get(operatorPrefix);
                }
            }
        }
        return "";
    }
}
