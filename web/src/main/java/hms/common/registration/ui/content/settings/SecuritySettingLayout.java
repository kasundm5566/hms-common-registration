/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.settings;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateHeading;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.getSpacer;


/**
 * Handles the layout design for user account security settings 
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SecuritySettingLayout extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecuritySettingLayout.class);
    private CommonRegistrationApp commonRegistrationApp;
    private UserService userService;

    /**
     * Generates layout for user account details
     * @param commonRegistrationApp
     */
    public SecuritySettingLayout(CommonRegistrationApp commonRegistrationApp) {
        setSizeFull();
        this.commonRegistrationApp = commonRegistrationApp;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        initSecuritySettingLayout();
    }

    /**
     * Initializes the security settings UI
     */
    private void initSecuritySettingLayout() {
        LOGGER.debug("Initializing security settings layout for user [{}]", commonRegistrationApp.currentUserName());
        Label changePasswordHeading = generateHeading("registration.change.password.heading", commonRegistrationApp);
        addComponent(getSpacer());
        addComponent(changePasswordHeading);
        addComponent(new ChangePasswordForm(commonRegistrationApp));
        addComponent(getSpacer());
        addComponent(getSpacer());
        /*String userName = commonRegistrationApp.currentUserName();
        try {
            User user = userService.findUserByName(userName);
            if(user.getUserType().equals(UserType.INDIVIDUAL) || user.getUserType().equals(UserType.CORPORATE)) {
                Label changeMpinHeading = generateHeading("registration.change.mpin.heading");
                addComponent(changeMpinHeading);
                addComponent(new ChangeMpinForm(commonRegistrationApp));
            }
        } catch (DataManipulationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }*/

    }

}

