/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.common;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: chandana
 * Date: 11/21/11
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */
public interface WindowCloseListener {

    public void onWindowClosed(Map data);
}
