/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.home;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.user.corporate.CorporateUserForm;
import hms.common.registration.ui.template.ContentLayout;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.template.MainLayout;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.vaadin.ui.Alignment.MIDDLE_CENTER;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.ui.common.util.ExternalLinks.*;
import static hms.common.registration.model.UserType.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;
import static hms.common.registration.util.ValidateUser.isValidUser;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@SuppressWarnings("serial")
public class Dashboard extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(Dashboard.class);

    private CommonRegistrationApp commonRegistrationApp;

    private MainLayout mainLayout;
    private UserService userService;
    private GridLayout gridLayout;

    public Dashboard(CommonRegistrationApp commonRegistrationApp, MainLayout mainLayout) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.mainLayout = mainLayout;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");

        init();
    }

    private void init() {
        setWidth("80%");
        setSizeFull();
        gridLayout = new GridLayout(3, 3);
        gridLayout.setSizeFull();
        gridLayout.setSpacing(true);
        gridLayout.setSizeFull();

        createDashboard();
    }

    private void createDashboard() {
        String loggedInUserName;
        try {
            loggedInUserName = commonRegistrationApp.currentUserName();
        } catch (NullPointerException e) {
            return;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while getting username", e);
            return;
        }

        try {
            User user = userService.findUserByName(loggedInUserName);
            if (user != null) {
                if (isValidUser(user)) {
                    Boolean firstTimeLogin = Boolean.TRUE.equals(user.isFirstTimeLogin()) ;
                    if(isFirstTimeLoginMessageEnabled() && firstTimeLogin) {
                        Label label = new Label(commonRegistrationApp.getMessage("registration.first.time.login.message"));
                        user.setFirstTimeLogin(Boolean.FALSE);
                        user = userService.merge(user);
                        addComponent(label);
                    }
                    filterLinksByUser(user);
                    addComponent(gridLayout);
                    ContentLayout contentLayout = mainLayout.getContentLayout();
                    ContentPanel contentPanel = contentLayout.getContentPanel();
                    contentLayout.removeAllComponents();
                    contentPanel.removeAllComponents();
                    contentPanel.addComponent(this);
                    contentPanel.setCaption(commonRegistrationApp.getMessage("registration.home.page.heading"));
                    contentLayout.setSizeFull();
                    contentLayout.addComponent(contentPanel);
                } else {
                    commonRegistrationApp.getFragmentUtility().setFragment("verify");
                }
            }
        } catch (DataManipulationException e) {
            logger.error("Error occurred while getting user details", e);
        } catch (Exception e) {
            logger.error("Error occurred while generating dashboard", e);
        }
    }

    private void filterLinksByUser(User user) {
        logger.debug("Logged user details");
        UserType userType = user.getUserType();
        logger.debug("user type  [{}]", userType);
        logger.debug("user group [{}]", user.getUserGroup());
        List<String> userRoles = user.getRolesAsStrings();

        if (user.getDomain().equals("dialog")) {
            if (userType.equals(INDIVIDUAL)) {
                createIndividualUserConvertView(user);
                return;
            } else {
                if (!checkBeneficiaryDetails(user)) {
                    return;
                }
            }
        }

        mainLayout.getTopHeaderLayout().setModulesMenuVisible(false);

        addDashboardLink(userRoles, "ROLE_PROV_LOGIN", PROVISIONING_URL, PROVISION_ICON, "module.provisioning", "module.provisioning.tip");
        if (userType.equals(ADMIN_USER) || (userType.equals(CORPORATE) && isSubCorporateUserEnabled())) {
            addDashboardLink(userRoles, "ROLE_REG_LOGIN", "#admin", REGISTRATION_ADMIN_ICON, "module.registration", "module.registration.tip");
        }
        addDashboardLink(userRoles, "ROLE_APP_LOGIN", APPSTORE_URL, APPSTORE_ICON, "module.appstore", "module.appstore.tip");
        addDashboardLink(userRoles, "ROLE_APP_ADMIN_LOGIN", APPSTORE_ADMIN_URL, APPSTORE_ADMIN_ICON, "module.appstore.admin", "module.appstore.admin.tip");
        addDashboardLink(userRoles, "ROLE_ADM_LOGIN", SDP_URL, SDP_ADMIN_ICON, "module.sdp", "module.sdp.tip");
        addDashboardLink(userRoles, "ROLE_SOL_LOGIN", SOLTURA_URL, SOLTURA_ICON, "module.soltura", "module.soltura.tip");
        addDashboardLink(userRoles, "ROLE_SDP_RPT_LOGIN", REPORTING_URL, REPORTING_ICON, "module.reporting", "module.reporting.tip");
        addDashboardLink(userRoles, "ROLE_PGW_CSC_LOGIN", PGW_SELF_CARE_URL, PGW_SELF_CARE_ICON, "module.pgw.self.care", "module.pgw.self.care.tip");
        addDashboardLink(userRoles, "ROLE_PGW_CS_LOGIN", PGW_ADMIN_CARE_URL, PGW_ADMIN_CARE_ICON, "module.pgw.admin.care", "module.pgw.admin.care.tip");
    }

    private void createIndividualUserConvertView(final User user) {
        VerticalLayout outerLayout = new VerticalLayout();
        VerticalLayout confirmation = new VerticalLayout();
        Label messageLbl = new Label(commonRegistrationApp.getMessage("registration.change.user.type.confirmation.message"));
        confirmation.addComponent(messageLbl);
        HorizontalLayout buttons = new HorizontalLayout();

        Button yesBtn = new Button("Yes", new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                convertUserAndGetAdditionalInfo(user);
            }
        });
        yesBtn.setImmediate(true);
        yesBtn.setStyleName("action-button");
        buttons.addComponent(yesBtn);
        VerticalLayout spacer = new VerticalLayout();
        spacer.setWidth("10px");
        buttons.addComponent(spacer);
        Button noBtn = new Button("No", new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                commonRegistrationApp.getMainWindow().open(new ExternalResource(APPSTORE_URL));
            }
        });
        noBtn.setImmediate(true);
        noBtn.setStyleName("action-button");
        buttons.addComponent(noBtn);

        buttons.setMargin(true);
        confirmation.addComponent(buttons);
        confirmation.setMargin(true);
        confirmation.setWidth("400px");
        confirmation.setComponentAlignment(messageLbl, MIDDLE_CENTER);
        confirmation.setComponentAlignment(buttons, MIDDLE_CENTER);

        mainLayout.getTopHeaderLayout().setTopLinksVisible(false);
        outerLayout.addComponent(confirmation);
        outerLayout.setSizeFull();
        outerLayout.setComponentAlignment(confirmation, MIDDLE_CENTER);
        addComponent(outerLayout);
    }

    private void convertUserAndGetAdditionalInfo(User user) {
        try {
            UserGroupService userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
            UserGroup corporateUserGroup = userGroupService.findUserGroupByGroupName("CORPORATE_USER");
            CorporateUser corporateUser = new CorporateUser(user, corporateUserGroup);
            corporateUser.setCorporateUser(true);
            corporateUser.setUserType(CORPORATE);
            corporateUser.setCorperateParentId(corporateUser);
            User newUser = userService.convertUser(user, corporateUser);
            if (isAutoSpCreationEnabled()) {
                userService.createDefaultSpForUser(user);
            }
            checkBeneficiaryDetails(newUser);
        } catch (DataManipulationException e) {
            logger.error("Error occurred while saving user data", e);
        }
    }

    private boolean checkBeneficiaryDetails(User user) {
        String beneficiaryName = user.getBeneficiaryName();
        if (beneficiaryName == null || beneficiaryName.isEmpty()) {
            logger.debug("Beneficiary details not available for user [{}]", user.getUsername());
            removeAllComponents();
            mainLayout.getTopHeaderLayout().setTopLinksVisible(false);
            addComponent(new CorporateUserForm(user, commonRegistrationApp, true));
            return false;
        }

        return true;
    }

    private void addDashboardLink(List<String> userRoles, String role, String linkLocation,
                                  ThemeResource icon, String linkHeading, String toolTip) {
        if (userRoles.contains(role)) {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setSpacing(true);
            Link link = new Link("", new ExternalResource(linkLocation));
            link.setDescription(commonRegistrationApp.getMessage(toolTip));
            link.setIcon(icon);
            layout.addComponent(link);
//            Label heading = new Label(commonRegistrationApp.getMessage(linkHeading));
//            heading.setStyleName("menu-headings");
            Link heading = new Link(commonRegistrationApp.getMessage(linkHeading), new ExternalResource(linkLocation));
            heading.setStyleName("menu-headings-view");
            layout.addComponent(heading);
            gridLayout.addComponent(layout);
        }
    }
}