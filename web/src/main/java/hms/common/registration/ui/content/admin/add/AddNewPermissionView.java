/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.admin.add;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.admin.search.ManagePermissionView;
import hms.common.registration.util.RoleWrapperBean;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import hms.common.registration.service.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.PropertyHolder.ROLE_DESCRIPTION_LENGTH_VALIDATION;
import static hms.common.registration.util.PropertyHolder.ROLE_NAME_LENGTH_VALIDATION;
import static hms.common.registration.service.ModuleService.*;

/**
 * Handle the Add new role form implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AddNewPermissionView extends VerticalLayout implements Property.ValueChangeListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddNewPermissionView.class);
    private CommonRegistrationApp commonRegistrationApp;
    private ModuleService moduleService;
    private Role role;
    private boolean editState;
    private RoleWrapperBean roleWrapperBean;

    public AddNewPermissionView(CommonRegistrationApp commonRegistrationApp, Role role, boolean editState) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.role = role;
        this.editState = editState;
        moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        roleWrapperBean = new RoleWrapperBean();
        try {
            generateAddRoleView();
        } catch (Exception e) {
            LOGGER.error("Error occured while creating new Permission", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.role.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
    }

    private void generateAddRoleView() {
        setSpacing(true);
        setSizeFull();
        setMargin(false);
        Form addRoleForm = new Form();
        addRoleForm.getLayout().setWidth("600px");
        if (editState) {
            roleWrapperBean.setDescription(role.getDescription());
            roleWrapperBean.setModule(role.getModule());
            roleWrapperBean.setName(role.getName());
        }
        BeanItem<RoleWrapperBean> beanItem = new BeanItem<RoleWrapperBean>(roleWrapperBean);
        addRoleForm.setImmediate(true);
        addRoleForm.setItemDataSource(beanItem);
        addRoleForm.setWriteThrough(false);
        addRoleForm.setFormFieldFactory(new PermissionFieldFactory(this, commonRegistrationApp));
        String[] visibleItemProperties;
        if (editState) {
            visibleItemProperties = new String[]{
                    MODULE, ROLE_NAME, ROLE_DESCRIPTION
            };
        } else {
            visibleItemProperties = new String[]{
                    MODULE, ROLE_PREFIX, ROLE_NAME, ROLE_DESCRIPTION, LIST_ALL_ROLES
            };
        }
        addRoleForm.setVisibleItemProperties(visibleItemProperties);
        if (editState) {
            addRoleForm.getField(MODULE).setReadOnly(true);
            addRoleForm.getField(ROLE_NAME).setReadOnly(true);
            addRoleForm.getField(ROLE_DESCRIPTION).setReadOnly(true);
        }
        createFormFooter(addRoleForm);
        addComponent(addRoleForm);
    }

    private void createFormFooter(final Form addRoleForm) {
        addRoleForm.setFooter(new VerticalLayout());
        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setWidth("600px");
        footerLayout.setHeight("50px");
        final HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setWidth("200px");
        final Button cancelButton = createButton("registration.button.cancel", commonRegistrationApp);
        cancelButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                LOGGER.debug("Discarding the input values..........");

                if(!editState){
                addRoleForm.discard();
                    changeView("registration.admin.permission.heading",
                                new ManagePermissionView(commonRegistrationApp, null),
                                "registration.admin.permission.manage.heading");
                }
                else{
                    commonRegistrationApp.getMainWindow().removeWindow(addRoleForm.getWindow());
                }
            }
        });

        final Button saveButton = createButton("registration.button.submit", commonRegistrationApp);
        if (editState) {
            saveButton.setCaption(commonRegistrationApp.getMessage("registration.button.save"));
        }
        saveButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                try {
                    addRoleForm.commit();
                    addRoleForm.validate();
                    if (!editState) {
                        roleWrapperBean.setRolePrefix((String) addRoleForm.getField(ROLE_PREFIX).getValue());
                    }
                    LOGGER.error(roleWrapperBean.toString());
                    LOGGER.error(roleWrapperBean.toString());
                    roleWrapperBean.persistRole(role);
                    if (!editState && moduleService.isRoleExist(role.getName())) {
                        notification(commonRegistrationApp.getMainWindow(), "registration.admin.role.name.already.exist", WARNING, commonRegistrationApp);
                    } else {
                        try {
                            saveRoleDetails();
                            removeAllComponents();
                            setMargin(true);
                            if (editState) {
                                addComponent(generateSuccessMessage("registration.user.role.edit.success", commonRegistrationApp));
                            } else {
                                addComponent(generateSuccessMessage("registration.user.role.creation.success", commonRegistrationApp));
                            }
                        } catch (DataManipulationException e) {
                            LOGGER.error("Error occurred while creating Permission", e);
                            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                                    "registration.user.role.creation.error", com.vaadin.ui.Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                        }
                    }
                } catch (Validator.InvalidValueException e) {
                    LOGGER.error("Invalid Form Entries", e);
                }
                LOGGER.debug("Creating New  Permission [{}]" + role.getName());
            }
        });
        buttonLayout.addComponent(cancelButton);
        buttonLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_LEFT);
        buttonLayout.addComponent(saveButton);
        buttonLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
        footerLayout.addComponent(buttonLayout);
        footerLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        addRoleForm.setFooter(footerLayout);

    }

    /**
     * Save additional details
     *
     * @throws DataManipulationException
     */
    private void saveRoleDetails() throws DataManipulationException {

        if (editState) {
            moduleService.mergeRole(role);
            LOGGER.debug("Edited Role [{}]", role);
        } else {
            moduleService.persistRole(role);
            LOGGER.debug("Created New Role [{}]", role);
        }
        moduleService.mergeRole(role);
    }

    private void changeView(String panelHeading,
                            Component component, String subPanelHeading) {
        commonRegistrationApp.getContentLayout().getContentPanel().addComponent(component);
        commonRegistrationApp.getInnerContentPanel().removeAllComponents();
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));

        commonRegistrationApp.getInnerContentPanel().addComponent(component);
    }

    @Override
    public void valueChange(Property.ValueChangeEvent event) {
    }
}

/**
 * Generates UI fields for add new permission view
 */
class PermissionFieldFactory implements FormFieldFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleFieldFactory.class);
    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.admin.role.";
    private ModuleService moduleService;
    private Module currentlySelectedModule;
    private ListSelect listSelect;
    private Field rolePrefixField;
    private AddNewPermissionView addNewPermissionView;
    private CommonRegistrationApp commonRegistrationApp;

    PermissionFieldFactory(AddNewPermissionView addNewPermissionView, CommonRegistrationApp commonRegistrationApp) {
        this.addNewPermissionView = addNewPermissionView;
        this.commonRegistrationApp = commonRegistrationApp;
        this.moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        listSelect = (ListSelect) createMultiSelectionList("", false, "", new ArrayList<String>(), commonRegistrationApp);
        rolePrefixField = createTextField("", false, "", commonRegistrationApp);
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;

        if (ROLE_NAME.equals(pid)) {
            Field roleNameText = createTextField(caption, true, "registration.admin.role.name.required", commonRegistrationApp);
            roleNameText.addValidator(new RegexpValidator(ROLE_NAME_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.admin.role.name.length.validation")));
            return roleNameText;
        } else if (ROLE_PREFIX.equals(pid)) {
            rolePrefixField.setCaption(caption);
            rolePrefixField.setReadOnly(true);
            return rolePrefixField;
        } else if (ROLE_DESCRIPTION.equals(pid)) {
            Field textArea = createTextArea(caption, true, "registration.admin.role.description.required", commonRegistrationApp);
            textArea.addValidator(new RegexpValidator(ROLE_DESCRIPTION_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.admin.role.description.length.validation")));
            return textArea;
        } else if (MODULE.equals(pid)) {
            Field field = createSelectionBox(caption);
            field.addListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    if (event.getProperty().getValue() != null) {
                        currentlySelectedModule = (Module) event.getProperty().getValue();
                        populateAllRolesList(listSelect, currentlySelectedModule);
                        populateRolePrefix(currentlySelectedModule);
                    }
                }
            });
            return field;
        } else if (LIST_ALL_ROLES.equals(pid)) {
            listSelect.setCaption(caption);
            return listSelect;
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            field.setWidth("200px");
            CommonUiComponentGenerator.filterNullValues(field);
            return field;
        }
    }

    private ComboBox createSelectionBox(String caption) {
        BeanItemContainer<Module> container = new BeanItemContainer<Module>(Module.class);
        List<Module> moduleList;
        try {
            moduleList = moduleService.findAllModules();
            container.addAll(moduleList);
        } catch (DataManipulationException e) {
            LOGGER.error("Error while finding all available modules", e);
        }
        ComboBox moduleNameList = new ComboBox(caption, container);
        moduleNameList.setWidth("200px");
        moduleNameList.setItemCaptionMode(ListSelect.ITEM_CAPTION_MODE_PROPERTY);
        moduleNameList.setItemCaptionPropertyId("moduleName");
        moduleNameList.setNullSelectionAllowed(true);
        moduleNameList.setImmediate(true);
        moduleNameList.setRequired(true);
        moduleNameList.setInputPrompt(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        moduleNameList.setRequiredError(commonRegistrationApp.getMessage("registration.admin.role.module.required"));
        return moduleNameList;
    }

    private void populateAllRolesList(ListSelect listSelect, Module module) {
        listSelect.removeAllItems();
        if (currentlySelectedModule != null) {
            Set<Role> roleList = module.getRoleList();
            for (Role role : roleList) {
                listSelect.addItem(role.getName());
            }
        }
    }

    private void populateRolePrefix(Module module) {
        rolePrefixField.setReadOnly(false);
        rolePrefixField.setValue(module.getRolePrefix());
        rolePrefixField.setReadOnly(true);
    }
}
