/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.template;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.home.Dashboard;

import static hms.common.registration.util.PropertyHolder.LEFT_SIDE_BANNER_CSS;
import static hms.common.registration.util.RegistrationFeatureRegistry.isShowLeftSideBanner;

/**
 * Defines the main layout for the Web UI
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MainLayout extends VerticalLayout {

    private CommonRegistrationApp commonRegistrationApp;

    private TopHeaderLayout topHeaderLayout;
    private HeaderLayout headerLayout;
    private FooterLayout footerLayout;
    private ContentLayout contentLayout;

    private void generateMainLayout() {
        this.setSizeFull();
        final VerticalLayout verticalLayout = new VerticalLayout();
        createContentLayout(verticalLayout);
        addComponent(verticalLayout);
        setComponentAlignment(verticalLayout, Alignment.TOP_CENTER);
    }

    private void generateLayoutWithLeftSideImage() {

        final HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSizeFull();
        horizontalLayout.setSpacing(true);

        final VerticalLayout contentLayout = new VerticalLayout();
        createContentLayout(contentLayout);

        String leftSideImageUrl = "images/left-side-logo.png";
        Embedded leftSideImage = new Embedded(null, new ThemeResource(leftSideImageUrl));
        leftSideImage.setStyleName(LEFT_SIDE_BANNER_CSS);
        leftSideImage.setVisible(false);

        horizontalLayout.addComponent(leftSideImage);
        horizontalLayout.addComponent(contentLayout);
        horizontalLayout.setComponentAlignment(contentLayout, Alignment.TOP_LEFT);

        addComponent(horizontalLayout);
    }

    private void createContentLayout(VerticalLayout verticalLayout) {
        verticalLayout.setSpacing(true);
        verticalLayout.setWidth("900px");
        topHeaderLayout = new TopHeaderLayout(commonRegistrationApp);
        headerLayout = new HeaderLayout(commonRegistrationApp);
        footerLayout = new FooterLayout(commonRegistrationApp);
        contentLayout = new ContentLayout();
        verticalLayout.addComponent(topHeaderLayout);
        verticalLayout.addComponent(headerLayout);
        new Dashboard(commonRegistrationApp, this);
        verticalLayout.addComponent(contentLayout);
        verticalLayout.addComponent(footerLayout);
    }

    public MainLayout(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;

        if(isShowLeftSideBanner()) {
            generateLayoutWithLeftSideImage();
        }
        else {
            generateMainLayout();
        }
    }

    public TopHeaderLayout getTopHeaderLayout() {
        return topHeaderLayout;
    }

    public HeaderLayout getHeaderLayout() {
        return headerLayout;
    }

    public FooterLayout getFooterLayout() {
        return footerLayout;
    }

    public ContentLayout getContentLayout() {
        return contentLayout;
    }
}
