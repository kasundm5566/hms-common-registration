/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui;

import java.util.Arrays;
import java.util.List;

public class SessionKeys {

    public static final String MSISDN_TMP = "tmp-msisdn";
    public static final String PRV_URL = "prv-url";

    public static final List<String> sessionKeys = Arrays.asList(MSISDN_TMP, PRV_URL);

}
