/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.user.individual;

import com.vaadin.data.Buffered;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.user.success.UserCreationSuccessView;
import hms.common.registration.ui.content.user.verification.UserVerificationView;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.validator.EmailCheckValidator;
import hms.common.registration.ui.validator.IndividualUserOperatorValidator;
import hms.common.registration.ui.validator.OperatorMsisdnValidator;
import hms.common.registration.ui.validator.UserNameCheckValidator;
import hms.common.registration.util.Encrypter;
import hms.common.registration.util.MailSender;
import hms.common.registration.util.sms.SmsMtDispatcher;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.*;
import java.util.*;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.ui.common.util.ExternalLinks.CAS_LOGIN_URL;
import static hms.common.registration.ui.common.util.ExternalLinks.CONSUMER_TERMS_URL;
import static hms.common.registration.ui.content.user.individual.IndividualUserForm.RETYPE_PASSWORD_PROPERTY_ID;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.util.RegistrationCommonUtil.getRedirectUrl;
import static hms.common.registration.util.WebUtils.generateVerificationCode;
import static hms.common.registration.util.WebUtils.isEmptyField;
import static hms.common.registration.model.UserType.CORPORATE;
import static hms.common.registration.model.UserType.INDIVIDUAL;
import static hms.common.registration.service.UserService.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;
import static hms.common.registration.util.ValidateUser.isValidUser;


/**
 * Handle the individual user creation form implementation
 * $LastChangedDate: 2011-09-28 17:13:35 +0530 (Wed, 28 Sep 2011) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 77309 $
 */
public class IndividualUserForm extends Form {

    private static final Logger logger = LoggerFactory.getLogger(IndividualUserForm.class);

    private static final int DEFAULT_NUM_OF_ROWS = 39;
    private static final int DEFAULT_NUM_OF_COLUMNS = 3;
    public static final String RETYPE_PASSWORD_PROPERTY_ID = "retypePassword";
    public static final String CONFIRM_MPIN_PROPERTY_ID = "confirmMpin";
    public static final String CONFIRM_EMAIL_PROPERTY_ID = "confirmEmail";

    protected CommonRegistrationApp commonRegistrationApp;
    protected UserGroupService userGroupService;
    private MailSender mailSender;

    protected User user;
    protected boolean editState;
    protected boolean isOpenIdUser;
    protected GridLayout advancedLayout;
    protected HorizontalLayout outerLayout;
    protected UserService userService;
    protected CheckBox termsAcceptedCheckBox;
    private TextField birthYearField;
    private ComboBox birthMonthField;
    private TextField birthDayField;
    protected Button saveButton;
    protected CaptchaField captchaField;
    protected TextField captchaText;
    private OperatorMsisdnValidator operatorMsisdnValidator;
    private boolean verifyUser;
    private IndividualUserOperatorValidator individualUserOperatorValidator;

    public static IndividualUserForm createOpenIdUserView(User user, CommonRegistrationApp commonRegistrationApp, boolean editState) {
        return new IndividualUserForm(user, commonRegistrationApp, editState, true, true);
    }

    public IndividualUserForm(User user, CommonRegistrationApp commonRegistrationApp, boolean editState, boolean verify) {
        this(user, commonRegistrationApp, editState);
        this.verifyUser = verify;
    }

    public IndividualUserForm(User user, CommonRegistrationApp commonRegistrationApp, boolean editState, boolean verify, boolean isOpenId) {
        this.verifyUser = verify;
        this.commonRegistrationApp = commonRegistrationApp;
        this.user = user;
        this.editState = editState;
        this.isOpenIdUser = isOpenId;
        user.setUserType(INDIVIDUAL);
        // Only avialable operator is safaricom for individual user
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        mailSender = (MailSender) commonRegistrationApp.getBean("mailSender");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        advancedLayout = new GridLayout(DEFAULT_NUM_OF_COLUMNS, DEFAULT_NUM_OF_ROWS);
        outerLayout = new HorizontalLayout();
        outerLayout.setWidth("850px");
        saveButton = new Button(commonRegistrationApp.getMessage("registration.button.submit"));
        individualUserOperatorValidator = new IndividualUserOperatorValidator(commonRegistrationApp);
        setImmediate(true);
        initUserCreationForm();
    }

    /**
     * Creates Form to add Individual User
     *
     * @param user
     * @param commonRegistrationApp
     */
    public IndividualUserForm(User user, CommonRegistrationApp commonRegistrationApp, boolean editState) {
        this.verifyUser = !editState;
        this.commonRegistrationApp = commonRegistrationApp;
        this.user = user;
        this.editState = editState;
        user.setUserType(INDIVIDUAL);
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        mailSender = (MailSender) commonRegistrationApp.getBean("mailSender");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        advancedLayout = new GridLayout(DEFAULT_NUM_OF_COLUMNS, DEFAULT_NUM_OF_ROWS);
        outerLayout = new HorizontalLayout();
        outerLayout.setWidth("850px");
        saveButton = new Button(commonRegistrationApp.getMessage("registration.button.submit"));
        individualUserOperatorValidator = new IndividualUserOperatorValidator(commonRegistrationApp);
        setImmediate(true);
        initUserCreationForm();
    }

    /**
     * Initialize individual user creation form
     */
    protected void initUserCreationForm() {
        advancedLayout.setMargin(true, false, true, true);
        advancedLayout.setSpacing(true);
        advancedLayout.space();
        advancedLayout.setWidth("800px");
        outerLayout.addComponent(advancedLayout);
        outerLayout.setComponentAlignment(advancedLayout, Alignment.TOP_CENTER);
        setLayout(outerLayout);
        BeanItem<User> beanItem = new BeanItem<User>(user);
        beanItem.addItemProperty(RETYPE_PASSWORD_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(CONFIRM_MPIN_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(CONFIRM_EMAIL_PROPERTY_ID, new ObjectProperty(null, String.class));
        setItemDataSource(beanItem);
        setWriteThrough(false);
        setFormFieldFactory(new FieldFactory(this, commonRegistrationApp));

        List<String> visibleItems = new ArrayList<String>(Arrays.asList(FIRST_NAME, EMAIL, PHONE_NUMBER, USER_NAME));

        if (editState) {
            visibleItems.add(OPERATOR);
        } else {
            if (!isDomainDialog()) {
                if (!isOpenIdUser) {
                    visibleItems.add(PASSWORD);
                    visibleItems.add(RETYPE_PASSWORD_PROPERTY_ID);
                }
                visibleItems.add(OPERATOR);
            }
        }

        setVisibleItemProperties(visibleItems);

        if (!editState && !isDomainDialog() && !isOpenIdUser) {
            addRetypePasswordValidator();
        }

        addOperatorMsisdnValidator();
        generatePageContent();
    }


    /**
     * Generates page heading contents
     */
    protected void generatePageContent() {

        Label personalDetailsHeading = generateHeading("registration.user.personal.details.heading", commonRegistrationApp);
        advancedLayout.addComponent(personalDetailsHeading, 0, 2, 2, 2);
        advancedLayout.addComponent(getSpacer(), 0, 6, 2, 6);

        if (!isOpenIdUser) {
            Label userIdHeading = generateHeading("registration.user.choose.user.name.password.heading", commonRegistrationApp);
            advancedLayout.addComponent(userIdHeading, 0, 7, 2, 7);
            advancedLayout.addComponent(getSpacer(), 0, 11, 2, 11);
        }

        advancedLayout.addComponent(generateBdayComponent(), 1, 3, 2, 3);
        generateUserTermsLayout();
        createFooter();
    }

    private HorizontalLayout generateBdayComponent() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.setCaption(commonRegistrationApp.getMessage("registration.user.birthday"));
        birthMonthField = (ComboBox) createComboBox(null, true, "registration.user.invalid.birthday", MONTHS, commonRegistrationApp);
        birthMonthField.setWidth("150px");
        birthMonthField.setImmediate(true);
        horizontalLayout.addComponent(birthMonthField);
        birthDayField = (TextField) createTextField(null, true, "registration.user.invalid.birthday", commonRegistrationApp);
        birthDayField.setInputPrompt(commonRegistrationApp.getMessage("registration.user.day"));
        birthDayField.setWidth("50px");
        birthDayField.setImmediate(true);
        horizontalLayout.addComponent(birthDayField);
        birthYearField = (TextField) createTextField(null, true, "registration.user.invalid.birthday", commonRegistrationApp);
        birthYearField.setInputPrompt(commonRegistrationApp.getMessage("registration.user.year"));
        birthYearField.setWidth("50px");
        birthYearField.setImmediate(true);

        // Find birthday for Open Id user
        if (isOpenIdUser && (user.getBirthday() != null)) {
            birthDayField.setValue(String.valueOf(user.getBirthday().getDate()));
            birthMonthField.setValue(MONTHS.get(user.getBirthday().getMonth()));

            SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
            birthYearField.setValue(simpleDateformat.format(user.getBirthday()));

            if (isDomainDialog()) {
                birthDayField.setRequired(false);
                birthDayField.setReadOnly(true);
                birthMonthField.setRequired(false);
                birthMonthField.setReadOnly(true);
                birthYearField.setRequired(false);
                birthYearField.setReadOnly(true);
            }
        }

        horizontalLayout.addComponent(birthYearField);
        setBirthdayValidator();
        return horizontalLayout;
    }

    private void setBirthdayValidator() {
        birthYearField.addListener(new ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (isValidYear()) {
                    birthYearField.setComponentError(null);
                    if (isValidDate()) {
                        birthDayField.setComponentError(null);
                        setComponentError(null);
                    } else {
                        birthDayField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.birth.date")));
                        birthDayField.setValue("");
                        birthDayField.focus();
                    }
                } else {
                    birthYearField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.birth.year")));
                    birthYearField.setValue("");
                    birthYearField.focus();
                }
            }
        });

        birthMonthField.addListener(new ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (isValidDate()) {
                    birthDayField.setComponentError(null);
                    setComponentError(null);
                } else {
                    birthDayField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.birth.date")));
                    birthDayField.setValue("");
                    birthDayField.focus();
                }
            }
        });

        birthDayField.addListener(new ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                if (isValidDate()) {
                    birthDayField.setComponentError(null);
                    setComponentError(null);
                } else {
                    birthDayField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.birth.date")));
                    birthDayField.setValue("");
                    birthDayField.focus();
                }
            }
        });
    }

    /**
     * Check if the user entered date is a valid date
     *
     * @return
     */
    protected boolean isValidDate() {
        if ((birthYearField.getValue() != null && !birthYearField.getValue().equals(""))
                && ((birthDayField.getValue() != null && !birthDayField.getValue().equals(""))
                && birthMonthField.getValue() != null) && !birthMonthField.getValue().equals("")) {
            try {
                String birthday = new DecimalFormat("00").format(Integer.valueOf((String) birthDayField.getValue())) + "-" +
                        ((String) birthMonthField.getValue()).substring(0, 3) + "-" +
                        new DecimalFormat("0000").format(Integer.valueOf((String) birthYearField.getValue()));
                logger.debug("Birthday field formatted input : " + birthday);
                return validateDate(birthday);
            } catch (NumberFormatException e) {
                logger.error("Invalid entry for number", e);
                return false;
            }
        } else {
            return true;
        }
    }

    protected boolean isValidYear() {
        if (birthYearField.getValue() != null && ((String) birthYearField.getValue()).trim().equals("")) {
            return false;
        } else {
            try {
                int year = Integer.parseInt((String) birthYearField.getValue());
                return year < (Calendar.getInstance().get(Calendar.YEAR));
            } catch (NumberFormatException ex) {
                return false;
            }
        }
    }

    protected boolean validateDate(String birthday) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            formatter.setLenient(false);
            Date date = formatter.parse(birthday);
            return date.before(new Date());
        } catch (ParseException e) {
            logger.debug("Error while parsing date", e);
            return false;
        }
    }

    protected void generateUserTermsLayout() {
        Label termsOfServiceHeading = new Label(commonRegistrationApp.getMessage("registration.user.user.terms"));
        termsOfServiceHeading.setStyleName("header-with-border");
        advancedLayout.addComponent(termsOfServiceHeading, 0, 12, 2, 12);
        if (isExternalTermsLinkEnabled()) {
            Link checkTermsLink = new Link(commonRegistrationApp.getMessage("registration.user.user.terms.link"),
                    new ExternalResource(CONSUMER_TERMS_URL), "_blank", -1, -1, Window.BORDER_DEFAULT);
            advancedLayout.addComponent(checkTermsLink, 0, 13);
        } else {
            Button checkTermsButton = new Button(commonRegistrationApp.getMessage("registration.user.user.terms.link"));
            checkTermsButton.setStyleName("link");
            checkTermsButton.addListener(new Button.ClickListener() {
                public void buttonClick(Button.ClickEvent clickEvent) {
                    getWindow().addWindow(CommonUiComponentGenerator.generateIndividualUserTermsSubWindow(commonRegistrationApp));
                }
            });
            advancedLayout.addComponent(checkTermsButton, 0, 13);
        }
        advancedLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.user.terms.last.message")),
                0, 14, 2, 14);

        advancedLayout.addComponent(CommonUiComponentGenerator.getSpacer());

        termsAcceptedCheckBox = new CheckBox(commonRegistrationApp.getMessage("registration.button.individual.create.account.button"));
        termsAcceptedCheckBox.setImmediate(true);
        termsAcceptedCheckBox.addListener(new ValueChangeListener() {

            public void valueChange(Property.ValueChangeEvent event) {
                logger.debug("terms accepted : " + event.getProperty().getValue());
                saveButton.setEnabled((Boolean) event.getProperty().getValue());
            }
        });
        advancedLayout.addComponent(termsAcceptedCheckBox,
                0, 15, 2, 15);
    }

    /**
     * Attaching fields to the grid layout
     *
     * @param propertyId
     * @param field
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        // personal details
        if (propertyId.equals(FIRST_NAME)) {
            advancedLayout.addComponent(field, 0, 3);
            field.setTabIndex(1);
        } else if (propertyId.equals(EMAIL)) {
            advancedLayout.addComponent(field, 0, 5);
        } else if (propertyId.equals(PHONE_NUMBER)) {
            if (!isDomainDialog()) {
                field.addValidator(individualUserOperatorValidator);
            }
            advancedLayout.addComponent(field, 1, 5);
        }

        // user details
        else if (propertyId.equals(USER_NAME)) {
            advancedLayout.addComponent(field, 0, 8);
        } else if (propertyId.equals(PASSWORD)) {
            advancedLayout.addComponent(field, 0, 10);
        } else if (propertyId.equals(RETYPE_PASSWORD_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 1, 10);
        }
    }

    /**
     * adding validator for retype mpin
     */
    protected void addRetypeMpinValidator() {
        getField(CONFIRM_MPIN_PROPERTY_ID).addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    logger.debug("Mpin not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.mpin.does.not.match"));
                }
            }

            public boolean isValid(Object value) {
                return value != null && value.equals(getField(MPIN).getValue());
            }
        });
    }

    /**
     * adding validator for retype mpin
     */
    protected void addRetypeEmailValidator() {
        getField(CONFIRM_EMAIL_PROPERTY_ID).addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    logger.debug("Retype email not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.email.does.not.match"));
                }
            }

            public boolean isValid(Object value) {
                return value != null && value.equals(getField(EMAIL).getValue());
            }
        });
    }

    /**
     * adding validator for retype password field
     */
    protected void addRetypePasswordValidator() {
        getField(RETYPE_PASSWORD_PROPERTY_ID).addValidator(new Validator() {

            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    logger.debug("Password not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.password.does.not.match"));
                }
            }

            public boolean isValid(Object value) {
                return value != null && value.equals(getField(PASSWORD).getValue());
            }
        });
    }

    protected void createFooter() {
        setFooter(new VerticalLayout());
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setHeight("50px");
        saveButton.setStyleName("action-button");
        if (termsAcceptedCheckBox.booleanValue()) {
            saveButton.setEnabled(true);
        } else {
            saveButton.setEnabled(false);
        }
        saveButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                logger.debug("Submitting User Account Details Started ...");
                try {
                    validateAllFields();
                    logger.debug("Validation completed successfully ...");
                    goToVerificationFlow();
                } catch (Validator.InvalidValueException e) {
                    logger.debug("Invalid Form Entries [{}]", e.getMessage());
                } catch (Exception e) {
                    logger.error("Unexpected error occurred {}", e);
                }
            }
        });
        buttonLayout.addComponent(saveButton);

        if (!isOpenIdUser) {
            Button resetButton = new Button(commonRegistrationApp.getMessage("registration.button.reset"),
                    new Button.ClickListener() {
                        public void buttonClick(Button.ClickEvent event) {
                            logger.debug("Discarding the input values..........");
                            discard();
                            setComponentError(null);
                            setValidationVisible(false);
                        }
                    });
            resetButton.setStyleName("action-button");
            buttonLayout.addComponent(resetButton);
        }

        Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"),
                new Button.ClickListener() {
                    public void buttonClick(Button.ClickEvent event) {
                        logger.debug("Discarding the input values..........");
                        commonRegistrationApp.setLogoutURL(CAS_LOGIN_URL);
                        commonRegistrationApp.close();
                    }
                });
        cancelButton.setStyleName("action-button");
        buttonLayout.addComponent(cancelButton);

        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setWidth("850px");
        footerLayout.addComponent(buttonLayout);
        footerLayout.setComponentAlignment(buttonLayout, Alignment.BOTTOM_CENTER);
        getFooter().addComponent(footerLayout);
    }

    private void validateAllFields() throws Validator.InvalidValueException {
        commit();
        validate();

        if (!user.isCorporateUser()) {
            if (isBirthdayEmpty() || !isValidDate()) {
                setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.birthday")));
                throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.invalid.birthday"));
            }
        }

        // Captcha text is only available for Corporate user
        if (user.getUserType() == CORPORATE && isCaptchaValidationEnabled()) {
            if (!captchaField.validateCaptcha((String) captchaText.getValue())) {
                captchaText.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.captcha.code.invalid")));
                setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.captcha.code.invalid")));
                throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.captcha.code.invalid"));
            } else {
                captchaText.setComponentError(null);
            }
        }
    }

    private boolean isBirthdayEmpty() {
        return (((birthYearField.getValue() == null || birthYearField.getValue().equals(""))
                || (birthDayField.getValue() == null || birthDayField.getValue().equals("")))
                || (birthMonthField.getValue() == null || birthMonthField.getValue().equals("")));
    }

    private void goToVerificationFlow() {
        if (!isVerifyUser()) {
            try {
                user = userService.merge(user);
            } catch (DataManipulationException e) {
                logger.error("Error occurred while updating user information", e);
            }

            redirectToOriginalService(true);
        } else {
            try {
                setAdditionalUserDetails();
                createUserAccount();
                handleUserVerificationFlow();

                if (isValidUser(user)) {
                    user.activate();
                    user = userService.merge(user);
                    if (user.isCorporateUser() && isAutoSpCreationEnabled()) {
                        userService.createDefaultSpForUser(user);
                    }

                    redirectToOriginalService(true);
                    return;
                }

                logger.debug("Redirecting to Account Verification Page");
                ContentPanel panel = commonRegistrationApp.getMainLayout().getContentLayout().getContentPanel();
                panel.removeAllComponents();
                panel.setCaption(commonRegistrationApp.getMessage("registration.user.complete.your.registration"));


                if ((!user.isCorporateUser() && isConsumerUserMsisdnVerifyEnabled())
                        || (user.isCorporateUser() && isCorporateUserMsisdnVerifyEnabled())) {
                    panel.addComponent(new UserVerificationView(user, commonRegistrationApp, false));
                } else {
                    panel.addComponent(new UserCreationSuccessView(user, commonRegistrationApp, UserCreationSuccessView.EMAIL));
                }
            } catch (Throwable e) {
                logger.error("Error occurred while creating user account for user [" +
                        user.getUsername() + " ]", e);
                generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                        "registration.user.user.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
            }
        }
    }

    private void redirectToOriginalService(boolean status) {
        String serviceUrl = getRedirectUrl(commonRegistrationApp, status);
        logger.debug("Redirecting to [{}]", serviceUrl);
        getWindow().open(new ExternalResource(serviceUrl));
    }

    /**
     * save additional details
     *
     * @throws DataManipulationException
     */
    protected void saveUserDetails() throws DataManipulationException {
        user.setCorperateParentId(user);
        user.setEnabled(true);
        String birthday = new DecimalFormat("00").format(Integer.valueOf((String) birthDayField.getValue())) + "-" +
                ((String) birthMonthField.getValue()).substring(0, 3) + "-" +
                new DecimalFormat("0000").format(Integer.valueOf((String) birthYearField.getValue()));

        logger.debug("Birthday field formatted input : " + birthday);

        DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        formatter.setLenient(false);
        try {
            Date date = formatter.parse(birthday);
            user.setBirthday(date);
        } catch (ParseException e) {
            logger.error("Error while parsing date", e);
        }
    }

    /**
     * Handles user verification flow
     *
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    private void handleUserVerificationFlow() throws NoSuchAlgorithmException, UnsupportedEncodingException, DataManipulationException {
        logger.debug("Creating User Account for user [ " + user + " ] Started");

        if (user.isCorporateUser()) {
            if (isCorporateUserEmailVerifyEnabled()) {
                handleEmailVerification();
            }
        } else {
            if (isConsumerUserEmailVerifyEnabled()) {
                handleEmailVerification();
            }
        }
    }

    /**
     * Creates user account
     *
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     * @throws DataManipulationException
     */
    private void createUserAccount() throws NoSuchAlgorithmException, UnsupportedEncodingException, DataManipulationException {
        logger.debug("Creating user account");
        if (user.getLastName() == null) {
            user.setLastName("");
        }
        userService.persist(user);
    }

    private void addOperatorMsisdnValidator() {
        this.operatorMsisdnValidator = new OperatorMsisdnValidator(commonRegistrationApp);

        final TextField mobileNoField = (TextField) getField(PHONE_NUMBER);
        final Field operatorSelect = getField(OPERATOR);
        if (mobileNoField != null && operatorSelect != null) {
            mobileNoField.addListener(new Property.ValueChangeListener() {

                public void valueChange(Property.ValueChangeEvent event) {
                    showMsisdnValidationErrors(mobileNoField, operatorSelect);
                }
            });
            operatorSelect.addListener(new ValueChangeListener() {

                public void valueChange(Property.ValueChangeEvent event) {
                    showMsisdnValidationErrors(mobileNoField, operatorSelect);
                }
            });
        }
    }

    protected void showMsisdnValidationErrors(TextField mobileNoField, Field operatorSelect) {
        if (user.getUserType() == CORPORATE) {
            operatorMsisdnValidator.setOperator((String) operatorSelect.getValue());

            mobileNoField.setComponentError(null);

            if (isMsisdnAlreadyTaken((String) mobileNoField.getValue())) {
                logger.debug("Msisdn Already Taken");
                mobileNoField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.msisdn.not.available")));
            } else if (!operatorMsisdnValidator.isValid(mobileNoField.getValue())) {
                logger.debug("Msisdn invalid");
                mobileNoField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));
            }
        }
    }

    private boolean isMsisdnAlreadyTaken(String value) {
        try {
            return userService.findUserByMsisdn(value) != null;
        } catch (DataManipulationException e) {
            logger.error("Data Access Error", e);
            return false;
        }
    }

    private String getOperatorFromMsisdn(String msisdn) {
        Set<String> supportedOperatorPrefixSet = OPERATOR_CODE_MAP.keySet();
        for (String operatorPrefix : supportedOperatorPrefixSet) {
            if (msisdn != null) {
                if (msisdn.startsWith(operatorPrefix)) {
                    return OPERATOR_CODE_MAP.get(operatorPrefix);
                }
            }
        }
        return "";
    }

    private void setAdditionalUserDetails() throws NoSuchAlgorithmException, UnsupportedEncodingException, DataManipulationException {
        user.setPassword(MD5(user.getPassword()));
        user.setOperator(getOperatorFromMsisdn(user.getMobileNo()));

        saveUserDetails();

        if (!isEmptyField(user.getMpin())) {
            user.setMpin(MD5(user.getMpin()));
        }

        String emailVerificationCode = generateVerificationCode();
        user.setEmailVerificationCode(Encrypter.MD5(emailVerificationCode));
        logger.debug("Additional user details are saved " + user);

        if (user.isCorporateUser()) {
            UserGroup corporateUserGroup = userGroupService.findUserGroupByGroupName("CORPORATE_USER");

            if (corporateUserGroup != null) {
                logger.debug("UserGroup Was found for UserGroupName[CORPORATE_USER] and Group Description is [{}]", corporateUserGroup.getDescription());
                user.setUserGroup(corporateUserGroup);
            } else {
                throw new IllegalStateException("UserGroup were not found for UserGroupName[CORPORATE_USER]");
            }
        } else {
            UserGroup consumerUserGroup = userGroupService.findUserGroupByGroupName("CONSUMER_USER");

            if (consumerUserGroup != null) {
                logger.debug("UserGroup Was found for UserGroupName[CONSUMER_USER] and Group Description is [{}]", consumerUserGroup.getDescription());
                user.setUserGroup(consumerUserGroup);
            } else {
                throw new IllegalStateException("UserGroup were not found for UserGroupName [CONSUMER_USER]");
            }
        }
    }

    /**
     * Sends out an email to the given email address in order to verify the user's email address
     *
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    private void handleEmailVerification() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (user.getUserType() == CORPORATE) {
            CorporateUser corporateUser = (CorporateUser) user;
            mailSender.sendUserVerificationEmail(corporateUser.getContactPersonName(), corporateUser.getUsername(), corporateUser.getEmail(),
                    corporateUser.getEmailVerificationCode(), corporateUser.getUserId());
        } else {
            mailSender.sendUserVerificationEmail(user.getFirstName(), user.getUsername(), user.getEmail(),
                    user.getEmailVerificationCode(), user.getUserId());

        }
    }

    protected void resetField(Field field, Object value) {
        if (!field.isReadOnly()) {
            field.setValue(value);
        }
    }

    @Override
    public void discard() throws Buffered.SourceException {
        Collection itemIds = getItemPropertyIds();
        for (Object itemId : itemIds) {
            resetField(getField(itemId), null);
        }

        resetField(birthMonthField, null);
        resetField(birthYearField, "");
        resetField(birthDayField, "");
        resetField(termsAcceptedCheckBox, false);
    }

    public boolean isDomainDialog() {
        return user.getDomain().equals("dialog");
    }

    public boolean isVerifyUser() {
        return verifyUser;
    }

    public User getUser() {
        return user;
    }
}

/**
 * Generates UI fields
 */
class FieldFactory implements FormFieldFactory {

    private static final Logger logger = LoggerFactory.getLogger(FieldFactory.class);

    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.user.individual.";

    private IndividualUserForm form;
    private CommonRegistrationApp commonRegistrationApp;

    FieldFactory(IndividualUserForm form, CommonRegistrationApp commonRegistrationApp) {
        this.form = form;
        this.commonRegistrationApp = commonRegistrationApp;
    }

    public Field createField(Item item, Object propertyId, Component component) {
        final String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;

        logger.debug("Creating field [{}] with [{}]", pid, caption);

        if (USER_NAME.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.username.required", commonRegistrationApp);
            if (form.isVerifyUser()) {
                field.addValidator(new UserNameCheckValidator(commonRegistrationApp));
            }
            field.setInvalidAllowed(true);
            if (form.isOpenIdUser) {
                field.setReadOnly(true);
                field.setVisible(false);
            } else {
                field.addValidator(new RegexpValidator(INDIVIDUAL_USER_USERNAME_REGEX_PATTERN, commonRegistrationApp.getMessage("registration.user.individual.invalid.username")));
            }
            return field;
        } else if (PASSWORD.equals(pid)) {
            Field field = createPasswordField(caption, "registration.user.password.required", commonRegistrationApp);
            field.addValidator(new RegexpValidator(INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.individual.invalid.password")));
            if (form.isDomainDialog()) {
                field.setRequired(false);
            }
            return field;
        } else if (RETYPE_PASSWORD_PROPERTY_ID.equals(pid)) {
            Field field = createPasswordField(caption, "registration.user.retype.password.required", commonRegistrationApp);
            if (form.isDomainDialog()) {
                field.setRequired(false);
            }
            return field;
        } else if (MPIN.equals(pid)) {
            Field field = createPasswordField(caption, "registration.user.mpin.required", commonRegistrationApp);
            field.addValidator(new RegexpValidator(MPIN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.mpin")));
            return field;
        } else if (IndividualUserForm.CONFIRM_MPIN_PROPERTY_ID.equals(pid)) {
            return createPasswordField(caption, "registration.user.mpin.required", commonRegistrationApp);
        } else if (FIRST_NAME.equals(pid)) {
            Field field = createTextField(commonRegistrationApp.getMessage("registration.user.individual.fullName"), true, "registration.user.full.name.required", commonRegistrationApp);
            field.addValidator(new RegexpValidator(FULL_NAME_LENGTH_VALIDATION, commonRegistrationApp.getMessage("registration.user.full.name.length.validation")));
            if (form.isDomainDialog()) {
                field.setReadOnly(true);
            }
            return field;
        } else if (LAST_NAME.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.last.name.required", commonRegistrationApp);
            field.addValidator(new RegexpValidator(LAST_NAME_LENGTH_VALIDATION, commonRegistrationApp.getMessage("registration.user.last.name.length.validation")));
            return field;
        } else if (EMAIL.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.email.required", commonRegistrationApp);
            EmailCheckValidator emailCheckValidator = new EmailCheckValidator(commonRegistrationApp);
            emailCheckValidator.setUserId(form.getUser().getUserId());
            if (form.isDomainDialog()) {
                field.setRequired(false);
                field.setReadOnly(true);
            } else {
                field.addValidator(emailCheckValidator);
            }
            return field;
        } else if (IndividualUserForm.CONFIRM_EMAIL_PROPERTY_ID.equals(pid)) {
            Field field = createTextField(caption, false, "registration.user.email.required", commonRegistrationApp);
            field.addValidator(new EmailValidator(commonRegistrationApp.getMessage("registration.user.invalid.email")));
            return field;
        } else if (PHONE_NUMBER.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.msisdn.required", commonRegistrationApp);
            String countryCode = (String) commonRegistrationApp.getBean("countryCode");
            User user = form.getUser();
            if (form.isDomainDialog()) {
                user.setMobileNo(user.getMobileNo());
                field.setReadOnly(true);
            } else {
                user.setMobileNo(countryCode);
            }
            return field;
        } else if (DISPLAY_NAME.equals(pid)) {
            return createTextField(caption, true, "registration.user.display.name.required", commonRegistrationApp);
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            CommonUiComponentGenerator.filterNullValues(field);
            return field;
        }
    }
}
