/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.account;

import com.google.common.base.Optional;
import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.util.Encrypter;
import hms.common.registration.util.MailSender;
import hms.common.registration.util.RegistrationFeatureRegistry;
import hms.common.registration.util.WebUtils;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateSuccessLayoutWithLink;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.getSpacer;
import static hms.common.registration.ui.common.util.ExternalLinks.CAS_LOGIN_URL;
import static hms.common.registration.util.RegistrationFeatureRegistry.isAccountRecoveryUsingEmail;

/**
 * Handles the user account recovery flow
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserAccountRecoveryView extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAccountRecoveryView.class);
    private static final int MAX_PASSWORD_LENGTH = 8;
    private TextField accountRecoveryInputFiled;
    private Label errorMessageIndicator;
    private UserService userService;
    private MailSender mailSender;
    private ContentPanel contentPanel;
    private UriFragmentUtility fragmentUtility;
    private CommonRegistrationApp commonRegistrationApp;
    public UserAccountRecoveryView(ContentPanel contentPanel, UriFragmentUtility fragmentUtility, CommonRegistrationApp commonRegistrationApp) {
        this.contentPanel = contentPanel;
        this.fragmentUtility = fragmentUtility;
        this.commonRegistrationApp = commonRegistrationApp;
        mailSender = (MailSender) commonRegistrationApp.getBean("mailSender");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        setSpacing(true);
        setSizeFull();
        initializeUserAccountRecoveryUi();
    }

    private void initializeUserAccountRecoveryUi() {
        Label heading = new Label(commonRegistrationApp.getMessage("registration.account.recovery.forgot.password"));
        heading.setStyleName("h2");
        addComponent(heading);
        Label accountRecoveryHint = new Label(commonRegistrationApp.getMessage("registration.account.recovery.message"));
        addComponent(accountRecoveryHint);
        accountRecoveryInputFiled = new TextField(commonRegistrationApp.getMessage("registration.account.recovery.username"));
        errorMessageIndicator = new Label("");
        errorMessageIndicator.setStyleName("v-form-errormessage");
        accountRecoveryInputFiled.setRequired(true);
        accountRecoveryInputFiled.setStyleName("h6");
        accountRecoveryInputFiled.setRequiredError(commonRegistrationApp.getMessage("registration.account.recovery.username.required"));
        accountRecoveryInputFiled.setInvalidAllowed(false);
        addComponent(accountRecoveryInputFiled);
        addComponent(getSpacer());
        addComponent(errorMessageIndicator);
        final Button submitButton = new Button(commonRegistrationApp.getMessage("registration.button.submit"));
        submitButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                LOGGER.debug("Sending Forgot Password Request for User Name [{} Started ", accountRecoveryInputFiled.getValue());
                handlePasswordResetRequest();
            }
        });
        addComponent(submitButton);

        final Button backButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        backButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Cancelling account recovery. Changing the fragment");

                }
                commonRegistrationApp.setLogoutURL(CAS_LOGIN_URL);
                commonRegistrationApp.close();
            }
        });
        submitButton.setStyleName("action-button");
        backButton.setStyleName("action-button");

        HorizontalLayout buttonLayout = new HorizontalLayout() {{
            addComponent(backButton);
            addComponent(submitButton);
        }};
        buttonLayout.setSpacing(true);
        addComponent(buttonLayout);
        setComponentAlignment(buttonLayout, Alignment.BOTTOM_CENTER);

        addComponent(getSpacer());
    }

    private void handlePasswordResetRequest() {
        try {
            accountRecoveryInputFiled.validate();
            errorMessageIndicator.setValue("");
            Optional<User> userOptional = retrieveAccount((String) accountRecoveryInputFiled.getValue());

            if (!userOptional.isPresent()) {
                accountRecoveryInputFiled.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.account.user.not.found")));
            } else {
                final User user = userOptional.get();
                String generatedPassword = WebUtils.getPassword(MAX_PASSWORD_LENGTH);
                user.setPassword(Encrypter.MD5(generatedPassword));
                LOGGER.debug("Updating user with the newly generated password");
                userService.merge(user);
                LOGGER.debug("Sending password reset email to User [{}]", user.getFirstName());

                if (user.getUserType() == UserType.CORPORATE) {
                    CorporateUser corporateUser = (CorporateUser) user;
                    mailSender.sendForgotPasswordReplyEmail(corporateUser.getContactPersonName(), corporateUser.getEmail(),
                            corporateUser.getUsername(), generatedPassword);

                } else {
                    mailSender.sendForgotPasswordReplyEmail(user.getFirstName(), user.getEmail(),
                            user.getUsername(), generatedPassword);

                }

                LOGGER.debug("Sending Forgot Password Request for User Name [{} Finished ", accountRecoveryInputFiled.getValue());

                CustomLayout userCreationSuccessLayout =
                        generateSuccessLayoutWithLink(commonRegistrationApp.getMessage("registration.account.recovery.success"),
                        commonRegistrationApp.getMessage("registration.account.recovery.success.link"), "login.failed.url.full.path.with.no.error.message");
                changeView(userCreationSuccessLayout);
            }
        } catch (Validator.InvalidValueException e) {
            errorMessageIndicator.setValue(e.getLocalizedMessage());
        } catch (Throwable th) {
            LOGGER.error("Error while handling account recovery request ", th);
            createNotificationMessage(commonRegistrationApp.getMessage("registration.account.recovery.error"));
        }
    }

    private Optional<User> retrieveAccount(String accountRecoveryText) {
        Optional<User> user = Optional.absent();
        try {
            user = Optional.fromNullable(userService.findUserByName(accountRecoveryText));
            if(user.isPresent()) {
                LOGGER.debug("Found user [{}] by username [{}]", user.get(), accountRecoveryText);
                return user;
            }
        } catch (DataManipulationException e) {
            LOGGER.debug("Error occurred while retrieving the user by username [{}]", accountRecoveryText);
        }

        if(isAccountRecoveryUsingEmail()) {
            user = userService.findUserByEmail(accountRecoveryText);
            if(user.isPresent()) {
                LOGGER.debug("Found user [{}] by email [{}]", user.get(), accountRecoveryText);
                return user;
            }
        }

        return user;
    }

    private void createNotificationMessage(String message) {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(new Label(message));
        verticalLayout.addComponent(getSpacer());
        changeView(verticalLayout);
    }

    private void changeView(Component componenet) {
        contentPanel.removeAllComponents();
        contentPanel.addComponent(componenet);
    }
}
