/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.common.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class RegistrationKeys {

    public static final String CUSTOM_CAS_ASSERTION = "custom_cas_assertion";
    public static final String CUSTOM_AUTHENTICATION_INTERCEPTOR_EXECUTED = "custom_authentication_interceptor_executed";
    public static final String LOCAL_SERVICE = "local_service";
    public static final String PROVIDER = "provider";
    public static final String USER_ID = "user_id";
}
