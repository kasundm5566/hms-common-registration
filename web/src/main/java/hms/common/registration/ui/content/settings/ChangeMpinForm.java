/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.settings;

import com.vaadin.data.Buffered;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.util.Encrypter;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.MPIN_REGEX_PATTERN;

/**
 * Handles the change MPIN flow
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class ChangeMpinForm extends Form {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeMpinForm.class);
    private static final String CURRENT_MPIN_PID = "currentMpin";
    private static final String NEW_MPIN_PID = "newMpin";
    private static final String CONFIRM_MPIN_PID = "confirmMpin";
    private CommonRegistrationApp commonRegistrationApp;
    private TextField newMpinField;
    private TextField confirmMpinField;
    private TextField currentMpinField;
    private UserService userService;
    private User user;
    private String userName;

    /**
     * Defines form constructor for change password
     *
     * @param commonRegistrationApp
     */
    public ChangeMpinForm(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        try {
            this.userName = commonRegistrationApp.currentUserName();
            this.user = userService.findUserByName(userName);
        } catch (DataManipulationException e) {
            LOGGER.error("Error while loading logged in user", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.change.mpin.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
        initChangeMpinForm();
    }

    /**
     * Initializes change mpin form
     */
    void initChangeMpinForm() {
        setImmediate(true);
        setWriteThrough(false);
        currentMpinField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.mpin.current.mpin"),
                "registration.change.mpin.required.error", commonRegistrationApp);
        newMpinField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.mpin.new.mpin"),
                "registration.change.mpin.required.error", commonRegistrationApp);
        newMpinField.addValidator(new RegexpValidator(MPIN_REGEX_PATTERN,
        commonRegistrationApp.getMessage("registration.user.invalid.mpin")));
        confirmMpinField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.mpin.confirm.mpin"),
                "registration.change.mpin.required.error", commonRegistrationApp);        if (user.getMpin() != null) {
            addField(CURRENT_MPIN_PID, currentMpinField);
            addCurrentMpinValidator();
        }
        addField(NEW_MPIN_PID, newMpinField);
        addField(CONFIRM_MPIN_PID, confirmMpinField);
        addRetypeMpinValidator();
        createFormFooter(userName);
    }

    private void addCurrentMpinValidator() {
        getField(CURRENT_MPIN_PID).addValidator(new Validator() {

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    LOGGER.debug("Current Mpin Incorrect");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.change.mpin.wrong"));
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (value == null) {
                    return false;
                } else if (mpinMatching(value)) {
                    return true;
                } else {
                    return false;
                }
            }

            private boolean mpinMatching(Object value) {
                try {
                    String encodedMpin = MD5((String) value);
                    return encodedMpin.equals(user.getMpin());
                } catch (NoSuchAlgorithmException e) {
                    LOGGER.error("NoSuchAlgorithmException", e);
                } catch (UnsupportedEncodingException e) {
                    LOGGER.error("UnsupportedEncodingException", e);
                }
                return false;
            }
        });
    }

    /**
     * adding validator for retype mpin field
     */
    private void addRetypeMpinValidator() {
        getField(CONFIRM_MPIN_PID).addValidator(new Validator() {

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    LOGGER.debug("Mpin not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.mpin.does.not.match"));
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (value == null) {
                    return false;
                } else if (value.equals(getField(NEW_MPIN_PID).getValue())) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }


    /**
     * Generates change mpin form footer
     *
     * @param userName
     * @return
     */
    VerticalLayout createFormFooter(final String userName) {
        VerticalLayout footerLayout = new VerticalLayout();
        setFooter(footerLayout);
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        final Button ChangeMpinButton = new Button(commonRegistrationApp.getMessage("registration.change.mpin.submit.button"));
        ChangeMpinButton.setStyleName("action-button");
        ChangeMpinButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                LOGGER.debug("Change MPIN Request Received for User [{}]", userName);
                try {
                    setComponentError(null);
                    user = userService.findUserByUserId(user.getUserId());
                    commit();
                    validateMpin();
                    user.setMpin(Encrypter.MD5((String) newMpinField.getValue()).trim());
                    userService.merge(user);
                    discard();
                    setReadOnly(true);

                    String successMessage = commonRegistrationApp.getMessage("registration.user.change.mpin.success.message");
                    String loginLink = commonRegistrationApp.getMessage("registration.user.change.mpin.success.message.login");
                    String path = commonRegistrationApp.getURL().getPath() + "j_spring_security_logout";
                    LOGGER.debug("Login link : " + path);
                    getFooter().removeAllComponents();
                    getFooter().addComponent(generateSuccessLayoutWithLink(successMessage, loginLink, path));

                } catch (Validator.InvalidValueException e) {
                    LOGGER.error("Error occur while validating - ", e.getMessage());
                } catch (Throwable th) {
                    LOGGER.error("Error while handling change MPIN request ", th);
                    generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                            "registration.user.change.mpin.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                }
            }
        });

        final Button resetButton = new Button(commonRegistrationApp.getMessage("registration.button.reset"));
        resetButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                LOGGER.debug("Discarding change password form values entered....");
                discard();
                if (getField(CURRENT_MPIN_PID) != null) {
                    getField(CURRENT_MPIN_PID).focus();
                } else {
                    getField(NEW_MPIN_PID).focus();
                }
            }
        });
        resetButton.setStyleName("action-button");
        buttonLayout.addComponent(ChangeMpinButton);
        buttonLayout.addComponent(resetButton);
        footerLayout.addComponent(buttonLayout);
        return footerLayout;
    }

    /**
     * Re-type MPIN validation
     *
     * @throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidValueException
     */
    void validateMpin() throws NoSuchAlgorithmException,
            UnsupportedEncodingException, Validator.InvalidValueException {

        String currentMpin = Encrypter.MD5((String) currentMpinField.getValue());
        String newMpin = (String) newMpinField.getValue();
        String confirmMpin = (String) confirmMpinField.getValue();
        if (!currentMpin.equals(user.getMpin()) && user.getMpin() != null) {
            currentMpinField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.change.mpin.wrong")));
            throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.change.mpin.wrong"));
        } else {
            currentMpinField.setComponentError(null);
            if (!newMpin.trim().equals(confirmMpin.trim())) {
                newMpinField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.mpin.does.not.match")));
                throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.mpin.does.not.match"));
        } else {
                newMpinField.setComponentError(null);
            }
        }
    }

    @Override
    public void discard() throws Buffered.SourceException {
        if (getField(CURRENT_MPIN_PID) != null) {
            getField(CURRENT_MPIN_PID).setValue("");
        }
        getField(NEW_MPIN_PID).setValue("");
        getField(CONFIRM_MPIN_PID).setValue("");
    }
}