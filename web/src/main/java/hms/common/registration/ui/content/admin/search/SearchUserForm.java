/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin.search;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.common.Condition;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static hms.common.registration.model.UserStatus.*;
import static hms.common.registration.model.UserType.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SearchUserForm extends HorizontalLayout {

    private static final Logger logger = LoggerFactory.getLogger(SearchUserForm.class);

    private static final String WIDTH = "125px";

    public static final String SEARCH_PARAMETER_USERNAME = "username";
    public static final String SEARCH_PARAMETER_FIRST_NAME = "firstName";
    public static final String SEARCH_PARAMETER_USER_STATUS = "userStatus";
    public static final String SEARCH_PARAMETER_USER_TYPE = "userType";
    public static final String SEARCH_PARAMETER_CORPORATE_PARENT_ID = "corperateParentId";

    private TextField usernameTxtFld;
    private TextField firstNameTxtFld;
    private ComboBox userStatusCombo;
    private ComboBox userTypeCombo;
    private ManageUserView manageUserView;
    private CommonRegistrationApp commonRegistrationApp;

    private void init() {
        setDebugId("UserSearchForm");
        setSpacing(true);

        User user = getLoggedInUser();

        usernameTxtFld = new TextField(commonRegistrationApp.getMessage("registration.admin.user.username"));
        usernameTxtFld.setDebugId("UsernameTextField");
        usernameTxtFld.setWidth(WIDTH);
        addComponent(usernameTxtFld);

        firstNameTxtFld = new TextField(commonRegistrationApp.getMessage("registration.admin.user.first.name"));
        firstNameTxtFld.setDebugId("FirstNameTextField");
        firstNameTxtFld.setWidth(WIDTH);
        addComponent(firstNameTxtFld);

        userStatusCombo = new ComboBox(commonRegistrationApp.getMessage("registration.admin.user.status"));
        userStatusCombo.setDebugId("StatusCombo");
        userStatusCombo.setWidth(WIDTH);
        userStatusCombo.addItem(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        userStatusCombo.addItem(ACTIVE);
        userStatusCombo.addItem(INITIAL);
        userStatusCombo.addItem(PENDING_FOR_APPROVAL);
        userStatusCombo.addItem(SUSPEND);
        userStatusCombo.setNullSelectionAllowed(true);
        userStatusCombo.setNullSelectionItemId(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        addComponent(userStatusCombo);

        if (user != null && user.getUserType().equals(ADMIN_USER)) {
            userTypeCombo = new ComboBox(commonRegistrationApp.getMessage("registration.admin.user.type"));
            userTypeCombo.setDebugId("UserType");
            userTypeCombo.setWidth(WIDTH);
            userTypeCombo.addItem(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
            userTypeCombo.addItem(ADMIN_USER);
            userTypeCombo.addItem(CORPORATE);
            userTypeCombo.addItem(INDIVIDUAL);
            userTypeCombo.addItem(OPEN_ID);
            userTypeCombo.addItem(SUB_CORPORATE);
            userTypeCombo.setNullSelectionAllowed(true);
            userTypeCombo.setNullSelectionItemId(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
            addComponent(userTypeCombo);
        }
    }

    private User getLoggedInUser() {
        UserService userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        User user = null;
        try {
            user = userService.findUserByName(commonRegistrationApp.currentUserName());
        } catch (Exception e) {
            logger.error("Error occurred while retrieving user details ", e);
        }
        return user;
    }

    public SearchUserForm(ManageUserView manageUserView, CommonRegistrationApp commonRegistrationApp) {
        this.manageUserView = manageUserView;
        this.commonRegistrationApp = commonRegistrationApp;

        init();
    }

    public List<Condition> getSearchConditionList() {
        List<Condition> searchConditionList = new ArrayList<Condition>();

        String username = usernameTxtFld.getValue().toString();

        if (username != null && !("").equals(username.trim())) {
            if (username.contains("'")) {
                manageUserView.showInputErrorMessage(commonRegistrationApp.getMessage("registration.search.invalid.parameters"));
            } else {
                searchConditionList.add(new Condition(SEARCH_PARAMETER_USERNAME, "%" + username + "%", "LIKE"));
            }
        }

        String firstName = firstNameTxtFld.getValue().toString();

        if (firstName != null && !("").equals(firstName.trim())) {
            if (firstName.contains("'")) {
                manageUserView.showInputErrorMessage(commonRegistrationApp.getMessage("registration.search.invalid.parameters"));
            } else {
                searchConditionList.add(new Condition(SEARCH_PARAMETER_FIRST_NAME, "%" + firstName + "%", "LIKE"));
            }
        }

        if (userStatusCombo.getValue() != null) {
            searchConditionList.add(new Condition(SEARCH_PARAMETER_USER_STATUS, userStatusCombo.getValue(), "="));
        }

        if (userTypeCombo != null && userTypeCombo.getValue() != null) {
            searchConditionList.add(new Condition(SEARCH_PARAMETER_USER_TYPE, userTypeCombo.getValue(), "="));
        }

        return searchConditionList;
    }
}