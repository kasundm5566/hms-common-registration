/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin.view;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.Role;
import hms.common.registration.model.SubCorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.CommonRegistrationApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Set;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateHeading;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.getSpacer;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class UserProfileView extends VerticalLayout {

    private static final int MAX_COLS = 3;
    private static final int MAX_ROWS = 100;
    private static Logger logger = LoggerFactory.getLogger(UserProfileView.class);
    private User user;
    private GridLayout mainLayout;
    private CommonRegistrationApp commonRegistrationApp;

    /**
     * User profile providing userID
     *
     * @param userId
     */
    public UserProfileView(String userId, CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        UserService userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        try {
            this.user = userService.findUserByUserId(userId);
            setWidth("900px");
            if (user == null) {
                createErrorView();
            } else {
                GridLayout gridLayout = createUserProfileView();
                addComponent(gridLayout);
                setComponentAlignment(gridLayout, Alignment.TOP_CENTER);
            }
        } catch (DataManipulationException e) {
            createErrorView();
            logger.error(e.getMessage(), e);
        } catch (Exception e) {
            createErrorView();
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * user profile providing User
     *
     * @param user
     */
    public UserProfileView(User user, CommonRegistrationApp commonRegistrationApp) {
        this.user = user;
        this.commonRegistrationApp = commonRegistrationApp;
        setWidth("650px");
        GridLayout gridLayout = createUserProfileView();
        addComponent(gridLayout);
        setComponentAlignment(gridLayout, Alignment.TOP_CENTER);
    }

    private GridLayout createUserProfileView() {
        mainLayout = new GridLayout(MAX_COLS, MAX_ROWS);
        mainLayout.setWidth("550px");
        mainLayout.setSpacing(true);
        mainLayout.setMargin(true);
        switch (user.getUserType()) {
            case INDIVIDUAL:
                createIndividualUserProfile();
                break;
            case CORPORATE:
                createCorporateUserProfile();
                break;
            case SUB_CORPORATE:
                createSubCorporateUserProfile();
                break;
            case ADMIN_USER:
                createAdminUserProfile();
                break;
            case OPEN_ID:
                createOpenidUserProfile();
                break;
        }
        return mainLayout;
    }

    private void createOpenidUserProfile() {
    }

    private void createAdminUserProfile() {
        mainLayout.addComponent(generateHeading("registration.user.basic.details.heading", commonRegistrationApp), 0, 0, 2, 0);
        mainLayout.addComponent(getSpacer(), 0, 1, 2, 1);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.username")), 0, 2);
        mainLayout.addComponent(new Label(user.getUsername()), 1, 2);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.email")), 0, 3);
        mainLayout.addComponent(new Label(user.getEmail()), 1, 3);

        mainLayout.addComponent(getSpacer(), 0, 4, 2, 4);
        mainLayout.addComponent(generateHeading("registration.user.personal.details.heading", commonRegistrationApp), 0, 5, 2, 5);
        mainLayout.addComponent(getSpacer(), 0, 6, 2, 6);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.firstName")), 0, 7);
        mainLayout.addComponent(new Label(user.getFirstName()), 1, 7);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.lastName")), 0, 8);
        mainLayout.addComponent(new Label(user.getLastName()), 1, 8);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.mobileNo")), 0, 9);
        mainLayout.addComponent(new Label(user.getMobileNo()), 1, 9);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.userGroup")), 0, 10);
        mainLayout.addComponent(new Label(user.getUserGroup().getName()), 1, 10);
    }

    private void createSubCorporateUserProfile() {
        mainLayout.addComponent(generateHeading("registration.user.basic.details.heading", commonRegistrationApp), 0, 0, 2, 0);
        mainLayout.addComponent(getSpacer(), 0, 1, 2, 1);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.username")), 0, 2);
        mainLayout.addComponent(new Label(user.getUsername()), 1, 2);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.email")), 0, 3);
        mainLayout.addComponent(new Label(user.getEmail()), 1, 3);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.admin.user.corporate.parent")), 0, 4);
        mainLayout.addComponent(new Label(user.getCorperateParentId().getUsername()), 1, 4);

        Set<Role> roleList = ((SubCorporateUser) user).getRoles();
        if (!roleList.isEmpty()) {
            mainLayout.addComponent(getSpacer(), 0, 5, 2, 5);
            mainLayout.addComponent(generateHeading("registration.admin.permission.heading", commonRegistrationApp), 0, 6, 2, 6);
            mainLayout.addComponent(getSpacer(), 0, 7, 2, 7);

            int rowCount = 8;

            for (Role role : roleList) {
                mainLayout.addComponent(new Label(role.getName()), 0, rowCount, 2, rowCount);
                rowCount++;
            }
        }
    }

    private void createCorporateUserProfile() {
        mainLayout.addComponent(generateHeading("registration.user.basic.details.heading", commonRegistrationApp), 0, 0, 2, 0);
        mainLayout.addComponent(getSpacer(), 0, 1, 2, 1);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.username")), 0, 2);
        mainLayout.addComponent(new Label(user.getUsername()), 1, 2);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.email")), 0, 3);
        mainLayout.addComponent(new Label(user.getEmail()), 1, 3);

        mainLayout.addComponent(getSpacer(), 0, 4, 2, 4);
        mainLayout.addComponent(generateHeading("registration.user.organization.details.heading", commonRegistrationApp), 0, 5, 2, 5);
        mainLayout.addComponent(getSpacer(), 0, 6, 2, 6);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.orgName")), 0, 7);
        mainLayout.addComponent(new Label(((CorporateUser) user).getOrgName()), 1, 7);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.orgSize")), 0, 8);
        mainLayout.addComponent(new Label(((CorporateUser) user).getOrgSize()), 1, 8);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.industry")), 0, 9);
        mainLayout.addComponent(new Label(((CorporateUser) user).getIndustry()), 1, 9);

        mainLayout.addComponent(getSpacer(), 0, 10, 2, 10);
        mainLayout.addComponent(generateHeading("registration.user.contact.details.heading", commonRegistrationApp), 0, 11, 2, 11);
        mainLayout.addComponent(getSpacer(), 0, 12, 2, 12);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.address")), 0, 13);
        mainLayout.addComponent(new Label(user.getAddress()), 1, 13);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.postCode")), 0, 14);
        mainLayout.addComponent(new Label(user.getPostCode()), 1, 14);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.city")), 0, 15);
        mainLayout.addComponent(new Label(user.getCity()), 1, 15);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.province")), 0, 16);
        mainLayout.addComponent(new Label(user.getProvince()), 1, 16);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.country")), 0, 17);
        mainLayout.addComponent(new Label(user.getCountry()), 1, 17);

        mainLayout.addComponent(getSpacer(), 0, 18, 2, 18);
        mainLayout.addComponent(generateHeading("registration.user.organization.contact.person.details.heading", commonRegistrationApp), 0, 19, 2, 19);
        mainLayout.addComponent(getSpacer(), 0, 20, 2, 20);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.contactPersonName")), 0, 21);
        mainLayout.addComponent(new Label(((CorporateUser) user).getContactPersonName()), 1, 21);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.contactPersonEmail")), 0, 22);
        mainLayout.addComponent(new Label(((CorporateUser) user).getContactPersonEmail()), 1, 22);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.contactPersonPhone")), 0, 23);
        mainLayout.addComponent(new Label(((CorporateUser) user).getContactPersonPhone()), 1, 23);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.userGroup")), 0, 24);
        mainLayout.addComponent(new Label(((CorporateUser) user).getUserGroup().getName()), 1, 24);
    }

    private void createIndividualUserProfile() {
        mainLayout.addComponent(generateHeading("registration.user.basic.details.heading", commonRegistrationApp), 0, 0, 2, 0);
        mainLayout.addComponent(getSpacer(), 0, 1, 2, 1);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.username")), 0, 2);
        mainLayout.addComponent(new Label(user.getUsername()), 1, 2);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.email")), 0, 3);
        mainLayout.addComponent(new Label(user.getEmail()), 1, 3);

        mainLayout.addComponent(getSpacer(), 0, 4, 2, 4);
        mainLayout.addComponent(generateHeading("registration.user.personal.details.heading", commonRegistrationApp), 0, 5, 2, 5);
        mainLayout.addComponent(getSpacer(), 0, 6, 2, 6);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.firstName")), 0, 7);
        mainLayout.addComponent(new Label(user.getFirstName()), 1, 7);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.lastName")), 0, 8);
        mainLayout.addComponent(new Label(user.getLastName()), 1, 8);

        if (user.getBirthday() != null) {
            mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.birthday")), 0, 9);
            mainLayout.addComponent(new Label(new SimpleDateFormat("dd-MMM-yyyy").format(user.getBirthday())), 1, 9);
        }

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.gender")), 0, 10);
        mainLayout.addComponent(new Label(user.getGender()), 1, 10);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.profession")), 0, 11);
        mainLayout.addComponent(new Label(user.getProfession()), 1, 11);

        mainLayout.addComponent(getSpacer(), 0, 12, 2, 12);
        mainLayout.addComponent(generateHeading("registration.user.contact.details.heading", commonRegistrationApp), 0, 13, 2, 13);
        mainLayout.addComponent(getSpacer(), 0, 14, 2, 14);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.address")), 0, 15);
        mainLayout.addComponent(new Label(user.getAddress()), 1, 15);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.postCode")), 0, 16);
        mainLayout.addComponent(new Label(user.getPostCode()), 1, 16);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.city")), 0, 17);
        mainLayout.addComponent(new Label(user.getCity()), 1, 17);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.province")), 0, 18);
        mainLayout.addComponent(new Label(user.getProvince()), 1, 18);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.country")), 0, 19);
        mainLayout.addComponent(new Label(user.getCountry()), 1, 19);

        mainLayout.addComponent(getSpacer(), 0, 20, 2, 20);
        mainLayout.addComponent(generateHeading("registration.user.other.details.heading", commonRegistrationApp), 0, 21, 2, 21);
        mainLayout.addComponent(getSpacer(), 0, 22, 2, 22);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.operator")), 0, 23);
        mainLayout.addComponent(new Label(user.getOperator()), 1, 23);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.mobileNo")), 0, 24);
        mainLayout.addComponent(new Label(user.getMobileNo()), 1, 24);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.phoneType")), 0, 25);
        mainLayout.addComponent(new Label(user.getPhoneType()), 1, 25);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.phoneModel")), 0, 26);
        mainLayout.addComponent(new Label(user.getPhoneModel()), 1, 26);

        mainLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.userGroup")), 0, 27);
        mainLayout.addComponent(new Label(user.getUserGroup().getName()), 1, 27);
    }

    private void createErrorView() {
        setSpacing(true);
        addComponent(new Label(commonRegistrationApp.getMessage("registration.user.cannot.find.user.message")));
    }

}
