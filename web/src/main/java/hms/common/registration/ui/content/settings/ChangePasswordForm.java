/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.settings;

import com.vaadin.data.Buffered;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.util.Encrypter;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN;
import static hms.common.registration.util.PropertyHolder.PASSWORD_REGEX_PATTERN;

/**
 * Handles the change password flow
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class ChangePasswordForm extends Form {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChangePasswordForm.class);
    private static final String CURRENT_PASSWORD_PID = "currentPassword";
    private static final String NEW_PASSWORD_PID = "newPassword";
    private static final String CONFIRM_PASSWORD_PID = "confirmPassword";
    private static final String USERNAME_PID = "username";
    private CommonRegistrationApp commonRegistrationApp;
    private TextField newPasswordField;
    private TextField confirmPasswordField;
    private TextField currentPasswordField;
    private UserService userService;
    private User user;
    private String userName;

    /**
     * Defines form constructor for change password
     *
     * @param commonRegistrationApp
     */
    public ChangePasswordForm(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        try {
            this.userName = commonRegistrationApp.currentUserName();
            this.user = userService.findUserByName(userName);
        } catch (DataManipulationException e) {
            LOGGER.error("Error while loading logged in user", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.change.password.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
        LOGGER.debug("Creating password change form for user [{}]", this.userName);
        initChangePasswordForm();
    }

    /**
     * Initializes change password form
     */
    void initChangePasswordForm() {
        setImmediate(true);
        setWriteThrough(false);
        TextField userNameField = new TextField(commonRegistrationApp.getMessage("registration.change.password.username"));
        userNameField.setEnabled(false);
        userNameField.setValue(userName);
        currentPasswordField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.password.current.password"),
                "registration.change.password.required.error", commonRegistrationApp);

        if (user.getUserType() == UserType.INDIVIDUAL) {
            newPasswordField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.password.new.password"),
                    "registration.change.password.required.error", commonRegistrationApp);
            newPasswordField.addValidator(new RegexpValidator(INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.individual.invalid.password")));

        } else {
            newPasswordField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.password.new.password"),
                    "registration.change.password.required.error", commonRegistrationApp);
            newPasswordField.addValidator(new RegexpValidator(PASSWORD_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.password")));
        }


        confirmPasswordField = (TextField) createPasswordField(commonRegistrationApp.getMessage("registration.change.password.confirm.password"),
                "registration.change.password.required.error", commonRegistrationApp);
        addField(USERNAME_PID, userNameField);
        addField(CURRENT_PASSWORD_PID, currentPasswordField);
        addField(NEW_PASSWORD_PID, newPasswordField);
        addField(CONFIRM_PASSWORD_PID, confirmPasswordField);
        LOGGER.debug("Password change form loaded successfully.");
        addCurrentPasswordValidator();
        addRetypePasswordValidator();
        setWriteThrough(false);
        createFormFooter(userName);
    }

    private void addCurrentPasswordValidator() {
        getField(CURRENT_PASSWORD_PID).addValidator(new Validator() {

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    LOGGER.debug("Current Password Incorrect");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.change.password.wrong"));
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (value == null) {
                    return false;
                } else if (passwordMatching(value)) {
                    return true;
                } else {
                    return false;
                }
            }

            private boolean passwordMatching(Object value) {
                try {
                    String encodedPassword = MD5((String) value);
                    return encodedPassword.equals(user.getPassword());
                } catch (NoSuchAlgorithmException e) {
                    LOGGER.error("NoSuchAlgorithmException", e);
                } catch (UnsupportedEncodingException e) {
                    LOGGER.error("UnsupportedEncodingException", e);
                }
                return false;
            }
        });
    }

    /**
     * Generates change password form footer
     *
     * @param userName
     * @return
     */
    VerticalLayout createFormFooter(final String userName) {
        VerticalLayout footerLayout = new VerticalLayout();
        setFooter(footerLayout);
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        final Button changePasswordButton = new Button(commonRegistrationApp.getMessage("registration.change.password.submit.button"));
        changePasswordButton.setStyleName("action-button");
        changePasswordButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                LOGGER.debug("Change Password Request Received for User [{}]", userName);
                try {
                    user = userService.findUserById(user.getId());
                    setComponentError(null);
                    commit();
                    validatePassword();
                    user.setPassword(Encrypter.MD5((String) newPasswordField.getValue()).trim());
                    userService.merge(user);
                    discard();
                    setReadOnly(true);
                    String successMessage = commonRegistrationApp.getMessage("registration.user.change.password.success.message");
                    String loginLink = commonRegistrationApp.getMessage("registration.user.change.password.success.message.login");
                    String path = commonRegistrationApp.getURL().getPath() + "j_spring_security_logout";
                    LOGGER.debug("Login link : " + path);
                    getFooter().removeAllComponents();
                    CustomLayout successLayout = generateSuccessLayoutWithLink(successMessage, loginLink, path);
                    successLayout.addStyleName("success-password-change");
                    getFooter().addComponent(successLayout);
                } catch (Validator.InvalidValueException e) {
                    LOGGER.debug("Invalid form input [{}]", e.getMessage());
                } catch (Throwable th) {
                    LOGGER.error("Error while handling change password request ", th);
                    generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                            "registration.user.change.password.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                }
            }
        });

        final Button resetButton = new Button(commonRegistrationApp.getMessage("registration.button.reset"));
        resetButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                LOGGER.debug("Discarding change password form values entered....");
                discard();
                getField(CURRENT_PASSWORD_PID).focus();
            }
        });
        resetButton.setStyleName("action-button");
        buttonLayout.addComponent(changePasswordButton);
        buttonLayout.addComponent(resetButton);
        footerLayout.addComponent(buttonLayout);
        return footerLayout;
    }

    /**
     * adding validator for retype password field
     */
    private void addRetypePasswordValidator() {
        getField(CONFIRM_PASSWORD_PID).addValidator(new Validator() {

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    LOGGER.debug("Password not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.password.does.not.match"));
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (value == null) {
                    return false;
                } else if (value.equals(getField(NEW_PASSWORD_PID).getValue())) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    /**
     * Re-type password validation
     */
    void validatePassword() throws NoSuchAlgorithmException,
            UnsupportedEncodingException, Validator.InvalidValueException {

        String currentPassword = Encrypter.MD5((String) currentPasswordField.getValue());
        String newPassword = (String) newPasswordField.getValue();
        String confirmPassword = (String) confirmPasswordField.getValue();
        if (!currentPassword.equals(user.getPassword())) {
            LOGGER.debug("User current password and entered password don't match");
            currentPasswordField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.change.password.wrong")));
            throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.change.password.wrong"));
        } else {
            LOGGER.debug("User current password matched with the entered password");
            currentPasswordField.setComponentError(null);
            currentPasswordField.setRequiredError(null);
            if (!newPassword.trim().equals(confirmPassword.trim())) {
                LOGGER.debug("New password and confirm password don't match!");
                newPasswordField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.password.does.not.match")));
                throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.password.does.not.match"));
            } else {
                LOGGER.debug("User current password, new password, confirm password are fine, going to update the db");
                newPasswordField.setComponentError(null);
                newPasswordField.setRequiredError(null);
                confirmPasswordField.setComponentError(null);
                confirmPasswordField.setRequiredError(null);
                currentPasswordField.setRequired(false);
                newPasswordField.setRequired(false);
                confirmPasswordField.setRequired(false);
                this.setComponentError(null);
            }
        }
    }

    @Override
    public void discard() throws Buffered.SourceException {
        getField(CURRENT_PASSWORD_PID).setValue("");
        getField(NEW_PASSWORD_PID).setValue("");
        getField(CONFIRM_PASSWORD_PID).setValue("");
    }
}