/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.MenuBar;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static hms.common.registration.ui.common.util.ExternalLinks.*;
import static hms.common.registration.util.ValidateUser.isValidUser;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ExternalModulesMenu extends MenuBar {

    private static final Logger logger = LoggerFactory.getLogger(ExternalModulesMenu.class);

    private CommonRegistrationApp commonRegistrationApp;

    private void init() {
        String loggedInUserName;
        try {
            loggedInUserName = commonRegistrationApp.currentUserName();
        } catch (NullPointerException e) {
            return;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while getting username", e);
            return;
        }

        try {
            UserService userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
            User user = userService.findUserByName(loggedInUserName);
            if (user != null && isValidUser(user)) {
                filterLinksByUser(user);
            }
        } catch (DataManipulationException e) {
            logger.error("Error occurred while getting user details", e);
        } catch (Exception e) {
            logger.error("Error occurred while generating top header links", e);
        }
    }

    private void filterLinksByUser(User user) {
        List<String> userRoles = user.getRolesAsStrings();

        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.provisioning"), PROVISIONING_URL, "ROLE_PROV_LOGIN");
        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.appstore"), APPSTORE_URL, "ROLE_APP_LOGIN");
        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.sdp"), SDP_URL, "ROLE_ADM_LOGIN");
        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.soltura"), SOLTURA_URL, "ROLE_SOL_LOGIN");
        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.reporting"), REPORTING_URL, "ROLE_SDP_RPT_LOGIN");
        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.pgw.self.care"), PGW_SELF_CARE_URL, "ROLE_PGW_CSC_LOGIN");
        addMenuLink(userRoles, commonRegistrationApp.getMessage("module.pgw.admin.care"), PGW_ADMIN_CARE_URL, "ROLE_PGW_CS_LOGIN");
    }

    private void addMenuLink(List<String> userRoles, String caption, final String externalUrl, String role) {
        if (userRoles.contains(role)) {
            final MenuItem menuItem = addItem(caption, new Command() {

                @Override
                public void menuSelected(MenuItem selectedItem) {
                    getWindow().open(new ExternalResource(externalUrl));
                }
            });
            menuItem.setStyleName("menu-link");
        }
    }

    public ExternalModulesMenu(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;

        init();
    }
}



