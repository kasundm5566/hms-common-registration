/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.validator;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserVerificationValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserVerificationValidator.class);
    private String generatedMsisdnVerificationCode;

    private String invalidVerificationCodeMsg;

    public UserVerificationValidator(String invalidVerificationCodeMsg) {
        this.invalidVerificationCodeMsg = invalidVerificationCodeMsg;
    }

    @Override
    public void validate(Object o) throws InvalidValueException {
        if(!isValid(o)) {
            LOGGER.debug("Invalid MSISDN Verification found [{}]" ,o);
            throw new InvalidValueException(invalidVerificationCodeMsg);
        }
    }

    @Override
    public boolean isValid(Object o) {
        String enteredVerificationCode = (String)o;
        return enteredVerificationCode.trim().equals(generatedMsisdnVerificationCode);

    }

    public void setGeneratedMsisdnVerificationCode(String generatedMsisdnVerificationCode) {
        LOGGER.debug("Generated verification code : " + generatedMsisdnVerificationCode);
        this.generatedMsisdnVerificationCode = generatedMsisdnVerificationCode;
    }
}