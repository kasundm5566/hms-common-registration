/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.service.UserService;

import java.util.Set;

import static hms.common.registration.util.PropertyHolder.MSISDN_REGEX_PATTERN;
import static hms.common.registration.util.PropertyHolder.OPERATOR_CODE_MAP;

/**
 * Created by IntelliJ IDEA.
 * User: isuru
 * Date: 1/26/12
 * Time: 5:28 PM
 */
public class IndividualUserOperatorValidator implements Validator {

    private String message;
    private OperatorMsisdnValidator operatorMsisdnValidator;
    private UserService userService;
    private CommonRegistrationApp commonRegistrationApp;

    public IndividualUserOperatorValidator(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        message = commonRegistrationApp.getMessage("registration.user.invalid.msisdn");
        operatorMsisdnValidator = new OperatorMsisdnValidator(commonRegistrationApp);
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(message);
        }
    }

    @Override
    public boolean isValid(Object value) {

        RegexpValidator regexPatternValidator = new RegexpValidator(MSISDN_REGEX_PATTERN,
                commonRegistrationApp.getMessage("registration.user.individual.invalid.msisdn"));

        operatorMsisdnValidator.setOperator(null);

        Set<String> supportedOperatorPrefixSet = OPERATOR_CODE_MAP.keySet();
        for (String operatorPrefix : supportedOperatorPrefixSet) {
            if (value != null) {
                if (((String) value).startsWith(operatorPrefix)) {
                    operatorMsisdnValidator.setOperator(OPERATOR_CODE_MAP.get(operatorPrefix));
                }
            }
        }

        if (msisdnAlreadyTaken((String) value)) {
            message = commonRegistrationApp.getMessage("registration.user.msisdn.not.available");
            return false;
        } else if (!regexPatternValidator.isValid(value)) {
            message = commonRegistrationApp.getMessage("registration.user.individual.invalid.msisdn");
            return false;
        } else if (operatorMsisdnValidator.getOperator() == null && regexPatternValidator.isValid(value)) {
            message = commonRegistrationApp.getMessage("registration.user.individual.not.supported.operator");
            return false;
        } else if (operatorMsisdnValidator.isValid(value)) {
            return true;
        } else {
            message = commonRegistrationApp.getMessage("registration.user.individual.invalid.msisdn");
            return false;
        }
    }

    private boolean msisdnAlreadyTaken(String value) {

        try {
            return userService.findUserByMsisdn(value) != null;
        } catch (DataManipulationException e) {
            return false;
        }
    }
}
