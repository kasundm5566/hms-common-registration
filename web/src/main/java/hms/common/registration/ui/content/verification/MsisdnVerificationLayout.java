/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.verification;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.util.ExternalLinks;
import hms.common.registration.ui.content.user.success.UserCreationSuccessView;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.validator.OperatorMsisdnValidator;
import hms.common.registration.ui.validator.UserVerificationValidator;
import hms.common.registration.util.ValidateUser;
import hms.common.registration.util.message.Msisdn;
import hms.common.registration.util.message.SmsMessage;
import hms.common.registration.util.sms.SmsMtDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Date;


import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateSystemNotificationMessage;
import static hms.common.registration.util.PropertyHolder.MSISDN_REGEX_PATTERN;
import static hms.common.registration.util.PropertyHolder.MSISDN_VERIFICATION_MSG_TEMPLATE;
import static hms.common.registration.util.RegistrationFeatureRegistry.isCorporateUserEmailVerifyEnabled;
import static hms.common.registration.util.WebUtils.generateVerificationCode;

public class MsisdnVerificationLayout extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(MsisdnVerificationLayout.class);

    private final CommonRegistrationApp commonRegistrationApp;
    private User user;
    private final UserService userService;
    private SmsMtDispatcher smsMtDispatcher;
    private Label mobileNoLabel = new Label();
    private TextField verificationCodeInputField;
    private UserVerificationValidator userVerificationValidator;
    private boolean currentlyLoggedIn = false;

    public MsisdnVerificationLayout(CommonRegistrationApp commonRegistrationApp,
                                    User user,
                                    UserService userService,
                                    SmsMtDispatcher smsMtDispatcher,
                                    UserVerificationValidator userVerificationValidator,
                                    Boolean currentlyLoggedIn) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.user = user;
        this.userService = userService;
        this.smsMtDispatcher = smsMtDispatcher;
        this.userVerificationValidator = userVerificationValidator;
        this.currentlyLoggedIn = currentlyLoggedIn;
        if(user.getMsisdnVerificationCode() == null) {
            sendVerificationMsg();
        }
        this.addComponent(createUserMsisdnLayout());
    }

    private void sendVerificationMsg() {
        String msisdnVerificationCode = generateVerificationCode();
        try {
            user.setMsisdnVerificationCode(msisdnVerificationCode);
            user = userService.merge(user);
            sendMessage(commonRegistrationApp.getMessage("system.default.sender.address"), user.getMobileNo(), "",
                    MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, user.getMsisdnVerificationCode()));
        } catch (DataManipulationException e) {
            logger.error("Error occured while sending the msisdn verification message [{}].", e);
        }
    }

    private GridLayout createUserMsisdnLayout() {
        GridLayout msisdnLayout = new GridLayout(3, 3);
        msisdnLayout.setWidth("500px");
        msisdnLayout.setHeight("150px");
        msisdnLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.msisdn.verification",
                new Object[]{user.getMsisdnVerificationCode(), user.getEmailVerificationCode()})), 0, 0, 2, 0);
        mobileNoLabel = new Label(commonRegistrationApp.getMessage("registration.user.mobileNo"));
        mobileNoLabel.setWidth("120px");
        msisdnLayout.addComponent(mobileNoLabel, 0, 1);
        Button notThisNoButton;
        if (user.getMobileNo() == null) {
            mobileNoLabel = new Label(commonRegistrationApp.getMessage("registration.user.mobile.number.expired"));
            mobileNoLabel.setStyleName("error-label");
            notThisNoButton = new Button(commonRegistrationApp.getMessage("registration.user.enter.new.mobile.number"));
        } else {
            mobileNoLabel = new Label(user.getMobileNo());
            notThisNoButton = new Button(commonRegistrationApp.getMessage("registration.user.user.verification.not.this.number"));
        }
        msisdnLayout.addComponent(mobileNoLabel, 1, 1);
        notThisNoButton.setStyleName(BaseTheme.BUTTON_LINK);
        notThisNoButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                Window newNumberWindow = generateEnterNewNumberWindow();
                getWindow().addWindow(newNumberWindow);
            }
        });
        msisdnLayout.addComponent(notThisNoButton, 2, 1);
        msisdnLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.user.verification.code")), 0, 2);
        msisdnLayout.addComponent(createVerificationCodeTextField(user), 1, 2);
        msisdnLayout.addComponent(createSubmitButton(), 2, 2);
        return msisdnLayout;
    }

    private Window generateEnterNewNumberWindow() {
        final Window newMobileNoWindow = new Window(commonRegistrationApp.getMessage("registration.user.enter.new.mobile.number"));
        newMobileNoWindow.setResizable(false);
        newMobileNoWindow.setModal(true);
        newMobileNoWindow.setStyleName("opaque");
        newMobileNoWindow.setWidth("400px");
        newMobileNoWindow.setHeight("150px");
        newMobileNoWindow.center();
        newMobileNoWindow.setImmediate(true);
        GridLayout newNoLayout = new GridLayout(2, 2);
        newNoLayout.setWidth("350px");
        newNoLayout.setSpacing(true);
        newNoLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.mobileNo")), 0, 0);

        final TextField mobileNoField = new TextField();

        mobileNoField.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN,
                commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));

        newNoLayout.addComponent(mobileNoField, 1, 0);

        Button submitButton = new Button(commonRegistrationApp.getMessage("registration.button.submit"));
        submitButton.setStyleName("action-button");
        submitButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {

                OperatorMsisdnValidator msisdnValidator = new OperatorMsisdnValidator(commonRegistrationApp);
                msisdnValidator.setOperator(user.getOperator());
                if (msisdnValidator.isValid(mobileNoField.getValue())) {
                    try {
                        if (userService.findUserByMsisdn((String) mobileNoField.getValue()) == null) {

                            String msisdnVerificationCode = generateVerificationCode();

                            if (user.getUserType() == UserType.CORPORATE) {
                                CorporateUser corporateUser = (CorporateUser) user;
                                corporateUser.setMobileNo((String) mobileNoField.getValue());
                                corporateUser.setContactPersonPhone((String) mobileNoField.getValue());
                                corporateUser.setMsisdnVerificationCode(msisdnVerificationCode);
                                user = userService.merge(corporateUser);

                            } else {
                                user.setMobileNo((String) mobileNoField.getValue());
                                user.setMsisdnVerificationCode(msisdnVerificationCode);
                                user = userService.merge(user);

                            }
                            sendMessage(commonRegistrationApp.getMessage("system.default.sender.address"), user.getMobileNo(), user.getOperator().toLowerCase(),
                                    MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, msisdnVerificationCode));
                            getWindow().removeWindow(newMobileNoWindow);
                            mobileNoLabel.setValue(user.getMobileNo());
                            verificationCodeInputField.setValue("");
                            verificationCodeInputField.setComponentError(null);
                            userVerificationValidator.setGeneratedMsisdnVerificationCode(user.getMsisdnVerificationCode());
                        } else {
                            mobileNoField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.msisdn.not.available")));
                        }
                    } catch (DataManipulationException e) {
                    }
                } else {
                    mobileNoField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));
                }
            }

        });
        newNoLayout.addComponent(submitButton, 1, 1);

        newMobileNoWindow.addComponent(newNoLayout);
        newMobileNoWindow.addListener(new Window.CloseListener() {
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(newMobileNoWindow);
            }
        });
        return newMobileNoWindow;
    }

    /**
     * Sends SMS MT message with the generated verification code to the user's phone number
     *
     * @param senderAddress
     * @param receiverAddress
     * @param operator
     * @param message
     */
    private void sendMessage(String senderAddress, String receiverAddress, String operator,
                             String message) throws DataManipulationException {
        SmsMessage verificationSms = new SmsMessage();
        verificationSms.setSenderAddress(new Msisdn(senderAddress, operator));
        verificationSms.setReceiverAddress(new Msisdn(receiverAddress, operator));
        verificationSms.setMessage(message);
        smsMtDispatcher.sendIndividualMessage(verificationSms);
        user.setLastSmsSendDate(new Date());
        user = userService.merge(user);
    }

    private Component createVerificationCodeTextField(User user) {
        verificationCodeInputField = new TextField();
        userVerificationValidator.setGeneratedMsisdnVerificationCode(user.getMsisdnVerificationCode());
        verificationCodeInputField.addValidator(userVerificationValidator);
        verificationCodeInputField.setRequiredError(commonRegistrationApp.getMessage("registration.user.verification.code.required"));
        verificationCodeInputField.setImmediate(true);
        verificationCodeInputField.setValidationVisible(true);
        return verificationCodeInputField;
    }

    private Button createSubmitButton() {
        Button submitButton = new Button(commonRegistrationApp.getMessage("registration.user.verify.button.caption"),
                new Button.ClickListener() {
                    public void buttonClick(Button.ClickEvent event) {
                        try {
                            verificationCodeInputField.commit();
                            verificationCodeInputField.validate();
                            if (user.getMsisdnVerificationCode().equals(verificationCodeInputField.getValue())) {
                                user.setMsisdnVerified(true);
                                try {
                                    user = userService.findUserById(user.getId());
                                    user.setMsisdnVerificationCode((String) verificationCodeInputField.getValue());
                                    user.setMsisdnVerified(true);

                                    if (ValidateUser.isValidUser(user)) {
                                        user.activate();
                                    }
                                    user = userService.merge(user);
                                    handleUserCreationSuccessFlow();
                                } catch (Throwable e) {

                                    generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                                            "registration.user.user.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                                }
                            } else {
                                verificationCodeInputField.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.user.verification.code.invalid")));
                            }
                        } catch (Validator.InvalidValueException e) {

                        }
                    }
                });
        submitButton.setStyleName("action-button");
        return submitButton;
    }

    private void handleUserCreationSuccessFlow() {
        if (currentlyLoggedIn) {
            getWindow().open(new ExternalResource(ExternalLinks.CAS_LOGIN_URL));
        } else {
            boolean emailVerificationNotDone = !user.isEmailVerified() && isCorporateUserEmailVerifyEnabled();
            if (emailVerificationNotDone) {
                ContentPanel panel = commonRegistrationApp.getMainLayout().getContentLayout().getContentPanel();
                panel.removeAllComponents();
                panel.setCaption(commonRegistrationApp.getMessage("registration.user.user.creation.success.heading"));
                panel.addComponent(new UserCreationSuccessView(user, commonRegistrationApp, UserCreationSuccessView.MSISDN));
            } else {
                commonRegistrationApp.close();
            }
        }
    }
}