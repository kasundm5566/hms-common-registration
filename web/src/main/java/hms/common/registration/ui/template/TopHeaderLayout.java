/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.template;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.admin.ExternalModulesMenu;
import hms.common.registration.ui.common.util.ExternalLinks;
import hms.common.registration.util.RegistrationFeatureRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class TopHeaderLayout extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(TopHeaderLayout.class);

    private HorizontalLayout topLinks;
    private HorizontalLayout basicLinks;
    private Button loggedInUserNameLink;
    private ExternalModulesMenu modulesMenu;

    private CommonRegistrationApp commonRegistrationApp;

    private void init() {
        setSpacing(true);
        setSizeFull();
        setWidth("100%");
        setHeight("20px");
        createTopNavigation();
    }

    private void createTopNavigation() {
        topLinks = new HorizontalLayout();
        topLinks.setSizeFull();
        basicLinks = new HorizontalLayout();
        basicLinks.setSpacing(false);

        try {
            modulesMenu = new ExternalModulesMenu(commonRegistrationApp);
            topLinks.addComponent(modulesMenu);
            topLinks.setComponentAlignment(modulesMenu, Alignment.TOP_LEFT);
            String loggedInUserName = commonRegistrationApp.currentUserName();

            if (loggedInUserName != null) {
                topLinks.addComponent(basicLinks);
                topLinks.setComponentAlignment(basicLinks, Alignment.TOP_RIGHT);
                loggedInUserNameLink = new Button(loggedInUserName, new Button.ClickListener() {

                    public void buttonClick(Button.ClickEvent clickEvent) {
                        commonRegistrationApp.getFragmentUtility().setFragment("settings");
                    }
                });
                loggedInUserNameLink.setStyleName(BaseTheme.BUTTON_LINK);
                basicLinks.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.hi.heading")));
                basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());
                basicLinks.addComponent(loggedInUserNameLink);
                basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());
                basicLinks.addComponent(new Label("|"));
                basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());
                Button homeLinkButton = new Button("Home", new Button.ClickListener() {

                    public void buttonClick(Button.ClickEvent clickEvent) {
                        commonRegistrationApp.getFragmentUtility().setFragment("home");
                    }
                });
                homeLinkButton.setStyleName(BaseTheme.BUTTON_LINK);
                basicLinks.addComponent(homeLinkButton);
                basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());

                if (RegistrationFeatureRegistry.isHelpLinkEnabled()) {
                    basicLinks.addComponent(new Label("|"));
                    basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());
                    Button helpLink = new Button(commonRegistrationApp.getMessage("registration.user.help.heading"), new Button.ClickListener() {

                        public void buttonClick(Button.ClickEvent clickEvent) {
                            getWindow().open(new ExternalResource(ExternalLinks.HELP_URL));
                        }
                    });
                    helpLink.setStyleName(BaseTheme.BUTTON_LINK);
                    basicLinks.addComponent(helpLink);
                }

                basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());
                basicLinks.addComponent(new Label("|"));
                basicLinks.addComponent(CommonUiComponentGenerator.getSpacer());
                Button logoutLink = new Button(commonRegistrationApp.getMessage("registration.user.signout.heading"),
                        new Button.ClickListener() {

                            public void buttonClick(Button.ClickEvent clickEvent) {
                                String path = commonRegistrationApp.getURL().getPath();
                                getWindow().open(new ExternalResource(path + "j_spring_security_logout"));
                            }
                        });
                logoutLink.setStyleName(BaseTheme.BUTTON_LINK);
                basicLinks.addComponent(logoutLink);
            }
            addComponent(topLinks);
        } catch (Exception ignored) {
        }
    }

    public TopHeaderLayout(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;

        init();
    }

    public void setTopLinksVisible(boolean value) {
        topLinks.setVisible(value);
    }

    public void setModulesMenuVisible(boolean value) {
        modulesMenu.setVisible(value);
    }
}