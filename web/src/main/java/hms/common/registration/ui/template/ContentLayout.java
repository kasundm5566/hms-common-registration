/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.template;

import com.vaadin.ui.HorizontalLayout;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ContentLayout extends HorizontalLayout {

    private ContentPanel contentPanel;

    public ContentLayout() {
        setSpacing(true);
        setWidth("100%");
        contentPanel = new ContentPanel();
        contentPanel.setWidth("100%");
        addComponent(contentPanel);
    }

    public ContentPanel getContentPanel() {
        return contentPanel;
    }

    public void setContentPanel(ContentPanel contentPanel) {
        this.contentPanel = contentPanel;
    }
}