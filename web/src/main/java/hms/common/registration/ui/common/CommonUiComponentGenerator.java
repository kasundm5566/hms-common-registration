/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.common;

import com.google.common.base.Optional;
import com.vaadin.event.MouseEvents;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.Sizeable;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.validator.DateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.vaadin.terminal.Sizeable.UNITS_PERCENTAGE;
import static hms.common.registration.util.PropertyHolder.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CommonUiComponentGenerator {

    private static final Logger logger = LoggerFactory.getLogger(CommonUiComponentGenerator.class);

    // Icons for the table
    public static final ThemeResource PROFILE_ICON = new ThemeResource(
            "images/profile.png");
    public static final ThemeResource USER_ACCOUNT_ICON = new ThemeResource(
            "images/account.gif");
    public static final ThemeResource SEARCH_ICON = new ThemeResource(
            "images/search.png");

    public static final ThemeResource VIEW_ICON = new ThemeResource(
            "images/view.png");

    public static final ThemeResource EDIT_ICON = new ThemeResource(
            "images/modify.png");

    public static final ThemeResource REMOVE_ICON = new ThemeResource(
            "images/remove.png");

    public static final ThemeResource ENABLE_ICON = new ThemeResource(
            "images/Enable_icon.png");

    public static final ThemeResource DISABLE_ICON = new ThemeResource(
            "images/Disable_icon.png");

    public static final ThemeResource PGW_SELF_CARE_ICON = new ThemeResource(
            "images/PGW_Self_Care_75x75.png");

    public static final ThemeResource PGW_ADMIN_CARE_ICON = new ThemeResource(
            "images/PGW_Admin_Care_75x75.png");

    public static final ThemeResource PROVISION_ICON = new ThemeResource(
            "images/Provisioning_75x75.png");

    public static final ThemeResource SOLTURA_ICON = new ThemeResource(
            "images/Soltura_75x75.png");

    public static final ThemeResource APPSTORE_ICON = new ThemeResource(
            "images/Appstore_75x75.png");

    public static final ThemeResource APPSTORE_ADMIN_ICON = new ThemeResource(
            "images/Appstore_75x75.png");

    public static final ThemeResource SDP_ADMIN_ICON = new ThemeResource(
            "images/SDP_Admin_75x75.png");

    public static final ThemeResource REGISTRATION_ADMIN_ICON = new ThemeResource(
            "images/Registration_75x75.png");

    public static final ThemeResource REPORTING_ICON = new ThemeResource(
            "images/Reporting_75x75.png");

    public static final int HUMANIZED = Window.Notification.TYPE_HUMANIZED_MESSAGE;
    public static final int WARNING = Window.Notification.TYPE_WARNING_MESSAGE;
    public static final int ERROR = Window.Notification.TYPE_ERROR_MESSAGE;
    public static final int TRAY = Window.Notification.TYPE_TRAY_NOTIFICATION;

    public static Label generateHorizontalSeperator() {
        Label horizontalSeparator = new Label("<hr/>", Label.CONTENT_XHTML);
        horizontalSeparator.setStyleName("seperator");
        return horizontalSeparator;
    }

    public static Label getSpacer() {
        return new Label("&nbsp;", Label.CONTENT_XHTML);
    }


    public static Button actionButton(String captionId, CommonRegistrationApp commonRegistrationApp) {
        final Button button = new Button(commonRegistrationApp.getMessage(captionId));
        button.setStyleName("action-button");
        button.setWidth(80, UNITS_PERCENTAGE);
        return button;
    }

    public static Window generateUserTermsSubWindow(CommonRegistrationApp commonRegistrationApp, String userTermsText) {
        Window termsAndConditionWindow = new Window(commonRegistrationApp.getMessage("registration.user.user.terms"));
        termsAndConditionWindow.setModal(true);
        termsAndConditionWindow.setWidth("500px");
        termsAndConditionWindow.setHeight("100%");
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(new Label(userTermsText, Label.CONTENT_XHTML));
        termsAndConditionWindow.addComponent(verticalLayout);
        return termsAndConditionWindow;
    }

    public static Window generateCorporateUserTermsSubWindow(CommonRegistrationApp commonRegistrationApp) {
        return generateUserTermsSubWindow(commonRegistrationApp, corporateUserTermsText);
    }

    public static Window generateIndividualUserTermsSubWindow(CommonRegistrationApp commonRegistrationApp) {
        return generateUserTermsSubWindow(commonRegistrationApp, individualUserTermsText);
    }

    public static void generateSystemNotificationMessage(Window mainWindow, String errorMessage, int typeErrorMessage, CommonRegistrationApp commonRegistrationApp) {
        String message = commonRegistrationApp.getMessage(errorMessage);
        mainWindow.showNotification(commonRegistrationApp.getMessage
                ("registration.user.system.error"),
                "<br/><i>" + message + "</i>",
                typeErrorMessage);
    }

    public static Field createComboBox(String caption, boolean isMandatory, String requiredError,
                                       List<String> itemValueList, CommonRegistrationApp commonRegistrationApp) {
        ComboBox items = new ComboBox(caption);
        items.setWidth("200px");
        items.setRequired(isMandatory);
        items.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        items.setInputPrompt(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        items.setImmediate(true);
        for (String s : itemValueList) {
            items.addItem(s);
        }
        return items;
    }


    public static ComboBox comboBox(String captionId, boolean isMandatory, String requiredErrorId,
                                       List<String> itemValueList, CommonRegistrationApp commonRegistrationApp) {
        ComboBox items = new ComboBox(commonRegistrationApp.getMessage(captionId), itemValueList);
        items.setRequired(isMandatory);
        items.setRequiredError(commonRegistrationApp.getMessage(requiredErrorId));
        items.setInputPrompt(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        items.setNullSelectionAllowed(false);
        items.setImmediate(true);

        if(isMandatory && itemValueList.size() == 1) {
            items.setValue(itemValueList.get(0));
        }

        return items;
    }

    public static Field createItemAllowedComboBox(String caption, boolean isMandatory, String requiredError,
                                                  List<String> itemValueList, CommonRegistrationApp commonRegistrationApp) {
        ComboBox items = new ComboBox(caption);
        items.setWidth("200px");
        items.setRequired(isMandatory);
        items.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        items.setInputPrompt(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        items.setImmediate(true);
        items.setNewItemsAllowed(true);
        for (String s : itemValueList) {
            items.addItem(s);
        }
        return items;
    }

    public static void filterNullValues(Field field) {
        if (field instanceof TextField) {
            ((TextField) field).setNullRepresentation("");
        }
    }

    public static Field createDateField(Field field, CommonRegistrationApp commonRegistrationApp) {
        field.addValidator(new DateValidator(commonRegistrationApp.getMessage("registration.user.invalid.birthday")));
        DateField dateField = (DateField) field;
        dateField.setDateFormat(COMMON_DATE_FORMAT);
        dateField.setValue(new Date());
        dateField.setRequired(true);
        dateField.setInvalidAllowed(true);
        dateField.setRequiredError(commonRegistrationApp.getMessage("registration.user.birthday.required"));
        return dateField;
    }

    public static Field createTextArea(String caption, boolean isMandatory, String requiredError, CommonRegistrationApp commonRegistrationApp) {
        TextArea textArea = new TextArea(caption);
        textArea.setWidth("200px");
        textArea.setRequired(isMandatory);
        textArea.setNullRepresentation("");
        textArea.setRows(2);
        textArea.setColumns(37);
        if (isMandatory) {
            textArea.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        return textArea;
    }

    public static TextArea textArea(String captionId, boolean isMandatory, String requiredError, CommonRegistrationApp commonRegistrationApp) {
        TextArea textArea = new TextArea(commonRegistrationApp.getMessage(captionId));
        textArea.setRequired(isMandatory);
        textArea.setNullRepresentation("");
        if (isMandatory) {
            textArea.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        return textArea;
    }

    public static Field createPasswordField(String caption, String requiredError, CommonRegistrationApp commonRegistrationApp) {
        TextField passwordField = new TextField(caption);
        passwordField.setWidth("200px");
        passwordField.setRequired(true);
        passwordField.setSecret(true);
        filterNullValues(passwordField);
        passwordField.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        return passwordField;
    }

    public static Field createTextField(String caption, boolean isMandatory,
                                        String requiredError, CommonRegistrationApp commonRegistrationApp) {
        TextField textField = new TextField(caption);
        textField.setRequired(isMandatory);
        textField.setWidth("200px");
        filterNullValues(textField);
        if (isMandatory) {
            textField.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        return textField;
    }

    public static TextField textField(String captionId, boolean isMandatory,
                                        String requiredError, CommonRegistrationApp commonRegistrationApp) {
        TextField textField = new TextField(commonRegistrationApp.getMessage(captionId));
        textField.setRequired(isMandatory);
        textField.setImmediate(true);
        filterNullValues(textField);
        if (isMandatory) {
            textField.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        return textField;
    }


    public static PasswordField passwordField(String captionId, boolean isMandatory,
                                      String requiredError, CommonRegistrationApp commonRegistrationApp) {
        PasswordField textField = new PasswordField(commonRegistrationApp.getMessage(captionId));
        textField.setRequired(isMandatory);
        textField.setImmediate(true);
        filterNullValues(textField);
        if (isMandatory) {
            textField.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        return textField;
    }

    public static Field createTextFieldWithPrompt(String caption, boolean isMandatory,
                                                  String requiredError, CommonRegistrationApp commonRegistrationApp) {
        TextField textField = new TextField();
        textField.setInputPrompt(caption);
        textField.setWidth("200px");
        textField.setRequired(isMandatory);
        filterNullValues(textField);
        if (isMandatory) {
            textField.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        return textField;
    }

    public static Field createMultiSelectionList(String caption, boolean isMandatory,
                                                 String requiredError, List<String> itemValueList, CommonRegistrationApp commonRegistrationApp) {
        return createSelection(caption, isMandatory, requiredError, itemValueList, true, commonRegistrationApp);
    }

    public static Field createSelection(String caption, boolean isMandatory, String requiredError,
                                        List<String> itemValueList, boolean multipleSelects, CommonRegistrationApp commonRegistrationApp) {
        ListSelect list = new ListSelect(caption);
        list.setWidth("200px");
        list.setRequired(isMandatory);
        if (isMandatory) {
            list.setRequiredError(commonRegistrationApp.getMessage(requiredError));
        }
        if (itemValueList != null) {
            for (String item : itemValueList) {
                list.addItem(item);
            }
        }
        list.setRows(itemValueList.size());
        list.setNullSelectionAllowed(true);
        list.setMultiSelect(multipleSelects);
        list.setImmediate(true);
        return list;
    }

    public static Button createImageLink(String caption, ThemeResource icon, String toolTipText, CommonRegistrationApp commonRegistrationApp) {
        Button linkButton = new Button();
        if (caption != null) {
            linkButton.setCaption(commonRegistrationApp.getMessage(caption));
        }
        if (toolTipText != null) {
            linkButton.setDescription(toolTipText);
        }
        linkButton.setIcon(icon);
        linkButton.setStyleName("link");
        return linkButton;
    }

    public static Button createButton(String caption, CommonRegistrationApp commonRegistrationApp) {
        Button button = new Button(commonRegistrationApp.getMessage(caption));
        button.setStyleName("action-button");
        return button;
    }

    public static Label generateSuccessMessage(String successMessage, CommonRegistrationApp commonRegistrationApp) {
        Label label = new Label(commonRegistrationApp.getMessage(successMessage));
        label.setStyleName("success-label");
        return label;
    }

    public static Label generateInfoMessage(String successMessage, CommonRegistrationApp commonRegistrationApp) {
        Label label = new Label(commonRegistrationApp.getMessage(successMessage));
        label.setStyleName("info");
        return label;
    }

    public static Label generateHeading(String heading, CommonRegistrationApp commonRegistrationApp) {
        Label headingLabel = new Label(commonRegistrationApp.getMessage(heading));
        headingLabel.setStyleName("header-with-border");
        return headingLabel;
    }

    /**
     * @param successMessage the complete message to be displayed to the user
     * @param linkFragment   the string which need to be a hyperlink
     * @param linkUrl        url of the hyperlink
     * @return
     */
    public static CustomLayout generateSuccessLayoutWithLink(String successMessage, String linkFragment, String linkUrl) {
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(new ByteArrayInputStream(successMessage.getBytes()));
            customLayout.setStyleName("success");
        } catch (IOException e) {
            logger.error("Error occurred while generating success layout with link ", e);
        }
        return customLayout;
    }

    public static CustomLayout generateSuccessLayoutWithLink(String successMessage, String linkFragment, String linkUrl, Button loginButton) {
        CustomLayout customLayout = null;
        try {
            successMessage = successMessage.replace(linkFragment, "<a href='" + linkUrl + "'>" + linkFragment + "</a>");
            customLayout = new CustomLayout(new ByteArrayInputStream(successMessage.getBytes()));
            customLayout.setStyleName("success");
            customLayout.addComponent(loginButton);
            Iterator<Component> componentIterator = customLayout.getComponentIterator();
            while (componentIterator.hasNext()) {
                Component next = componentIterator.next();
                System.out.println(next.getClass());
            }
            customLayout.addListener(new Component.Listener() {
                @Override
                public void componentEvent(Component.Event event) {
                    if (event instanceof MouseEvents.ClickEvent) {
                        System.out.println("clicked");
                    }
                }
            });
        } catch (IOException e) {
            logger.error("Error occurred while generating success layout with link ", e);
        }
        return customLayout;
    }

    private static void doIt() {

    }

    public static void notification(Window window, String message, int messageType, CommonRegistrationApp commonRegistrationApp) {
        Window.Notification notification = new Window.Notification(commonRegistrationApp.getMessage(message), messageType);
        if (messageType != TRAY) {
            notification.setPosition(Window.Notification.POSITION_CENTERED);
        }
        window.showNotification(notification);
    }
}