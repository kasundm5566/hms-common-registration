/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.admin.add;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.admin.search.ManageModuleGroupView;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.*;
import hms.common.registration.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.createTextField;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateSystemNotificationMessage;
import static hms.common.registration.util.PropertyHolder.GROUP_DESCRIPTION_LENGTH_VALIDATION;
import static hms.common.registration.util.PropertyHolder.GROUP_NAME_LENGTH_VALIDATION;
import static hms.common.registration.service.ModuleGroupService.GROUP_DESC;
import static hms.common.registration.service.ModuleGroupService.GROUP_NAME;

/**
 * Handle the Add new module group form implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AddNewModuleGroupView extends VerticalLayout implements Property.ValueChangeListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddNewModuleGroupView.class);
    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.admin.group.";
    public static final String GROUP_USERS_ID = "users";
    public static final String GROUP_ROLES_ID = "roles";

    protected CommonRegistrationApp commonRegistrationApp;
    private ModuleService moduleService;
    private UserGroupService userGroupService;

    private TextField groupNameText;
    private TextArea groupDescriptionText;
    private TwinColSelect roleSelector;

    private Set<String> selectedRoles = new HashSet<String>();
    private Set<Object> selectedUsers = new HashSet<Object>();

    private Form addModuleGroupForm;
    private boolean editState;

    private HashSet<String> previouslySelectedUserSet = new HashSet<String>();
    private UserGroup userGroup;
    private Set<Role> selectedRolesAsList;

    public AddNewModuleGroupView(CommonRegistrationApp commonRegistrationApp, UserGroup userGroup, boolean editState) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.userGroup = userGroup;
        this.editState = editState;
        moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        try {
            generateAddModuleGroupView(userGroup);
        } catch (Throwable e) {
            LOGGER.error("Error occured while adding new Module Group ", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.group.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }

    }

    private void generateAddModuleGroupView(final UserGroup userGroup) throws DataManipulationException {

        addModuleGroupForm = new Form();
        addModuleGroupForm.getLayout().setWidth("600px");
        // BeanItem<ModuleGroup> beanItem = new BeanItem<ModuleGroup>(moduleGroup);

        BeanItem<UserGroup> beanItem = new BeanItem<UserGroup>(userGroup);
        addModuleGroupForm.setImmediate(true);

        groupNameText = (TextField) createTextField(commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + "name"), true,
                "registration.admin.group.name.required", commonRegistrationApp);
        groupNameText.setWidth("235px");
        groupNameText.addValidator(new RegexpValidator(GROUP_NAME_LENGTH_VALIDATION,
                commonRegistrationApp.getMessage("registration.admin.group.name.length.validation")));
        groupNameText.setValue(userGroup.getName());



        groupDescriptionText= (TextArea) CommonUiComponentGenerator.createTextArea(commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + "description"), true, "registration.admin.group.description.required", commonRegistrationApp);
        groupDescriptionText.setColumns(40);
        groupDescriptionText.addValidator(new RegexpValidator(GROUP_DESCRIPTION_LENGTH_VALIDATION,
                commonRegistrationApp.getMessage("registration.admin.group.description.length.validation")));
        groupDescriptionText.setValue(userGroup.getDescription());


        createRoleSelector("captionValue", userGroup,this);


//        ((TextArea) textArea).setColumns(40);
//            textArea.addValidator(new RegexpValidator(GROUP_DESCRIPTION_LENGTH_VALIDATION,
//                    getMessage("registration.admin.group.description.length.validation")));



        //beanItem.addItemProperty(GROUP_MODULE_ID, new ObjectProperty(null, String.class));
//        beanItem.addItemProperty(GROUP_ROLES_ID, new ObjectProperty(getSelectedRoleSet(userGroup), Set.class));
//        getSelectedRoleSet(userGroup);
//        beanItem.addItemProperty(GROUP_USERS_ID, new ObjectProperty(getSelectedUsersSet(userGroup), Set.class));
//        addModuleGroupForm.setItemDataSource(beanItem);
        addModuleGroupForm.setWriteThrough(false);

//        addModuleGroupForm.setFormFieldFactory(new ModuleGroupFieldFactory(this));
//        addModuleGroupForm.setVisibleItemProperties(new String[]{
//                GROUP_NAME, GROUP_DESC, GROUP_ROLES_ID});
        if (editState) {
            groupNameText.setReadOnly(true);
        }
        //setAddNewModuleGroupView(this);
        addModuleGroupForm.addField("groupName", groupNameText);
        addModuleGroupForm.addField("description", groupDescriptionText);
        addModuleGroupForm.addField("caption", roleSelector);
        createFormFooter(addModuleGroupForm);
        addComponent(addModuleGroupForm);

    }

    private Set<String> getSelectedUsersSet(UserGroup userGroup) {
        previouslySelectedUserSet = new HashSet<String>();
        if (editState) {
            Set<User> users = userGroupService.findUserGroupWithAllUsers(userGroup.getId()).getUsers();
            for (User user : users) {
                LOGGER.debug("User assigned to group : [{}]", user.getUsername());
                previouslySelectedUserSet.add(user.getUsername());
            }
        }
        return previouslySelectedUserSet;
    }

//    private Set<String> getSelectedRoleSet(UserGroup userGroup) throws DataManipulationException {
//        previouslySelectedRoleSet = new HashSet<String>();
//        if (editState) {
//            LOGGER.debug("**********************************************************************************");
//            for (Role role : userGroup.getRoles()) {
//                LOGGER.debug("Role assigned to group : [{}] is [{}]", userGroup.getName(), role.getName());
//                previouslySelectedRoleSet.add(role.getName());
//            }
//        }
//        return previouslySelectedRoleSet;
//    }

//    private void setAddNewModuleGroupView(AddNewModuleGroupView view){
//
//        addNewModuleGroupView=view;
//    }


    private void createFormFooter(final Form addGroupForm) {
        addGroupForm.setFooter(new VerticalLayout());
        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setWidth("600px");
        footerLayout.setHeight("50px");
        final HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setWidth("200px");
        final Button cancelButton = new com.vaadin.ui.Button(commonRegistrationApp.getMessage("registration.button.cancel"),
                new com.vaadin.ui.Button.ClickListener() {
                    public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
                        LOGGER.debug("Discarding the input values..........");

                        if(!editState){
                        addGroupForm.discard();
                        changeView("registration.admin.groups.heading",
                                new ManageModuleGroupView(commonRegistrationApp, null),
                                "registration.admin.groups.manage.heading");
                        }
                        else{
                           commonRegistrationApp.getMainWindow().removeWindow(addGroupForm.getWindow());
                        }
                    }
                });

        final Button saveButton = new com.vaadin.ui.Button(commonRegistrationApp.getMessage("registration.button.submit"));
        saveButton.addListener(new com.vaadin.ui.Button.ClickListener() {
            public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
                LOGGER.debug("Creating New  User Group [{}]", userGroup);
                try {
                    addGroupForm.commit();
                    addGroupForm.validate();
                    addGroupForm.setReadOnly(true);
                    try {
                        saveModuleGroupDetails();
//                        addGroupForm.setValidationVisible(false);
//                        buttonLayout.removeComponent(saveButton);
//                        buttonLayout.removeComponent(cancelButton);
                        removeAllComponents();
                        setMargin(true);
                        if (editState) {
                            addComponent(CommonUiComponentGenerator.generateSuccessMessage("registration.user.group.edit.success", commonRegistrationApp));
                        } else {
                            addComponent(CommonUiComponentGenerator.generateSuccessMessage("registration.user.group.creation.success", commonRegistrationApp));
                        }
                    } catch (DataManipulationException e) {
                        LOGGER.error("Data access error", e);
                    }
                } catch (Validator.InvalidValueException e) {
                    LOGGER.debug("Invalid Form Entries {} ", e.getMessage());
                }
            }
        });
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        buttonLayout.addComponent(cancelButton);
        buttonLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_LEFT);
        buttonLayout.addComponent(saveButton);
        buttonLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
        footerLayout.addComponent(buttonLayout);
        footerLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        addGroupForm.setFooter(footerLayout);
    }

    @Override
    public void valueChange(Property.ValueChangeEvent event) {

        LOGGER.debug("*************************************value changed *************************************************");
        if (event.getProperty().getClass().equals(TwinColSelect.class)) {
            // selectedRoles = (Set<Object>) event.getProperty().getValue();

            LOGGER.debug("*********************selected (Set<Role>) event.getProperty().getValue();*************" + (Set<String>) event.getProperty().getValue());

            selectedRoles = (Set<String>) event.getProperty().getValue();


//            if (initiallySelectedRoleNames == null) {
//
//                LOGGER.debug("number of initial loaded roles *************[{}]", previouslySelectedRoleSet.size());
//
//                initiallySelectedRoleNames = new HashSet<String>();
//
//                Iterator itr = previouslySelectedRoleSet.iterator();
//                while (itr.hasNext()) {
//
//                    // initiallySelectedRoles.add((Role)itr.next());
//                    String roleName = itr.next().toString();
//                    LOGGER.debug("-----------------------------------");
//                    LOGGER.debug("  role name[{}] ", roleName);
//                    LOGGER.debug("-----------------------------------");
//
////                        Role role=moduleService.findRoleByName(roleName);
////
////                        LOGGER.debug("role retrieved [{}]",role);
//                    initiallySelectedRoleNames.add(roleName);
//
//                }
//
//                LOGGER.debug("number of initially selected role names [{}]", initiallySelectedRoleNames.size());
//
//
//            }//initiallySelectedRoleNames

            LOGGER.debug("Edit state " + editState);

            //start

            if (selectedRoles != null && !editState) {

                LOGGER.debug(" selected Roles is " + selectedRoles);

                Iterator itr = selectedRoles.iterator();

                while (itr.hasNext()) {
                    Object element = itr.next();
                    LOGGER.debug("element is " + element);

//                    LOGGER.debug("!selectedRolesAsList.contains(element) " + (!selectedRolesAsList.contains(element)));

//                    if (!selectedRolesAsList.contains(element)) {
//                        //selectedRolesAsList.add(moduleService.findRoleByName(element.toString()));
//                        selectedRolesAsSet.add(moduleService.findRoleByName(element.toString()));
//                    }

                }

//                selectedRolesAsList = new HashSet<Role>(selectedRolesAsSet);
//
//                LOGGER.debug("number of roles set " + selectedRolesAsSet.size());

                // LOGGER.debug("number of roles list " + selectedRolesAsList.size());
            } else if (selectedRoles != null && editState) {

//                selectedRolesAsList=new ArrayList<Role>();
//                Iterator itr = selectedRoles.iterator();
//                while(itr.hasNext()) {
//                    LOGGER.debug(" is Role ? [{}]**************** ", (itr.next()) instanceof Role);
//                    Role element =(Role) itr.next();
//                    selectedRolesAsList.add(element);
//                    LOGGER.debug(" Selected Role in Edit state  [{}]",element);
//                    LOGGER.debug(" is Role ? [{}] ",element instanceof Role);
//                    LOGGER.debug(" Role Name [{}] ",element.getName());
//                }

                LOGGER.debug(" selected Roles is (edit value changed)**************** [{}]", selectedRoles);
                LOGGER.debug("---------------------------------------------------------------------------");
//                LOGGER.debug(" previously selected roles set [{}]", previouslySelectedRoleSet);
            }


            //end

        } else if (event.getProperty().getClass().equals(ListSelect.class)) {
            selectedUsers = (Set<Object>) event.getProperty().getValue();
            //   selectedUsers = new HashSet<Object>();
            LOGGER.debug("Selected User : " + selectedUsers);
        }
    }

    /**
     * Save Module group details
     *
     * @throws DataManipulationException
     */
    private void saveModuleGroupDetails() throws DataManipulationException {

        LOGGER.debug("Select Roles [{}]", selectedRoles);
        LOGGER.debug("Select Users [{}]", selectedUsers);
        LOGGER.debug(" Edit state is [{}] ", editState);

        // LOGGER.debug("*********************************group name is ********************"+groupName);

        String groupName=groupNameText.getValue().toString();
        String groupDescription=groupDescriptionText.getValue().toString();

        LOGGER.debug(" group name [{}] ",groupName);
        LOGGER.debug(" group description [{}]",groupDescription);

        userGroup.setName(groupName);
        userGroup.setDescription(groupDescription);


        if (editState) {

            LOGGER.debug("-----------all selected roles-------------------");
            LOGGER.debug("number of selected roles set  [{}]", selectedRoles.size());



            List<Role> userSelectedRoles = new ArrayList<Role>();
            List<String> selectedRoleNames = new ArrayList<String>();

            Object roleObject = null;

            Iterator itr = selectedRoles.iterator();
            Role role;
            while (itr.hasNext()) {

                try {

                    roleObject = itr.next();
                    role = (Role) roleObject;

                } catch (Exception ex) {


                    if (roleObject != null) {
                        selectedRoleNames.add(roleObject.toString());
                    }
                    // userSelectedRoles.add(moduleService.findRoleByName(roleName));
                }

            }

            LOGGER.debug(" number of newly selected roles is [{}]", userSelectedRoles.size());

            LOGGER.debug("selected role names are [{}]", selectedRoleNames);

            for (int i = 0; i < selectedRoleNames.size(); i++) {

                LOGGER.debug(" Role name [{}]", selectedRoleNames.get(i));

            }

            LOGGER.debug(" number of selected role names [{}]", selectedRoleNames.size());
//            LOGGER.debug(" number of initially and current selected role names   [{}]", initiallySelectedRoleNames.size());
//            LOGGER.debug(" initially and currently selected role names [{}]", initiallySelectedRoleNames);
//
//
//            Set<Role> listOfRolesForMerging = new HashSet<Role>();
//            for (String roleName : initiallySelectedRoleNames) {
//
//                listOfRolesForMerging.add(moduleService.findRoleByName(roleName));
//
//            }

//            LOGGER.debug(" number of roles for the merging [{}]", listOfRolesForMerging.size());
//
//            userGroup.setRoles(listOfRolesForMerging);

            Set<Role> roleSet = new HashSet<Role>();
            for (String selectedRole : selectedRoles) {
                roleSet.add(moduleService.findRoleByName(selectedRole));
            }

            LOGGER.debug("*************************selected roles ***********************[{}]",roleSet);

            if(selectedRoles.size()!=0){
            userGroup.setRoles(roleSet);
            }

            userGroupService.merge(userGroup);

            LOGGER.debug("Edited New Module Group [{}]");
        } else {
            /*
           Role role1=new Role();
           role1.setName("Role1");
           role1.setDescription("Sample Role1 Description");
           role1.setId(192L);
           List<Role> testRoles=new ArrayList<Role>();
           testRoles.add(role1);
           userGroup.setRoles(testRoles);
            */
//            for (Object selectedRole : selectedRoles) {
//                LOGGER.debug("Type " + selectedRole.getClass());
//                LOGGER.debug("Value " + selectedRole);
//            }
            LOGGER.debug("user group {}", userGroup);

            Set<Role> roleSet = new HashSet<Role>();
            for (String selectedRole : selectedRoles) {
                roleSet.add(moduleService.findRoleByName(selectedRole));
            }

            LOGGER.debug("Selected roles {}", roleSet);

            userGroup.setRoles(roleSet);
            userGroupService.persist(userGroup);
            LOGGER.debug("Created Module Group [{}]",userGroup);
        }

//        for (Object selectedRole : selectedRoles) {
//            if (!previouslySelectedRoleSet.contains(selectedRole)) {
//                Role role = roleService.findRoleByName((String) selectedRole);
//                GroupRole groupRole = new GroupRole(moduleGroup, role);
//                // role.getGroupRoleList().add(groupRole);
//                groupRoleService.persist(groupRole);
//                //groupRoleSet.add(groupRole);
//            }
//        }
//        for (Object previousRole : previouslySelectedRoleSet) {
//            if (!selectedRoles.contains(previousRole)) {
//                GroupRole groupRole = groupRoleService.findGroupRoleHavingModuleGroupAndName(moduleGroup, (String) previousRole);
//                groupRoleService.remove(groupRole);
//                //groupRoleSet.remove(groupRole);
//            }
//        }
        // moduleGroup.setGroupRoleList(groupRoleSet);

//        Set<UserGroup> userSet = moduleGroup.getUserGroupList();
//        if (userSet == null) {
//            userSet = new HashSet<UserGroup>();
//        }
//        for (Object selectedUser : selectedUsers) {
//            if (!previouslySelectedUserSet.contains(selectedUser)) {
//                User user = userService.findUserByNamAndPassword((String) selectedUser);
//                UserGroup userGroup = new UserGroup(moduleGroup, user);
//                user.getUserGroupList().add(userGroup);
//                userGroupService.persist(userGroup);
//                userSet.add(userGroup);
//            }
//        }
//        for (Object previousUser : previouslySelectedUserSet) {
//            if (!selectedUsers.contains(previousUser)) {
//                UserGroup userGroup = userGroupService.findUserGroupHavingUserAndGroup((String) previousUser, moduleGroup);
//                userGroupService.remove(userGroup);
        // userSet.remove(userGroup);
//            }
//        }
        // moduleGroup.setUserGroupList(userSet);
//        moduleGroupService.merge(moduleGroup);


    }

    private void createRoleSelector(String caption, UserGroup userGroup, AddNewModuleGroupView moduleGroupView) {

        BeanItemContainer<String> container = new BeanItemContainer<String>(String.class);
        try {
            List<String> roleList = moduleService.findAllRoleNames();

            container.addAll(roleList);
            Set<String> roles = new HashSet<String>();
            for (String s : roleList) {
                roles.add(s);
            }
            roleSelector = new TwinColSelect(caption, container);
            LOGGER.debug("ADDING ROLES TO TWIN SELECTOR " + userGroup.getRoles());
            roleSelector.setValue(userGroup.getRolesAsStrings());
            roleSelector.setLeftColumnCaption(commonRegistrationApp.getMessage("registration.admin.group.roles.available.roles"));
            roleSelector.setRightColumnCaption(commonRegistrationApp.getMessage("registration.admin.group.roles.selected.roles"));
            roleSelector.setWidth("520px");
            roleSelector.setMultiSelect(true);
            roleSelector.setImmediate(true);
            roleSelector.addListener(moduleGroupView);
            roleSelector.setRequiredError(commonRegistrationApp.getMessage("registration.admin.group.roles.required"));
        } catch (DataManipulationException e) {
            LOGGER.error("Error while finding all available roles", e);
        }
    }

    private void changeView(String panelHeading,
                            Component component, String subPanelHeading) {
        commonRegistrationApp.getContentLayout().getContentPanel().addComponent(component);
        commonRegistrationApp.getInnerContentPanel().removeAllComponents();
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        commonRegistrationApp.getInnerContentPanel().addComponent(component);
    }

}


class ModuleGroupFieldFactory implements FormFieldFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldFactory.class);
    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.admin.group.";

    private AddNewModuleGroupView addNewModuleGroupView;
    private RoleService roleService;
    private ModuleService moduleService;
    private UserService userService;
    private TwinColSelect roleSelector;
    private ModuleGroup moduleGroup;
    private boolean editState;
    private GroupRoleService groupRoleService;
    protected CommonRegistrationApp commonRegistrationApp;

    ModuleGroupFieldFactory(AddNewModuleGroupView addNewModuleGroupView) {
        this.addNewModuleGroupView = addNewModuleGroupView;
        this.commonRegistrationApp = addNewModuleGroupView.commonRegistrationApp;
        roleService = (RoleService) commonRegistrationApp.getBean("roleServiceImpl");
        moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        groupRoleService = (GroupRoleService) commonRegistrationApp.getBean("groupRoleServiceImpl");
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;
        if (GROUP_NAME.equals(pid)) {
            Field textField = createTextField(caption, true, "registration.admin.group.name.required", commonRegistrationApp);
            ((TextField) textField).setWidth("235px");
            textField.addValidator(new RegexpValidator(GROUP_NAME_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.admin.group.name.length.validation")));
            return textField;
        } else if (GROUP_DESC.equals(pid)) {
            Field textArea = (TextArea) CommonUiComponentGenerator.createTextArea(caption, true, "registration.admin.group.description.required", commonRegistrationApp);
            ((TextArea) textArea).setColumns(40);
            textArea.addValidator(new RegexpValidator(GROUP_DESCRIPTION_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.admin.group.description.length.validation")));
            return textArea;
        } else if (AddNewModuleGroupView.GROUP_ROLES_ID.equals(pid)) {
            //createRoleSelector(caption);
            return roleSelector;
//        } else if(AddNewModuleGroupView.GROUP_MODULE_ID.equals(pid)) {
//            try {
//                Field comboBox = CommonUiComponentGenerator.createComboBox(caption,
//                        true, "registration.admin.group.module.required", moduleService.findAllModuleNames());
//                comboBox.addListener(addNewModuleGroupView);
//                return comboBox;
//            } catch (DataManipulationException e) {
//                LOGGER.error("Error while finding all available modules", e);
//                return null;
//            }
        } else if (AddNewModuleGroupView.GROUP_USERS_ID.equals(pid)) {
            try {
                Field multiSelectionList = CommonUiComponentGenerator.createMultiSelectionList(caption, false, "registration.admin.group.users.required",
                        userService.getAllUsernamesByType(UserType.ADMIN_USER), commonRegistrationApp);
                multiSelectionList.addListener(addNewModuleGroupView);
                multiSelectionList.setWidth("235px");
                return multiSelectionList;
            } catch (DataManipulationException e) {
                LOGGER.error("Error while loading usernames");
                return null;
            }
        } else {
//            LOGGER.error("grp users id no");
//            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
//            field.setCaption(caption);
//            CommonUiComponentGenerator.filterNullValues(field);
//            return field;
            return null;
        }
    }

    /**
     * Create the TwinCoSelector containing Role list
     *
     * @param caption
     */
    @Deprecated
    private void createRoleSelector(String caption) {

        BeanItemContainer<String> container = new BeanItemContainer<String>(String.class);
        try {
            List<String> roleList = roleService.findAllRoleNames();
            container.addAll(roleList);
            Set<String> roles = new HashSet<String>();
            for (String s : roleList) {
                roles.add(s);
            }
            roleSelector = new TwinColSelect(caption, container);
            roleSelector.setValue(roles);
            roleSelector.setLeftColumnCaption(commonRegistrationApp.getMessage("registration.admin.group.roles.available.roles"));
            roleSelector.setRightColumnCaption(commonRegistrationApp.getMessage("registration.admin.group.roles.selected.roles"));
            roleSelector.setWidth("520px");
            roleSelector.setMultiSelect(true);
            roleSelector.addListener(addNewModuleGroupView);
            roleSelector.setRequiredError(commonRegistrationApp.getMessage("registration.admin.group.roles.required"));
        } catch (DataManipulationException e) {
            LOGGER.error("Error while finding all available roles", e);
        }
    }
}