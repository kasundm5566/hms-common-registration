/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.validator;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

import static hms.common.registration.util.PropertyHolder.COMMON_DATE_FORMAT;

/**
 * A Validator to validate date durations
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class DateValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(DateValidator.class);    
    private String errorMessage; 
    
    public DateValidator(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public void validate(Object o) throws InvalidValueException {
        if (!isValid(o)) {
            logger.debug("Invalid Birthday found [{}] ", o);
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object o) {
        Date currentDate = new Date();
        Date enteredDate = (Date) o;
        return enteredDate.before(currentDate) || enteredDate.equals(currentDate);
    }
}