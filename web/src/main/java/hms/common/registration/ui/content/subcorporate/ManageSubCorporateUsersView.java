/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.subcorporate;

import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.admin.search.ManageUserView;
import hms.common.registration.ui.content.admin.search.SearchUserForm;
import hms.common.registration.common.Condition;
import hms.common.registration.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static hms.common.registration.model.UserType.SUB_CORPORATE;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ManageSubCorporateUsersView extends ManageUserView {

    private static final Logger logger = LoggerFactory.getLogger(ManageSubCorporateUsersView.class);

    public ManageSubCorporateUsersView(CommonRegistrationApp commonRegistrationApp, User loggedInUser, List<Condition> searchParametersMap) {
        super(commonRegistrationApp, getInitialConditionList(searchParametersMap, loggedInUser), null);
    }

    /**
     * Set user type as a parameter in parameters map
     *
     * @return
     */
    private static List<Condition> getInitialConditionList(List<Condition> conditionList, User loggedInUser) {
        logger.debug("Logged in Corporate user :" + loggedInUser.getUsername());
        conditionList.add(new Condition(SearchUserForm.SEARCH_PARAMETER_CORPORATE_PARENT_ID, loggedInUser, "="));
        conditionList.add(new Condition(SearchUserForm.SEARCH_PARAMETER_USER_TYPE, SUB_CORPORATE, "="));
        return conditionList;
    }
}
