/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin.search;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.admin.add.AddNewModuleGroupView;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Role;
import hms.common.registration.model.UserGroup;
import hms.common.registration.service.UserGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ManageModuleGroupView extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageModuleGroupView.class);
    private CommonRegistrationApp commonRegistrationApp;
    private UserGroupService userGroupService;
    private Role role;
    private boolean searchByRole;

    private VerticalLayout tableLayout;
    private Table moduleGroupTable;
    private IndexedContainer container;

    private String nameColumn;
    private String descColumn;
    private String actionColumn;

    public ManageModuleGroupView(CommonRegistrationApp commonRegistrationApp, Role role) {
        this.commonRegistrationApp = commonRegistrationApp;
        if (role != null) {
            this.role = role;
            searchByRole = true;
        }
        nameColumn = commonRegistrationApp.getMessage("registration.admin.group.name");
        descColumn = commonRegistrationApp.getMessage("registration.admin.group.description");
        actionColumn = commonRegistrationApp.getMessage("registration.action.heading");
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        tableLayout = new VerticalLayout();
        moduleGroupTable = new Table();
        createIndexedContainer();
        generateManageModuleGroupView();
    }

    private void generateManageModuleGroupView() {

        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setWidth("400px");
        topLayout.setSpacing(true);
        topLayout.addComponent(createSearchComponent());
        addComponent(topLayout);

        moduleGroupTable.setSizeFull();
        moduleGroupTable.setSelectable(true);
        moduleGroupTable.setContainerDataSource(container);
        moduleGroupTable.setImmediate(true);
        tableLayout.addComponent(moduleGroupTable);
        addComponent(tableLayout);
        setMargin(true);
        setSpacing(true);
    }

    private void createIndexedContainer() {

        container = new IndexedContainer();
        container.addContainerProperty(nameColumn, String.class, "");
        container.addContainerProperty(descColumn, String.class, "");

        List<UserGroup> moduleGroupList;
        if (searchByRole) {
            moduleGroupList = userGroupService.findUserGroupByRole(role);
        } else {
            container.addContainerProperty(actionColumn, HorizontalLayout.class, "");
            moduleGroupList = userGroupService.findAllUserGroups();
        }
        LOGGER.debug("Available ModuleGroupList [{}]", moduleGroupList);
        loadToTable(moduleGroupList);
    }

    private void loadToTable(List<UserGroup> userGroupList) {
        container.removeAllItems();
        LOGGER.debug("Available User Group List [{}]", userGroupList);
        for (final UserGroup userGroup : userGroupList) {
            LOGGER.debug("Add User Group [{}]", userGroup);
            final Item item = container.addItem(userGroup);
            if (item == null) {
                LOGGER.debug("Item already exists for userGroup [{}]", userGroup);
            }
            if (!searchByRole) {
                final Button viewUsersLink = createImageLink(null, VIEW_ICON, commonRegistrationApp.getMessage("registration.admin.group.view.users"), commonRegistrationApp);
                final Button editLink = createImageLink(null, EDIT_ICON, commonRegistrationApp.getMessage("registration.button.edit"), commonRegistrationApp);
                final Button removeLink = createImageLink(null, REMOVE_ICON, commonRegistrationApp.getMessage("registration.button.delete"), commonRegistrationApp);

                viewUsersLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window viewUsersWindow = generateViewUsersSubWindow(userGroup, "registration.admin.role.view.groups");
                        getWindow().addWindow(viewUsersWindow);
                    }
                });
                editLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        UserGroup refreshedGroup = userGroupService.findUserGroupByGroupName(userGroup.getName());
                        Window addModuleWindow = generateModuleGroupManageSubWindow(true, refreshedGroup, "registration.admin.group.edit.heading");
                        getWindow().addWindow(addModuleWindow);
                    }
                });
                removeLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window removeModuleWindow = createRemoveConfirmationBox(userGroup);
                        getWindow().addWindow(removeModuleWindow);
                    }
                });
                final HorizontalLayout horizontalLayout = new HorizontalLayout();
                horizontalLayout.addComponent(viewUsersLink);
                horizontalLayout.addComponent(editLink);
                horizontalLayout.addComponent(removeLink);
                item.getItemProperty(actionColumn).setValue(horizontalLayout);
            }
            item.getItemProperty(nameColumn).setValue(userGroup.getName());
            item.getItemProperty(descColumn).setValue(userGroup.getDescription());
        }
    }

    /**
     * Display all users belong to the module group
     *
     * @param caption
     * @return
     */
    private Window generateViewUsersSubWindow(UserGroup userGroup, String caption) {
        final Window viewUsersWindow = new Window(commonRegistrationApp.getMessage(caption));
        viewUsersWindow.setStyleName("opaque");
        viewUsersWindow.setWidth("750px");
        viewUsersWindow.center();
        viewUsersWindow.setImmediate(true);
        viewUsersWindow.addComponent(new ManageUserView(commonRegistrationApp,
                new ArrayList<Condition>(), userGroup));

        viewUsersWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(viewUsersWindow);
            }
        });
        return viewUsersWindow;
    }

    /**
     * Create the Module Group edit subwindow
     *
     * @param editState
     * @param userGroup
     * @param caption
     * @return
     */
    private Window generateModuleGroupManageSubWindow(boolean editState, UserGroup userGroup, String caption) {
        final Window addModuleGroupWindow = new Window(commonRegistrationApp.getMessage(caption));
        addModuleGroupWindow.setStyleName("opaque");
        addModuleGroupWindow.setWidth("750px");
        addModuleGroupWindow.center();
        addModuleGroupWindow.setImmediate(true);
        addModuleGroupWindow.addComponent(new AddNewModuleGroupView(commonRegistrationApp, userGroup, editState));
        addModuleGroupWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(addModuleGroupWindow);
            }
        });
        return addModuleGroupWindow;
    }

    /**
     * Create the Module Group remove confirmation box
     *
     * @return
     */
    private Window createRemoveConfirmationBox(final UserGroup userGroup) {

        final Window confirmation = new Window();
        confirmation.setCaption(commonRegistrationApp.getMessage("registration.confirmation.caption"));
        confirmation.setStyleName("opaque");
        confirmation.setWidth("425px");
        confirmation.setHeight("150px");
        confirmation.center();

        Button yesBtn = new Button(commonRegistrationApp.getMessage("registration.button.yes"));
        yesBtn.setStyleName("action-button");
        yesBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
                try {
                    userGroupService.remove(userGroup);
                    container.removeItem(userGroup);
                    getWindow().showNotification(new Window.Notification("", commonRegistrationApp.getMessage("registration.user.group.delete.success")));
                } catch (DataManipulationException e) {
                    LOGGER.error("Data Access Error", e);
                }
            }
        });

        Button noBtn = new Button(commonRegistrationApp.getMessage("registration.button.no"));
        noBtn.setStyleName("action-button");
        noBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
            }
        });
        Label msg = new Label(MessageFormat.format(commonRegistrationApp.getMessage("registration.admin.group.remove.confirmation"),
                userGroup.getName()));

        GridLayout conBoxLayout = new GridLayout(2, 2);
        conBoxLayout.setSpacing(true);
        conBoxLayout.setSizeFull();
        conBoxLayout.addComponent(msg, 0, 0, 1, 0);
        conBoxLayout.addComponent(yesBtn);
        conBoxLayout.setComponentAlignment(yesBtn, Alignment.MIDDLE_CENTER);
        conBoxLayout.addComponent(noBtn);
        conBoxLayout.setComponentAlignment(noBtn, Alignment.MIDDLE_CENTER);

        confirmation.addComponent(conBoxLayout);
        return confirmation;
    }

    private HorizontalLayout createSearchComponent() {
        HorizontalLayout searchComponent = new HorizontalLayout();
        searchComponent.setSpacing(true);
        searchComponent.addComponent(new Label(commonRegistrationApp.getMessage("registration.admin.group.name")));

        final TextField groupNameText = new TextField();
        groupNameText.setInputPrompt(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        searchComponent.addComponent(groupNameText);

        final Button searchButton = createButton("registration.button.search", commonRegistrationApp);
        searchButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                String groupName = (String) groupNameText.getValue();
                List<UserGroup> searchedGroupList;
                if (searchByRole) {
                    searchedGroupList = userGroupService.findUserGroupByRoleHavingName(role, groupName);
                } else {
                    searchedGroupList = userGroupService.findUserGroupByName(groupName);
                }
                loadToTable(searchedGroupList);
            }
        });
        searchComponent.addComponent(searchButton);
        return searchComponent;
    }
}