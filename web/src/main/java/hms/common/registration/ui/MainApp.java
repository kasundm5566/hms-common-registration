/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.AbstractApplicationServlet;
import hms.common.registration.scheduler.EmailAddressChecker;
import hms.common.registration.scheduler.MsisdnChecker;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MainApp extends AbstractApplicationServlet {

    private ApplicationContext appContext;
    private ResourceBundle resourceBundle;
    private Locale locale;

    private static final Logger logger = LoggerFactory.getLogger(MainApp.class);

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        logger.info("Initializing the Main Web Application Context");
        super.init(servletConfig);
        appContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        locale = new Locale(servletConfig.getInitParameter("locale"));
        resourceBundle = ResourceBundle.getBundle("registration_messages", locale);
        new Thread(new MsisdnChecker((UserService) getBean("userServiceImpl"))).start();
        new Thread(new EmailAddressChecker((UserService) getBean("userServiceImpl"))).start();
    }

    @Override
    protected Application getNewApplication(HttpServletRequest request) throws ServletException {
        return new CommonRegistrationApp(this);
    }

    @Override
    protected Class<? extends Application> getApplicationClass() throws ClassNotFoundException {
        return CommonRegistrationApp.class;
    }

    public Object getBean(final String beanName) {
        return appContext.getBean(beanName);
    }

    public String getMessage(final String key, final Object... arguments) {
        try {
            if (arguments != null) {
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            } else {
                return resourceBundle.getString(key);
            }
        } catch (MissingResourceException mre) {
            logger.error("Message not found for key [" + key + "]");
            return '!' + key;
        }
    }

    @Override
    protected Application.SystemMessages getSystemMessages() {
        return (new Application.CustomizedSystemMessages() {
            @Override
            public String getCommunicationErrorCaption() {
                return null;
            }

            @Override
            public String getCommunicationErrorMessage() {
                return null;
            }
        });
    }
}
