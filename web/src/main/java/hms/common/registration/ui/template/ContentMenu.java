/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.template;

import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.admin.add.AddNewModuleGroupView;
import hms.common.registration.ui.content.admin.add.AddNewModuleView;
import hms.common.registration.ui.content.admin.add.AddNewPermissionView;
import hms.common.registration.ui.content.admin.add.AddNewUserView;
import hms.common.registration.ui.content.admin.search.ManageModuleGroupView;
import hms.common.registration.ui.content.admin.search.ManageModuleView;
import hms.common.registration.ui.content.admin.search.ManagePermissionView;
import hms.common.registration.ui.content.admin.search.ManageUserView;
import hms.common.registration.ui.content.subcorporate.AddSubCorporateUserView;
import hms.common.registration.ui.content.subcorporate.ManageSubCorporateUsersView;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.*;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ContentMenu extends Accordion {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentMenu.class);
    private CommonRegistrationApp commonRegistrationApp;
    private ContentLayout contentLayout;
    private Map<String, java.util.List<Button>> menuList;
    private java.util.List<Button> manageUsersMenus;
    private java.util.List<Button> manageGroupsMenus;
    private java.util.List<Button> managePermissionsMenus;
    private java.util.List<Button> manageModulesMenus;
    private java.util.List<Button> manageSubCorporateUsersMenus;
    private HorizontalLayout topHorizontalLayout;
    private VerticalLayout innerContentPanel;
    private UserService userService;
    private User loggedInUser;
    private String usersTab;
    private String subUsersTab;
    private String groupsTab;
    private String permissionsTab;
    private String modulesTab;

    public ContentMenu(CommonRegistrationApp commonRegistrationApp, ContentLayout contentLayout) {
        setWidth("200px");
        setStyleName("light");
        addTabChangeListener();
        setImmediate(true);
        contentLayout.removeAllComponents();
        this.contentLayout = contentLayout;
        this.commonRegistrationApp = commonRegistrationApp;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        String loggedInUserName = commonRegistrationApp.currentUserName();
        try {
            loggedInUser = userService.findUserByName(loggedInUserName);
        } catch (DataManipulationException e) {
            LOGGER.error("Error finding user by username", e);
        }
        topHorizontalLayout = new HorizontalLayout();
        innerContentPanel = commonRegistrationApp.getInnerContentPanel();
        innerContentPanel.setSizeFull();
        contentLayout.addComponent(this);
        contentLayout.setContentPanel(new ContentPanel());
        contentLayout.getContentPanel().setCaption(commonRegistrationApp.getMessage("registration.admin.heading"));
        initializeMenuList();
        buildMenuBar();
        topHorizontalLayout.addComponent(innerContentPanel);
        topHorizontalLayout.setSizeFull();
        contentLayout.addComponent(contentLayout.getContentPanel());
        contentLayout.setExpandRatio(contentLayout.getContentPanel(), 9.9f);
        contentLayout.getContentPanel().addComponent(topHorizontalLayout);
    }


    private void initializeMenuList() {
        // Links
        initializeManageUserMenu();
        initializeManageSubCorporateUserMenu();
        initializeManageGroupMenu();
        initializeManagePermissionsMenu();
        initializeManageModulesMenu();

        // Tabs
        usersTab = commonRegistrationApp.getMessage("registration.admin.users.heading");
        subUsersTab = commonRegistrationApp.getMessage("registration.subcorporate.users.heading");
        groupsTab = commonRegistrationApp.getMessage("registration.admin.groups.heading");
        permissionsTab = commonRegistrationApp.getMessage("registration.admin.permission.heading");
        modulesTab = commonRegistrationApp.getMessage("registration.admin.modules.heading");

        menuList = new LinkedHashMap<>();

        List<String> userRoles = loggedInUser.getRolesAsStrings();

        if (userRoles.contains("ROLE_REG_ADD_USER")) {
            addToMenu(usersTab, manageUsersMenus);
        }

        if (userRoles.contains("ROLE_REG_ADD_MODULE")) {
            addToMenu(modulesTab, manageModulesMenus);
        }

        if (userRoles.contains("ROLE_REG_ADD_SUB_COOPERATE_USER")) {
            addToMenu(subUsersTab, manageSubCorporateUsersMenus);
        }

        if (userRoles.contains("ROLE_REG_ADD_USER_GROUP")) {
            addToMenu(groupsTab, manageGroupsMenus);
        }

        if (loggedInUser.getUserGroup().getName().equals("SYSTEM_ADMIN")) {
            addToMenu(permissionsTab, managePermissionsMenus);
        }
    }

    private void buildMenuBar() {
        removeAllComponents();
        Set<String> keys = menuList.keySet();
        for (String key : keys) {
            VerticalLayout layout = new VerticalLayout();
            java.util.List<Button> menus = menuList.get(key);
            for (Button link : menus) {
                link.setStyleName("menu_link");
                layout.addComponent(link);
            }
            addTab(layout, key, null);
        }
    }

    private void initializeManageUserMenu() {
        manageUsersMenus = new ArrayList<Button>();

        Button addUserLink = new Button(commonRegistrationApp.getMessage("registration.admin.users.add.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        User newUser = new User();
                        changeView("registration.admin.users.heading",
                                new AddNewUserView(commonRegistrationApp, newUser, false, null),
                                "registration.admin.users.add.heading");
                    }
                });
        addUserLink.setDebugId("AddUserLink");
        manageUsersMenus.add(addUserLink);

        Button manageUserLink = new Button(commonRegistrationApp.getMessage("registration.admin.users.manage.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        changeView("registration.admin.users.heading",
                                new ManageUserView(commonRegistrationApp, new ArrayList<Condition>(), null),
                                "registration.admin.users.manage.heading");
                    }
                });
        manageUserLink.setDebugId("manageUserLink");
        manageUsersMenus.add(manageUserLink);
    }

    private void initializeManageSubCorporateUserMenu() {
        manageSubCorporateUsersMenus = new ArrayList<Button>();
        Button addSubUserLink = new Button(commonRegistrationApp.getMessage("registration.subcorporate.users.add.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {

                        LOGGER.debug(" logged in user is [{}]", loggedInUser.getUsername());
                        LOGGER.debug(" request is for creating new subcorporate user ");

                        SubCorporateUser createdUser = new SubCorporateUser();
                        createdUser.setCorperateParentId(loggedInUser);
                        changeView("registration.subcorporate.users.heading",
                                new AddSubCorporateUserView(commonRegistrationApp, createdUser, false, loggedInUser, null), "registration.subcorporate.users.add.heading");
                    }
                });
        addSubUserLink.setDebugId("addSubUserLink");
        Button manageSubUserLink = new Button(commonRegistrationApp.getMessage("registration.subcorporate.users.manage.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        try {
                            List<Condition> conditionList = new ArrayList<Condition>();
                            userService.merge(loggedInUser);
                            changeView("registration.subcorporate.users.heading",
                                    new ManageSubCorporateUsersView(commonRegistrationApp, loggedInUser, conditionList), "registration.subcorporate.users.manage.heading");
                        } catch (Exception e) {
                            LOGGER.error("Unexpected error occurred ", e);
                        }
                    }
                });
        manageSubUserLink.setDebugId("manageSubUserLink");
        manageSubCorporateUsersMenus.add(addSubUserLink);
        manageSubCorporateUsersMenus.add(manageSubUserLink);
    }

    private void initializeManageGroupMenu() {
        manageGroupsMenus = new ArrayList<Button>();
        Button addGroupLink = new Button(commonRegistrationApp.getMessage("registration.admin.groups.add.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        changeView("registration.admin.groups.heading",
                                new AddNewModuleGroupView(commonRegistrationApp, new UserGroup(), false),
                                "registration.admin.groups.add.heading");
                    }
                });
        addGroupLink.setDebugId("addGroupLink");
        manageGroupsMenus.add(addGroupLink);
        Button manageGroupLink = new Button(commonRegistrationApp.getMessage("registration.admin.groups.manage.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        changeView("registration.admin.groups.heading",
                                new ManageModuleGroupView(commonRegistrationApp, null),
                                "registration.admin.groups.manage.heading");
                    }
                });
        manageGroupLink.setDebugId("manageGroupLink");
        manageGroupsMenus.add(manageGroupLink);
    }


    private void initializeManagePermissionsMenu() {
        managePermissionsMenus = new ArrayList<Button>();
        Button addRoleLink = new Button(commonRegistrationApp.getMessage("registration.admin.permission.add.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        changeView("registration.admin.permission.heading",
                                new AddNewPermissionView(commonRegistrationApp, new Role(), false),
                                "registration.admin.permission.add.heading");
                    }
                });
        addRoleLink.setDebugId("addRoleLink");
        managePermissionsMenus.add(addRoleLink);
        Button managePermissionLink = new Button(commonRegistrationApp.getMessage("registration.admin.permission.manage.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        changeView("registration.admin.permission.heading",
                                new ManagePermissionView(commonRegistrationApp, null),
                                "registration.admin.permission.manage.heading");
                    }
                });
        managePermissionLink.setDebugId("managePermissionLink");
        managePermissionsMenus.add(managePermissionLink);
    }

    private void initializeManageModulesMenu() {
        manageModulesMenus = new ArrayList<Button>();
        Button addModuleLink = new Button(commonRegistrationApp.getMessage("registration.admin.modules.add.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        changeView("registration.admin.modules.heading",
                                new AddNewModuleView(commonRegistrationApp, new Module(), false),
                                "registration.admin.modules.add.heading");
                    }
                });
        addModuleLink.setDebugId("addModuleLink");
        manageModulesMenus.add(addModuleLink);
        Button manageModuleLink = new Button(commonRegistrationApp.getMessage("registration.admin.modules.manage.heading"),
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        changeView("registration.admin.modules.heading",
                                new ManageModuleView(commonRegistrationApp),
                                "registration.admin.modules.manage.heading");
                    }
                });
        manageModuleLink.setDebugId("manageModuleLink");
        manageModulesMenus.add(manageModuleLink);
    }

    private void changeView(String panelHeading,
                            Component component, String subPanelHeading) {
        contentLayout.getContentPanel().addComponent(component);
        innerContentPanel.removeAllComponents();
        contentLayout.getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        innerContentPanel.addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        innerContentPanel.addComponent(component);
    }

    public void addToMenu(String key, java.util.List<Button> links) {
        menuList.put(key, links);
        VerticalLayout layout = new VerticalLayout();
        for (Button link : links) {
            link.setStyleName("link");
            layout.addComponent(link);
        }
        addTab(layout, key, null);
    }

    /**
     * To change right panel view on tab selection
     */
    private void addTabChangeListener() {
        this.addListener(new SelectedTabChangeListener() {
            @Override
            public void selectedTabChange(SelectedTabChangeEvent event) {
                String caption = getTab(event.getTabSheet().getSelectedTab()).getCaption();
                LOGGER.debug("Selected Tab : " + getTab(event.getTabSheet().getSelectedTab()).getCaption());

                if (subUsersTab.equals(caption)) {
                    changeView("registration.subcorporate.users.heading",
                            new ManageSubCorporateUsersView(commonRegistrationApp, loggedInUser, new ArrayList<Condition>()), "registration.subcorporate.users.manage.heading");

                } else if (usersTab.equals(caption)) {
                    changeView("registration.admin.users.heading",
                            new ManageUserView(commonRegistrationApp, new ArrayList<Condition>(), null),
                            "registration.admin.users.manage.heading");

                } else if (modulesTab.equals(caption)) {
                    changeView("registration.admin.modules.heading",
                            new ManageModuleView(commonRegistrationApp),
                            "registration.admin.modules.manage.heading");

                } else if (permissionsTab.equals(caption)) {
                    changeView("registration.admin.permission.heading",
                            new ManagePermissionView(commonRegistrationApp, null),
                            "registration.admin.permission.manage.heading");

                } else if (groupsTab.equals(caption)) {
                    changeView("registration.admin.groups.heading",
                            new ManageModuleGroupView(commonRegistrationApp, null),
                            "registration.admin.groups.manage.heading");
                }
            }
        });
    }

}