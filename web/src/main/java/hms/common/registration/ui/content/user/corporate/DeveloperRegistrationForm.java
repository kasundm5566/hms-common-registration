package hms.common.registration.ui.content.user.corporate;

import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.util.ui.GridLayoutUtil;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.actionButton;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateHeading;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.getSpacer;
import static hms.common.registration.ui.common.util.ExternalLinks.CAS_LOGIN_URL;
import static hms.common.registration.util.ui.GridLayoutUtil.render;

public class DeveloperRegistrationForm extends Form {

    private GridLayout gridLayout = new GridLayout(2, 4);
    private CommonRegistrationApp commonRegistrationApp;
    private String businessAccountId;
    private String individualAccountId;

    private final Button nextButton;
    private final Button cancelButton;
    private OptionGroup devTypeOptionGroup = new OptionGroup();

    public DeveloperRegistrationForm(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        businessAccountId = commonRegistrationApp.getMessage("registration.developer.type.business.label");
        individualAccountId = commonRegistrationApp.getMessage("registration.developer.type.individual.label");

        nextButton = actionButton("registration.button.next", commonRegistrationApp);
        cancelButton = actionButton("registration.button.cancel", commonRegistrationApp);

        init();
        this.setLayout(gridLayout);
    }

    private void init() {
        addDeveloperTypeSelectionView();
    }

    private void addDeveloperTypeSelectionView() {

        gridLayout.setWidth(100, UNITS_PERCENTAGE);

        renderDevTypeSelectionView();

        gridLayout.addComponent(getSpacer(), 0, 2, 1, 2);

        addControlButtonsToTheView();
    }

    private void addControlButtonsToTheView() {

        final GridLayout buttonLayout = new GridLayout(2, 1);

        nextButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                cleanView();

                Form form;
                if(individualAccountId.equals(devTypeOptionGroup.getValue())) {
                    form = new IndividualDeveloperForm(commonRegistrationApp);
                } else {
                    form = new CorporateRegistrationForm(commonRegistrationApp);
                }
                addRegistrationFormToLayout(form);

            }
        });

        cancelButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                commonRegistrationApp.setLogoutURL(CAS_LOGIN_URL);
                commonRegistrationApp.close();
            }
        });

        buttonLayout.addComponent(nextButton, 0, 0, 0, 0);
        buttonLayout.addComponent(cancelButton, 1, 0, 1, 0);

        buttonLayout.setComponentAlignment(nextButton, Alignment.MIDDLE_CENTER);
        buttonLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_CENTER);

        render(buttonLayout, new GridLayoutUtil.GridArea(0, 3, 1, 3), gridLayout, 30, Alignment.MIDDLE_CENTER);
    }

    private void renderDevTypeSelectionView() {
        final Label developerTypeHeading = generateHeading("registration.developer.type.selection.heading", commonRegistrationApp);
        developerTypeHeading.setWidth(98, UNITS_PERCENTAGE);
        gridLayout.setComponentAlignment(developerTypeHeading, Alignment.TOP_CENTER);
        gridLayout.addComponent(developerTypeHeading, 0, 0, 1, 0);

        devTypeOptionGroup.addItem(individualAccountId);
        devTypeOptionGroup.addItem(businessAccountId);
        devTypeOptionGroup.setMultiSelect(false);
        devTypeOptionGroup.setImmediate(true);
        devTypeOptionGroup.setWidth(98, UNITS_PERCENTAGE);
        devTypeOptionGroup.setStyleName("v-select-optiongroup-horizontal");
        devTypeOptionGroup.select(individualAccountId);

        gridLayout.setComponentAlignment(devTypeOptionGroup, Alignment.TOP_RIGHT);
        gridLayout.addComponent(devTypeOptionGroup, 0, 1, 1, 1);
    }

    private void cleanView() {
        gridLayout.removeAllComponents();
    }

    private void addRegistrationFormToLayout(Form form) {
        gridLayout.addComponent(getSpacer(), 0, 2, 1, 2);
        form.setWidth(98, UNITS_PERCENTAGE);
        gridLayout.addComponent(form, 0, 3, 1, 3);
        gridLayout.setComponentAlignment(form, Alignment.TOP_CENTER);
    }
}
