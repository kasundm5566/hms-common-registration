/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin.add;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserGroup;
import hms.common.registration.service.ModuleGroupService;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.common.WindowCloseListener;
import hms.common.registration.ui.content.admin.search.ManageUserView;
import hms.common.registration.ui.content.admin.view.UserProfileView;
import hms.common.registration.ui.validator.EmailCheckValidator;
import hms.common.registration.ui.validator.UserNameCheckValidator;
import hms.common.registration.util.ServiceProvider;
import hms.common.registration.util.ldap.LdapClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.CommunicationException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static hms.common.registration.model.UserStatus.ACTIVE;
import static hms.common.registration.model.UserType.*;
import static hms.common.registration.service.UserService.*;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.ui.content.admin.add.AddNewUserView.*;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;
import static hms.common.registration.util.UserGroupToTypeMapper.GroupTypeMappingNotFoundException;
import static hms.common.registration.util.UserGroupToTypeMapper.getUserTypeByGroup;

/**
 * Handle the Add new user form implementation
 */

public class AddNewUserView extends VerticalLayout implements Property.ValueChangeListener {

    public static final String RETYPE_PASSWORD_PROPERTY_ID = "retypePassword";
    public static final String MODULE_GROUPS_ID = "groups";
    public static final String IS_LDAP_USER_ID = "isLdapUser";
    private static final Logger logger = LoggerFactory.getLogger(AddNewUserView.class);
    private WindowCloseListener parentWindow;
    private CommonRegistrationApp commonRegistrationApp;
    private User user;
    private boolean editState;
    private String selectedUserGroup;
    private UserService userService;
    private Form addUserForm;
    private BeanItem<User> beanItem;
    private Set<String> previouslySelectedModuleGroups;
    private UserGroupService userGroupService;
    private LdapClient ldapClient;
    private ServiceProvider serviceProvider;

    public AddNewUserView(CommonRegistrationApp commonRegistrationApp, User user, boolean editState, WindowCloseListener parentWindow) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.user = user;
        this.editState = editState;
        this.parentWindow = parentWindow;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        ldapClient = (LdapClient) commonRegistrationApp.getBean("ldapClient");
        serviceProvider = (ServiceProvider) commonRegistrationApp.getBean("serviceProvider");
        generateAddUserView();
    }

    private void generateAddUserView() {

        setMargin(true);
        setWidth("700px");
        addUserForm = new Form();
        addUserForm.setWidth("400px");
        beanItem = new BeanItem<>(user);
        addUserForm.setImmediate(true);
        beanItem.addItemProperty(RETYPE_PASSWORD_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(MODULE_GROUPS_ID, new ObjectProperty(getUserGroupName(user), String.class));
        beanItem.addItemProperty(IS_LDAP_USER_ID, new ObjectProperty(user.isLdapUser(), Boolean.class));

        addUserForm.setItemDataSource(beanItem);
        addUserForm.setWriteThrough(false);
        addUserForm.setFormFieldFactory(new FieldFactory(this, commonRegistrationApp));

        if (editState) {
            if (user.getUserType().equals(ADMIN_USER) || user.getUserType().equals(AUDITING_USER)) {
                addUserForm.setVisibleItemProperties(getEditAdminFields());
            } else if (user.getUserType().equals(INDIVIDUAL) || user.getUserType().equals(CORPORATE)) {
                addUserForm.setVisibleItemProperties(getEditOtherUserFields());
            }
            addUserForm.getField(USER_NAME).setReadOnly(true);
            for (Validator v : new ArrayList<>(addUserForm.getField(USER_NAME).getValidators())) {
                addUserForm.getField(USER_NAME).removeValidator(v);
            }
        } else {
            addUserForm.setVisibleItemProperties(getAddUserFields());
        }

        addUserForm.getField(MODULE_GROUPS_ID).setEnabled(!editState);

        generateFormFooter(addUserForm);
        addComponent(addUserForm);
        setComponentAlignment(addUserForm, Alignment.MIDDLE_CENTER);
    }

    private String[] getAddUserFields() {

        List<String> fields = new ArrayList<>();

        fields.add(FIRST_NAME);
        fields.add(LAST_NAME);
        if (isProfessionFieldVisible()){
            fields.add(PROFESSION);
        }
        fields.add(EMAIL);
        if (isLdapFieldVisible()) {
            fields.add(IS_LDAP_USER_ID);
            addLdapCheckBoxListener();
            addUserForm.getField(IS_LDAP_USER_ID).setEnabled(isLdapOptionEnabled());
        }
        fields.add(USER_NAME);
        fields.add(PASSWORD);
        fields.add(RETYPE_PASSWORD_PROPERTY_ID);
        fields.add(PHONE_NUMBER);
        fields.add(ADDRESS);
        fields.add(MODULE_GROUPS_ID);

        addRetypePasswordValidator();

        String[] fieldsArray = new String[fields.size()];
        fieldsArray = fields.toArray(fieldsArray);
        return fieldsArray;
    }

    private String[] getEditAdminFields() {

        List<String> fields = new ArrayList<>();

        fields.add(USER_NAME);
        fields.add(FIRST_NAME);
        fields.add(LAST_NAME);
        if (isProfessionFieldVisible()){
            fields.add(PROFESSION);
        }
        fields.add(EMAIL);
        if (isLdapFieldVisible()) {
            fields.add(IS_LDAP_USER_ID);
            addLdapCheckBoxListener();
            addUserForm.getField(IS_LDAP_USER_ID).setEnabled(isLdapOptionEnabled());
        }
        fields.add(PHONE_NUMBER);
        fields.add(ADDRESS);
        fields.add(MODULE_GROUPS_ID);

        String[] fieldsArray = new String[fields.size()];
        fieldsArray = fields.toArray(fieldsArray);
        return fieldsArray;
    }

    private String[] getEditOtherUserFields() {

        List<String> fields = new ArrayList<>();

        fields.add(USER_NAME);
        fields.add(FIRST_NAME);
        fields.add(LAST_NAME);
        fields.add(ADDRESS);
        fields.add(MODULE_GROUPS_ID);

        String[] fieldsArray = new String[fields.size()];
        fieldsArray = fields.toArray(fieldsArray);
        return fieldsArray;
    }

    private String getUserGroupName(User user) {
        if (user.getUserGroup() != null) {
            return user.getUserGroup().getName();
        }
        return null;
    }

    /**
     * Hide or show password field according to the LDAP user checkbox selection
     */
    private void addLdapCheckBoxListener() {
        ((CheckBox) addUserForm.getField(IS_LDAP_USER_ID)).setImmediate(true);
        addUserForm.getField(IS_LDAP_USER_ID).addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {

                if (event.getProperty().getClass().equals(CheckBox.class)) {
                    logger.debug("Selected LDAP user [{}]", event.getProperty().getValue());

                    addUserForm.setComponentError(new UserError(null));
                    if (event.getProperty().getValue() == true) {

                        logger.debug("LDAP user option is selected ");
                        validateLDAPClient();
                    }
                    addUserForm.getField(IS_LDAP_USER_ID).setValue(event.getProperty().getValue());
                }
            }
        });
    }

    private void validateLDAPClient() {
        String name = (String) addUserForm.getField(USER_NAME).getValue();
        if (name.trim().equals("")) {
            try {
                if (!ldapClient.isUserAvailable(name)) {
                    addUserForm.setComponentError(new UserError(commonRegistrationApp.getMessage("registration.ldap.user.not.available")));
                } else {
                    addUserForm.setComponentError(new UserError(null));
                }
            } catch (CommunicationException ex) {
                logger.debug("Connecting to LDAP server failed.");
            }
        }//if trim
    }

    private void saveUserEnteredDetails() {
        Collection itemIds = addUserForm.getItemPropertyIds();
        for (Object itemId : itemIds) {
            if (addUserForm.getField(itemId).getValue() != null) {
                beanItem.getItemProperty(itemId).setValue(addUserForm.getField(itemId).getValue());
            }
        }
    }

    private void generateFormFooter(final Form form) {
        form.setFooter(new VerticalLayout());
        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setWidth("400px");
        footerLayout.setHeight("50px");
        final HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setWidth("200px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"),
                new Button.ClickListener() {
                    public void buttonClick(Button.ClickEvent event) {
                        logger.debug("Discarding the input values..........");
                        if (!editState) {
                            form.discard();
                            changeView("registration.admin.users.heading",
                                    new ManageUserView(commonRegistrationApp, new ArrayList<Condition>(), null),
                                    "registration.admin.users.manage.heading");
                        } else {
                            commonRegistrationApp.getMainWindow().removeWindow(form.getWindow());
                        }
                    }
                });

        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.submit"));
        if (editState) {
            saveButton.setCaption(commonRegistrationApp.getMessage("registration.button.save"));
        }
        saveButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                logger.debug("Creating Admin User Account for [{}] Started.", user.getUsername());
                try {
                    form.commit();
                    form.validate();
                    logger.debug("Form validated successfully: " + user.getEmail());

                    try {
                        saveUserDetails();
                        removeAllComponents();
                        if (editState) {
                            addComponent(CommonUiComponentGenerator.generateSuccessMessage("registration.user.user.edit.success", commonRegistrationApp));
                        } else {
                            addComponent(CommonUiComponentGenerator.generateSuccessMessage("registration.user.user.creation.success", commonRegistrationApp));
                        }
                        addComponent(new UserProfileView(user, commonRegistrationApp));
                    } catch (Exception e) {
                        logger.error("Error occurred while creating user account for user", e);
                        generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                                "registration.user.user.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                    }
                } catch (Validator.InvalidValueException e) {
                    logger.debug("Invalid Form Entries [{}]", e.getMessage());
                }
            }
        });
        cancelButton.setStyleName("action-button");
        buttonLayout.addComponent(cancelButton);
        buttonLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_LEFT);
        saveButton.setStyleName("action-button");
        buttonLayout.addComponent(saveButton);
        buttonLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
        footerLayout.addComponent(buttonLayout);
        footerLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        form.setFooter(footerLayout);
    }

    private void changeView(String panelHeading,
                            Component component, String subPanelHeading) {
        commonRegistrationApp.getContentLayout().getContentPanel().addComponent(component);
        commonRegistrationApp.getInnerContentPanel().removeAllComponents();
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        commonRegistrationApp.getInnerContentPanel().addComponent(component);
    }

    private void saveUserDetails() throws DataManipulationException, NoSuchAlgorithmException, UnsupportedEncodingException, GroupTypeMappingNotFoundException {
        UserGroup userGroup = userGroupService.findUserGroupByGroupName((String) addUserForm.getField(MODULE_GROUPS_ID).getValue());
        user.setUserGroup(userGroup);
        user.setUserType(getUserTypeByGroup(user.getUserGroup().getName()));

        if (user.getUserType().equals(CORPORATE)) {
            CorporateUser corporateUser = new CorporateUser(user, user.getUserGroup());

            if (editState) {
                user = userService.merge(user);
            } else {
                userService.persist(corporateUser);
            }

            if (!editState) {
                corporateUser.setPassword(MD5(corporateUser.getPassword()));
                corporateUser.setUserStatus(ACTIVE);
                corporateUser.setEnabled(true);
                user = userService.merge(corporateUser);
                if (isAutoSpCreationEnabled()) {
                    serviceProvider.createDefaultSpUser(corporateUser);
                }
            }

        } else {
            if (editState) {
                user = userService.merge(user);
            } else {
                userService.persist(user);
            }

            if (!editState) {
                if (isLdapFieldVisible() && !(Boolean) addUserForm.getField(IS_LDAP_USER_ID).getValue()) {
                    user.setPassword(MD5(user.getPassword()));
                } else {
                    user.setPassword(MD5(user.getPassword()));
                }
                user.setUserStatus(ACTIVE);
                user.setEnabled(true);
            }
            if (isLdapFieldVisible() && user.getUserType().equals(ADMIN_USER)) {
                user.setLdapUser((Boolean) addUserForm.getField(IS_LDAP_USER_ID).getValue());
            }
            user = userService.merge(user);
        }
    }

    /**
     * adding validator for retype password field
     */
    private void addRetypePasswordValidator() {
        addUserForm.getField(RETYPE_PASSWORD_PROPERTY_ID).addValidator(new Validator() {

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    logger.debug("Password not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.password.does.not.match"));
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (value == null) {
                    return false;
                } else if (value.equals(addUserForm.getField(PASSWORD).getValue())) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    @Override
    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
        selectedUserGroup = (String) valueChangeEvent.getProperty().getValue();
    }

    public User getUser() {
        return user;
    }
}

class FieldFactory implements FormFieldFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldFactory.class);
    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.user.";
    private Field passwordField;
    private ModuleGroupService moduleGroupService;
    private AddNewUserView addNewUserView;
    private UserGroupService userGroupService;
    private CommonRegistrationApp commonRegistrationApp;

    FieldFactory(AddNewUserView addNewUserView, CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        moduleGroupService = (ModuleGroupService) commonRegistrationApp.getBean("moduleGroupServiceImpl");
        this.addNewUserView = addNewUserView;
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;

        LOGGER.debug("pid [{}] ", pid);

        if (USER_NAME.equals(pid)) {
            Field field = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.username.required", commonRegistrationApp);
            field.addValidator(new UserNameCheckValidator(commonRegistrationApp));
            field.addValidator(new RegexpValidator(USER_NAME_REGEX_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.username")));
            return field;
        } else if (PASSWORD.equals(pid)) {
            passwordField = CommonUiComponentGenerator.createPasswordField(caption, "registration.user.password.required", commonRegistrationApp);
            passwordField.addValidator(new RegexpValidator(PASSWORD_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.password")));
            return passwordField;
        } else if (RETYPE_PASSWORD_PROPERTY_ID.equals(pid)) {
            return CommonUiComponentGenerator.createPasswordField(caption, "registration.user.retype.password.required", commonRegistrationApp);
        } else if (FIRST_NAME.equals(pid)) {
            Field textField = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.first.name.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(FIRST_NAME_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.user.first.name.length.validation")));
            return textField;
        } else if (LAST_NAME.equals(pid)) {
            Field textField = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.last.name.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(LAST_NAME_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.user.last.name.length.validation")));
            return textField;
        } else if (MODULE_GROUPS_ID.equals(pid)) {

            LOGGER.debug("caption [{}]", caption);

            try {
                List<String> userGroupNames = userGroupService.findAllUserGroupNames();
                Field userGroupComboBox = CommonUiComponentGenerator.createComboBox(commonRegistrationApp.getMessage("registration.user.userGroup"), true, "registration.user.groups.required", userGroupNames, commonRegistrationApp);
                return userGroupComboBox;
            } catch (Exception e) {
                LOGGER.error("Error while loading available module names");
                return null;
            }
        } else if (EMAIL.equals(pid)) {
            Field textField = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.email.required", commonRegistrationApp);
            EmailCheckValidator emailValidator = new EmailCheckValidator(commonRegistrationApp);
            emailValidator.setUserId(addNewUserView.getUser().getUserId());
            textField.addValidator(emailValidator);
            return textField;
        } else if (PHONE_NUMBER.equals(pid)) {
            Field textField = CommonUiComponentGenerator.createTextField(caption, false, "registration.user.msisdn.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));
            return textField;
        } else if (PROFESSION.equals(pid)) {
            return createComboBox(caption, false, "",
                    PROFESSIONS, commonRegistrationApp);
        } else if (ADDRESS.equals(pid)) {
            TextArea addressText = (TextArea) createTextArea(caption, false, "", commonRegistrationApp);
            addressText.setColumns(15);
            addressText.addValidator(new RegexpValidator(ADDRESS_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.user.address.length.validation")));
            return addressText;
        } else if (IS_LDAP_USER_ID.equals(pid)) {
            return new CheckBox(commonRegistrationApp.getMessage("registration.user.isLdapUser"));
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            CommonUiComponentGenerator.filterNullValues(field);
            return field;
        }
    }
}



