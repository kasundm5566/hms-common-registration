package hms.common.registration.ui.validator;

import com.google.common.base.Optional;
import com.vaadin.data.Validator;
import com.vaadin.ui.Field;
import hms.common.registration.ui.CommonRegistrationApp;

public class RetypeValidator implements Validator {

    private final Field field;
    private String error;

    public RetypeValidator(Field field, String error) {
        this.field = field;
        this.error = error;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if(!isValid(value)) {
            throw new InvalidValueException(error);
        }
    }
    @Override
    public boolean isValid(Object value) {
        final Optional<Object> passwordString = Optional.fromNullable(field.getValue());
        if(!passwordString.isPresent()) {
            return true;
        }
        return passwordString.get().equals(value);
    }
}
