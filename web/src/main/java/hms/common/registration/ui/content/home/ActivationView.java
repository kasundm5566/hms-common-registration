/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.home;

import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.user.verification.UserVerificationView;
import hms.common.registration.ui.template.ContentLayout;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.template.MainLayout;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.PropertyHolder.USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION;
import static hms.common.registration.model.UserType.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;
import static hms.common.registration.util.ValidateUser.isValidUser;
import static hms.common.registration.util.ValidateUser.isVerified;


public class ActivationView extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(Dashboard.class);

    private CommonRegistrationApp commonRegistrationApp;

    private MainLayout mainLayout;
    private UserService userService;
    private GridLayout gridLayout;

    public ActivationView(CommonRegistrationApp commonRegistrationApp, MainLayout mainLayout, String fragment) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.mainLayout = mainLayout;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");

        if(fragment.equals(commonRegistrationApp.msisdnVerificationRetryFrag)){
            createMsisdnVerificatioRetryFlow();
        }
        init();
    }

    private void init() {
        setWidth("80%");
        setSizeFull();
        gridLayout = new GridLayout(3, 3);
        gridLayout.setSizeFull();
        gridLayout.setSpacing(true);
        gridLayout.setSizeFull();

        createDashboard();
    }

    private void createDashboard() {
        String loggedInUserName;
        try {
            loggedInUserName = commonRegistrationApp.currentUserName();
        } catch (NullPointerException e) {
            return;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while getting username", e);
            return;
        }

        try {
            User user = userService.findUserByName(loggedInUserName);
            if (user != null) {
                if (isValidUser(user)) {
                    commonRegistrationApp.getFragmentUtility().setFragment("/");
                } else {
                    if(mainLayout == null) {
                        return;
                    }
                    showValidationUI(user);
                    addComponent(gridLayout);
                    ContentLayout contentLayout = mainLayout.getContentLayout();
                    ContentPanel contentPanel = contentLayout.getContentPanel();
                    contentLayout.removeAllComponents();
                    contentPanel.removeAllComponents();
                    contentPanel.addComponent(this);
                    contentPanel.setCaption(commonRegistrationApp.getMessage("registration.user.verification.heading"));
                    contentLayout.setSizeFull();
                    contentLayout.addComponent(contentPanel);
                }
            }
        } catch (DataManipulationException e) {
            logger.error("Error occurred while getting user details", e);
        } catch (Exception e) {
            logger.error("Error occurred while generating dashboard", e);
        }
    }

    private void showValidationUI(User user) {
        if (user.getUserType().equals(INDIVIDUAL)) {
            if (!isVerified(isConsumerUserEmailVerifyEnabled(), user.isEmailVerified())) {
                createEmailNotVerificationView();
            } else if (!isVerified(isConsumerUserMsisdnVerifyEnabled(), user.isMsisdnVerified())) {
                createMsisdnVerificationView(user);
            }
        } else {
            if (!isVerified(isCorporateUserEmailVerifyEnabled(), user.isEmailVerified())) {
                createEmailNotVerificationView();
            } else if (!isVerified(isCorporateUserMsisdnVerifyEnabled(), user.isMsisdnVerified())) {
                createMsisdnVerificationView(user);
            }
        }
    }

    private void createEmailNotVerificationView() {
        gridLayout.addComponent(getSpacer());
        gridLayout.addComponent(new Label(MessageFormat.format(commonRegistrationApp.getMessage("registration.user.email.verification"),
                USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION)), 0, 1, 2, 1);
        gridLayout.addComponent(getSpacer());
    }

    private void createMsisdnVerificationView(User user) {
        gridLayout.addComponent(new UserVerificationView(user, commonRegistrationApp, true));
    }

    private void createMsisdnVerificatioRetryFlow() {
        String loggedInUserName;
        try {
            loggedInUserName = commonRegistrationApp.currentUserName();
            User user = userService.findUserByName(loggedInUserName);

            if (user != null) {
                user.setMsisdnVerifySkip(false);
                user = userService.merge(user);
            }
        } catch (NullPointerException e) {
            return;
        } catch (DataManipulationException e) {
            logger.error("Error occurred while setting user msisdn verification retry", e);
        } catch (Exception e) {
            logger.error("Unexpected error occurred while setting user msisdn verification retry", e);
            return;
        }
    }

}
