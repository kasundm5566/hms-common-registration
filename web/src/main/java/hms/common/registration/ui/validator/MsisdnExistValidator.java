/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.validator;


import com.vaadin.data.Validator;
import hms.common.registration.model.User;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.SessionKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by IntelliJ IDEA.
 * User: isuru
 * Date: 1/19/12
 * Time: 1:04 PM
 */
public class MsisdnExistValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(MsisdnExistValidator.class);

    private UserService userService;
    private String  msisdnUnavailableMessage;
    private CommonRegistrationApp commonRegistrationApp;

    public MsisdnExistValidator(CommonRegistrationApp commonRegistrationApp){
        this.commonRegistrationApp = commonRegistrationApp;
        msisdnUnavailableMessage = commonRegistrationApp.getMessage("registration.user.msisdn.not.available");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            logger.debug(" Entered phone number  [{}] already exist " + value);
            throw new InvalidValueException(msisdnUnavailableMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            String loggedInUserName = null;
            String tmpMsisdn = commonRegistrationApp.getFromSession(SessionKeys.MSISDN_TMP);

            try {
                loggedInUserName = commonRegistrationApp.currentUserName();
            } catch (Exception e) {
            }

            User userByMsisdn = userService.findUserByMsisdn((String) value);
            if (userByMsisdn == null || userByMsisdn.getUsername().equals(loggedInUserName) || userByMsisdn.getMobileNo().equals(tmpMsisdn)) {
                return true;
            } else {
                return false;
            }
        } catch (DataManipulationException ex) {
            return false;
        }
    }
}
