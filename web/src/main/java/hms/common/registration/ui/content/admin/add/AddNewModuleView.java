/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.admin.add;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.admin.search.ManageModuleView;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import hms.common.registration.service.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.PropertyHolder.MODULE_DESCRIPTION_LENGTH_VALIDATION;
import static hms.common.registration.util.PropertyHolder.MODULE_NAME_LENGTH_VALIDATION;
import static hms.common.registration.util.PropertyHolder.ROLE_NAME_REGEX_PATTERN;
import static hms.common.registration.service.ModuleService.MODULE_DESCRIPTION;
import static hms.common.registration.service.ModuleService.MODULE_NAME;
import static hms.common.registration.service.ModuleService.ROLE_PREFIX;

/**
 * Handle the Add new module form implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AddNewModuleView extends VerticalLayout {

    private static Logger logger = LoggerFactory.getLogger(AddNewModuleView.class);
    private CommonRegistrationApp commonRegistrationApp;
    private ModuleService moduleService;
    private Module module;
    private boolean editState;
    private Form addModuleForm;

    public AddNewModuleView(CommonRegistrationApp commonRegistrationApp, Module module, boolean editState) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.editState = editState;
        moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        this.module = module;
        try {
            if(editState) {
                this.module = moduleService.findModuleByName(module.getModuleName());
            }
            generateAddModuleView();
        } catch (Exception e) {
            logger.error("Error occured while creating new module", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.module.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
    }

    private void generateAddModuleView() {
        setSizeFull();
        setSpacing(true);
        setMargin(true);
        addModuleForm = new Form();
        addModuleForm.getLayout().setWidth("600px");
        BeanItem<Module> beanItem = new BeanItem<Module>(module);
        addModuleForm.setImmediate(true);
        addModuleForm.setItemDataSource(beanItem);
        addModuleForm.setWriteThrough(false);
        addModuleForm.setFormFieldFactory(new ModuleFieldFactory(this, moduleService, commonRegistrationApp));
        addModuleForm.setVisibleItemProperties(new String[]{
                MODULE_NAME, ROLE_PREFIX, MODULE_DESCRIPTION,
        });
        if (editState) {
            addModuleForm.getField(MODULE_NAME).setReadOnly(true);
        }
        createFormFooter(addModuleForm);
        addComponent(addModuleForm);
    }

    private void createFormFooter(final Form addModuleForm) {
        addModuleForm.setFooter(new VerticalLayout());
        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setWidth("600px");
        footerLayout.setHeight("50px");
        final HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setWidth("200px");
        final Button cancelButton = createButton("registration.button.cancel", commonRegistrationApp);
        cancelButton.addListener(new Button.ClickListener() {
            public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
                logger.debug("Discarding the input values..........");

                if(!editState){
                    addModuleForm.discard();
                    changeView("registration.admin.modules.heading",
                                new ManageModuleView(commonRegistrationApp),
                                "registration.admin.modules.manage.heading");
                } else {
                   commonRegistrationApp.getMainWindow().removeWindow(addModuleForm.getWindow());
                }

            }
        });
        final Button saveButton = createButton("registration.button.submit", commonRegistrationApp);
        if (editState) {
            saveButton.setCaption(commonRegistrationApp.getMessage("registration.button.save"));
        }
        saveButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                logger.debug("Saving Module [{}] details", module.toString());
                try {
                    addModuleForm.commit();
                    addModuleForm.validate();
                    if (!isModuleAlreadyExist(module)) {
                        try {
                            saveModuleDetails();
                            removeAllComponents();
                            if (editState) {
                                addComponent(generateSuccessMessage("registration.user.module.edit.success", commonRegistrationApp));
                            } else {
                                addComponent(generateSuccessMessage("registration.user.module.creation.success", commonRegistrationApp));
                            }
                        } catch (DataManipulationException e) {
                            logger.error("Error occured while creating new module", e);
                            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                                    "registration.user.module.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                        }
                    }
                } catch (Validator.InvalidValueException e) {
                    logger.error("Invalid Form Entries", e);
                }
            }
        });
        if (!addModuleForm.isReadOnly()) {
            buttonLayout.addComponent(cancelButton);
            buttonLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_LEFT);
            buttonLayout.addComponent(saveButton);
            buttonLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
            footerLayout.addComponent(buttonLayout);
            footerLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        }
        addModuleForm.setFooter(footerLayout);
    }

    private boolean isModuleAlreadyExist(Module module) {
        if (moduleService.isModuleExist(module.getModuleName()) && !editState) {
           notification(commonRegistrationApp.getMainWindow(), "registration.admin.module.name.already.exist", WARNING, commonRegistrationApp);
            return true;
        } else if (moduleService.isRolePrefixAlreadyDefined(module.getRolePrefix())) {
            notification(commonRegistrationApp.getMainWindow(), "registration.admin.module.role.prefix", WARNING, commonRegistrationApp);
            return true;
        }
        return false;
    }

    /**
     * Save Module Details
     *
     * @throws DataManipulationException
     */
    private void saveModuleDetails() throws DataManipulationException {
        if (editState) {
            module = moduleService.update(module);
        } else {
            moduleService.persist(module);
        }
    }

    private void changeView(String panelHeading,
                            Component component, String subPanelHeading) {
        commonRegistrationApp.getContentLayout().getContentPanel().addComponent(component);
        commonRegistrationApp.getInnerContentPanel().removeAllComponents();
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        commonRegistrationApp.getInnerContentPanel().addComponent(component);
    }
}

class ModuleFieldFactory implements FormFieldFactory {
    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.admin.module.";
    private TwinColSelect roleSelector;
    private AddNewModuleView addNewModuleView;
    private ModuleService moduleService;
    private CommonRegistrationApp commonRegistrationApp;

    ModuleFieldFactory(AddNewModuleView addNewModuleView, ModuleService moduleService, CommonRegistrationApp commonRegistrationApp) {
        this.addNewModuleView = addNewModuleView;
        this.moduleService = moduleService;
        this.commonRegistrationApp = commonRegistrationApp;
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;
        if (MODULE_NAME.equals(pid)) {
            Field textField = createTextField(caption, true, "registration.admin.module.name.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(MODULE_NAME_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.admin.module.name.length.validation")));
            return textField;
        } else if (MODULE_DESCRIPTION.equals(pid)) {
            Field textArea = createTextArea(caption, true, "registration.admin.module.description.required", commonRegistrationApp);
            textArea.addValidator(new RegexpValidator(MODULE_DESCRIPTION_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.admin.module.description.length.validation")));
            return textArea;
        } else if (ROLE_PREFIX.equals(pid)) {
            Field textField = createTextField(caption, true, "registration.admin.module.role.prefix.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(ROLE_NAME_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.admin.module.rolePrefix.invalid")));
            return textField;
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            CommonUiComponentGenerator.filterNullValues(field);
            return field;
        }
    }

    /**
     * Create the TwinCoSelector containing Role list
     *
     * @param caption
     */
    private void createRoleSelector(String caption) {

        BeanItemContainer<Role> container = new BeanItemContainer<Role>(Role.class);
        List<Role> moduleList = moduleService.findAllRoles();
        container.addAll(moduleList);
        roleSelector = new TwinColSelect(caption, container);
        roleSelector.setWidth("450px");
//            roleSelector.addListener(addNewModuleView);
        roleSelector.setRequiredError(commonRegistrationApp.getMessage("registration.admin.module.roles.required"));
    }
}
