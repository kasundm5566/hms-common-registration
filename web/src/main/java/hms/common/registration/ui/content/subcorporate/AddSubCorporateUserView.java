/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.subcorporate;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.common.WindowCloseListener;
import hms.common.registration.ui.content.admin.view.UserProfileView;
import hms.common.registration.ui.validator.EmailCheckValidator;
import hms.common.registration.ui.validator.UserNameCheckValidator;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.*;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateSystemNotificationMessage;
import static hms.common.registration.ui.common.CommonUiComponentGenerator.getSpacer;
import static hms.common.registration.ui.content.subcorporate.AddSubCorporateUserView.RETYPE_PASSWORD_PROPERTY_ID;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.PASSWORD_REGEX_PATTERN;
import static hms.common.registration.service.UserService.*;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AddSubCorporateUserView extends VerticalLayout implements WindowCloseListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddSubCorporateUserView.class);
    public static final String RETYPE_PASSWORD_PROPERTY_ID = "retypePassword";

    private WindowCloseListener parentWindow;
    private SubCorporateUser user;
    private User parentCorporateUser;
    private boolean editState;
    private UserService userService;
    private Set<Role> selectedRoleSet;
    private Form addUserForm;
    private CommonRegistrationApp commonRegistrationApp;
    private VerticalLayout checkBoxLayout;
    private Map<Role, Boolean> existingRolesMap;

    public AddSubCorporateUserView(CommonRegistrationApp commonRegistrationApp, SubCorporateUser createdUser, boolean editState, WindowCloseListener parentWindow) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.user = createdUser;
        this.parentCorporateUser = user.getCorperateParentId();
        this.editState = editState;
        this.parentWindow = parentWindow;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        selectedRoleSet = createdUser.getRoles();
        existingRolesMap = new HashMap<Role, Boolean>();
        if (editState) {
            for (Role role : user.getRoles()) {
                existingRolesMap.put(role, true);
            }
        }

        createAddCorporateUserView();
    }


    public AddSubCorporateUserView(CommonRegistrationApp commonRegistrationApp, SubCorporateUser createdUser, boolean editState, User parentCorporateUser, WindowCloseListener parentWindow) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.user = createdUser;
        this.parentCorporateUser = parentCorporateUser;
        this.editState = editState;
        this.parentWindow = parentWindow;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        selectedRoleSet = new HashSet<Role>();
        existingRolesMap = new HashMap<Role, Boolean>();
        if (editState) {
            for (Role role : user.getRoles()) {
                existingRolesMap.put(role, true);
            }
        }
        createAddCorporateUserView();
    }

    private void createAddCorporateUserView() {

        setMargin(true);
        setWidth("700px");
        addUserForm = new Form();
        addUserForm.setWidth("400px");
        BeanItem<SubCorporateUser> beanItem = new BeanItem<SubCorporateUser>(user);
        addUserForm.setImmediate(true);
        beanItem.addItemProperty(RETYPE_PASSWORD_PROPERTY_ID, new ObjectProperty(null, String.class));
        addUserForm.setItemDataSource(beanItem);
        addUserForm.setWriteThrough(false);
        addUserForm.setFormFieldFactory(new SubCorporateUserFieldFactory(this, commonRegistrationApp));
        if (editState) {
            addUserForm.setVisibleItemProperties(new String[]{
                    USER_NAME, EMAIL});
            addUserForm.getField(USER_NAME).setReadOnly(true);
            for (Validator v : new ArrayList<Validator>(addUserForm.getField(USER_NAME).getValidators())) {
                addUserForm.getField(USER_NAME).removeValidator(v);
            }
        } else {
            addUserForm.setVisibleItemProperties(new String[]{
                    USER_NAME, PASSWORD, RETYPE_PASSWORD_PROPERTY_ID, EMAIL});
            addRetypePasswordValidator();
        }
        addComponent(addUserForm);
        setComponentAlignment(addUserForm, Alignment.TOP_CENTER);

        checkBoxLayout = createCheckBoxLayout();
        checkBoxLayout.setWidth("400px");
        addComponent(checkBoxLayout);
        setComponentAlignment(checkBoxLayout, Alignment.MIDDLE_CENTER);
        HorizontalLayout footerLayout = createFooterLayout();
        addComponent(footerLayout);
        setComponentAlignment(footerLayout, Alignment.BOTTOM_CENTER);
    }

    /**
     * adding validator for retype password field
     */
    private void addRetypePasswordValidator() {
        addUserForm.getField(RETYPE_PASSWORD_PROPERTY_ID).addValidator(new Validator() {

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    LOGGER.debug("Password not matching");
                    throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.password.does.not.match"));
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (value == null) {
                    return false;
                } else if (value.equals(addUserForm.getField(PASSWORD).getValue())) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    private HorizontalLayout createFooterLayout() {

        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setWidth("400px");
        final HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setHeight("50px");
        buttonLayout.setWidth("200px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"),
                new Button.ClickListener() {
                    public void buttonClick(Button.ClickEvent event) {
                        LOGGER.debug("Discarding the input values..........");
                        if (!editState) {
                            resetForm();
                            List<Condition> conditionList = new ArrayList<Condition>();
                            changeView("registration.subcorporate.users.heading",
                                    new ManageSubCorporateUsersView(commonRegistrationApp, parentCorporateUser, conditionList), "registration.subcorporate.users.manage.heading");
                        } else {
                            commonRegistrationApp.getMainWindow().removeWindow(addUserForm.getWindow());
                        }
                    }
                });

        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.submit"));
        if (editState) {
            saveButton.setCaption(commonRegistrationApp.getMessage("registration.button.save"));
        }
        saveButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                LOGGER.debug("Creating Subcoporate User Account for [{}] Started.", user.getUsername());
                try {
                    addUserForm.commit();
                    addUserForm.validate();
                    try {
                        saveUserDetails();
                        removeAllComponents();
                        if (editState) {
                            addComponent(CommonUiComponentGenerator.generateSuccessMessage("registration.user.user.edit.success", commonRegistrationApp));
                        } else {
                            addComponent(CommonUiComponentGenerator.generateSuccessMessage("registration.user.user.creation.success", commonRegistrationApp));
                        }
                        addComponent(getSpacer());
                        addComponent(new UserProfileView(user, commonRegistrationApp));
                    } catch (Exception e) {
                        LOGGER.error("Error occurred while creating/updating user account for user" +
                                user.getUsername(), e);
                        generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                                "registration.user.user.creation.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
                    }
                } catch (Validator.InvalidValueException e) {
                    LOGGER.error("Invalid Form Entries", e);
                }
            }
        });
        cancelButton.setStyleName("action-button");
        buttonLayout.addComponent(cancelButton);
        buttonLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_LEFT);
        saveButton.setStyleName("action-button");
        buttonLayout.addComponent(saveButton);
        buttonLayout.setComponentAlignment(saveButton, Alignment.MIDDLE_RIGHT);
        footerLayout.addComponent(buttonLayout);
        footerLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        return footerLayout;
    }

    private void closeWindow() {
        LOGGER.debug("Closing Window ...");
        if (parentWindow != null) {
            parentWindow.onWindowClosed(new HashMap());
        }
    }


    private void resetForm() {
//        Collection itemIds = addUserForm.getItemPropertyIds();
//        for (Object itemId : itemIds) {
//            if(!addUserForm.getField(itemId).isReadOnly()) {
//                addUserForm.getField(itemId).setValue(null);
//            }
//        }
//        //reset check boxes
//        LOGGER.debug("Resetting existing roles");
//        checkBoxLayout.removeAllComponents();
//        checkBoxLayout.addComponent(createCheckBoxLayout());
        //todo implement reset
    }

    /**
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     * @throws DataManipulationException
     */
    private void saveUserDetails() throws NoSuchAlgorithmException, UnsupportedEncodingException, DataManipulationException {

//        HashMap<String, Role> existingRoleHashMap = new HashMap<String, Role>();
//        for (Role role : user.getCorperateParentId().getUserGroup().getRoles()) {
//            existingRoleHashMap.put(role.getName(), role);
//        }
        LOGGER.debug(" selected role set is [{}]", selectedRoleSet);
        LOGGER.debug(" user is [{}] ", user);

        user.setRoles(selectedRoleSet);
        user.setUserStatus(UserStatus.ACTIVE);

        if (!editState) {
            user.setPassword(MD5(user.getPassword()));
            user.setUserType(UserType.SUB_CORPORATE);
            user.setEnabled(true);
            userService.persist(user);
        } else {
            user = (SubCorporateUser) userService.merge(user);
        }

    }

    //    /**
//     * create the layout containing checkboxes to select roles
//     * @return
//     */
    private VerticalLayout createCheckBoxLayout() {
        VerticalLayout checkBoxLayout = new VerticalLayout();
        checkBoxLayout.setWidth("300px");
        checkBoxLayout.setSpacing(true);


        LOGGER.debug(" user name is [{}] ", parentCorporateUser.getUsername());

        final Set<Role> roles = parentCorporateUser.getUserGroup().getRoles();

//        final Set<Role> roles = user.getCorperateParentId().getUserGroup().getRoles();

//        LOGGER.debug(" user name is [{}] ",user.getUsername());
//        LOGGER.debug("  user.getCorperateParentId() [{}] ", user.getCorperateParentId());
//        LOGGER.debug(" user.getCorperateParentId().getUserGroup() [{}] ",user.getCorperateParentId().getUserGroup());
//        LOGGER.debug(" user.getCorperateParentId().getUserGroup().getRoles() [{}]",user.getCorperateParentId().getUserGroup().getRoles());

        final Hashtable<Module, List<Role>> rolesAndModules = new Hashtable<Module, List<Role>>();

        for (final Role role : roles) {
            List<Role> moduleRoleList = rolesAndModules.get(role.getModule());
            boolean notAllowedRole = role.getName().equals("ROLE_REG_ADD_SUB_COOPERATE_USER");

            if (!notAllowedRole) {
                if (moduleRoleList == null) {
                    rolesAndModules.put(role.getModule(), new ArrayList<Role>() {{
                        add(role);
                    }});
                } else {
                    moduleRoleList.add(role);
                }
            }

        }

        Enumeration enumeration = rolesAndModules.keys();
        while (enumeration.hasMoreElements()) {
            Module module = (Module) enumeration.nextElement();
            List<Role> roleList = rolesAndModules.get(module);
            Label moduleName = new Label(module.getModuleName());
            moduleName.setStyleName("header-with-border");
            checkBoxLayout.addComponent(moduleName);
            for (final Role role : roleList) {
                final CheckBox checkBox = new CheckBox(role.getDescription());
                if (editState && existingRolesMap.containsKey(role)) {
                    checkBox.setValue(true);
                }
                if (role.getName().equals("ROLE_REG_LOGIN")) {
                    LOGGER.debug("Registration log in role is added by default");
                    checkBox.setValue(true);
                    checkBox.setEnabled(false);
                    selectedRoleSet.add(role);
                }
                checkBox.setImmediate(true);
                checkBox.addListener(new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent event) {
                        if ((Boolean) event.getProperty().getValue()) {
                            LOGGER.debug("Selected Role Added : " + role.getName());
                            selectedRoleSet.add(role);
                        } else {
                            if (existingRolesMap.containsKey(role)) {
                                LOGGER.debug("Existing Role Marked to be Removed : " + role.getName());
                                existingRolesMap.put(role, false);
                            }
                            if (selectedRoleSet.remove(role)) {
                                LOGGER.debug("Selected Role Removed : " + role.getName());
                            }
                        }
                    }
                });
                checkBoxLayout.addComponent(checkBox);
            }
        }
        return checkBoxLayout;
    }

    private void changeView(String panelHeading,
                            Component component, String subPanelHeading) {
        commonRegistrationApp.getContentLayout().getContentPanel().addComponent(component);
        commonRegistrationApp.getInnerContentPanel().removeAllComponents();
        commonRegistrationApp.getContentLayout().getContentPanel().setCaption(commonRegistrationApp.getMessage(panelHeading));
        commonRegistrationApp.getInnerContentPanel().addComponent(CommonUiComponentGenerator.generateHeading(subPanelHeading, commonRegistrationApp));
        commonRegistrationApp.getInnerContentPanel().addComponent(component);
    }

    @Override
    public void onWindowClosed(Map data) {
        if (parentWindow != null) {
            parentWindow.onWindowClosed(data);
        }
    }

    public SubCorporateUser getUser() {
        return user;
    }
}

class SubCorporateUserFieldFactory implements FormFieldFactory {

    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.user.";

    private AddSubCorporateUserView addSubCorporateUserView;
    private CommonRegistrationApp commonRegistrationApp;

    SubCorporateUserFieldFactory(AddSubCorporateUserView addSubCorporateUserView, CommonRegistrationApp commonRegistrationApp) {
        this.addSubCorporateUserView = addSubCorporateUserView;
        this.commonRegistrationApp = commonRegistrationApp;
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;
        if (USER_NAME.equals(pid)) {
            Field field = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.username.required", commonRegistrationApp);
            field.addValidator(new UserNameCheckValidator(commonRegistrationApp));
            field.setInvalidAllowed(true);
            return field;
        } else if (PASSWORD.equals(pid)) {
            Field passwordField = CommonUiComponentGenerator.createPasswordField(caption, "registration.user.password.required", commonRegistrationApp);
            passwordField.addValidator(new RegexpValidator(PASSWORD_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.password")));
            return passwordField;
        } else if (RETYPE_PASSWORD_PROPERTY_ID.equals(pid)) {
            return CommonUiComponentGenerator.createPasswordField(caption, "registration.user.retype.password.required", commonRegistrationApp);
        } else if (EMAIL.equals(pid)) {
            Field textField = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.email.required", commonRegistrationApp);
            EmailCheckValidator emailValidator = new EmailCheckValidator(commonRegistrationApp);
            emailValidator.setUserId(addSubCorporateUserView.getUser().getUserId());
            textField.addValidator(emailValidator);
            return textField;
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            CommonUiComponentGenerator.filterNullValues(field);
            if (ROLES.equals(pid)) {
                field.setVisible(false);
            }
            return field;
        }
    }
}
