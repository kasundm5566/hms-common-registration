/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.settings;

import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class UserSettingTabSheet extends TabSheet {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserSettingTabSheet.class);

    public UserSettingTabSheet(CommonRegistrationApp commonRegistrationApp) {
        LOGGER.debug("Settings UI  Initialization Started...");

        UserService userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        User user = new User();
        try {
            user = userService.findUserByName(commonRegistrationApp.currentUserName());
        } catch (DataManipulationException e) {
            LOGGER.error("Error while loading logged in user", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.change.password.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }

        // Tab 1 content
        VerticalLayout l1 = new VerticalLayout();
        l1.setSpacing(true);
        l1.setMargin(true);
        l1.addComponent(new UserInfoView(commonRegistrationApp));
        addTab(l1, commonRegistrationApp.getMessage("registration.user.settings.profile"), PROFILE_ICON);

        // Tab 2 content
        if (!user.getDomain().equals("dialog")) {
            VerticalLayout l2 = new VerticalLayout();
            l2.setMargin(true);
            l2.setSpacing(true);
            l2.addComponent(new SecuritySettingLayout(commonRegistrationApp));
            addTab(l2, commonRegistrationApp.getMessage("registration.user.settings.account"), USER_ACCOUNT_ICON);
        }
    }
}