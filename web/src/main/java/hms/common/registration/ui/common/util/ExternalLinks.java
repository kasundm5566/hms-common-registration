/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.common.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ExternalLinks {

    public static String CAS_SERVER_URL;
    public static String PROVISIONING_URL;
    public static String APPSTORE_URL;
    public static String APPSTORE_ADMIN_URL;
    public static String SDP_URL;
    public static String REGISTRATION_URL;
    public static String SOLTURA_URL;
    public static String hsenid_mobile_url;
    public static String REPORTING_URL;
    public static String PGW_ADMIN_CARE_URL;
    public static String PGW_SELF_CARE_URL;
    public static String CAS_LOGIN_URL;
    public static String CAS_LOGOUT_URL;
    public static String HSENID_MOBILE_URL;
    public static String HELP_URL;
    public static String cas_login_url;
    public static String CORPORATE_TERMS_URL;
    public static String CONSUMER_TERMS_URL;

    public void setCAS_SERVER_URL(String CAS_SERVER_URL) {
        ExternalLinks.CAS_SERVER_URL = CAS_SERVER_URL;
    }

    public void setPROVISIONING_URL(String PROVISIONING_URL) {
        ExternalLinks.PROVISIONING_URL = PROVISIONING_URL;
    }

    public void setAPPSTORE_URL(String APPSTORE_URL) {
        ExternalLinks.APPSTORE_URL = APPSTORE_URL;
    }

    public void setSDP_URL(String SDP_URL) {
        ExternalLinks.SDP_URL = SDP_URL;
    }

    public void setREGISTRATION_URL(String REGISTRATION_URL) {
        ExternalLinks.REGISTRATION_URL = REGISTRATION_URL;
    }

    public void setSOLTURA_URL(String SOLTURA_URL) {
        ExternalLinks.SOLTURA_URL = SOLTURA_URL;
    }

    public void setHsenid_mobile_url(String hsenid_mobile_url) {
        ExternalLinks.hsenid_mobile_url = hsenid_mobile_url;
    }

    public void setREPORTING_URL(String REPORTING_URL) {
        ExternalLinks.REPORTING_URL = REPORTING_URL;
    }

    public void setPGW_ADMIN_CARE_URL(String PGW_ADMIN_CARE_URL) {
        ExternalLinks.PGW_ADMIN_CARE_URL = PGW_ADMIN_CARE_URL;
    }

    public void setPGW_SELF_CARE_URL(String PGW_SELF_CARE_URL) {
        ExternalLinks.PGW_SELF_CARE_URL = PGW_SELF_CARE_URL;
    }

    public void setCAS_LOGIN_URL(String CAS_LOGIN_URL) {
        ExternalLinks.CAS_LOGIN_URL = CAS_LOGIN_URL;
    }

    public void setCAS_LOGOUT_URL(String CAS_LOGOUT_URL) {
        ExternalLinks.CAS_LOGOUT_URL = CAS_LOGOUT_URL;
    }

    public void setHSENID_MOBILE_URL(String HSENID_MOBILE_URL) {
        ExternalLinks.HSENID_MOBILE_URL = HSENID_MOBILE_URL;
    }

    public void setHELP_URL(String HELP_URL) {
        ExternalLinks.HELP_URL = HELP_URL;
    }

    public void setCas_login_url(String cas_login_url) {
        ExternalLinks.cas_login_url = cas_login_url;
    }

    public void setCORPORATE_TERMS_URL(String CORPORATE_TERMS_URL) {
        ExternalLinks.CORPORATE_TERMS_URL = CORPORATE_TERMS_URL;
    }

    public void setCONSUMER_TERMS_URL(String CONSUMER_TERMS_URL) {
        ExternalLinks.CONSUMER_TERMS_URL = CONSUMER_TERMS_URL;
    }

    public void setAPPSTORE_ADMIN_URL(String APPSTORE_ADMIN_URL) {
        ExternalLinks.APPSTORE_ADMIN_URL = APPSTORE_ADMIN_URL;
    }
}