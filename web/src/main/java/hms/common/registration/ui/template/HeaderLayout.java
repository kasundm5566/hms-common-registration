/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.template;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.VerticalLayout;
import hms.common.registration.ui.CommonRegistrationApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.common.registration.util.RegistrationCommonUtil.getBranding;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class HeaderLayout extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(HeaderLayout.class);

    private CommonRegistrationApp commonRegistrationApp;

    private void init() {
        String topBannerUrl = "images/top-banner.jpg";

        String branding = getBranding(commonRegistrationApp);

        logger.debug("Current branding [{}]", branding);

        if (branding.equals("ideamart")) {
            topBannerUrl = "images/ideamart-top-banner.jpg";
        } else if (branding.equals("allapps")) {
            topBannerUrl = "images/allapps-top-banner.jpg";
        }

        Embedded topBarImage = new Embedded(null, new ThemeResource(topBannerUrl));
        setWidth("902px");
        setHeight("166px");
        addComponent(topBarImage);
        setSpacing(true);
    }

    public HeaderLayout(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;

        init();
    }

    public void updateTopBanner() {
        removeAllComponents();
        init();
    }
}
