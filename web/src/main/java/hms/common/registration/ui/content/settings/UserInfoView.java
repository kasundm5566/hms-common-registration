/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.settings;

import com.google.common.base.Optional;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.*;
import hms.common.registration.model.DeveloperType;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.validator.EmailCheckValidator;
import hms.common.registration.util.WebUtils;
import hms.common.registration.util.message.Msisdn;
import hms.common.registration.util.message.SmsMessage;
import hms.common.registration.util.sms.SmsMtDispatcher;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.ui.content.settings.UserInfoView.CORPORATE_PARENT_PROPERTY_ID;
import static hms.common.registration.ui.content.user.corporate.CorporateUserForm.*;
import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.util.WebUtils.generateVerificationCode;
import static hms.common.registration.model.UserType.*;
import static hms.common.registration.service.UserService.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;

/**
 * Handles the User Information Management flow
 * <p/>
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class UserInfoView extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(UserInfoView.class);

    private UserService userService;
    private CommonRegistrationApp commonRegistrationApp;
    private User user;
    private VerticalLayout userBasicDetailsLayout;
    private VerticalLayout userContactDetailsLayout;
    private VerticalLayout userOtherDetailsLayout;
    private VerticalLayout companyContactPersonDetailsLayout;
    private VerticalLayout orgDetailsLayout;
    private VerticalLayout subUserDetailsLayout;
    private Button basicDetailsEditButton;
    private Button contactDetailsEditButton;
    private Button otherDetailsEditButton;
    private Button contactPersonDetailsEditButton;
    private Button orgDetailsEditButton;
    private Button beneficiaryDetailEditButton;
    private Button reconciliationDetailEditButton;
    private Label fullName;
    private Label basicData;

    public static final String CORPORATE_PARENT_PROPERTY_ID = "corporateParent";

    public UserInfoView(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        setMargin(true);
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        String userName = commonRegistrationApp.currentUserName();
        try {
            logger.debug("Finding User by username [{}] for My Info View", userName);
            user = userService.findUserByName(userName);
        } catch (Throwable e) {
            logger.debug("Error while finding User by username [{}] for My Info View", userName, e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.user.edit.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
        setImmediate(true);
        createUserInfoView();
    }

    /**
     * create user info view by user type
     */
    private void createUserInfoView() {

        switch (user.getUserType()) {
            case INDIVIDUAL:
                createIndividualUserInfoView();
                break;
            case CORPORATE:
                createCorporateUserInfoView();
                break;
            case SUB_CORPORATE:
                createSubCorporateUserInfoView();
                break;
            case ADMIN_USER:
                createAdminUserInfoView();
                break;
        }
    }

    /**
     * user info view for INDIVIDUAL users
     */
    private void createIndividualUserInfoView() {
        createMemberBasicDetails();
        initUserBasicDetailsForm();
        initUserContactDetailsForm();
        initUserOtherDetailsForm();
    }

    /**
     * user info view for CORPORATE users
     */
    private void createCorporateUserInfoView() {
        final boolean isAnIndividualDeveloper = isMultipleDeveloperAccountTypeEnabled() && DeveloperType.INDIVIDUAL.equals(((CorporateUser) user).getDeveloperType());
        if(!isAnIndividualDeveloper) {
            initOrganizationDetailsForm();
        }

        initUserContactDetailsForm();
        initCompanyContactPersonDetailsForm();

        if (isBeneficiaryAvailable()) {
            initBeneficiaryDetailsForm();
        }

        if(isReconciliationDetailsEnabled()) {
            initReconciliationDetails();
        }
    }

    /**
     * user info view for SUB_CORPORATE users
     */
    private void createSubCorporateUserInfoView() {
        initSubCorporateUserForm();
    }

    /**
     * user info view for ADMIN users
     */
    private void createAdminUserInfoView() {
        createMemberBasicDetails();
        initUserBasicDetailsForm();
        initUserContactDetailsForm();
    }

    /**
     * Create top heading with member basic details
     */
    private void createMemberBasicDetails() {
        fullName = new Label(user.getFirstName() + " " + user.getLastName());
        fullName.setStyleName("h1");
        HorizontalLayout memberDetails = new HorizontalLayout();
        memberDetails.setImmediate(true);
        memberDetails.setSpacing(true);

        DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        String createdDate = formatter.format(user.getCreatedTime());

        Label userCreatedDate = new Label(commonRegistrationApp.getMessage("registration.settings.member.details") + " " + createdDate);
        userCreatedDate.setStyleName("small-text-description");
        addComponent(fullName);
        if (user.getUserType().equals(INDIVIDUAL)) {
            basicData = new Label("(" + user.getFirstName() + ")  - " + WebUtils.getAge(user.getBirthday()));
            memberDetails.addComponent(basicData);
        }
        memberDetails.addComponent(userCreatedDate);
        addComponent(memberDetails);
        addComponent(getSpacer());
    }

    /**
     * Initializes user basic details form
     */
    private void initUserBasicDetailsForm() {
        userBasicDetailsLayout = new VerticalLayout();
        userBasicDetailsLayout.setMargin(true);
        final Form basicInfo = new Form();
        basicInfo.setImmediate(true);
        basicInfo.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        basicInfo.setItemDataSource(beanItem);
        if (user.getUserType().equals(ADMIN_USER)) {
            basicInfo.setVisibleItemProperties(new String[]{
                    FIRST_NAME, LAST_NAME, PROFESSION});
        } else if (user.getUserType().equals(INDIVIDUAL)) {
            basicInfo.setVisibleItemProperties(new String[]{
                    FIRST_NAME, GENDER, PROFESSION,
                    BIRTHDAY, DISPLAY_NAME});
        } else {
            basicInfo.setVisibleItemProperties(new String[]{
                    FIRST_NAME, LAST_NAME, GENDER, PROFESSION,
                    BIRTHDAY, DISPLAY_NAME});
        }
        basicInfo.setReadOnly(true);
        setReadOnlyBehaviourForBasicInfo(basicInfo);
        basicInfo.setWriteThrough(false);
        HorizontalLayout headingLayout = generateBasicDetailsHeaderLayout(basicInfo);
        addComponent(headingLayout);
        userBasicDetailsLayout.addComponent(basicInfo);
        addComponent(userBasicDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    /**
     * Initializes user other details form
     */
    private void initUserOtherDetailsForm() {
        userOtherDetailsLayout = new VerticalLayout();
        final Form otherInfo = new Form();
        otherInfo.setImmediate(true);
        otherInfo.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        otherInfo.setItemDataSource(beanItem);
        if (user.getUserType() == INDIVIDUAL) {
            otherInfo.setVisibleItemProperties(new String[]{
                    OPERATOR, PHONE_NUMBER});
        } else {
            otherInfo.setVisibleItemProperties(new String[]{
                    OPERATOR, PHONE_NUMBER, PHONE_TYPE, PHONE_MODEL});
        }
        otherInfo.setReadOnly(true);
        setReadOnlyBehaviourForOtherInfo(otherInfo);
        otherInfo.setWriteThrough(false);
        HorizontalLayout headingLayout = generateOtherDetailsHeaderLayout(otherInfo);
        addComponent(headingLayout);
        userOtherDetailsLayout.addComponent(otherInfo);
        addComponent(userOtherDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    /**
     * Initialize coporate user organization details form
     */
    private void initOrganizationDetailsForm() {
        orgDetailsLayout = new VerticalLayout();
        final Form orgInfoForm = new Form();
        orgInfoForm.setImmediate(true);
        orgInfoForm.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        beanItem.addItemProperty(INDUSTRY_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(ORGANIZATION_NAME_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(ORGANIZATION_SIZE_PROPERTY_ID, new ObjectProperty(null, String.class));
        orgInfoForm.setItemDataSource(beanItem);
        orgInfoForm.setVisibleItemProperties(new String[]{
                ORGANIZATION_NAME_PROPERTY_ID, ORGANIZATION_SIZE_PROPERTY_ID, INDUSTRY_PROPERTY_ID});
        orgInfoForm.setReadOnly(true);
        setReadOnlyBehaviourForOrgInfo(orgInfoForm);
        orgInfoForm.setWriteThrough(false);
        HorizontalLayout headingLayout = generateOrgDetailsHeaderLayout(orgInfoForm);
        addComponent(headingLayout);
        orgDetailsLayout.addComponent(orgInfoForm);
        addComponent(orgDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    private void initBeneficiaryDetailsForm() {
        companyContactPersonDetailsLayout = new VerticalLayout();
        final Form beneficiaryInfoForm = new Form();
        beneficiaryInfoForm.setImmediate(true);
        beneficiaryInfoForm.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        beanItem.addItemProperty(BENEFICIARY_NAME, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(BANK_CODE, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(BANK_BRANCH_CODE, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(BANK_BRANCH_NAME, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(BANK_ACCOUNT_NUMBER, new ObjectProperty(null, String.class));

        beneficiaryInfoForm.setItemDataSource(beanItem);
        beneficiaryInfoForm.setVisibleItemProperties(new String[]{
                BENEFICIARY_NAME, BANK_CODE, BANK_BRANCH_CODE, BANK_BRANCH_NAME, BANK_ACCOUNT_NUMBER});
        beneficiaryInfoForm.setReadOnly(true);
        setReadOnlyBehaviourForBeneficiaryInfo(beneficiaryInfoForm);
        beneficiaryInfoForm.setWriteThrough(false);
        HorizontalLayout headingLayout = generateBeneficiaryDetailsHeaderLayout(beneficiaryInfoForm);
        addComponent(headingLayout);
        companyContactPersonDetailsLayout.addComponent(beneficiaryInfoForm);
        addComponent(companyContactPersonDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    private void initReconciliationDetails() {

        final boolean isIndividualDeveloper = DeveloperType.INDIVIDUAL.equals(((CorporateUser) user).getDeveloperType());

        String[] propertyIds = isIndividualDeveloper ? new String[]{TIN} : new String[]{TIN, BUSINESS_LICENSE_NUMBER, BUSINESS_ID};

        companyContactPersonDetailsLayout = new VerticalLayout();
        final Form reconciliationDetailsForm = new Form();
        reconciliationDetailsForm.setImmediate(true);
        reconciliationDetailsForm.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);

        for (String propertyId : propertyIds) {
            beanItem.addItemProperty(propertyId, new ObjectProperty(null, String.class));
        }

        reconciliationDetailsForm.setItemDataSource(beanItem);
        reconciliationDetailsForm.setVisibleItemProperties(propertyIds);
        reconciliationDetailsForm.setReadOnly(true);
        setReadOnlyBehaviourForReconciliationInfo(reconciliationDetailsForm);
        reconciliationDetailsForm.setWriteThrough(false);
        HorizontalLayout headingLayout = generateReconciliationDetailsHeaderLayout(reconciliationDetailsForm);
        addComponent(headingLayout);
        companyContactPersonDetailsLayout.addComponent(reconciliationDetailsForm);
        addComponent(companyContactPersonDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    /**
     * Initializes sub corporate user details form
     */
    private void initSubCorporateUserForm() {
        subUserDetailsLayout = new VerticalLayout();
        subUserDetailsLayout.setMargin(true);
        final Form corporateUserInfoForm = new Form();
        corporateUserInfoForm.setImmediate(true);
        corporateUserInfoForm.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        beanItem.addItemProperty(CORPORATE_PARENT_PROPERTY_ID, new ObjectProperty(null, String.class));
        corporateUserInfoForm.setItemDataSource(beanItem);
        corporateUserInfoForm.setVisibleItemProperties(new String[]{
                USER_NAME, EMAIL, CORPORATE_PARENT_PROPERTY_ID});
        corporateUserInfoForm.getField(CORPORATE_PARENT_PROPERTY_ID).setValue(user.getCorperateParentId().getUsername());
        corporateUserInfoForm.setReadOnly(true);
        corporateUserInfoForm.setWriteThrough(false);
        HorizontalLayout headingLayout = generateHeader("registration.settings.personal.details.heading");
        addComponent(headingLayout);
        subUserDetailsLayout.addComponent(corporateUserInfoForm);
        addComponent(subUserDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    /**
     * Initializes user contact details form
     */
    private void initUserContactDetailsForm() {
        userContactDetailsLayout = new VerticalLayout();
        final Form contactInfo = new Form();
        contactInfo.setImmediate(true);
        contactInfo.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        if (user.isCorporateUser()) {
            beanItem.addItemProperty(ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(ORGANIZATION_FAX_NO_PROPERTY_ID, new ObjectProperty(null, String.class));
        }
        contactInfo.setItemDataSource(beanItem);
        if (user.isCorporateUser()) {
            contactInfo.setVisibleItemProperties(new String[]{
                    COUNTRY, PROVINCE, CITY, POST_CODE, ADDRESS, EMAIL, ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID, ORGANIZATION_FAX_NO_PROPERTY_ID});
        } else if (user.getUserType().equals(ADMIN_USER)) {
            contactInfo.setVisibleItemProperties(new String[]{
                    ADDRESS, EMAIL, PHONE_NUMBER});
        } else {
            contactInfo.setVisibleItemProperties(new String[]{
                    COUNTRY, PROVINCE, CITY, POST_CODE, ADDRESS, EMAIL});
        }
        contactInfo.setReadOnly(true);
        setReadOnlyBehaviourForContactInfo(contactInfo);
        contactInfo.setWriteThrough(false);
        HorizontalLayout headingLayout = generateContactDetailsHeaderLayout(contactInfo);
        addComponent(headingLayout);
        userContactDetailsLayout.addComponent(contactInfo);
        addComponent(userContactDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    /**
     * Initilize contact person details
     */
    private void initCompanyContactPersonDetailsForm() {
        companyContactPersonDetailsLayout = new VerticalLayout();
        final Form contactPersonInfoForm = new Form();
        contactPersonInfoForm.setImmediate(true);
        contactPersonInfoForm.setFormFieldFactory(new BasicFormFieldFactory(user, commonRegistrationApp));
        final BeanItem<User> beanItem = new BeanItem<User>(user);
        beanItem.addItemProperty(CONTACT_PERSON_NAME_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(PHONE_NUMBER, new ObjectProperty(null, String.class));
        contactPersonInfoForm.setItemDataSource(beanItem);
        contactPersonInfoForm.setVisibleItemProperties(new String[]{
                CONTACT_PERSON_NAME_PROPERTY_ID, PHONE_NUMBER});
        contactPersonInfoForm.setReadOnly(true);
        setReadOnlyBehaviourForContactPersonInfo(contactPersonInfoForm);
        contactPersonInfoForm.setWriteThrough(false);
        HorizontalLayout headingLayout = generateContactPersonDetailsHeaderLayout(contactPersonInfoForm);
        addComponent(headingLayout);
        companyContactPersonDetailsLayout.addComponent(contactPersonInfoForm);
        addComponent(companyContactPersonDetailsLayout);
        addComponent(getSpacer());
        addComponent(getSpacer());
    }

    /**
     * header layout for organization details
     *
     * @param orgInfoForm
     * @return
     */
    private HorizontalLayout generateOrgDetailsHeaderLayout(final Form orgInfoForm) {
        HorizontalLayout headingLayout = generateHeader("registration.settings.organization.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, orgInfoForm, saveButton, orgDetailsEditButton);
                setReadOnlyBehaviourForOrgInfo(orgInfoForm);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                logger.debug("Updating Coporate User Organization Details Started....");
                handleUserInfoUpdateRequest(clickEvent, orgInfoForm, cancelButton, orgDetailsEditButton);
                setReadOnlyBehaviourForOrgInfo(orgInfoForm);
            }
        });
        orgDetailsEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, orgInfoForm, saveButton, cancelButton);
                setReadOnlyBehaviourForOrgInfo(orgInfoForm);
            }
        });
        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        orgDetailsEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(orgDetailsEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }

    /**
     * Generates Heading for user basic details form
     *
     * @param basicInfo
     * @return
     */
    private HorizontalLayout generateBasicDetailsHeaderLayout(final Form basicInfo) {
        HorizontalLayout headingLayout = generateHeader("registration.settings.personal.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, basicInfo, saveButton, basicDetailsEditButton);
                setReadOnlyBehaviourForBasicInfo(basicInfo);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                logger.debug("Updating User Basic Details Started....");
                handleUserInfoUpdateRequest(clickEvent, basicInfo, cancelButton, basicDetailsEditButton);
                if (user.getUserType().equals(INDIVIDUAL)) {
                    fullName.setValue(user.getFirstName());
                    basicData.setValue("(" + user.getFirstName() + ")  - " + WebUtils.getAge(user.getBirthday())
                            + " " + (user.getGender() != null ? user.getGender() : ""));
                } else {
                    fullName.setValue(user.getFirstName() + " " + user.getLastName());
                }
                setReadOnlyBehaviourForBasicInfo(basicInfo);
            }
        });
        basicDetailsEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, basicInfo, saveButton, cancelButton);
                setReadOnlyBehaviourForBasicInfo(basicInfo);
            }
        });

        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        basicDetailsEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(basicDetailsEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }

    /**
     * header layout for company contact person details
     *
     * @param contactPersonInfoForm
     * @return
     */
    private HorizontalLayout generateContactPersonDetailsHeaderLayout(final Form contactPersonInfoForm) {
        HorizontalLayout headingLayout = generateHeader("registration.settings.org.contact.person.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, contactPersonInfoForm, saveButton, contactPersonDetailsEditButton);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                logger.debug("Updating Contact Person Details Started....");
                handleUserInfoUpdateRequest(clickEvent, contactPersonInfoForm, cancelButton, contactPersonDetailsEditButton);
                setReadOnlyBehaviourForContactPersonInfo(contactPersonInfoForm);
            }
        });
        contactPersonDetailsEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, contactPersonInfoForm, saveButton, cancelButton);

                UserType userType = user.getUserType();
                String domain = user.getDomain();

                if ((userType.equals(CORPORATE) && isCorporateUserMsisdnVerifyEnabled())
                        || ("dialog").equals(domain)) {
                    contactPersonInfoForm.getField(PHONE_NUMBER).setEnabled(false);
                }
                if (userType.equals(CORPORATE) && ("dialog").equals(domain)) {
                    contactPersonInfoForm.getField(CONTACT_PERSON_NAME_PROPERTY_ID).setEnabled(false);
                }
                setReadOnlyBehaviourForContactPersonInfo(contactPersonInfoForm);
            }
        });

        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        contactPersonDetailsEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(contactPersonDetailsEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }

    private HorizontalLayout generateBeneficiaryDetailsHeaderLayout(final Form beneficiaryInfoForm) {
        HorizontalLayout headingLayout = generateHeader("registration.user.beneficiary.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, beneficiaryInfoForm, saveButton, beneficiaryDetailEditButton);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                logger.debug("Updating Beneficiary Details Started....");
                handleUserInfoUpdateRequest(clickEvent, beneficiaryInfoForm, cancelButton, beneficiaryDetailEditButton);
                setReadOnlyBehaviourForBeneficiaryInfo(beneficiaryInfoForm);
            }
        });
        beneficiaryDetailEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, beneficiaryInfoForm, saveButton, cancelButton);
                setReadOnlyBehaviourForBeneficiaryInfo(beneficiaryInfoForm);
            }
        });

        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        beneficiaryDetailEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(beneficiaryDetailEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }


    private HorizontalLayout generateReconciliationDetailsHeaderLayout(final Form reconciliationDetailsForm) {
        HorizontalLayout headingLayout = generateHeader("registration.user.reconciliation.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, reconciliationDetailsForm, saveButton, reconciliationDetailEditButton);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoUpdateRequest(clickEvent, reconciliationDetailsForm, cancelButton, reconciliationDetailEditButton);
                setReadOnlyBehaviourForReconciliationInfo(reconciliationDetailsForm);
            }
        });
        reconciliationDetailEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, reconciliationDetailsForm, saveButton, cancelButton);
                reconciliationDetailsForm.getField(TIN).setEnabled(false);
                reconciliationDetailsForm.getField(BUSINESS_LICENSE_NUMBER).setEnabled(false);
                setReadOnlyBehaviourForReconciliationInfo(reconciliationDetailsForm);
            }
        });

        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        reconciliationDetailEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(reconciliationDetailEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }

    /**
     * Generates Heading for user contact details form
     *
     * @param contactInfo
     * @return
     */
    private HorizontalLayout generateContactDetailsHeaderLayout(final Form contactInfo) {
        HorizontalLayout headingLayout = generateHeader("registration.settings.contact.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, contactInfo, saveButton, contactDetailsEditButton);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                logger.debug("Updating User Contact Details Started....");
                handleUserInfoUpdateRequest(clickEvent, contactInfo, cancelButton, contactDetailsEditButton);
                setReadOnlyBehaviourForContactInfo(contactInfo);
            }
        });
        contactDetailsEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, contactInfo, saveButton, cancelButton);

                UserType userType = user.getUserType();
                String domain = user.getDomain();

                if ((userType.equals(CORPORATE) && isCorporateUserEmailVerifyEnabled())
                        || (userType.equals(INDIVIDUAL) && isConsumerUserEmailVerifyEnabled())
                        || ("dialog").equals(domain)) {
                    contactInfo.getField(EMAIL).setEnabled(false);
                }

                setReadOnlyBehaviourForContactInfo(contactInfo);
            }
        });

        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        contactDetailsEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(contactDetailsEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }

    /**
     * Generates Heading for user other details form
     *
     * @param otherInfo
     * @return
     */
    private HorizontalLayout generateOtherDetailsHeaderLayout(final Form otherInfo) {
        HorizontalLayout headingLayout = generateHeader("registration.settings.other.details.heading");
        headingLayout.setSizeFull();
        HorizontalLayout rightLayout = new HorizontalLayout();
        rightLayout.setWidth("250px");
        final Button cancelButton = new Button(commonRegistrationApp.getMessage("registration.button.cancel"));
        final Button saveButton = new Button(commonRegistrationApp.getMessage("registration.button.save"));
        cancelButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoCancelRequest(clickEvent, otherInfo, saveButton, otherDetailsEditButton);
                setReadOnlyBehaviourForOtherInfo(otherInfo);
            }
        });
        saveButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                logger.debug("Updating User Other Details Started....");
                String previousNumber = user.getMobileNo();
                String previousOperator = user.getOperator();
                if (user.getUserType() == INDIVIDUAL) {
                    try {
                        boolean newPhnNumberEntered = previousNumber != otherInfo.getField(PHONE_NUMBER).getValue()
                                || previousOperator != otherInfo.getField(OPERATOR).getValue();
                        if (newPhnNumberEntered) {
                            String msisdnVerificationCode = generateVerificationCode();
                            try {
                                otherInfo.commit();
                                otherInfo.validate();
                                user.setMsisdnVerificationCode(msisdnVerificationCode);
                                user.setMsisdnVerified(false);
                                sendMessage(commonRegistrationApp.getMessage("system.default.sender.address"), user.getMobileNo(), user.getOperator().toLowerCase(),
                                        MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, msisdnVerificationCode));
                                user = userService.merge(user);
                                otherInfo.setReadOnly(true);
                                clickEvent.getButton().setVisible(false);
                                cancelButton.setVisible(false);
                                otherDetailsEditButton.setVisible(true);
                                logger.debug("Updating User Account Details Finished....");
                            } catch (DataManipulationException e) {
                                logger.error("{0}", e);
                            }

                            Window newNumberWindow = generateChangeMsisdnNotificationWindow();
                            getWindow().addWindow(newNumberWindow);
                        } else {
                            otherInfo.setReadOnly(true);
                            clickEvent.getButton().setVisible(false);
                            cancelButton.setVisible(false);
                            otherDetailsEditButton.setVisible(true);
                        }
                    } catch (Validator.InvalidValueException e) {
                        logger.error("Error occur while validating - ", e.getMessage());
                    }

                } else {
                    handleUserInfoUpdateRequest(clickEvent, otherInfo, cancelButton, otherDetailsEditButton);
                }
                setReadOnlyBehaviourForOtherInfo(otherInfo);
            }
        });
        otherDetailsEditButton = new Button(commonRegistrationApp.getMessage("registration.button.edit"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                handleUserInfoEditRequest(clickEvent, otherInfo, saveButton, cancelButton);
                setReadOnlyBehaviourForOtherInfo(otherInfo);
            }
        });

        HorizontalLayout saveLayout = new HorizontalLayout();
        HorizontalLayout editLayout = new HorizontalLayout();
        saveButton.setStyleName("action-button");
        cancelButton.setStyleName("action-button");
        otherDetailsEditButton.setStyleName("action-button");
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        editLayout.setWidth("75px");
        saveLayout.setWidth("150px");
        saveLayout.addComponent(saveButton);
        saveLayout.addComponent(cancelButton);
        rightLayout.addComponent(saveLayout);
        rightLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);
        editLayout.addComponent(otherDetailsEditButton);
        rightLayout.addComponent(editLayout);
        rightLayout.setComponentAlignment(editLayout, Alignment.BOTTOM_RIGHT);
        headingLayout.addComponent(rightLayout);
        headingLayout.setComponentAlignment(rightLayout, Alignment.BOTTOM_RIGHT);
        return headingLayout;
    }

    private Window generateChangeMsisdnNotificationWindow() {
        final Window changeMsisdnNotificationWindow = new Window(commonRegistrationApp.getMessage("registration.change.msisdn.notification.window.caption"));
        changeMsisdnNotificationWindow.setResizable(false);
        changeMsisdnNotificationWindow.setModal(true);
        changeMsisdnNotificationWindow.setStyleName("opaque");
        changeMsisdnNotificationWindow.setWidth("400px");
        changeMsisdnNotificationWindow.setHeight("150px");
        changeMsisdnNotificationWindow.center();
        changeMsisdnNotificationWindow.setImmediate(true);

        GridLayout changeMsisdnNotification = new GridLayout(2, 2);
        changeMsisdnNotification.setWidth("350px");
        changeMsisdnNotification.setSpacing(true);
        changeMsisdnNotification.addComponent(new Label(commonRegistrationApp.getMessage("registration.change.msisdn.notification.window.message")), 0, 0, 1, 0);

        Button acceptButton = new Button(commonRegistrationApp.getMessage("registration.change.msisdn.notification.window.accept.button"));
        acceptButton.setStyleName("action-button");

        acceptButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String path = commonRegistrationApp.getURL().getPath();
                getWindow().open(new ExternalResource(path + "j_spring_security_logout"));
            }
        });

        changeMsisdnNotification.addComponent(acceptButton, 1, 1);
        changeMsisdnNotificationWindow.addComponent(changeMsisdnNotification);

        return changeMsisdnNotificationWindow;
    }

    /**
     * Handles user info save request
     *
     * @param clickEvent
     * @param form
     * @param cancelButton
     * @param editButton
     */
    private void handleUserInfoUpdateRequest(Button.ClickEvent clickEvent, Form form, Button cancelButton, Button
            editButton) {
        try {
            form.commit();
            form.validate();
            userService.merge(user);
            // To avoid data stale : reload the user object
            user = userService.findUserByUserId(user.getUserId());
            form.setReadOnly(true);
            clickEvent.getButton().setVisible(false);
            cancelButton.setVisible(false);
            editButton.setVisible(true);
            logger.debug("Updating User Account Details Finished....");
        } catch (Validator.InvalidValueException e) {
            logger.debug("Invalid form entries [{}]", e.getMessage());
        } catch (Exception e) {
            logger.error("Error occured while updating user account details for user ", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.user.edit.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
    }

    /**
     * Handles user info edit request
     *
     * @param clickEvent
     * @param form
     * @param saveButton
     * @param cancelButton
     */
    private void handleUserInfoEditRequest(Button.ClickEvent clickEvent, Form form, Button saveButton,
                                           Button cancelButton) {
        try {
            reloadForm(form);
            form.setReadOnly(false);
            clickEvent.getButton().setVisible(false);
            saveButton.setVisible(true);
            cancelButton.setVisible(true);
        } catch (Exception e) {
            logger.error("Unexpected error occurred", e);
            generateSystemNotificationMessage(commonRegistrationApp.getMainWindow(),
                    "registration.user.user.edit.error", Window.Notification.TYPE_ERROR_MESSAGE, commonRegistrationApp);
        }
    }


    private void reloadForm(Form form) throws DataManipulationException {
        user = userService.findUserById(user.getId());
        Collection visibleProperties = form.getVisibleItemProperties();
        BeanItem beanItem = new BeanItem<User>(user);
        form.setItemDataSource(beanItem);
        form.setVisibleItemProperties(visibleProperties);
        BasicFormFieldFactory formFieldFactory = (BasicFormFieldFactory) form.getFormFieldFactory();
        formFieldFactory.setUser(user);
    }

    /**
     * Handles user info cancel request
     *
     * @param clickEvent
     * @param form
     * @param saveButton
     * @param editButton
     */
    private void handleUserInfoCancelRequest(Button.ClickEvent clickEvent, Form form, Button saveButton,
                                             Button editButton) {
        form.discard();
        form.setReadOnly(true);
        clickEvent.getButton().setVisible(false);
        saveButton.setVisible(false);
        editButton.setVisible(true);
    }

    /**
     * Generates common header information
     *
     * @param header
     * @return
     */
    private HorizontalLayout generateHeader(String header) {
        HorizontalLayout headingLayout = new HorizontalLayout();
        headingLayout.setSizeFull();
        headingLayout.setStyleName("header-with-border");
        final Label basicDetailsHeading = new Label(commonRegistrationApp.getMessage(header));
        headingLayout.addComponent(basicDetailsHeading);
        return headingLayout;
    }

    private void setReadOnlyBehaviourForBasicInfo(Form basicInfo) {
        if (user.getUserType().equals(ADMIN_USER)) {
            filterReadOnlyFields(basicInfo, Arrays.<Object>asList(FIRST_NAME, LAST_NAME, PROFESSION));
        } else if (user.getUserType() == INDIVIDUAL) {
            filterReadOnlyFields(basicInfo, Arrays.<Object>asList(FIRST_NAME, GENDER, PROFESSION, BIRTHDAY));
        } else {
            filterReadOnlyFields(basicInfo, Arrays.<Object>asList(FIRST_NAME, LAST_NAME, GENDER, PROFESSION, BIRTHDAY));
        }
    }

    private void setReadOnlyBehaviourForOtherInfo(Form otherInfo) {
        if (user.getUserType() == INDIVIDUAL) {
            filterReadOnlyFields(otherInfo, Arrays.<Object>asList(OPERATOR, PHONE_NUMBER));
        } else {
            filterReadOnlyFields(otherInfo, Arrays.<Object>asList(OPERATOR, PHONE_NUMBER, PHONE_TYPE, PHONE_MODEL));
        }
    }

    private void setReadOnlyBehaviourForOrgInfo(Form orgInfoForm) {
        filterReadOnlyFields(orgInfoForm, Arrays.<Object>asList(ORGANIZATION_NAME_PROPERTY_ID, ORGANIZATION_SIZE_PROPERTY_ID, INDUSTRY_PROPERTY_ID));
    }

    private void setReadOnlyBehaviourForContactInfo(Form contactInfo) {
        if (user.getUserType().equals(ADMIN_USER)) {
            filterReadOnlyFields(contactInfo, Arrays.<Object>asList(ADDRESS, EMAIL, PHONE_NUMBER));
        } else {
            filterReadOnlyFields(contactInfo, Arrays.<Object>asList(COUNTRY, PROVINCE, CITY, ADDRESS, EMAIL));
        }
    }

    private void setReadOnlyBehaviourForContactPersonInfo(Form contactPersonInfoForm) {
        filterReadOnlyFields(contactPersonInfoForm, Arrays.<Object>asList(
                CONTACT_PERSON_NAME_PROPERTY_ID, PHONE_NUMBER));
    }

    private void setReadOnlyBehaviourForBeneficiaryInfo(Form beneficiaryInfo) {
        filterReadOnlyFields(beneficiaryInfo, Arrays.<Object>asList(
                BENEFICIARY_NAME, BANK_CODE, BANK_BRANCH_CODE, BANK_BRANCH_NAME, BANK_ACCOUNT_NUMBER));
    }

    private void setReadOnlyBehaviourForReconciliationInfo(Form reconciliationDetailsForm) {
        filterReadOnlyFields(reconciliationDetailsForm, Arrays.<Object>asList(
                TIN, BUSINESS_LICENSE_NUMBER, BUSINESS_ID));
    }

    /**
     * Manually changing the combo boxed read mandatory setting due to the bug in Vaadin
     *
     * @param form
     * @param fieldEntries
     */
    private void filterReadOnlyFields(Form form, List<Object> fieldEntries) {
        for (Object pid : fieldEntries) {
            Field field = form.getField(pid);

            if(!Optional.fromNullable(field).isPresent()) {
                continue;
            }

            if (form.isReadOnly()) {
                field.setRequired(false);
            } else {
                field.setRequired(field.isRequired());
            }
        }
    }

    private void sendMessage(String senderAddress, String receiverAddress, String operator,
                             String message) throws DataManipulationException {
        SmsMessage verificationSms = new SmsMessage();
        verificationSms.setSenderAddress(new Msisdn(senderAddress, operator));
        verificationSms.setReceiverAddress(new Msisdn(receiverAddress, operator));
        verificationSms.setMessage(message);
        logger.debug("MSISDN verification process started......" + verificationSms);
        SmsMtDispatcher smsMtDispatcher = (SmsMtDispatcher) commonRegistrationApp.getBean("smsMtDispatcher");
        smsMtDispatcher.sendIndividualMessage(verificationSms);
        user.setLastSmsSendDate(new Date());
    }
}

/**
 * UI Fields Generator for My Profile View
 */
class BasicFormFieldFactory implements FormFieldFactory {

    private static final Logger logger = LoggerFactory.getLogger(BasicFormFieldFactory.class);

    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.user.";
    private ComboBox phoneModels;
    private User user;
    private CommonRegistrationApp commonRegistrationApp;

    BasicFormFieldFactory(User user, CommonRegistrationApp commonRegistrationApp) {
        this.user = user;
        this.commonRegistrationApp = commonRegistrationApp;
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;
        if (COUNTRY.equals(pid)) {
            return createComboBox(caption, true, "registration.user.country.required",
                    SUPPORTED_COUNTRIES, commonRegistrationApp);
        } else if (PROVINCE.equals(pid)) {
            return createComboBox(caption, true, "registration.user.province.required",
                    SUPPORTED_PROVINCES, commonRegistrationApp);
        } else if (GENDER.equals(pid)) {
            return createComboBox(caption, true, "registration.user.gender.required",
                    GENDER_SELECTIONS, commonRegistrationApp);
        } else if (OPERATOR.equals(pid)) {
            return createComboBox(caption, true, "registration.user.operator.required",
                    SUPPORTED_OPERATORS, commonRegistrationApp);
        } else if (CITY.equals(pid)) {
            return createComboBox(caption, true, "registration.user.city.required",
                    SUPPORTED_CITIES, commonRegistrationApp);
        } else if (PHONE_TYPE.equals(pid)) {
            final Field field = createComboBox(caption, true, "registration.user.phone.type.required",
                    new ArrayList<String>(SUPPORTED_PHONE_MODELS.keySet()), commonRegistrationApp);
            field.addListener(new Property.ValueChangeListener() {
                public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                    if (valueChangeEvent.getProperty().getClass().equals(ComboBox.class)) {
                        Object value = valueChangeEvent.getProperty().getValue();
                        if (value != null) {
                            String selectedPhoneModel = value.toString();
                            String phoneModel = (String) phoneModels.getValue();
                            phoneModels.removeAllItems();
                            logger.debug("Phone type is [{}]", selectedPhoneModel);
                            logger.debug("User Phone type is [{}]", user.getPhoneType());
                            logger.debug("Setting Phone models [{}] ", SUPPORTED_PHONE_MODELS.get(selectedPhoneModel));
                            for (String s : SUPPORTED_PHONE_MODELS.get(selectedPhoneModel)) {
                                phoneModels.addItem(s);
                            }
                            phoneModels.setValue(phoneModel);
                        }
                    }
                }
            });
            return field;
        } else if (PHONE_MODEL.equals(pid)) {
            List<String> phoneModelsList = SUPPORTED_PHONE_MODELS.get(user.getPhoneType());
            if (phoneModelsList == null) {
                phoneModelsList = new ArrayList<String>();
            }
            phoneModels = (ComboBox) createComboBox(caption, true, "registration.user.phone.model.required", phoneModelsList, commonRegistrationApp);
            return phoneModels;
        } else if (ADDRESS.equals(pid)) {
            return CommonUiComponentGenerator.createTextArea(caption, true, "registration.user.address.required", commonRegistrationApp);
        } else if (FIRST_NAME.equals(pid)) {
            return CommonUiComponentGenerator.createTextField(caption, true, "registration.user.first.name.required", commonRegistrationApp);
        } else if (LAST_NAME.equals(pid)) {
            return CommonUiComponentGenerator.createTextField(caption, true, "registration.user.last.name.required", commonRegistrationApp);
        } else if (PROFESSION.equals(pid)) {
            return createComboBox(caption, true, "registration.user.profession.required",
                    PROFESSIONS, commonRegistrationApp);
        } else if (EMAIL.equals(pid)) {
            Field textField = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.email.required", commonRegistrationApp);
            EmailCheckValidator emailCheckValidator = new EmailCheckValidator(commonRegistrationApp);
            emailCheckValidator.setUserId(user.getUserId());
            textField.addValidator(emailCheckValidator);
            return textField;
        } else if (POST_CODE.equals(pid)) {
            return CommonUiComponentGenerator.createTextField(caption, false, null, commonRegistrationApp);
        } else if (PHONE_NUMBER.equals(pid)) {
            Field field = CommonUiComponentGenerator.createTextField(caption, true, "registration.user.msisdn.required", commonRegistrationApp);
            field.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));
            return field;
        } else if (ORGANIZATION_NAME_PROPERTY_ID.equals(pid)) {
            return createTextField(caption, true, "registration.user.organization.name.required", commonRegistrationApp);
        } else if (INDUSTRY_PROPERTY_ID.equals(pid)) {
            return createComboBox(caption, true, "registration.user.industry.required",
                    INDUSTRIES, commonRegistrationApp);
        } else if (ORGANIZATION_SIZE_PROPERTY_ID.equals(pid)) {
            return createComboBox(caption, true, "registration.user.organization.size.required",
                    ORGANIZATION_SIZE, commonRegistrationApp);
        } else if (CONTACT_PERSON_NAME_PROPERTY_ID.equals(pid)) {
            return createTextField(caption, true, "registration.user.organization.contact.person.name.required", commonRegistrationApp);
        } else if (CONTACT_PERSON_EMAIL_PROPERTY_ID.equals(pid)) {
            TextField textField = (TextField) createTextField(caption, true, "registration.user.organization.contact.person.email.required", commonRegistrationApp);
            textField.addValidator(new EmailValidator(commonRegistrationApp.getMessage("registration.user.invalid.email")));
            return textField;
        } else if (CONTACT_PERSON_PHONE_NO_PROPERTY_ID.equals(pid)) {
            TextField textField = (TextField) createTextField(caption, true, "registration.user.organization.contact.person.phone.no.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));
            return textField;
        } else if (ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID.equals(pid)) {
            TextArea phoneNos = (TextArea) createTextArea(caption, true, "registration.user.organization.phone.no.required", commonRegistrationApp);
            phoneNos.setColumns(15);
            return phoneNos;
        } else if (ORGANIZATION_FAX_NO_PROPERTY_ID.equals(pid)) {
            TextArea faxNos = (TextArea) createTextArea(caption, false, "", commonRegistrationApp);
            faxNos.setColumns(15);
            return faxNos;
        } else if (CORPORATE_PARENT_PROPERTY_ID.equals(pid)) {
            return createTextField(caption, false, "", commonRegistrationApp);
        } else if (BENEFICIARY_NAME.equals((pid))) {
            return createTextField(caption, true, "registration.user.beneficiaryName.required", commonRegistrationApp);
        } else if (BANK_CODE.equals((pid))) {
            return createTextField(caption, true, "registration.user.bankCode.required", commonRegistrationApp);
        } else if (BANK_BRANCH_NAME.equals((pid))) {
            return createTextField(caption, true, "registration.user.bankBranchName.required", commonRegistrationApp);
        } else if (BANK_BRANCH_CODE.equals((pid))) {
            return createTextField(caption, true, "registration.user.bankBranchCode.required", commonRegistrationApp);
        } else if (BANK_ACCOUNT_NUMBER.equals((pid))) {
            return createTextField(caption, true, "registration.user.bankAccountNumber.required", commonRegistrationApp);
        } else if (TIN.equals((pid))) {
            return createTextField(caption, true, "registration.user.tin.number.required", commonRegistrationApp);
        } else if (BUSINESS_LICENSE_NUMBER.equals((pid))) {
            return createTextField(commonRegistrationApp.getMessage("registration.business.license.number"), true, "registration.business.license.number.required", commonRegistrationApp);
        } else if (BUSINESS_ID.equals((pid))) {
            Field textField = createTextField(caption, false, "registration.business.license.number.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(BUSINESS_ID_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.business.id")));
            return textField;
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            CommonUiComponentGenerator.filterNullValues(field);
            if (BIRTHDAY.equals(pid)) {
                return CommonUiComponentGenerator.createDateField(field, commonRegistrationApp);
            }
            return field;
        }
    }

    public void setUser(User user) {
        this.user = user;
    }
}