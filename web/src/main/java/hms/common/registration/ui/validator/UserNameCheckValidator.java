/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.validator;

import com.google.common.base.Optional;
import com.vaadin.data.Validator;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Validator to check the existing user names
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserNameCheckValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(UserNameCheckValidator.class);

    private UserService userService;

    private String unavailableUserNameMessage;
    
    public UserNameCheckValidator(CommonRegistrationApp commonRegistrationApp){
        unavailableUserNameMessage = commonRegistrationApp.getMessage("registration.user.username.not.available");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
    }
    
    @Override
    public void validate(Object o) throws InvalidValueException {
        if(!isValid(o)) {
            logger.debug("User account already exists with the given user name [{}] ", o);
            throw new InvalidValueException(unavailableUserNameMessage);
        }
    }

    @Override
    public boolean isValid(Object o) {
        User user;
        try {
            if(!Optional.fromNullable(o).isPresent()) {
                return false;
            }

            user = userService.findUserByName(((String) o).toLowerCase());
            return user == null;
        } catch (DataManipulationException e) {
            logger.error("Error while finding user ", e);
            return false;
        }
    }
}