/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.template;

import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.VerticalLayout;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.CommonRegistrationApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static hms.common.registration.ui.common.util.ExternalLinks.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.isShowTermsAndCondition;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class FooterLayout extends VerticalLayout {

    private CommonRegistrationApp commonRegistrationApp;
    private UserService userService;
    private Map<UserType, String> showTermsAndConditionsUserGroups = new HashMap<>();
    public static final Logger logger = LoggerFactory.getLogger(FooterLayout.class);
    public FooterLayout(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        showTermsAndConditionsUserGroups.put(UserType.INDIVIDUAL, CONSUMER_TERMS_URL);
        showTermsAndConditionsUserGroups.put(UserType.CORPORATE, CORPORATE_TERMS_URL);

        setHeight("20px");
        setWidth("900px");
        addComponent(generateFooterLayoutWithLink());
        setSpacing(true);
    }

    public CustomLayout generateFooterLayoutWithLink() {
        String url = hsenid_mobile_url;

        String copyRightStatement = commonRegistrationApp.getMessage("registration.copyright.statement");
        String termsUrl = null;

        try {
            if(commonRegistrationApp.currentUserName() != null) {
                try {
                    final User userByName = userService.findUserByName(commonRegistrationApp.currentUserName());
                    termsUrl = showTermsAndConditionsUserGroups.get(userByName.getUserType());
                } catch (DataManipulationException e) {
                }
            }
        } catch (NullPointerException e) {
        }

        if(isShowTermsAndCondition() && termsUrl != null) {
            copyRightStatement = commonRegistrationApp.getMessage("registration.copyright.statement.with.terms.and.conditions");
            String hsenidMobileLinkString = commonRegistrationApp.getMessage("registration.copyright.statement.hsenid.mobile");
            final String hsenidMobileFragment = MessageFormat.format("<a href=''{0}''>{1}</a>", url, hsenidMobileLinkString);
            String termsAndConditionsLinkString = commonRegistrationApp.getMessage("registration.copyright.statement.terms.and.conditions");
            final String termsLinkFragment  = MessageFormat.format("<a href=''{0}'' target=\"_blank\" >{1}</a>", termsUrl, termsAndConditionsLinkString);
            copyRightStatement = MessageFormat.format(copyRightStatement, Integer.toString(Calendar.getInstance().get(Calendar.YEAR)), hsenidMobileFragment, termsLinkFragment);
        } else {
            copyRightStatement = MessageFormat.format(copyRightStatement, Integer.toString(Calendar.getInstance().get(Calendar.YEAR)));
            String linkFragment = commonRegistrationApp.getMessage("registration.copyright.statement.hsenid.mobile");
            copyRightStatement = copyRightStatement.replace(linkFragment, MessageFormat.format("<a href=''{0}''>{1}</a>", url, linkFragment));
        }

        CustomLayout customLayout = null;
        try {
            logger.debug("Copy right statement {}", copyRightStatement);
            customLayout = new CustomLayout(new ByteArrayInputStream(copyRightStatement.getBytes()));
            customLayout.setStyleName("footer-bottom");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customLayout;
    }

}