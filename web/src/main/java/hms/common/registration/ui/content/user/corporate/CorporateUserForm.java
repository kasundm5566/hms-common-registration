/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.user.corporate;

import com.vaadin.data.Buffered;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.CommonUiComponentGenerator;
import hms.common.registration.ui.content.user.individual.IndividualUserForm;
import hms.common.registration.ui.validator.EmailCheckValidator;
import hms.common.registration.ui.validator.MsisdnExistValidator;
import hms.common.registration.ui.validator.UserNameCheckValidator;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.ui.common.util.ExternalLinks.CORPORATE_TERMS_URL;
import static hms.common.registration.ui.content.user.corporate.CorporateUserForm.*;
import static hms.common.registration.ui.content.user.corporate.CorporateUserForm.BUSINESS_ID;
import static hms.common.registration.ui.content.user.individual.IndividualUserForm.CONFIRM_EMAIL_PROPERTY_ID;
import static hms.common.registration.ui.content.user.individual.IndividualUserForm.CONFIRM_MPIN_PROPERTY_ID;
import static hms.common.registration.ui.content.user.individual.IndividualUserForm.RETYPE_PASSWORD_PROPERTY_ID;
import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.model.UserType.CORPORATE;
import static hms.common.registration.service.UserService.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;

/**
 * Handle the subcorporate user creation form implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CorporateUserForm extends IndividualUserForm {

    private static final Logger logger = LoggerFactory.getLogger(CorporateUserForm.class);

    private int termsAndConditionStart = 26;

    public static final String INDUSTRY_PROPERTY_ID = "industry";
    public static final String ORGANIZATION_NAME_PROPERTY_ID = "orgName";
    public static final String ORGANIZATION_SIZE_PROPERTY_ID = "orgSize";
    public static final String ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID = "orgPhoneNumbers";
    public static final String ORGANIZATION_FAX_NO_PROPERTY_ID = "orgFaxNo";
    public static final String CONTACT_PERSON_NAME_PROPERTY_ID = "contactPersonName";
    public static final String CONTACT_PERSON_EMAIL_PROPERTY_ID = "contactPersonEmail";
    public static final String CONTACT_PERSON_PHONE_NO_PROPERTY_ID = "contactPersonPhone";
    public static final String BENEFICIARY_NAME = "beneficiaryName";
    public static final String BANK_CODE = "bankCode";
    public static final String BANK_BRANCH_CODE = "bankBranchCode";
    public static final String BANK_BRANCH_NAME = "bankBranchName";
    public static final String BANK_ACCOUNT_NUMBER = "bankAccountNumber";

    // reconciliation details
    public static final String TIN = "tin";
    public static final String BUSINESS_ID = "businessId";
    public static final String BUSINESS_LICENSE_NUMBER = "businessLicenseNumber";

    public CorporateUserForm(User user, CommonRegistrationApp commonRegistrationApp, boolean editState) {
        super(user, commonRegistrationApp, editState);
        user.setUserType(CORPORATE);
    }

    protected void initUserCreationForm() {
        super.advancedLayout.setMargin(true, false, true, true);
        advancedLayout.setSpacing(true);
        advancedLayout.space();
        advancedLayout.setWidth("800px");
        outerLayout.addComponent(advancedLayout);
        outerLayout.setComponentAlignment(advancedLayout, Alignment.TOP_CENTER);
        setLayout(outerLayout);
        BeanItem<User> beanItem = new BeanItem<User>(super.user);
        beanItem.addItemProperty(PASSWORD, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(RETYPE_PASSWORD_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(INDUSTRY_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(ORGANIZATION_NAME_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(ORGANIZATION_SIZE_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(ORGANIZATION_FAX_NO_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(CONTACT_PERSON_NAME_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(CONTACT_PERSON_EMAIL_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(CONTACT_PERSON_PHONE_NO_PROPERTY_ID, new ObjectProperty(null, String.class));
        beanItem.addItemProperty(CONFIRM_EMAIL_PROPERTY_ID, new ObjectProperty(null, String.class));

        List<String> visibleItems = new ArrayList<String>(Arrays.asList(ORGANIZATION_NAME_PROPERTY_ID, ORGANIZATION_SIZE_PROPERTY_ID,
                INDUSTRY_PROPERTY_ID, COUNTRY, PROVINCE, CITY, POST_CODE, ADDRESS, ORGANIZATION_FAX_NO_PROPERTY_ID,
                ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID, EMAIL, CONFIRM_EMAIL_PROPERTY_ID,
                CONTACT_PERSON_NAME_PROPERTY_ID, CONTACT_PERSON_EMAIL_PROPERTY_ID, CONTACT_PERSON_PHONE_NO_PROPERTY_ID, USER_NAME));

        if (!isDomainDialog()) {
            visibleItems.add(PASSWORD);
            visibleItems.add(RETYPE_PASSWORD_PROPERTY_ID);
        }

        if (isCorporateUserMsisdnVerifyEnabled()) {
            beanItem.addItemProperty(OPERATOR, new ObjectProperty(null, String.class));
            visibleItems.add(OPERATOR);
        }

        if (isMpinEnabled()) {
            beanItem.addItemProperty(CONFIRM_MPIN_PROPERTY_ID, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(MPIN, new ObjectProperty(null, String.class));
            visibleItems.add(MPIN);
            visibleItems.add(CONFIRM_MPIN_PROPERTY_ID);
        }

        if (isBeneficiaryAvailable()) {
            beanItem.addItemProperty(BENEFICIARY_NAME, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(BANK_CODE, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(BANK_BRANCH_CODE, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(BANK_BRANCH_NAME, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(BANK_ACCOUNT_NUMBER, new ObjectProperty(null, String.class));

            visibleItems.add(BENEFICIARY_NAME);
            visibleItems.add(BANK_CODE);
            visibleItems.add(BANK_BRANCH_CODE);
            visibleItems.add(BANK_BRANCH_NAME);
            visibleItems.add(BANK_ACCOUNT_NUMBER);
        }

        if(isReconciliationDetailsEnabled()) {
            beanItem.addItemProperty(TIN, new ObjectProperty(null, String.class));
            beanItem.addItemProperty(BUSINESS_ID, new ObjectProperty(null, String.class));

            visibleItems.add(TIN);
            visibleItems.add(BUSINESS_ID);
        }

        logger.debug("Visible fields [{}] ", visibleItems);

        setItemDataSource(beanItem);
        setWriteThrough(false);
        setFormFieldFactory(new CorporateUserFieldFactory(this, commonRegistrationApp));

        setVisibleItemProperties(visibleItems);

        if (editState) {
            getField(USER_NAME).setReadOnly(true);
        } else {
            if (isMpinEnabled()) {
                addRetypeMpinValidator();
            }
        }

        if (!editState && !isDomainDialog() && !isOpenIdUser) {
            addRetypePasswordValidator();
        }

        addRetypeEmailValidator();
        generatePageContent();
    }

    /**
     * Headings of corporate user form
     */
    protected void generatePageContent() {
        Label organizationDetails = generateHeading("registration.user.organization.details.heading", commonRegistrationApp);
        advancedLayout.addComponent(organizationDetails, 0, 0, 2, 0);
        advancedLayout.addComponent(getSpacer(), 0, 4, 2, 4);
        Label contactDetailsHeading = generateHeading("registration.user.contact.details.heading", commonRegistrationApp);
        advancedLayout.addComponent(contactDetailsHeading, 0, 5, 2, 5);
        advancedLayout.addComponent(getSpacer(), 0, 11, 2, 11);
        Label otherDetailsHeading = generateHeading("registration.user.organization.contact.person.details.heading", commonRegistrationApp);
        advancedLayout.addComponent(otherDetailsHeading, 0, 12, 2, 12);
        advancedLayout.addComponent(getSpacer(), 0, 16, 2, 16);
        if (!isDomainDialog()) {
            Label userIdHeading = generateHeading("registration.user.select.user.id.heading", commonRegistrationApp);
            advancedLayout.addComponent(userIdHeading, 0, 17, 2, 17);
            advancedLayout.addComponent(getSpacer(), 0, 21, 2, 21);
        }

        int captchaStart = 22;

        if (isBeneficiaryAvailable()) {
            Label beneficiaryHeading = generateHeading("registration.user.beneficiary.details.heading", commonRegistrationApp);
            advancedLayout.addComponent(beneficiaryHeading, 0, 22, 2, 22);
            advancedLayout.addComponent(getSpacer(), 0, 28, 2, 28);
            captchaStart = 29;
            termsAndConditionStart = 29;
        } else if(isReconciliationDetailsEnabled()) {
            Label reconciliationDetailsHeading = generateHeading("registration.user.reconciliation.details.heading", commonRegistrationApp);
            advancedLayout.addComponent(reconciliationDetailsHeading, 0, 22, 2, 22);
            advancedLayout.addComponent(getSpacer(), 0, 28, 2, 28);
            captchaStart = 29;
            termsAndConditionStart = 29;
        }

        if (isCaptchaValidationEnabled()) {
            Label wordVerificationHeading = generateHeading("registration.user.word.verification.heading", commonRegistrationApp);
            wordVerificationHeading.setStyleName("header-with-border");
            advancedLayout.addComponent(wordVerificationHeading, 0, captchaStart, 2, captchaStart);
            captchaField = new CaptchaField(super.commonRegistrationApp);
            advancedLayout.addComponent(captchaField, 0, captchaStart + 1, 1, captchaStart + 1);
            captchaText = new TextField();
            captchaText.setCaption(commonRegistrationApp.getMessage("registration.user.captcha.msg"));
            advancedLayout.addComponent(captchaText, 0, captchaStart + 2);
            advancedLayout.addComponent(getSpacer(), 0, captchaStart + 3, 2, captchaStart + 3);
            termsAndConditionStart = captchaStart + 4;
        }
        generateUserTermsLayout();
        super.createFooter();
    }

    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals(ORGANIZATION_NAME_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 0, 1);
        } else if (propertyId.equals(ORGANIZATION_SIZE_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 0, 2);
        } else if (propertyId.equals(INDUSTRY_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 0, 3);
        } else if (propertyId.equals(COUNTRY)) {
            if (!editState) {
                field.setValue(SUPPORTED_COUNTRIES.iterator().next());
            }
            advancedLayout.addComponent(field, 0, 6);
        } else if (propertyId.equals(PROVINCE)) {
            advancedLayout.addComponent(field, 1, 6);
        } else if (propertyId.equals(CITY)) {
            advancedLayout.addComponent(field, 0, 7);
        } else if (propertyId.equals(POST_CODE)) {
            advancedLayout.addComponent(field, 1, 7);
        } else if (propertyId.equals(ADDRESS)) {
            advancedLayout.addComponent(field, 0, 8, 2, 8);
        } else if (propertyId.equals(ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 0, 9);
        } else if (propertyId.equals(ORGANIZATION_FAX_NO_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 1, 9);
        } else if (propertyId.equals(EMAIL)) {
            advancedLayout.addComponent(field, 0, 10);
        } else if (propertyId.equals(CONFIRM_EMAIL_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 1, 10);
        } else if (propertyId.equals(CONTACT_PERSON_NAME_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 0, 13);
        } else if (propertyId.equals(CONTACT_PERSON_EMAIL_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 0, 14);
        } else if (propertyId.equals(OPERATOR) && isCorporateUserMsisdnVerifyEnabled()) {
            if (!editState) {
                field.setValue(getOperatorNames().iterator().next());
            }
            advancedLayout.addComponent(field, 0, 15);
        } else if (propertyId.equals(CONTACT_PERSON_PHONE_NO_PROPERTY_ID)) {
            if (isCorporateUserMsisdnVerifyEnabled()) {
                field.setRequired(true);
                advancedLayout.addComponent(field, 1, 15);
            } else {
                advancedLayout.addComponent(field, 0, 15);
            }
        } else if (propertyId.equals(USER_NAME)) {
            advancedLayout.addComponent(field, 0, 18);
        } else if (propertyId.equals(PASSWORD)) {
            advancedLayout.addComponent(field, 0, 19);
        } else if (propertyId.equals(RETYPE_PASSWORD_PROPERTY_ID)) {
            advancedLayout.addComponent(field, 1, 19);
        } else if (propertyId.equals(MPIN) && isMpinEnabled()) {
            advancedLayout.addComponent(field, 0, 20);
        } else if (propertyId.equals(CONFIRM_MPIN_PROPERTY_ID) && isMpinEnabled()) {
            advancedLayout.addComponent(field, 1, 20);
        } else if (isBeneficiaryAvailable()) {
            if (propertyId.equals((BENEFICIARY_NAME))) {
                advancedLayout.addComponent(field, 0, 23);
            } else if (propertyId.equals((BANK_CODE))) {
                advancedLayout.addComponent(field, 0, 24);
            } else if (propertyId.equals((BANK_BRANCH_CODE))) {
                advancedLayout.addComponent(field, 0, 25);
            } else if (propertyId.equals((BANK_BRANCH_NAME))) {
                advancedLayout.addComponent(field, 0, 26);
            } else if (propertyId.equals((BANK_ACCOUNT_NUMBER))) {
                advancedLayout.addComponent(field, 0, 27);
            }
        } else if (isReconciliationDetailsEnabled()) {
            if (propertyId.equals((TIN))) {
                advancedLayout.addComponent(field, 0, 23);
            } else if (propertyId.equals((BUSINESS_ID))) {
                advancedLayout.addComponent(field, 0, 24);
            }
        }
    }

    protected void generateUserTermsLayout() {
        Label termsOfServiceHeading = new Label(commonRegistrationApp.getMessage("registration.user.user.terms"));
        termsOfServiceHeading.setStyleName("header-with-border");
        advancedLayout.addComponent(termsOfServiceHeading, 0, termsAndConditionStart, 2, termsAndConditionStart);
        if (isExternalTermsLinkEnabled()) {
            Link checkTermsLink = new Link(commonRegistrationApp.getMessage("registration.user.user.terms.link"),
                    new ExternalResource(CORPORATE_TERMS_URL), "_blank", -1, -1, Window.BORDER_DEFAULT);
            advancedLayout.addComponent(checkTermsLink, 0, termsAndConditionStart + 1);
        } else {
            Button checkTermsButton = new Button(commonRegistrationApp.getMessage("registration.user.user.terms.link"));
            checkTermsButton.setStyleName("link");
            checkTermsButton.addListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    getWindow().addWindow(CommonUiComponentGenerator.generateCorporateUserTermsSubWindow(commonRegistrationApp));
                }
            });
            advancedLayout.addComponent(checkTermsButton, 0, termsAndConditionStart + 1);
        }
        advancedLayout.addComponent(new Label(commonRegistrationApp.getMessage("registration.user.user.terms.last.message")),
                0, termsAndConditionStart + 2, 2, termsAndConditionStart + 2);
        termsAcceptedCheckBox = new CheckBox(commonRegistrationApp.getMessage("registration.button.create.account.button"));
        termsAcceptedCheckBox.setImmediate(true);
        termsAcceptedCheckBox.addListener(new ValueChangeListener() {

            public void valueChange(Property.ValueChangeEvent event) {
                logger.debug("terms accepted : " + event.getProperty().getValue());
                saveButton.setEnabled((Boolean) event.getProperty().getValue());
            }
        });
        advancedLayout.addComponent(termsAcceptedCheckBox,
                0, termsAndConditionStart + 3, 2, termsAndConditionStart + 3);
    }

    @Override
    public void discard() throws Buffered.SourceException {
        Collection itemIds = getItemPropertyIds();
        for (Object itemId : itemIds) {
            resetField(getField(itemId), null);
        }
        if (isCaptchaValidationEnabled()) {
            resetField(captchaText, "");
        }
        resetField(termsAcceptedCheckBox, false);
    }

    /**
     * Save the additional information of corporate-user
     *
     * @throws DataManipulationException
     */
    protected void saveUserDetails() throws DataManipulationException {
        ((CorporateUser) user).setIndustry((String) getField(INDUSTRY_PROPERTY_ID).getValue());
        ((CorporateUser) user).setOrgName((String) getField(ORGANIZATION_NAME_PROPERTY_ID).getValue());
        ((CorporateUser) user).setOrgSize((String) getField(ORGANIZATION_SIZE_PROPERTY_ID).getValue());
        ((CorporateUser) user).setOrgFaxNo((String) getField(ORGANIZATION_FAX_NO_PROPERTY_ID).getValue());
        ((CorporateUser) user).setOrgPhoneNumbers((String) getField(ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID).getValue());
        ((CorporateUser) user).setContactPersonName((String) getField(CONTACT_PERSON_NAME_PROPERTY_ID).getValue());
        ((CorporateUser) user).setContactPersonPhone((String) getField(CONTACT_PERSON_PHONE_NO_PROPERTY_ID).getValue());
        ((CorporateUser) user).setContactPersonEmail((String) getField(CONTACT_PERSON_EMAIL_PROPERTY_ID).getValue());
        saveFeaturedDetails();
        user.setMobileNo(((CorporateUser) user).getContactPersonPhone());

        user.setEnabled(true);
    }

    private void saveFeaturedDetails() {
        if (isCorporateUserMsisdnVerifyEnabled()) {
            user.setOperator((String) getField(OPERATOR).getValue());
        }
    }
}

class CorporateUserFieldFactory implements FormFieldFactory {

    private static final Logger logger = LoggerFactory.getLogger(CorporateUserFieldFactory.class);

    private static final String REG_IND_RESOURCE_BUNDLE_PREFIX = "registration.user.";

    private CorporateUserForm form;
    private CommonRegistrationApp commonRegistrationApp;

    CorporateUserFieldFactory(CorporateUserForm form, CommonRegistrationApp commonRegistrationApp) {
        this.form = form;
        this.commonRegistrationApp = commonRegistrationApp;
    }

    @Override
    public Field createField(Item item, Object propertyId, Component component) {
        String caption = commonRegistrationApp.getMessage(REG_IND_RESOURCE_BUNDLE_PREFIX + propertyId);
        String pid = (String) propertyId;

        logger.debug("Creating field [{}] with [{}]", pid, caption);

        if (ORGANIZATION_NAME_PROPERTY_ID.equals(pid)) {
            return createTextField(caption, false, "registration.user.organization.name.required", commonRegistrationApp);
        } else if (INDUSTRY_PROPERTY_ID.equals(pid)) {
            return createItemAllowedComboBox(caption, false, "registration.user.industry.required",
                    INDUSTRIES, commonRegistrationApp);
        } else if (ORGANIZATION_SIZE_PROPERTY_ID.equals(pid)) {
            return createComboBox(caption, false, "registration.user.organization.size.required",
                    ORGANIZATION_SIZE, commonRegistrationApp);
        } else if (CONTACT_PERSON_NAME_PROPERTY_ID.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.organization.contact.person.name.required", commonRegistrationApp);
            if (form.isDomainDialog()) {
                field.setReadOnly(true);
            }
            return field;
        } else if (CONTACT_PERSON_EMAIL_PROPERTY_ID.equals(pid)) {
            TextField textField = (TextField) createTextField(caption, false, "registration.user.organization.contact.person.email.required", commonRegistrationApp);
            textField.addValidator(new EmailValidator(commonRegistrationApp.getMessage("registration.user.invalid.email")));
            return textField;
        } else if (CONTACT_PERSON_PHONE_NO_PROPERTY_ID.equals(pid)) {
            TextField field = (TextField) createTextField(caption, true, "registration.user.organization.contact.person.phone.no.required", commonRegistrationApp);
            field.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.corporate.user.invalid.msisdn")));
            if (isCorporateUserMsisdnVerifyEnabled()) {
                field.addValidator(new MsisdnExistValidator(commonRegistrationApp));
            }
            if (form.isDomainDialog()) {
                field.setReadOnly(true);
            }
            return field;
        } else if (ORGANIZATION_PHONE_NUMBERS_PROPERTY_ID.equals(pid)) {
            TextArea phoneNos = (TextArea) createTextArea(caption, false, "registration.user.organization.phone.no.required", commonRegistrationApp);
            phoneNos.setColumns(15);
            return phoneNos;
        } else if (ORGANIZATION_FAX_NO_PROPERTY_ID.equals(pid)) {
            TextArea faxNos = (TextArea) createTextArea(caption, false, "", commonRegistrationApp);
            faxNos.setColumns(15);
            return faxNos;
        } else if (COUNTRY.equals(pid)) {
            return createComboBox(caption, false, "registration.user.country.required",
                    SUPPORTED_COUNTRIES, commonRegistrationApp);
        } else if (PROVINCE.equals(pid)) {
            return createComboBox(caption, false, "registration.user.province.required",
                    SUPPORTED_PROVINCES, commonRegistrationApp);
        } else if (CITY.equals(pid)) {
            return createComboBox(caption, false, "registration.user.city.required",
                    SUPPORTED_CITIES, commonRegistrationApp);
        } else if (ADDRESS.equals(pid)) {
            TextArea addressText = (TextArea) createTextArea(caption, false, "registration.user.address.required", commonRegistrationApp);
            addressText.setColumns(43);
            addressText.setWordwrap(true);
            addressText.addValidator(new RegexpValidator(ADDRESS_LENGTH_VALIDATION,
                    commonRegistrationApp.getMessage("registration.user.address.length.validation")));
            return addressText;
        } else if (USER_NAME.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.username.required", commonRegistrationApp);
            if (form.isVerifyUser()) {
                field.addValidator(new UserNameCheckValidator(commonRegistrationApp));
            }
            field.setInvalidAllowed(true);
            if (form.isDomainDialog()) {
                field.setReadOnly(true);
                field.setVisible(false);
            } else {
                field.addValidator(new RegexpValidator(USER_NAME_REGEX_PATTERN, commonRegistrationApp.getMessage("registration.user.invalid.username")));
            }
            return field;
        } else if (OPERATOR.equals(pid)) {
            return createComboBox(caption, true, "registration.user.operator.required",
                    getOperatorNames(), commonRegistrationApp);
        } else if (PASSWORD.equals(pid)) {
            Field passwordField = createPasswordField(caption, "registration.user.password.required", commonRegistrationApp);
            if (form.isDomainDialog()) {
                passwordField.setVisible(false);
                passwordField.setRequired(false);
            }
            passwordField.addValidator(new RegexpValidator(PASSWORD_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.password")));
            return passwordField;
        } else if (RETYPE_PASSWORD_PROPERTY_ID.equals(pid)) {
            Field passwordField = createPasswordField(caption, "registration.user.retype.password.required", commonRegistrationApp);
            if (form.isDomainDialog()) {
                passwordField.setVisible(false);
                passwordField.setRequired(false);
            }
            return passwordField;
        } else if (MPIN.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.mpin.required", commonRegistrationApp);
            ((TextField) field).setSecret(true);
            field.addValidator(new RegexpValidator(MPIN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.mpin")));
            return field;
        } else if (CONFIRM_MPIN_PROPERTY_ID.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.retype.mpin.required", commonRegistrationApp);
            ((TextField) field).setSecret(true);
            return field;
        } else if (EMAIL.equals(pid)) {
            Field field = createTextField(caption, true, "registration.user.email.required", commonRegistrationApp);
            EmailCheckValidator emailValidator = new EmailCheckValidator(commonRegistrationApp);
            emailValidator.setUserId(form.getUser().getUserId());
            if (form.isDomainDialog()) {
                field.setReadOnly(true);
                field.setRequired(false);
            } else {
                field.addValidator(emailValidator);
            }
            return field;
        } else if (CONFIRM_EMAIL_PROPERTY_ID.equals(pid)) {
            Field textField = createTextField(caption, true, "registration.user.email.required", commonRegistrationApp);
            if (form.isDomainDialog()) {
                textField.setVisible(false);
                textField.setRequired(false);
            } else {
                textField.addValidator(new EmailValidator(commonRegistrationApp.getMessage("registration.user.invalid.email")));
            }
            return textField;
        } else if (POST_CODE.equals(pid)) {
            return createTextField(caption, false, null, commonRegistrationApp);
        } else if (PHONE_NUMBER.equals(pid)) {
            Field textField = createTextField(caption, true, "registration.user.msisdn.required", commonRegistrationApp);
            textField.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN,
                    commonRegistrationApp.getMessage("registration.user.invalid.msisdn")));
            if (isCorporateUserMsisdnVerifyEnabled()) {
                textField.addValidator(new MsisdnExistValidator(commonRegistrationApp));
            }
            return textField;
        } else if (BENEFICIARY_NAME.equals(pid)) {
            return createTextField(caption, true, "registration.user.beneficiaryName.required", commonRegistrationApp);
        } else if (BANK_CODE.equals(pid)) {
            return createTextField(caption, true, "registration.user.bankCode.required", commonRegistrationApp);
        } else if (BANK_BRANCH_CODE.equals(pid)) {
            return createTextField(caption, true, "registration.user.bankBranchCode.required", commonRegistrationApp);
        } else if (BANK_BRANCH_NAME.equals(pid)) {
            return createTextField(caption, true, "registration.user.bankBranchName.required", commonRegistrationApp);
        } else if (BANK_ACCOUNT_NUMBER.equals(pid)) {
            return createTextField(caption, true, "registration.user.bankAccountNumber.required", commonRegistrationApp);
        } else if (TIN.equals(pid)) {
            return createTextField(caption, true, "registration.user.tin.number.required", commonRegistrationApp);
        } else if (BUSINESS_ID.equals(pid)) {
            return createTextField(caption, false, "", commonRegistrationApp);
        } else {
            Field field = DefaultFieldFactory.get().createField(item, propertyId, component);
            field.setCaption(caption);
            CommonUiComponentGenerator.filterNullValues(field);
            return field;
        }
    }
}
