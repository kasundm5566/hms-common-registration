/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.verification;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.model.UserStatus;
import hms.common.registration.model.UserType;
import hms.common.registration.service.UserService;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.SessionKeys;
import hms.common.registration.ui.common.util.ExternalLinks;
import hms.common.registration.ui.content.user.success.UserCreationSuccessView;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.validator.MsisdnExistValidator;
import hms.common.registration.ui.validator.UserVerificationValidator;
import hms.common.registration.util.message.Msisdn;
import hms.common.registration.util.message.SmsMessage;
import hms.common.registration.util.sms.SmsMtDispatcher;
import hms.common.registration.util.RegistrationFeatureRegistry.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.MessageFormat;
import java.util.Date;

import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.ui.common.util.ExternalLinks.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;
import static hms.common.registration.util.WebUtils.generateVerificationCode;

public class MsisdnVerificationWithRetryLayout extends HorizontalLayout {

    private static final Logger logger = LoggerFactory.getLogger(MsisdnVerificationWithRetryLayout.class);

    private final GridLayout gridLayout = new GridLayout(20, 8);
    private final Label mobileNoLbl;
    private final TextField mobileNoTxt;
    private final Button editBtn;
    private final Button cancelBtn;
    private final Button verificationCodeBtn;
    private final Label verificationCodeLbl;
    private final TextField verificationCodeTxt;
    private final Button submitBtn;
    private final Button clearBtn;
    private final Button skipStepBtn;
    private final Label verificationMsgLbl;
    private final Label editMobileNoMsgLbl;
    private final Label requestVerifyCodeMsgLbl;
    private final Label errorLbl;
    private final Label successLbl;
    private final CommonRegistrationApp app;
    private UserService userService;
    private User user;
    private boolean isEditMode = false;
    private final SmsMtDispatcher smsMtDispatcher;
    private UserVerificationValidator userVerificationValidator;
    private boolean currentlyLoggedIn;
    private VerificationViewMode viewMode;

    public enum VerificationViewMode {
        FIRST_TIME_MODE,
        EDIT_MOE
    }

    public MsisdnVerificationWithRetryLayout(CommonRegistrationApp commonRegistrationApp,
                                             UserService userService,
                                             User user,
                                             SmsMtDispatcher smsMtDispatcher,
                                             boolean currentlyLoggedIn,
                                             VerificationViewMode viewMode) {

        this.app = commonRegistrationApp;
        this.userService = userService;
        this.user = user;
        this.smsMtDispatcher = smsMtDispatcher;
        this.currentlyLoggedIn = currentlyLoggedIn;
        this.viewMode = viewMode;

        app.putToSession(SessionKeys.MSISDN_TMP, user.getMobileNo());

        errorLbl = new Label("");
        errorLbl.setStyleName("mvr_error_lbl");
        successLbl = new Label("");
        successLbl.setStyleName("mvr_success_lbl");
        successLbl.setVisible(false);

        mobileNoLbl = new Label(app.getMessage("registration.user.msisdn.verification.mobile.number"));
        mobileNoTxt = new TextField();
        mobileNoTxt.setRequired(true);
        mobileNoTxt.setRequiredError(app.getMessage("registration.user.msisdn.verification.mobile.number.empty.error"));
        mobileNoTxt.addValidator(new RegexpValidator(MSISDN_REGEX_PATTERN, app.getMessage("registration.user.msisdn.verification.mobile.number.format.error")));
        mobileNoTxt.addValidator(new MsisdnExistValidator(app));

        editMobileNoMsgLbl = new Label(app.getMessage("registration.user.msisdn.change.label"));
        requestVerifyCodeMsgLbl = new Label(app.getMessage("registration.user.msisdn.verification.resend.label"));

        editBtn = new Button(app.getMessage("registration.user.msisdn.verification.edit"));
        editBtn.setStyleName("action-button mvr_edit_btn_width");
        cancelBtn = new Button(app.getMessage("registration.user.msisdn.verification.cancel"));
        cancelBtn.setStyleName("action-button");
        verificationMsgLbl = new Label(app.getMessage("registration.user.msisdn.verification.message"));

        verificationCodeBtn = new Button(app.getMessage("registration.user.msisdn.verification.code.button"));
        verificationCodeBtn.setStyleName("action-button");

        verificationCodeLbl = new Label(app.getMessage("registration.user.msisdn.verification.code.label"));
        verificationCodeTxt = new TextField();

        submitBtn = new Button(app.getMessage("registration.user.msisdn.verification.submit"));
        submitBtn.setStyleName("action-button");
        clearBtn = new Button(app.getMessage("registration.user.msisdn.verification.clear"));
        clearBtn.setStyleName("action-button");

        skipStepBtn = new Button(app.getMessage("registration.user.msisdn.verification.skip"));
        skipStepBtn.setVisible(false);
        skipStepBtn.setStyleName("action-button");

        gridLayout.setSpacing(true);
        gridLayout.setWidth("800px");

        userVerificationValidator = new UserVerificationValidator(commonRegistrationApp.getMessage("registration.user.msisdn.verification.error.code.mismatch"));
        userVerificationValidator.setGeneratedMsisdnVerificationCode(user.getMsisdnVerificationCode());

        alignToGridLayout();

        addComponent(gridLayout);
        gridLayout.setImmediate(true);

        setViewMode();
        buttonActions();

        sendInitialMsisdnVerificationCode(userService, user);
    }

    private void sendInitialMsisdnVerificationCode(UserService userService, User user) {
        try {
            if (user.getMsisdnVerificationCode() != null) {
                userVerificationValidator.setGeneratedMsisdnVerificationCode(user.getMsisdnVerificationCode());
                user.setLastSmsSendDate(new Date());
                sendVerificationCodeMsg(user.getMsisdnVerificationCode());
            } else {
                String verificationCode = generateVerificationCode();
                user.setMsisdnVerificationCode(verificationCode);
                user.setLastSmsSendDate(new Date());
                userVerificationValidator.setGeneratedMsisdnVerificationCode(verificationCode);
                sendVerificationCodeMsg(verificationCode);
            }
            this.user = userService.merge(user);
        } catch (DataManipulationException e) {
            logger.error("Error occurred while updating user-details", e);
        }
    }

    private void setViewMode() {
        if (user.isCorporateUser() && isCorporateUserMsisdnVerifyEnabled() && isCorporateUserMsisdnVerifySkippable()) {
            skipStepBtn.setVisible(true);
        }

        errorLbl.setVisible(false);

        isEditMode = false;
        editBtn.setCaption(app.getMessage("registration.user.msisdn.verification.edit"));
        cancelBtn.setVisible(false);
        mobileNoTxt.setValue(app.getFromSession(SessionKeys.MSISDN_TMP));
        mobileNoTxt.setEnabled(false);
        verificationCodeBtn.setEnabled(true);
        verificationCodeTxt.setEnabled(true);
        verificationCodeTxt.setValue("");
        submitBtn.setEnabled(true);
        clearBtn.setEnabled(true);
    }

    private void setDoVerificationMode() {
        isEditMode = false;
        editBtn.setCaption(app.getMessage("registration.user.msisdn.verification.edit"));
        cancelBtn.setVisible(false);
        mobileNoTxt.setValue(app.getFromSession(SessionKeys.MSISDN_TMP));
        mobileNoTxt.setEnabled(false);
        verificationCodeBtn.setEnabled(true);
        verificationCodeTxt.setEnabled(true);
        verificationCodeTxt.setValue("");
        submitBtn.setEnabled(true);
        clearBtn.setEnabled(true);
    }

    private void setEditMode() {
        isEditMode = true;
        editBtn.setCaption(app.getMessage("registration.user.msisdn.verification.save"));
        cancelBtn.setVisible(true);
        successLbl.setVisible(false);
        mobileNoTxt.setEnabled(true);
        verificationCodeBtn.setEnabled(false);
        verificationCodeTxt.setEnabled(false);
        verificationCodeTxt.setValue("");
        submitBtn.setEnabled(false);
        clearBtn.setEnabled(false);
    }

    private void buttonActions() {
        editBtn.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                if(isEditMode) {
                    setComponentError(null);
                    errorLbl.setVisible(false);
                    if (!isValidateMobileNo()) setEditMode();
                    else if (user.getMobileNo().equals(((String) mobileNoTxt.getValue()).trim())) setDoVerificationMode();
                    else {
                        int msisdnChangeAttempts =
                                user.getMsisdnChangeAttempts() == null ? 0 : user.getMsisdnChangeAttempts();
                        if(msisdnChangeAttempts < Integer.parseInt(MAX_MSISDN_CHANGE_ATTEMPTS)) {
                            app.putToSession(SessionKeys.MSISDN_TMP, (String) mobileNoTxt.getValue());
                            user.setMobileNo(((String) mobileNoTxt.getValue()).trim());
                            user.setMsisdnChangeAttempts(msisdnChangeAttempts + 1);
                            user.setMsisdnVerificationAttempts(0);
                            user.setMsisdnVerificationCodeReqCount(0);
                            try {
                                user = userService.merge(user);
                            } catch (DataManipulationException e) {
                                logger.error("Error occured while saving the new mobile-number [{}] to user [{}].",
                                        user.getMobileNo(), user.getUsername());
                            }
                            setDoVerificationMode();
                        } else {
                            displayError(app.getMessage("registration.user.msisdn.change.attempts.exceed"));
                            return;
                        }
                    }
                }
                else setEditMode();
            }
        });

        cancelBtn.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                setViewMode();
            }
        });

        clearBtn.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                setComponentError(null);
                errorLbl.setVisible(false);
                successLbl.setVisible(false);
                verificationCodeTxt.setValue("");
            }
        });

        verificationCodeBtn.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                setComponentError(null);
                errorLbl.setVisible(false);
                successLbl.setVisible(false);
                boolean verificationCodeRequestAttemptExceed =
                        user.getMsisdnVerificationCodeReqCount() != null &&
                                user.getMsisdnVerificationCodeReqCount() >= Integer.parseInt(MAX_MSISDN_VERIFY_CODE_REQ_COUNT);
                if(verificationCodeRequestAttemptExceed) {
                    displayError(app.getMessage("registration.user.msisdn.verification.code.request.attempts.exceed"));
                    return;
                }

                String verificationCode = generateVerificationCode();
                logger.debug("Generated new mobile number verification code [{}] to user [{}].", verificationCode, user.getUsername());
                user.setMsisdnVerified(false);
                user.setMsisdnVerificationCode(verificationCode);
                user.setMobileNo(((String) mobileNoTxt.getValue()).trim());
                userVerificationValidator.setGeneratedMsisdnVerificationCode(user.getMsisdnVerificationCode());

                Integer verificationCount = user.getMsisdnVerificationCodeReqCount() == null ? 0 : user.getMsisdnVerificationCodeReqCount();
                user.setMsisdnVerificationCodeReqCount(verificationCount + 1);
                user.setMsisdnVerificationAttempts(0);
                try {
                    user = userService.merge(user);
                } catch (DataManipulationException e) {
                    logger.error("Error occured while saving the new verification-code [{}] and mobile-number [{}] to user [{}].",
                            verificationCode, user.getMobileNo(), user.getUsername());
                }
                try {
                    sendVerificationCodeMsg(verificationCode);
                    displaySuccessNotice(MessageFormat.format(app.getMessage("registration.user.msisdn.verification.resend.success.message"),
                            (Integer.parseInt(MAX_MSISDN_VERIFY_CODE_REQ_COUNT) - (verificationCount + 1))));
                } catch(Exception e) {
                    logger.error("Error occured while sending the message [{}].", e);
                    displayError(app.getMessage("registration.user.msisdn.verification.error.sending.message"));
                }
                setViewMode();
            }
        });

        submitBtn.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                app.getMainLayout().setComponentError(null);
                errorLbl.setVisible(false);
                successLbl.setVisible(false);
                Integer verificationAttempts = user.getMsisdnVerificationAttempts() == null ? 0 : user.getMsisdnVerificationAttempts();
                boolean verificationAttemptExceed = verificationAttempts >= Integer.parseInt(MAX_MSISDEN_VERIFY_ATTEMPTS);

                if(verificationAttemptExceed) {
                    displayError(app.getMessage("registration.user.msisdn.verification.attempts.exceed"));
                    return;
                }

                user.setMsisdnVerificationAttempts(verificationAttempts + 1);
                boolean verificationValid = isValidVerificationCode();

                if(verificationValid) {
                    logger.debug("User [{}] Mobile number verification code is being validated successfully.", user.getUsername());

                    user.setMsisdnVerificationAttempts(0);
                    user.setMsisdnVerificationCodeReqCount(0);
                    user.setMsisdnVerified(true);
                    user.setMsisdnVerificationCode(null);

                    if(isUserActivatable(user)) {
                        logger.debug("User [{}] activated after verifying mobile number [{}]", user.getUsername(), user.getMobileNo());
                        user.activate();
                    }

                    if(viewMode == VerificationViewMode.FIRST_TIME_MODE) {
                        handleUserCreationSuccessFlow();
                    } else if(viewMode == VerificationViewMode.EDIT_MOE) {
                        setViewMode();
                    }
                }
                try {
                    user = userService.merge(user);
                } catch (DataManipulationException e) {
                    logger.error("Error occured while completing the msisdn verification code validation. [{}]", e);
                }
            }
        });

        skipStepBtn.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                boolean emailVerificationNotDone = !user.isEmailVerified() && isCorporateUserEmailVerifyEnabled();
                user.setMsisdnVerifySkip(true);

                try {
                    user = userService.merge(user);
                    ContentPanel panel = app.getMainLayout().getContentLayout().getContentPanel();

                    String previousUrl = app.getFromSession(SessionKeys.PRV_URL);
                    if(previousUrl != null && !previousUrl.isEmpty()) {
                        app.putToSession(SessionKeys.PRV_URL, "");
                        panel.getWindow().open(new ExternalResource(CAS_SERVER_URL + previousUrl));
                    } else {
                        panel.removeAllComponents();
                        if(emailVerificationNotDone) {
                            panel.setCaption(app.getMessage("registration.user.complete.your.registration"));
                            panel.addComponent(new UserCreationSuccessView(user, app, UserCreationSuccessView.EMAIL));
                        } else {
                            panel.getWindow().open(new ExternalResource(CAS_LOGIN_URL));
                        }
                    }
                } catch (DataManipulationException e) {
                    logger.error("Error occured while skipping the msisdn verification step. [{}]", e);
                }
            }
        });
    }

    private void sendVerificationCodeMsg(String verificationCode) {
        SmsMessage smsMessage = new SmsMessage();
        String operator = user.getOperator().toLowerCase();
        smsMessage.setSenderAddress(new Msisdn(app.getMessage("system.default.sender.address"), operator));
        smsMessage.setReceiverAddress(new Msisdn(user.getMobileNo(), operator));
        smsMessage.setMessage(MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, verificationCode));
        smsMtDispatcher.sendIndividualMessage(smsMessage);
    }

    private boolean isValidateMobileNo() {
        try {
            mobileNoTxt.validate();
            return true;
        } catch (Validator.InvalidValueException e) {
            return false;
        }
    }

    private boolean isValidVerificationCode() {
        try {
            userVerificationValidator.validate(verificationCodeTxt.getValue());
            return true;
        } catch (Validator.InvalidValueException e) {
            displayError(app.getMessage("registration.user.msisdn.verification.error.code.mismatch"));
            return false;
        }
    }

    private void alignToGridLayout() {
        gridLayout.addComponent(successLbl, 0, 0, 19, 0);
        gridLayout.addComponent(errorLbl, 0, 1, 19, 1);

        gridLayout.addComponent(mobileNoLbl, 0, 2, 8, 2);
        gridLayout.addComponent(mobileNoTxt, 9, 2, 12, 2);

        gridLayout.addComponent(editMobileNoMsgLbl, 0, 3, 8, 3);
        gridLayout.addComponent(editBtn, 9, 3, 9, 3);
        gridLayout.addComponent(cancelBtn, 10, 3, 10, 3);

        gridLayout.addComponent(requestVerifyCodeMsgLbl, 0, 4, 8, 4);
        gridLayout.addComponent(verificationCodeBtn, 9, 4, 10, 4);

        gridLayout.addComponent(verificationCodeLbl, 0, 5, 8, 5);
        gridLayout.addComponent(verificationCodeTxt, 9, 5, 12, 5);

        gridLayout.addComponent(submitBtn, 0, 7, 0, 7);
        gridLayout.addComponent(clearBtn, 1, 7, 1, 7);
        gridLayout.addComponent(skipStepBtn, 2, 7, 2, 7);
    }

    private void handleUserCreationSuccessFlow() {
        if (currentlyLoggedIn) {
            String previousUrl = app.getFromSession(SessionKeys.PRV_URL);
            if(previousUrl != null && !previousUrl.isEmpty()) {
                app.putToSession(SessionKeys.PRV_URL, "");
                getWindow().open(new ExternalResource(CAS_SERVER_URL + previousUrl));
            }  else {
                getWindow().open(new ExternalResource(CAS_LOGIN_URL));
            }
        } else {
            boolean emailVerificationNotDone = !user.isEmailVerified() && isCorporateUserEmailVerifyEnabled();
            if (emailVerificationNotDone) {
                ContentPanel panel = app.getMainLayout().getContentLayout().getContentPanel();
                panel.removeAllComponents();
                panel.setCaption(app.getMessage("registration.user.user.creation.success.heading"));
                panel.addComponent(new UserCreationSuccessView(user, app, UserCreationSuccessView.MSISDN));
            } else {
                app.close();
            }
        }
    }

    private boolean isUserActivatable(User user) {
        if(user.getUserType().equals(UserType.INDIVIDUAL)) {
            if(!isConsumerUserEmailVerifyEnabled()) {
                return true;
            }
        } else if(user.getUserType().equals(UserType.CORPORATE)) {
            if(isCorporateUserMsisdnVerifyEnabled() && !isCorporateUserMsisdnVerifySkippable() && user.isEmailVerified()) {
                return true;
            }
        }
        return false;
    }

    private void displayError(String error) {
        errorLbl.setVisible(true);
        successLbl.setVisible(false);
        errorLbl.setValue(error);
    }

    private void displaySuccessNotice(String msg) {
        successLbl.setVisible(true);
        successLbl.setValue(msg);
    }

}