/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.user.success;

import com.vaadin.event.MouseEvents;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import hailu.vaadin.authentication.core.VaadinAbstractApplicationServlet;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.model.User;
import hms.common.registration.util.ValidateUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.naming.Context;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.MessageFormat;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.generateSuccessLayoutWithLink;
import static hms.common.registration.util.PropertyHolder.USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserCreationSuccessView extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(UserCreationSuccessView.class);

    public static final String MSISDN = "msisdn";
    public static final String EMAIL = "email";

    private User user;
    private CommonRegistrationApp commonRegistrationApp;

    private void createSuccessLayoutForMsisdnVerification() {
        StringBuilder successMessage = new StringBuilder();
        successMessage.append(commonRegistrationApp.getMessage("registration.user.msisdn.verification.success.message"));
        final String linkString = commonRegistrationApp.getMessage("registration.user.user.success.login.link");
        String url = commonRegistrationApp.getURL().getPath();
        logger.debug("Path of the application : " + url);
        if (url.contains("default")) {
            url = url.substring(0, url.indexOf("/default"));
        } else {
            url = url + "#home";
        }
        if (!ValidateUser.isValidUser(user)) {
            successMessage.append(" ")
                    .append(commonRegistrationApp.getMessage("registration.user.email.verification.pending.message"));
        }

        final String logoutUrl = url;
        successMessage.append(" ").append(commonRegistrationApp.getMessage("registration.user.login.message"));
        logger.debug("Url to login page : " + url);
        Button login = getLoginLink(linkString, logoutUrl);
        CustomLayout userCreationSuccessLayout = generateSuccessLayoutWithLink(successMessage.toString(), linkString, url);
        addComponent(userCreationSuccessLayout);
        addComponent(login);
        setSpacing(true);
    }

    private Button getLoginLink(String linkString, final String logoutUrl) {
        Button login = new Button(linkString);
        login.setStyleName("success-button");
        login.setWidth(10, UNITS_PERCENTAGE);
        login.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                commonRegistrationApp.getMainWindow().open(new ExternalResource(logoutUrl));
                commonRegistrationApp.close();
            }
        });
        return login;
    }


    private void createSuccessLayoutForEmailVerification() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("800px");
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);

        Label successLabel = new Label(commonRegistrationApp.getMessage("registration.user.user.creation.success.message"));
        verticalLayout.addComponent(successLabel);
        verticalLayout.setComponentAlignment(successLabel, Alignment.TOP_CENTER);

        String verificationMessage = MessageFormat.format(commonRegistrationApp.getMessage("registration.user.email.verification"),
                USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION);
        Label verificationMessageLabel = new Label(verificationMessage);
        verticalLayout.addComponent(verificationMessageLabel);
        verticalLayout.setComponentAlignment(verificationMessageLabel, Alignment.MIDDLE_CENTER);

        String loginLinkMessage = commonRegistrationApp.getMessage("registration.user.email.verification.click.here");
        String linkString = commonRegistrationApp.getMessage("registration.user.user.success.login.link");
        String url = commonRegistrationApp.getURL().getPath();
        logger.debug("Path of the application : " + url);
        if (url.contains("default")) {
            url = url.substring(0, url.indexOf("/default"));
        } else {
            url = url + "#home";
        }
        logger.debug("Url to login page : " + url);
        loginLinkMessage = loginLinkMessage.replace(linkString, "<a href='" + url + "'>" + linkString + "</a>");
        try {
            CustomLayout customLayout = new CustomLayout(new ByteArrayInputStream(loginLinkMessage.getBytes()));
            verticalLayout.addComponent(customLayout);
            verticalLayout.setComponentAlignment(customLayout, Alignment.BOTTOM_CENTER);
        } catch (IOException e) {
            logger.error("Error while converting to byte stream ", e);
        }
        addComponent(verticalLayout);
    }

    public UserCreationSuccessView(User user, CommonRegistrationApp commonRegistrationApp, String successType) {
        this.user = user;
        this.commonRegistrationApp = commonRegistrationApp;

        if (MSISDN.equals(successType)) {
            createSuccessLayoutForMsisdnVerification();
        } else {
            createSuccessLayoutForEmailVerification();
        }
    }
}