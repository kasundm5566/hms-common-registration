/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.validator;

import com.vaadin.data.Validator;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hms.common.registration.util.PropertyHolder.EMAIL_REGEX_PATTERN;

/**
 * A Validator to check the existing user names
 * <p/>
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class EmailCheckValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(EmailCheckValidator.class);

    private UserService userService;
    private Pattern pattern;
    private String userId;
    private String invalidEmailMessage;
    private String unavailableEmailMessage;

    public EmailCheckValidator(CommonRegistrationApp commonRegistrationApp){
        pattern = Pattern.compile(EMAIL_REGEX_PATTERN);
        invalidEmailMessage = commonRegistrationApp.getMessage("registration.user.invalid.email");
        unavailableEmailMessage = commonRegistrationApp.getMessage("registration.user.email.not.available");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
    }
    
    @Override
    public void validate(Object o) throws InvalidValueException {
        logger.debug("Validating Email [{}]", o);
        if (!isValidEmail((String) o)) {
            logger.debug("Invalid Email Address [{}] ", o);
            throw new InvalidValueException(invalidEmailMessage);
        } else if (!userService.isEmailAvailableForUserId((String) o, userId)) {
            logger.debug("User account already exists with the given Email [{}] ", o);
            throw new InvalidValueException(unavailableEmailMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        return isValidEmail((String) value) && userService.isEmailAvailableForUserId((String) value, userId);
    }

    public boolean isValidEmail(String email) {
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}