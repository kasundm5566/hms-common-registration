/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.ui.content.admin.search;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.admin.add.AddNewModuleView;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.service.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.List;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;

/**
 * Handle the search module form implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ManageModuleView extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageModuleView.class);
    private CommonRegistrationApp commonRegistrationApp;
    private ModuleService moduleService;
    private TextField searchTextField;
    private VerticalLayout tableLayout;
    private IndexedContainer moduleContainer;
    private Table moduleList = new Table();
    private String nameColumn;
    private String descColumn;
    private String actionColumn;


    public ManageModuleView(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        nameColumn = commonRegistrationApp.getMessage("registration.admin.module.moduleName");
        descColumn = commonRegistrationApp.getMessage("registration.admin.module.description");
        actionColumn = commonRegistrationApp.getMessage("registration.action.heading");
        moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setImmediate(true);
        createShowAllView();
        generateManageModuleView();
    }

    private void createShowAllView() {
        moduleContainer = new IndexedContainer();
        moduleContainer.addContainerProperty(nameColumn, String.class, "");
        moduleContainer.addContainerProperty(descColumn, String.class, "");
        moduleContainer.addContainerProperty(actionColumn, HorizontalLayout.class, "");
        try {
            List<Module> moduleList = moduleService.findAllModules();
            for (final Module module : moduleList) {
                final Item item = moduleContainer.addItem(module);
                final Button viewPermissionsLink = createImageLink(null, VIEW_ICON, commonRegistrationApp.getMessage("registration.admin.module.view.permissions"), commonRegistrationApp);
                final Button editLink = createImageLink(null, EDIT_ICON, commonRegistrationApp.getMessage("registration.button.edit"), commonRegistrationApp);
                final Button removeLink = createImageLink(null, REMOVE_ICON,commonRegistrationApp.getMessage("registration.button.delete"), commonRegistrationApp);

                viewPermissionsLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window viewPermissionsWindow = generateViewPermissionsSubWindow(module, "registration.admin.module.view.permissions");
                        getWindow().addWindow(viewPermissionsWindow);
                    }
                });
                editLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window addModuleWindow = generateModuleManageSubWindow(true, module, "registration.admin.module.edit.heading");
                        getWindow().addWindow(addModuleWindow);
                    }
                });
                removeLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window removeModuleWindow = createRemoveConfirmationBox(module);
                        getWindow().addWindow(removeModuleWindow);
                    }
                });
                final HorizontalLayout horizontalLayout = new HorizontalLayout();
                horizontalLayout.addComponent(viewPermissionsLink);
                horizontalLayout.addComponent(editLink);
                horizontalLayout.addComponent(removeLink);
                item.getItemProperty(nameColumn).setValue(module.getModuleName());
                item.getItemProperty(descColumn).setValue(module.getDescription());
                item.getItemProperty(actionColumn).setValue(horizontalLayout);
            }
        } catch (DataManipulationException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param module
     * @param caption
     * @return
     */
    private Window generateViewPermissionsSubWindow(Module module, String caption) {
        final Window viewGroupsWindow = new Window(commonRegistrationApp.getMessage(caption));
        VerticalLayout verticalLayout = new VerticalLayout();
        TextField moduleName = (TextField) createTextField(commonRegistrationApp.getMessage("registration.admin.role.module"), false, "", commonRegistrationApp);
        moduleName.setValue(module.getModuleName());
        moduleName.setReadOnly(true);
        viewGroupsWindow.setStyleName("opaque");
        viewGroupsWindow.setWidth("750px");
        viewGroupsWindow.center();
        viewGroupsWindow.setImmediate(true);
        verticalLayout.addComponent(moduleName);
        verticalLayout.addComponent(new ManagePermissionView(commonRegistrationApp, module));
        viewGroupsWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(viewGroupsWindow);
            }
        });
        viewGroupsWindow.addComponent(verticalLayout);
        return viewGroupsWindow;
    }

    private void generateManageModuleView() {
        HorizontalLayout topLinks = new HorizontalLayout();
        topLinks.setWidth("400px");
        topLinks.setSpacing(true);
        topLinks.addComponent(createSearchComponent());
        addComponent(topLinks);
        moduleList.setSizeFull();
        moduleList.setSelectable(true);
        moduleList.setContainerDataSource(moduleContainer);
        moduleList.setImmediate(true);
        moduleList.addListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                Object id = moduleList.getValue();
                Window addModuleWindow = generateModuleManageSubWindow(true, (Module)id, "registration.admin.module.edit.heading");
                getWindow().addWindow(addModuleWindow);
            }
        });

        tableLayout.addComponent(moduleList);
        addComponent(tableLayout);
        setMargin(true);
        setSpacing(true);
    }

    /**
     *
     * @return
     */
    private HorizontalLayout createSearchComponent() {
        HorizontalLayout searchComponent = new HorizontalLayout();
        searchComponent.setSpacing(true);
        searchTextField = new TextField();
        searchTextField.setInputPrompt(commonRegistrationApp.getMessage("registration.admin.module.moduleName"));
        searchTextField.setImmediate(true);
        searchTextField.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                moduleContainer.removeContainerFilters(nameColumn);
                if (searchTextField.toString().length() > 0 && !nameColumn.equals(searchTextField.toString())) {
                    moduleContainer.addContainerFilter(nameColumn, searchTextField.toString(),
                            true, false);
                }
                getWindow().showNotification("",MessageFormat.format(commonRegistrationApp.getMessage("registration.search.success"),
                        moduleContainer.size()));
            }
        });
        searchComponent.addComponent(new Embedded(null, SEARCH_ICON));
        searchComponent.addComponent(searchTextField);
        return searchComponent;
    }

    /**
     *
     * @param editState
     * @param module
     * @param caption
     * @return
     */
    private Window generateModuleManageSubWindow(boolean editState, Module module, String caption) {
        final Window addModuleWindow = new Window(commonRegistrationApp.getMessage(caption));
        addModuleWindow.setStyleName("opaque");
        addModuleWindow.setWidth("720px");
//        addModuleWindow.setHeight("400px");
        addModuleWindow.center();
        addModuleWindow.setImmediate(true);
        addModuleWindow.addComponent(new AddNewModuleView(commonRegistrationApp, module, editState));
        addModuleWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(addModuleWindow);
            }
        });
        return addModuleWindow;
    }

    /**
     *
     * @param module
     * @return
     */
    private Window createRemoveConfirmationBox(final Module module) {
        final Window confirmation = new Window();
        confirmation.setCaption(commonRegistrationApp.getMessage("registration.confirmation.caption"));
        confirmation.setStyleName("opaque");
        confirmation.setWidth("425px");
        confirmation.setHeight("150px");
        confirmation.center();

        Button yesBtn = new Button(commonRegistrationApp.getMessage("registration.button.yes"));
        yesBtn.setStyleName("action-button");
        yesBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
                try {
                    moduleService.remove(module);
                    moduleContainer.removeItem(module);
                    getWindow().showNotification(new Window.Notification(commonRegistrationApp.getMessage("registration.user.module.delete.success")));
                } catch (DataManipulationException e) {
                    LOGGER.error("Data access error", e);
                }
            }
        });

        Button noBtn = new Button(commonRegistrationApp.getMessage("registration.button.no"));
        noBtn.setStyleName("action-button");
        noBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
            }
        });
        Label msg = new Label(MessageFormat.format(commonRegistrationApp.getMessage("registration.admin.module.remove.confirmation"),
                module.getModuleName()));

        GridLayout conBoxLayout = new GridLayout(2, 2);
        conBoxLayout.setSpacing(true);
        conBoxLayout.setSizeFull();
        conBoxLayout.addComponent(msg, 0, 0, 1, 0);
        conBoxLayout.addComponent(yesBtn);
        conBoxLayout.setComponentAlignment(yesBtn, Alignment.MIDDLE_CENTER);
        conBoxLayout.addComponent(noBtn);
        conBoxLayout.setComponentAlignment(noBtn, Alignment.MIDDLE_CENTER);

        confirmation.addComponent(conBoxLayout);
        return confirmation;
    }

}