/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin.search;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.common.registration.model.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.common.WindowCloseListener;
import hms.common.registration.ui.content.admin.add.AddNewUserView;
import hms.common.registration.ui.content.admin.view.UserProfileView;
import hms.common.registration.ui.content.subcorporate.AddSubCorporateUserView;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.pagingcomponent.PagingComponent;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.PropertyHolder.TABLE_PAGING_NO_OF_PAGES_PER_VIEW;
import static hms.common.registration.util.PropertyHolder.TABLE_PAGING_NO_OF_RECORDS_PER_PAGE;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ManageUserView extends VerticalLayout implements WindowCloseListener {

    private static final Logger logger = LoggerFactory.getLogger(ManageUserView.class);

    protected CommonRegistrationApp commonRegistrationApp;
    protected UserService userService;

    private HorizontalLayout searchLayout = new HorizontalLayout();
    private VerticalLayout tableLayout = new VerticalLayout();
    private HorizontalLayout pagingLayout = new HorizontalLayout();
    private Table userTable = new Table();
    private IndexedContainer container;
    private Label infoLabel = new Label();
    protected SearchUserForm searchUserForm;
    private boolean searchByModuleGroup;
    private UserGroupService userGroupService;
    private UserGroup userGroup;

    private int currentPageStart;
    private List<Condition> currentConditionList;

    private String usernameColumn;
    private String firstNameColumn;
    private String lastNameColumn;
    private String userStatusColumn;
    private String userTypeColumn;
    private String emailColumn;
    private String mobileNoColumn;
    private String designationColumn;
    private String ldapColumn;
    private String addressColumn;
    private String actionColumn;

    private static final int recordsPerPage = Integer.parseInt(TABLE_PAGING_NO_OF_RECORDS_PER_PAGE);
    private static final int pagesPerView = Integer.parseInt(TABLE_PAGING_NO_OF_PAGES_PER_VIEW);
    private List<Condition> originalConditionList = new ArrayList<Condition>();

    public ManageUserView(CommonRegistrationApp commonRegistrationApp, List<Condition> originalConditionList, UserGroup userGroup) {
        this.commonRegistrationApp = commonRegistrationApp;
        this.originalConditionList = originalConditionList;
        if (userGroup != null) {
            this.userGroup = userGroup;
            searchByModuleGroup = true;
        }
        userGroupService = (UserGroupService) commonRegistrationApp.getBean("userGroupServiceImpl");
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        searchUserForm = new SearchUserForm(this, commonRegistrationApp);
        usernameColumn = commonRegistrationApp.getMessage("registration.admin.user.username");
        firstNameColumn = commonRegistrationApp.getMessage("registration.admin.user.first.name");
        lastNameColumn = commonRegistrationApp.getMessage("registration.admin.user.last.name");
        userStatusColumn = commonRegistrationApp.getMessage("registration.admin.user.status");
        userTypeColumn = commonRegistrationApp.getMessage("registration.admin.user.type");
        emailColumn = commonRegistrationApp.getMessage("registration.admin.user.email");
        mobileNoColumn = commonRegistrationApp.getMessage("registration.admin.user.phone.no");
        designationColumn = commonRegistrationApp.getMessage("registration.admin.user.designation");
        ldapColumn = commonRegistrationApp.getMessage("registration.admin.user.ldap.user");
        addressColumn = commonRegistrationApp.getMessage("registration.admin.user.address");
        actionColumn = commonRegistrationApp.getMessage("registration.action.heading");

        tableLayout.setSpacing(true);
        container = createIndexedContainer();
        generateManageUserView();
        populateTableWithPaging();
    }

    private void generateManageUserView() {
        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.setHeight("65px");
        searchLayout.addComponent(searchUserForm);

        HorizontalLayout searchButtonLayout = createSearchButton();
        searchLayout.addComponent(searchButtonLayout);
        searchLayout.setComponentAlignment(searchButtonLayout, Alignment.MIDDLE_RIGHT);
        addComponent(searchLayout);

        userTable.setSizeFull();
        userTable.setSelectable(true);
        userTable.setContainerDataSource(container);
        tableLayout.addComponent(infoLabel);
        tableLayout.addComponent(userTable);
        addComponent(tableLayout);

        addComponent(pagingLayout);
        setComponentAlignment(pagingLayout, Alignment.BOTTOM_CENTER);
        setMargin(true);
        setSpacing(true);
    }

    /**
     * Create the indexed container for user records view
     *
     * @return
     */
    private IndexedContainer createIndexedContainer() {
        IndexedContainer ic = new IndexedContainer();
        ic.addContainerProperty(usernameColumn, String.class, "");
        ic.addContainerProperty(firstNameColumn, String.class, "");
        ic.addContainerProperty(lastNameColumn, String.class, "");
        ic.addContainerProperty(userStatusColumn, String.class, "");
        ic.addContainerProperty(userTypeColumn, String.class, "");
        ic.addContainerProperty(emailColumn, String.class, "");
        ic.addContainerProperty(mobileNoColumn, String.class, "");
        ic.addContainerProperty(designationColumn, String.class, "");
        ic.addContainerProperty(ldapColumn, String.class, "");
        ic.addContainerProperty(addressColumn, String.class, "");

        if (!searchByModuleGroup) {
            ic.addContainerProperty(actionColumn, HorizontalLayout.class, "");
        }
        return ic;
    }

    /**
     * Create the search button
     * Search includes paging
     *
     * @return
     */
    private HorizontalLayout createSearchButton() {
        HorizontalLayout searchButtonLayout = new HorizontalLayout();
        searchButtonLayout.setSpacing(true);
        searchButtonLayout.setMargin(true);
        final Button searchButton = new Button(commonRegistrationApp.getMessage("registration.button.search"));
        searchButton.setStyleName("action-button");
        searchButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                populateTableWithPaging();
            }
        });
        searchButtonLayout.addComponent(searchButton);
        return searchButtonLayout;
    }

    /**
     * Load the user details in to Table with paging
     */
    private void populateTableWithPaging() {
        infoLabel.setValue(null);
        container.removeAllItems();
        final List<Condition> conditionList = searchUserForm.getSearchConditionList();
        conditionList.addAll(originalConditionList);

        try {
            if (searchByModuleGroup) {
                conditionList.add(new Condition("userGroup", userGroup, " = "));
            }
            List<Long> list = userService.findUserIdListByConditionList(conditionList);

            if (list != null && list.size() > 0) {
                logger.debug("No of records: " + list.size());
                PagingComponent pagingComponent = new PagingComponent(recordsPerPage, pagesPerView,
                        list, new PagingComponent.PagingComponentListener() {
                    @Override
                    public void displayPage(PagingComponent.ChangePageEvent changePageEvent) {
                        reloadPage(changePageEvent.getPageRange().getIndexPageStart(), conditionList);
                    }
                });
                pagingLayout.removeAllComponents();
                pagingLayout.addComponent(pagingComponent);
                pagingLayout.setComponentAlignment(pagingComponent, Alignment.MIDDLE_CENTER);
            } else {
                infoLabel.setValue(commonRegistrationApp.getMessage("registration.admin.user.search.no.records.found"));
            }
        } catch (Exception e) {
            logger.error("Data Manipulation exception", e);
        }
    }

    private void reloadPage(int indexStart, List<Condition> conditionList) {
        try {
            logger.debug("Reloading user details table ...");
            currentPageStart = indexStart;
            currentConditionList = conditionList;
            container.removeAllItems();
            List<User> userList;
            if (searchByModuleGroup) {
                conditionList.add(new Condition("userGroup", userGroup, "="));
            }
            userList = userService.findUserListByConditionList(indexStart, recordsPerPage, conditionList);
            addRecordsToTable(userList);
        } catch (Exception e) {
            logger.error("Data Manipulation exception", e);
        }
    }

    private void reloadPage() {
        reloadPage(currentPageStart, currentConditionList);
    }

    private void addRecordsToTable(List<User> userList) {
        for (final User user : userList) {
            Item item = container.addItem(user);
            item.getItemProperty(usernameColumn).setValue(user.getUsername());
            item.getItemProperty(firstNameColumn).setValue(user.getFirstName());
            item.getItemProperty(lastNameColumn).setValue(user.getLastName());
            item.getItemProperty(userStatusColumn).setValue(user.getUserStatus().toString());
            item.getItemProperty(userTypeColumn).setValue(user.getUserType());
            item.getItemProperty(ldapColumn).setValue(user.isLdapUser());
            item.getItemProperty(emailColumn).setValue(user.getEmail());
            item.getItemProperty(addressColumn).setValue(user.getAddress());
            item.getItemProperty(designationColumn).setValue(user.getProfession());
            item.getItemProperty(mobileNoColumn).setValue(user.getMobileNo());

            if (!searchByModuleGroup) {
                final Button viewLink = createImageLink(null, VIEW_ICON, commonRegistrationApp.getMessage("registration.admin.user.view.profile"), commonRegistrationApp);
                final Button editLink = createImageLink(null, EDIT_ICON, commonRegistrationApp.getMessage("registration.button.edit"), commonRegistrationApp);
                final Button removeLink = createImageLink(null, REMOVE_ICON, commonRegistrationApp.getMessage("registration.button.delete"), commonRegistrationApp);
                final Button activateLink = createImageLink(null, DISABLE_ICON, commonRegistrationApp.getMessage("registration.button.disable"), commonRegistrationApp);
                if (user.isEnabled()) {
                    activateLink.setIcon(DISABLE_ICON);
                    activateLink.setDescription(commonRegistrationApp.getMessage("registration.button.disable"));
                } else {
                    activateLink.setIcon(ENABLE_ICON);
                    activateLink.setDescription(commonRegistrationApp.getMessage("registration.button.enable"));
                }
                activateLink.setStyleName(BaseTheme.BUTTON_LINK);
                activateLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        try {
                            Window confirmation = createEnableDisableConfirmationBox(user, activateLink);
                            getWindow().addWindow(confirmation);
                        } catch (Exception e) {
                            logger.error("Unexpected error occurred", e);
                        }
                    }
                });
                viewLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window userProfileWindow = generateUserProfileSubWindow(user, "registration.admin.user.view.profile");
                        getWindow().addWindow(userProfileWindow);
                    }
                });
                editLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window addModuleWindow = generateUserManageSubWindow(true, user, "registration.admin.user.edit.heading", ManageUserView.this);
                        getWindow().addWindow(addModuleWindow);
                    }
                });
                removeLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window removeModuleWindow = createRemoveConfirmationBox(user);
                        getWindow().addWindow(removeModuleWindow);
                    }
                });
                final HorizontalLayout horizontalLayout = new HorizontalLayout();
                horizontalLayout.setWidth("100px");
                horizontalLayout.addComponent(viewLink);
                horizontalLayout.addComponent(editLink);
                horizontalLayout.addComponent(removeLink);
                horizontalLayout.addComponent(activateLink);
                item.getItemProperty(actionColumn).setValue(horizontalLayout);
            }
        }
    }

    private Window createEnableDisableConfirmationBox(final User user, final Button activateLink) {

        final Window confirmation = new Window();
        confirmation.setCaption(commonRegistrationApp.getMessage("registration.confirmation.caption"));
        confirmation.setStyleName("opaque");
        confirmation.setWidth("425px");
        confirmation.setHeight("125px");
        confirmation.center();

        Button yesBtn = new Button(commonRegistrationApp.getMessage("registration.button.yes"));
        yesBtn.setStyleName("action-button");
        yesBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Window.Notification notification;
                if (user.isEnabled()) {
                    user.setEnabled(false);
                    user.setUserStatus(UserStatus.SUSPEND);
                    activateLink.setIcon(ENABLE_ICON);
                    notification = new Window.Notification("", MessageFormat.format(commonRegistrationApp.getMessage("registration.user.disable.success"),
                            user.getUsername()));
                } else {
                    user.setEnabled(true);
                    user.setUserStatus(UserStatus.ACTIVE);
                    activateLink.setIcon(DISABLE_ICON);
                    notification = new Window.Notification("", MessageFormat.format(commonRegistrationApp.getMessage("registration.user.enable.success"),
                            user.getUsername()));
                }
                try {
                    userService.merge(user);
                } catch (DataManipulationException e) {
                    logger.error("Error while enabling/disabling user ", e);
                }
                getWindow().removeWindow(confirmation);
                getWindow().showNotification(notification);
                reloadPage();
            }
        });

        Button noBtn = new Button(commonRegistrationApp.getMessage("registration.button.no"));
        noBtn.setStyleName("action-button");
        noBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
            }
        });
        String enableDisableWord;
        if (user.isEnabled()) {
            enableDisableWord = commonRegistrationApp.getMessage("registration.button.disable");
        } else {
            enableDisableWord = commonRegistrationApp.getMessage("registration.button.enable");
        }
        Label msg = new Label(MessageFormat.format(commonRegistrationApp.getMessage("registration.admin.user.enable.disable.confirmation"),
                enableDisableWord, user.getUsername()));

        GridLayout conBoxLayout = new GridLayout(2, 2);
        conBoxLayout.setSpacing(true);
        conBoxLayout.setSizeFull();
        conBoxLayout.addComponent(msg, 0, 0, 1, 0);
        conBoxLayout.addComponent(yesBtn);
        conBoxLayout.setComponentAlignment(yesBtn, Alignment.MIDDLE_CENTER);
        conBoxLayout.addComponent(noBtn);
        conBoxLayout.setComponentAlignment(noBtn, Alignment.MIDDLE_CENTER);

        confirmation.addComponent(conBoxLayout);
        return confirmation;
    }

    /**
     * @param user
     * @param caption
     * @return
     */
    private Window generateUserProfileSubWindow(User user, String caption) {
        final Window userProfileWindow = new Window(commonRegistrationApp.getMessage(caption));
        userProfileWindow.setStyleName("white-background");
        userProfileWindow.setWidth("700px");
        userProfileWindow.center();
        userProfileWindow.setImmediate(true);
        userProfileWindow.addComponent(new UserProfileView(user, commonRegistrationApp));
        userProfileWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(userProfileWindow);
            }
        });
        return userProfileWindow;
    }

    /**
     * Create a window to edit user details
     *
     * @param editState
     * @param user      user whose details to be edited
     * @param caption   caption of the edit user sub window
     * @return
     */
    private Window generateUserManageSubWindow(boolean editState, User user, String caption, WindowCloseListener windowCloseListener) {
        final Window addUserWindow = new Window(commonRegistrationApp.getMessage(caption));
        addUserWindow.setStyleName("opaque");
        addUserWindow.setWidth("700px");
        addUserWindow.center();
        addUserWindow.setImmediate(true);
        addUserWindow.addComponent(createEditUserView(user, new WindowCloseListener() {
            @Override
            public void onWindowClosed(Map data) {
                getWindow().removeWindow(addUserWindow);
                reloadPage();
            }
        }));
        addUserWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getWindow().removeWindow(addUserWindow);
                reloadPage();
            }
        });

        return addUserWindow;
    }

    private Component createEditUserView(User user, WindowCloseListener parentWindow) {
        Component editUserView = null;
        switch (user.getUserType()) {
            case INDIVIDUAL:
                editUserView = new AddNewUserView(commonRegistrationApp, user, true, parentWindow);
                break;
            case CORPORATE:
                editUserView = new AddNewUserView(commonRegistrationApp, user, true, parentWindow);
                break;
            case ADMIN_USER:
                editUserView = new AddNewUserView(commonRegistrationApp, user, true, parentWindow);
                break;
            case AUDITING_USER:
                editUserView = new AddNewUserView(commonRegistrationApp, user, true, parentWindow);
                break;
            case SUB_CORPORATE:
                editUserView = new AddSubCorporateUserView(commonRegistrationApp, (SubCorporateUser) user, true, parentWindow);
                break;
            default:
                editUserView = new AddNewUserView(commonRegistrationApp, user, true, parentWindow);
                break;
        }
        return editUserView;
    }

    /**
     * Create a confirmation window
     *
     * @param user user to be removed
     * @return confirmation window
     */
    private Window createRemoveConfirmationBox(final User user) {
        final Window confirmation = new Window();
        confirmation.setCaption(commonRegistrationApp.getMessage("registration.confirmation.caption"));
        confirmation.setStyleName("opaque");
        confirmation.setWidth("425px");
        confirmation.setHeight("150px");
        confirmation.center();

        Button yesBtn = new Button(commonRegistrationApp.getMessage("registration.button.yes"));
        yesBtn.setStyleName("action-button");
        yesBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
                try {
                    userService.remove(user);
                    container.removeItem(user);
                    getWindow().showNotification(new Window.Notification("", MessageFormat.format(commonRegistrationApp.getMessage("registration.user.delete.success"),
                            user.getUsername())));
                } catch (DataManipulationException e) {
                    logger.error("Data Access Error", e);
                }
            }
        });

        Button noBtn = new Button(commonRegistrationApp.getMessage("registration.button.no"));
        noBtn.setStyleName("action-button");
        noBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
            }
        });
        Label msg = new Label(MessageFormat.format(commonRegistrationApp.getMessage("registration.admin.user.remove.confirmation"),
                user.getUsername()));

        GridLayout conBoxLayout = new GridLayout(2, 2);
        conBoxLayout.setSpacing(true);
        conBoxLayout.setSizeFull();
        conBoxLayout.addComponent(msg, 0, 0, 1, 0);
        conBoxLayout.addComponent(yesBtn);
        conBoxLayout.setComponentAlignment(yesBtn, Alignment.MIDDLE_CENTER);
        conBoxLayout.addComponent(noBtn);
        conBoxLayout.setComponentAlignment(noBtn, Alignment.MIDDLE_CENTER);

        confirmation.addComponent(conBoxLayout);
        return confirmation;
    }

    public void showInputErrorMessage(String errorMessage) {
        infoLabel.setValue(errorMessage);
    }

    @Override
    public void onWindowClosed(Map data) {
        reloadPage();
        getWindow().removeWindow((Window) data.get("window"));
        logger.debug("Window Closed ...");
    }
}


