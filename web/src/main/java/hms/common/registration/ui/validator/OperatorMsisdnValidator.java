/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import hms.common.registration.ui.CommonRegistrationApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;

import static hms.common.registration.util.PropertyHolder.MSISDN_REGEX_PATTERN;
import static hms.common.registration.util.PropertyHolder.OPERATOR_CODE_MAP;

/**
 * Created by IntelliJ IDEA.
 * User: hms
 * Date: 7/29/11
 * Time: 10:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class OperatorMsisdnValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperatorMsisdnValidator.class);

    private String operator;
    private Set<Map.Entry<String, String>> operators = OPERATOR_CODE_MAP.entrySet();
    private CommonRegistrationApp commonRegistrationApp;

    public OperatorMsisdnValidator(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            LOGGER.debug("Checking for Operator and Msisdn matching");
            throw new InvalidValueException(commonRegistrationApp.getMessage("registration.user.invalid.msisdn"));
        }
    }

    @Override
    public boolean isValid(Object value) {
        RegexpValidator regexPatternValidator = new RegexpValidator(MSISDN_REGEX_PATTERN,
                commonRegistrationApp.getMessage("registration.user.invalid.msisdn"));
        if (operator == null) {
            LOGGER.debug("Operator is null");
            return false;
        } else if (!regexPatternValidator.isValid(value)) {
            LOGGER.debug("Regex pattern not matching");
            return false;
        } else if (!operatorCodeMatching(operator, (String) value)) {
            LOGGER.debug("Msisdn is not matching with the operator");
            return false;
        } else {
            return true;
        }
    }


    private boolean operatorCodeMatching(String operator, String msisdn) {
        for (Map.Entry entry : operators) {
            if (msisdn.startsWith((String) entry.getKey()) && operator.equals((String) entry.getValue())) {
                LOGGER.debug("Operator and MSISDN matching");
                return true;
            }
        }
        return false;
    }

    public void setOperator(String operator) {
        LOGGER.debug("Operator is set to : " + operator);
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }
}
