/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.gwt.server.HttpServletRequestListener;
import com.vaadin.terminal.gwt.server.WebApplicationContext;
import com.vaadin.ui.*;
import hailu.vaadin.authentication.core.VaadinApplication;
import hms.common.registration.ui.common.util.ExternalLinks;
import hms.common.registration.ui.content.account.UserAccountRecoveryView;
import hms.common.registration.ui.content.admin.view.UserProfileView;
import hms.common.registration.ui.content.home.ActivationView;
import hms.common.registration.ui.content.home.Dashboard;
import hms.common.registration.ui.content.settings.UserSettingTabSheet;
import hms.common.registration.ui.content.user.corporate.CorporateUserForm;
import hms.common.registration.ui.content.user.corporate.DeveloperRegistrationForm;
import hms.common.registration.ui.content.user.individual.IndividualUserForm;
import hms.common.registration.ui.template.ContentLayout;
import hms.common.registration.ui.template.ContentMenu;
import hms.common.registration.ui.template.ContentPanel;
import hms.common.registration.ui.template.MainLayout;
import hms.common.registration.util.UserRoles;
import hms.common.registration.model.CorporateUser;
import hms.common.registration.model.User;
import hms.common.registration.model.UserStatus;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.ui.common.util.RegistrationKeys.*;
import static hms.common.registration.util.RegistrationCommonUtil.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.isConsumerUserEnabled;
import static hms.common.registration.util.RegistrationFeatureRegistry.isCorporateUserEnabled;
import static hms.common.registration.ui.SessionKeys.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.isMultipleDeveloperAccountTypeEnabled;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@SuppressWarnings("serial")
public class CommonRegistrationApp extends VaadinApplication implements HttpServletRequestListener {

    private static final Logger logger = LoggerFactory.getLogger(CommonRegistrationApp.class);

    public static final String individualUserCreationFrag = "common/individual";
    public static final String corporateUserCreationFrag = "common/corporate";
    public static final String openIdUserCreationFrag = "common/openid";
    public static final String dialogOpenIdUserCreationFrag = "common/dialog";
    public static final String accountRecoveryFrag = "common/account/recovery";
    public static final String homeFrag = "home";
    public static final String userSettingsFrag = "settings";
    public static final String adminFrag = "admin";
    public static final String viewUserFrag = "viewuser";
    public static final String accountActivationFrag = "verify";
    public static final String msisdnVerificationRetryFrag = "verify/msisdn";

    private Window mainWindow;
    private MainLayout mainLayout;
    private UriFragmentUtility fragmentUtility;
    private ContentPanel contentPanel;
    private ContentLayout contentLayout;
    private VerticalLayout innerContentPanel;
    private MainApp mainApp;
    private String serviceUrl = "";
    private boolean isAuthenticatedUser = false;
    private Map<String, String> sessionData = new HashMap<String, String>();

    public CommonRegistrationApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public void init() {
        setTheme("registration");
        mainWindow = new Window(getMessage("registration.main.window.title"));
        mainWindow.setName("mainWindow");
        final VerticalLayout content = new VerticalLayout();
        innerContentPanel = new VerticalLayout();
        createFragmentUtility();
        mainWindow.setImmediate(true);
        mainWindow.center();
        mainWindow.setSizeFull();
        mainWindow.addComponent(fragmentUtility);
        mainLayout = new MainLayout(this);
        contentLayout = mainLayout.getContentLayout();
        contentPanel = contentLayout.getContentPanel();
        content.addComponent(mainLayout);
        content.setComponentAlignment(mainLayout, Alignment.MIDDLE_CENTER);
        mainWindow.addComponent(content);
        setMainWindow(mainWindow);
        this.setLogoutURL(ExternalLinks.CAS_LOGOUT_URL);
    }

    private void createFragmentUtility() {

        fragmentUtility = new UriFragmentUtility();
        fragmentUtility.addListener(new UriFragmentUtility.FragmentChangedListener() {

            public void fragmentChanged(UriFragmentUtility.FragmentChangedEvent source) {
                try {
                    final String fragment = source.getUriFragmentUtility().getFragment();
                    logger.debug("Current fragment [{}]", fragment);
                    if (fragment.equals(individualUserCreationFrag)) {
                        createIndividualUserCreationView();
                    } else if (fragment.equals(corporateUserCreationFrag)) {
                        if(isMultipleDeveloperAccountTypeEnabled()) {
                            createDeveloperRegistrationForm();
                        } else {
                            createCorporateUserCreationView();
                        }
                    } else if (fragment.startsWith(openIdUserCreationFrag)) {
                        createOpenIdUserView();
                    } else if (fragment.startsWith(dialogOpenIdUserCreationFrag)) {
                        createDialogOpenIdUserView();
                    } else if (fragment.equals(accountRecoveryFrag)) {
                        createAccountRecoveryView();
                    } else if (fragment.equals(homeFrag)) {
                        createHomeView();
                    } else if (fragment.equals(userSettingsFrag)) {
                        createUserSettingView();
                    } else if (fragment.equals(adminFrag)) {
                        createAdminView();
                    } else if (fragment.startsWith(viewUserFrag)) {
                        createUserView(fragment);
                    } else if (fragment.equals(accountActivationFrag)) {
                        createActivationView();
                    } else if (fragment.startsWith(msisdnVerificationRetryFrag)) {
                        createMsisdnVerificationRetryView();
                    }   else if (fragment.equals("/")) {
                        createHomeView();
                    } else {
                        logger.warn("Unknown fragment [" + fragment + "]. Show home view");
                        createHomeView();
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        });
    }

    private void createUserView(String fragment) {
        mainLayout.removeComponent(mainLayout.getHeaderLayout());
        mainLayout.removeComponent(mainLayout.getFooterLayout());
        mainLayout.getTopHeaderLayout().setModulesMenuVisible(true);
        String[] tokens = fragment.split("/");
        contentPanel.setCaption(getMessage("registration.admin.user.view.profile"));
        changeView(new UserProfileView(tokens[tokens.length - 1], this));
    }


    private void createHomeView() {
        new Dashboard(this, mainLayout);
    }

    private void createActivationView() {
        new ActivationView(this, mainLayout, accountActivationFrag);
    }

    private void createMsisdnVerificationRetryView() {
        new ActivationView(this, mainLayout, msisdnVerificationRetryFrag);
    }

    private void createAdminView() {
        contentLayout.removeAllComponents();
        mainLayout.getTopHeaderLayout().setModulesMenuVisible(true);
        if (this.hasAnyRole(UserRoles.ROLE_REG_LOGIN)) {
            new ContentMenu(this, contentLayout);
        }
    }

    private void createAccountRecoveryView() {
        contentPanel.setCaption(getMessage("registration.user.account.recovery.heading"));
        changeView(new UserAccountRecoveryView(contentPanel, fragmentUtility, this));
    }

    private boolean isUnauthorizedUser() {
        return !isAuthenticatedUser;
    }

    private void createIndividualUserCreationView() {
        if (isValidInternalUserCreation(isConsumerUserEnabled())) return;

        contentPanel.setCaption(getMessage("registration.user.create.account"));
        User user = new User();
        user.setUserStatus(UserStatus.INITIAL);
        changeView(new IndividualUserForm(new User(), this, false));
    }

    private void createCorporateUserCreationView() {
        if (isValidInternalUserCreation(isCorporateUserEnabled())) return;

        CorporateUser corporateUser = new CorporateUser();
        corporateUser.setUserStatus(UserStatus.INITIAL);
        corporateUser.setCorporateUser(true);
        contentPanel.setCaption(getMessage("registration.corporate.create.account"));
        changeView(new CorporateUserForm(corporateUser, this, false));
    }

    private void createDeveloperRegistrationForm() {
        contentPanel.setCaption(getMessage("registration.corporate.create.account"));
        changeView(new DeveloperRegistrationForm(this));
    }

    private boolean isValidInternalUserCreation(boolean isEnable) {
        if (isUnauthorizedUser() || !isEnable) {
            logger.debug("Unauthorized access detected in internal user creation");
            casLogoutView();
            return true;
        }
        return false;
    }

    private void createOpenIdUserView() {
        contentPanel.setCaption(getMessage("registration.user.create.account.from.open.id"));
        User user = fillOpenIdUserData();

        if (isValidOpenIdUserCreation(user)) return;

        changeView(IndividualUserForm.createOpenIdUserView(user, this, true));
    }

    private User fillOpenIdUserData() {
        User user = new User();

        Assertion assertion = getAssertionFromSession();

        if (assertion == null) {
            return user;
        }

        Map<String, String> userDetails = getUserDetails(assertion);

        String id = assertion.getPrincipal().getName();
        String firstName = userDetails.get("first_name");
        String gender = userDetails.get("gender");
        String lastName = userDetails.get("last_name");
        String fullName = (firstName != null ? firstName : "") + (lastName != null ? (" " + lastName) : "");

        logger.debug("Found data for user {}, {}/{}/{}", new Object[]{id, firstName, lastName, gender});

        /*user.setFirstName(firstName);
        user.setLastName(lastName);*/
        user.setFirstName(fullName);
        user.setUsername(id);
        user.setDomain(id.substring(0, id.indexOf('#')));
        user.setDomainId(id.substring(id.indexOf('#') + 1));

        if (gender != null && gender.matches("(?i)male|female")) {
            gender = gender.substring(0, 1).toUpperCase() + gender.substring(1).toLowerCase();
            user.setGender(gender);
        }
        return user;
    }

    private boolean isValidOpenIdUserCreation(User user) {
        String username = user.getUsername();
        if (isUnauthorizedUser() || username == null || username.isEmpty()) {
            logger.debug("Unauthorized access detected in open id user creation");
            casLogoutView();
            return true;
        }
        return false;
    }

    private Assertion getAssertionFromSession() {
        return (Assertion) ((WebApplicationContext) getContext()).getHttpSession().getAttribute(CUSTOM_CAS_ASSERTION);
    }

    private void createDialogOpenIdUserView() {
        String branding = getBranding(this);

        mainLayout.getHeaderLayout().updateTopBanner();

        if (branding.equals("ideamart")) {
            contentPanel.setCaption(getMessage("registration.corporate.create.account"));
            CorporateUser corporateUser = fillDialogCorporateUserData();

            if (isValidOpenIdUserCreation(corporateUser)) return;

            corporateUser.setCorporateUser(true);
            changeView(new CorporateUserForm(corporateUser, this, false));
        } else if (branding.equals("allapps")) {
            contentPanel.setCaption(getMessage("registration.user.create.account"));
            User user = fillDialogConsumerUserData();

            if (isValidOpenIdUserCreation(user)) return;

            changeView(new IndividualUserForm(user, this, false, true, true));
        } else {
            casLogoutView();
        }
    }

    private CorporateUser fillDialogCorporateUserData() {
        CorporateUser corporateUser = new CorporateUser();
        Date birthday = getDefaultBirthday();

        Assertion assertion = getAssertionFromSession();

        if (assertion == null) {
            logger.debug("Corporate user data filling failed since assertion not found in session");
            return corporateUser;
        }

        Map<String, String> userDetails = getUserDetails(assertion);

        logger.debug("Found data for user [{}]", userDetails);

        String id = assertion.getPrincipal().getName();
        String firstName = userDetails.get("first_name");
        String gender = userDetails.get("gender");
        String lastName = userDetails.get("last_name");
        String mobileNumber = userDetails.get("msisdn");
        String email = userDetails.get("email");
        String dob = userDetails.get("birth_day");
        String fullName = (firstName != null ? firstName : "") + (lastName != null ? (" " + lastName) : "");

        try {
            birthday = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
        } catch (ParseException e) {
            logger.error("Invalid birthday format detected [{}]", dob);
        } catch (Exception e) {
            logger.error("Error occurred while processing the birthday", e);
        }

        String countryCode = (String) getBean("countryCode");

        corporateUser.setUsername(id);
        corporateUser.setContactPersonName(fullName);
        corporateUser.setEmail(email);
        if (!mobileNumber.startsWith(countryCode)) {
            corporateUser.setMobileNo(countryCode + mobileNumber);
            corporateUser.setContactPersonPhone(countryCode + mobileNumber);
        } else {
            corporateUser.setMobileNo(mobileNumber);
            corporateUser.setContactPersonPhone(mobileNumber);
        }
        corporateUser.setBirthday(birthday);
        corporateUser.setDomainId(id);
        corporateUser.setDomain("dialog");
        corporateUser.setGender(gender);
        corporateUser.setPassword(String.valueOf(new Date().getTime()));

        logger.debug("Filled corporate user information [{}]", corporateUser);

        return corporateUser;
    }

    private User fillDialogConsumerUserData() {
        User user = new User();
        Date birthday = getDefaultBirthday();

        Assertion assertion = getAssertionFromSession();

        if (assertion == null) {
            logger.debug("Consumer user data filling failed since assertion not found in session");
            return user;
        }

        Map<String, String> userDetails = getUserDetails(assertion);

        String id = assertion.getPrincipal().getName();
        String firstName = userDetails.get("first_name");
        String gender = userDetails.get("gender");
        String lastName = userDetails.get("last_name");
        String email = userDetails.get("email");
        String mobileNumber = userDetails.get("msisdn");
        String dob = userDetails.get("birth_day");
        String fullName = (firstName != null ? firstName : "") + (lastName != null ? (" " + lastName) : "");

        try {
            birthday = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
        } catch (ParseException e) {
            logger.error("Invalid birthday format detected [{}]", dob);
        } catch (Exception e) {
            logger.error("Error occurred while processing the birthday", e);
        }

        String countryCode = (String) getBean("countryCode");

        user.setUsername(id);
        user.setFirstName(fullName);
        user.setEmail(email);
        if (!mobileNumber.startsWith(countryCode)) {
            user.setMobileNo(countryCode + mobileNumber);
        } else {
            user.setMobileNo(mobileNumber);
        }
        user.setBirthday(birthday);
        user.setDomainId(id);
        user.setDomain("dialog");
        user.setGender(gender);
        user.setPassword(String.valueOf(new Date().getTime()));

        logger.debug("Filled consumer user information [{}]", user);

        return user;
    }

    private void createUserSettingView() {
        mainLayout.getTopHeaderLayout().setModulesMenuVisible(true);
        contentPanel.setCaption(getMessage("registration.user.settings.heading"));
        UserSettingTabSheet settingTabSheet = new UserSettingTabSheet(this);
        changeView(settingTabSheet);
    }

    private void casLogoutView() {
        String url = getURL().getPath();
        if (url.contains("default")) {
            url = url.substring(0, url.indexOf("/default")) + "/";
        }
        String redirectUrl = url + "j_spring_security_logout";
        getMainWindow().open(new ExternalResource(redirectUrl));
    }

    public Object getBean(String beanName) {
        return mainApp.getBean(beanName);
    }

    public String getMessage(String key, final Object... arguments) {
        return mainApp.getMessage(key);
    }

    private void changeView(Component component) {
        contentPanel.removeAllComponents();
        contentPanel.addComponent(component);
        contentLayout.removeAllComponents();
        contentLayout.setSizeFull();
        contentLayout.addComponent(contentPanel);
    }

    public UriFragmentUtility getFragmentUtility() {
        return fragmentUtility;
    }

    public MainLayout getMainLayout() {
        return mainLayout;
    }

    public Window getMainWindow() {
        return mainWindow;
    }

    public void setMainWindow(Window mainWindow) {
        super.setMainWindow(mainWindow);
        super.getMainWindow();
        this.mainWindow = mainWindow;
    }

    public VerticalLayout getInnerContentPanel() {
        return innerContentPanel;
    }

    public ContentLayout getContentLayout() {
        return contentLayout;
    }

    public void onRequestStart(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        if (session.getAttribute(CUSTOM_AUTHENTICATION_INTERCEPTOR_EXECUTED) == null && !isAuthenticatedUser) {
            isAuthenticatedUser = true;
        }

        serviceUrl = (String) session.getAttribute(LOCAL_SERVICE);
        setPreviousUrl(request);

        Object sessionTimeoutCookie = session.getAttribute("session-time-out");
        if (sessionTimeoutCookie != null) {
            if (getMainWindow() != null) {
                logger.debug("Session time out cookie found {}", sessionTimeoutCookie);
                casLogoutView();
            }
        }
    }

    @Override
    public void setLogoutURL(String logoutURL) {
        super.setLogoutURL(logoutURL);
    }

    public void onRequestEnd(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("<<<<<<<- Request processing ended ->>>>>>");
    }

    /**
     * @param sessionKey
     * @param value
     * @return
     */
    public boolean putToSession(String sessionKey, String value) {
        // To enforce - put any object to session session key need to be register in the session key registry
        if(!sessionKeys.contains(sessionKey)) {
            return false;
        }
        this.sessionData.put(sessionKey, value);
        return true;
    }

    public String getFromSession(String sessionKey) {
        return sessionData.get(sessionKey);
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    private void setPreviousUrl(HttpServletRequest request) {
        String paramValue = request.getParameter(PREVIOUS_URL_QUERY_PARAM);
        if(paramValue != null) {
            putToSession(PRV_URL, paramValue);
        }
    }
}