/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.user.verification;

import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.verification.MsisdnVerificationLayout;
import hms.common.registration.ui.content.verification.MsisdnVerificationWithRetryLayout;
import hms.common.registration.ui.validator.UserVerificationValidator;
import hms.common.registration.util.sms.SmsMtDispatcher;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static hms.common.registration.util.RegistrationFeatureRegistry.*;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.getSpacer;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserVerificationView extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(UserVerificationView.class);

    private User user;
    private UserService userService;
    private CommonRegistrationApp commonRegistrationApp;
    private SmsMtDispatcher smsMtDispatcher;
    private UserVerificationValidator userVerificationValidator;
    private boolean currentlyLoggedIn = false;

    public UserVerificationView(User user, CommonRegistrationApp commonRegistrationApp, boolean currentlyLoggedIn) {
        logger.debug("Loading User MSISDN and Email verification page content for user [{}]", user.getUsername());
        this.user = user;
        this.commonRegistrationApp = commonRegistrationApp;
        this.currentlyLoggedIn = currentlyLoggedIn;
        userService = (UserService) commonRegistrationApp.getBean("userServiceImpl");
        smsMtDispatcher = (SmsMtDispatcher) commonRegistrationApp.getBean("smsMtDispatcher");
        setWidth("850px");
        addComponent(getSpacer());
        userVerificationValidator = new UserVerificationValidator(commonRegistrationApp.getMessage("registration.user.verification.code.invalid"));

        Component verificationLayout;

        if(isMsisdnVerificationLayoutWithRetry()) {
            verificationLayout = new MsisdnVerificationWithRetryLayout(commonRegistrationApp, userService, user, smsMtDispatcher, currentlyLoggedIn,
                    MsisdnVerificationWithRetryLayout.VerificationViewMode.FIRST_TIME_MODE);
        } else {
            verificationLayout = new MsisdnVerificationLayout(commonRegistrationApp, user, userService, smsMtDispatcher, userVerificationValidator, currentlyLoggedIn);
        }

        addComponent(verificationLayout);
        setComponentAlignment(verificationLayout, Alignment.MIDDLE_CENTER);
    }
}