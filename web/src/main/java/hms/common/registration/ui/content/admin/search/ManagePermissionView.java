/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.ui.content.admin.search;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import hms.common.registration.ui.CommonRegistrationApp;
import hms.common.registration.ui.content.admin.add.AddNewPermissionView;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.Module;
import hms.common.registration.model.Role;
import hms.common.registration.service.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.pagingcomponent.PagingComponent;

import java.text.MessageFormat;
import java.util.List;

import static hms.common.registration.ui.common.CommonUiComponentGenerator.*;
import static hms.common.registration.util.PropertyHolder.TABLE_PAGING_NO_OF_PAGES_PER_VIEW;
import static hms.common.registration.util.PropertyHolder.TABLE_PAGING_NO_OF_RECORDS_PER_PAGE;

/**
 * Created by IntelliJ IDEA.
 * User: hms
 * Date: 6/20/11
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class ManagePermissionView extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagePermissionView.class);
    private CommonRegistrationApp commonRegistrationApp;
    private ModuleService moduleService;
    private Module module;
    private boolean searchByModule;

    private VerticalLayout tableLayout;
    private Table roleTable;
    private IndexedContainer container;

    private String nameColumn;
    private String descColumn;
    private String moduleColumn;
    private String actionColumn;
    private Label infoLabel;
    private HorizontalLayout pagingLayout;
    private static final int recordsPerPage = Integer.parseInt(TABLE_PAGING_NO_OF_RECORDS_PER_PAGE);
    private static final int pagesPerView = Integer.parseInt(TABLE_PAGING_NO_OF_PAGES_PER_VIEW);

    public ManagePermissionView(CommonRegistrationApp commonRegistrationApp, Module module) {
        this.commonRegistrationApp = commonRegistrationApp;
        if(module != null) {
            this.module = module;
            searchByModule = true;
        }
        nameColumn = commonRegistrationApp.getMessage("registration.admin.role.name");
        descColumn = commonRegistrationApp.getMessage("registration.admin.role.description");
        moduleColumn = commonRegistrationApp.getMessage("registration.admin.role.module");
        actionColumn = commonRegistrationApp.getMessage("registration.action.heading");

        moduleService = (ModuleService) commonRegistrationApp.getBean("moduleServiceImpl");
        tableLayout = new VerticalLayout();
        pagingLayout = new HorizontalLayout();
        pagingLayout.setWidth("600px");
        infoLabel = new Label();
        roleTable = new Table();
        container = createIndexedContainer();
        generateManageRoleView();
    }

    private void generateManageRoleView() {

        if(!searchByModule) {
            HorizontalLayout topLayout = new HorizontalLayout();
            topLayout.setWidth("400px");
            topLayout.setSpacing(true);
            topLayout.addComponent(createSearchComponent());
            addComponent(topLayout);
        }

        roleTable.setSizeFull();
        roleTable.setSelectable(true);
        roleTable.setContainerDataSource(container);
        roleTable.setImmediate(true);
        tableLayout.addComponent(roleTable);
        tableLayout.addComponent(infoLabel);
        addComponent(tableLayout);
        addComponent(pagingLayout);
        setMargin(true);
        setSpacing(true);
    }

    private IndexedContainer createIndexedContainer() {

        container = new IndexedContainer();
        container.addContainerProperty(nameColumn, String.class, "");
        container.addContainerProperty(descColumn, String.class, "");
        if(!searchByModule) {
            container.addContainerProperty(moduleColumn, String.class, "");
            container.addContainerProperty(actionColumn, HorizontalLayout.class, "");
        }
        populateTableWithPaging();
        return container;
    }

    private void loadToTable(List<Role> roleList) {
        container.removeAllItems();
        LOGGER.debug("Container" + container.getContainerPropertyIds());
        for (final Role role : roleList) {
            final Item item = container.addItem(role);

            if(container.getContainerPropertyIds().contains(actionColumn)) {
                final Button viewGroupsLink = createImageLink(null, VIEW_ICON, commonRegistrationApp.getMessage("registration.admin.role.view.groups"), commonRegistrationApp);
                final Button editLink = createImageLink(null, EDIT_ICON, commonRegistrationApp.getMessage("registration.button.edit"), commonRegistrationApp);
                final Button removeLink = createImageLink(null, REMOVE_ICON,commonRegistrationApp.getMessage("registration.button.delete"), commonRegistrationApp);

                viewGroupsLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window viewGroupsWindow = generateViewGroupsSubWindow(role, "registration.admin.role.view.groups");
                        getWindow().addWindow(viewGroupsWindow);
                    }
                });

                editLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window addModuleWindow = generateRoleManageSubWindow(true, role, "registration.admin.role.edit.heading");
                        getWindow().addWindow(addModuleWindow);
                    }
                });
                removeLink.addListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Window removeModuleWindow = createRemoveConfirmationBox(role);
                        getWindow().addWindow(removeModuleWindow);
                    }
                });
                final HorizontalLayout horizontalLayout = new HorizontalLayout();
                horizontalLayout.setWidth("100px");
                horizontalLayout.addComponent(viewGroupsLink);
                horizontalLayout.addComponent(editLink);
                horizontalLayout.addComponent(removeLink);
                item.getItemProperty(actionColumn).setValue(horizontalLayout);
            }
            item.getItemProperty(nameColumn).setValue(role.getName());
            item.getItemProperty(descColumn).setValue(role.getDescription());
            if(container.getContainerPropertyIds().contains(moduleColumn)) {
                item.getItemProperty(moduleColumn).setValue(role.getModule().getModuleName());
            }
        }
    }

    private void populateTableWithPaging() {
        infoLabel.setValue(null);
        container.removeAllItems();
        List<Long> list;
        if(searchByModule) {
            list = moduleService.findAllRoleIdsByModule(module);
        } else {
            list = moduleService.findAllRoleIds();
        }
        if(list != null && list.size() > 0) {
            LOGGER.debug("No of records: "+list.size());
            PagingComponent pagingComponent = new PagingComponent(recordsPerPage, pagesPerView,
                    list, new PagingComponent.PagingComponentListener() {
                        @Override
                        public void displayPage(PagingComponent.ChangePageEvent changePageEvent) {
                            container.removeAllItems();
                            try {
                                int indexStart = changePageEvent.getPageRange().getIndexPageStart();
                                List<Role> roleList;
                                if(searchByModule) {
                                    roleList = moduleService.findAllRolesByModuleWithPaging(module, indexStart, recordsPerPage);
                                } else {
                                    roleList = moduleService.findAllRolesWithPaging(indexStart, recordsPerPage);
                                }
                                loadToTable(roleList);
                            } catch (Exception e) {
                                LOGGER.error("Data Access Error", e);
                            }
                        }
                    });
            pagingLayout.removeAllComponents();
            pagingLayout.addComponent(pagingComponent);
            pagingLayout.setComponentAlignment(pagingComponent, Alignment.MIDDLE_CENTER);
        } else {
            infoLabel.setValue(commonRegistrationApp.getMessage("registration.admin.user.search.no.records.found"));
        }
    }

    /**
     *
     * @param role
     * @param caption
     * @return
     */
    private Window generateViewGroupsSubWindow(Role role, String caption) {
        final Window viewGroupsWindow = new Window(commonRegistrationApp.getMessage(caption));
        viewGroupsWindow.setStyleName("opaque");
        viewGroupsWindow.setWidth("750px");
        viewGroupsWindow.center();
        viewGroupsWindow.setImmediate(true);
        viewGroupsWindow.addComponent(new ManageModuleGroupView(commonRegistrationApp, role));
        viewGroupsWindow.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(viewGroupsWindow);
            }
        });
        return viewGroupsWindow;
    }

    /**
     * Create the Role edit subwindow
     * @param editState
     * @param role
     * @param caption
     * @return
     */

    private Window generateRoleManageSubWindow(boolean editState, Role role, String caption) {

        final Window editPermission = new Window(commonRegistrationApp.getMessage(caption));
        editPermission.setStyleName("opaque");
        editPermission.setWidth("720px");
        editPermission.center();
        editPermission.setImmediate(true);
        editPermission.addComponent(new AddNewPermissionView(commonRegistrationApp, role, editState));
        editPermission.addListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                getParent().setEnabled(true);
                getParent().getParent().setEnabled(true);
                getParent().getParent().getParent().setEnabled(true);
                getWindow().removeWindow(editPermission);
            }
        });
        return editPermission;
    }

    /**
     * Create the Role remove confirmation box
     * @param role
     * @return
     */
    private Window createRemoveConfirmationBox(final Role role) {

        final Window confirmation = new Window();
        confirmation.setCaption(commonRegistrationApp.getMessage("registration.confirmation.caption"));
        confirmation.setStyleName("opaque");
        confirmation.setWidth("425px");
        confirmation.setHeight("150px");
        confirmation.center();

        Button yesBtn = new Button(commonRegistrationApp.getMessage("registration.button.yes"));
        yesBtn.setStyleName("action-button");
        yesBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
                moduleService.remove(role);
                container.removeItem(role);
                getWindow().showNotification(new Window.Notification("",commonRegistrationApp.getMessage("registration.user.role.delete.success")));
            }
        });

        Button noBtn = new Button(commonRegistrationApp.getMessage("registration.button.no"));
        noBtn.setStyleName("action-button");
        noBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().removeWindow(confirmation);
            }
        });
        Label msg = new Label(MessageFormat.format(commonRegistrationApp.getMessage("registration.admin.role.remove.confirmation"),
                role.getName()));

        GridLayout conBoxLayout = new GridLayout(2, 2);
        conBoxLayout.setSpacing(true);
        conBoxLayout.setSizeFull();
        conBoxLayout.addComponent(msg, 0, 0, 1, 0);
        conBoxLayout.addComponent(yesBtn);
        conBoxLayout.setComponentAlignment(yesBtn, Alignment.MIDDLE_CENTER);
        conBoxLayout.addComponent(noBtn);
        conBoxLayout.setComponentAlignment(noBtn, Alignment.MIDDLE_CENTER);

        confirmation.addComponent(conBoxLayout);
        return confirmation;
    }

    /**
     * create search component
     * @return
     */
    private HorizontalLayout createSearchComponent() {
        HorizontalLayout searchComponent = new HorizontalLayout();
        searchComponent.setSpacing(true);
        searchComponent.addComponent(new Label(commonRegistrationApp.getMessage("registration.admin.role.module")));

        BeanItemContainer<Module> container = new BeanItemContainer<Module>(Module.class);
        try {
            List<Module> moduleList = moduleService.findAllModules();
            container.addAll(moduleList);
        } catch (DataManipulationException e) {
            LOGGER.error("Error while finding all available modules", e);
        }
        final ComboBox moduleNameCombo = new ComboBox();
        moduleNameCombo.setContainerDataSource(container);
        moduleNameCombo.setItemCaptionMode(ListSelect.ITEM_CAPTION_MODE_PROPERTY);
        moduleNameCombo.setItemCaptionPropertyId("moduleName");
        moduleNameCombo.setInputPrompt(commonRegistrationApp.getMessage("registration.user.please.select.msg"));
        searchComponent.addComponent(moduleNameCombo);

        final Button searchButton = createButton("registration.button.search", commonRegistrationApp);
        searchButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                if(moduleNameCombo.getValue() == null) {
                    searchByModule = false;
                } else {
                    searchByModule = true;
                    module = (Module) moduleNameCombo.getValue();
                }
                populateTableWithPaging();
            }
        });
        searchComponent.addComponent(searchButton);
        return searchComponent;
    }
}
