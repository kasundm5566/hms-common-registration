/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.scheduler;

import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static hms.common.registration.util.PropertyHolder.USER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION;

/**
 * Created by IntelliJ IDEA.
 * User: hms
 * Date: 7/27/11
 * Time: 4:33 PM
 * To change this template use File | Settings | File Templates.
 */



public class MsisdnChecker implements Runnable{

    private static final Logger LOGGER = LoggerFactory.getLogger(MsisdnChecker.class);
    private static final long CHECKING_INTERVAL = Long.parseLong(USER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION);
    private static final TimeUnit TIME_UNIT = TimeUnit.MINUTES;
    private ScheduledExecutorService executorService;

    private UserService userService;

    public MsisdnChecker(UserService userService){
        this.userService = userService;
    }

    @Override
    public void run() {

        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    LOGGER.debug("Scheduler running for msisdn validity checking");
                    try {
                        userService.removeAllExpiredMsisdns(TIME_UNIT, CHECKING_INTERVAL);
                    } catch (DataManipulationException e) {
                        LOGGER.error("Data Manipulation error",e);
                    } catch (Exception e) {
                        LOGGER.error("Scheduler error", e);
                    }
                }
            }, CHECKING_INTERVAL, CHECKING_INTERVAL, TIME_UNIT);
        }
    }
}

