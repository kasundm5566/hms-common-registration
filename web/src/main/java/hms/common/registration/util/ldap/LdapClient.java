/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util.ldap;



import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import javax.naming.directory.SearchControls;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: chathuranga
 * Date: 10/11/11
 * Time: 8:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class LdapClient {





    private LdapTemplate ldapTemplate;
    private LdapContextSource contextSource;
    private boolean userFound=false;



    public boolean isUserAvailable(String username) {


        SearchControls searchControl = getSearchControls();

        List<String> result = ldapTemplate.search("CN=Users,DC=virtualcity,DC=co,DC=ke",
                "sAMAccountName="+username, searchControl, new ContextMapper() {

                    @Override
                    public Object mapFromContext(Object o) {

                        System.out.println("o = " + o);


                       if(o!=null){

                            userFound=true;
                        }


                        return "user_found";//todo
                    }
                });



        System.out.println(" result is "+result);

        if(result.contains("user_found")){

        userFound=true;

        System.out.println("user found is "+userFound);

        }
        else{

         userFound=false;

         System.out.println("user found is "+userFound);

        }

         System.out.println("final user found status "+userFound);

        return userFound;

        //return new HashMap<String, Object>();
    }



    public void setLdapTemplate(LdapTemplate ldapTemplate) {
        this.ldapTemplate = ldapTemplate;
    }


    private SearchControls getSearchControls() {

        final SearchControls constraints = new SearchControls();
        constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
        constraints.setReturningAttributes(new String[0]);
        constraints.setTimeLimit(1000);
        constraints.setCountLimit(1000);

        return constraints;

    }//search Controls


   public void setContextSource(LdapContextSource ContextSource) {
        contextSource = ContextSource;
    }

    public LdapContextSource getContextSource() {
        return contextSource;
    }



}


