/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Contains Utility method implementations for the web module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class WebUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebUtils.class);

    public static String generateVerificationCode() {
        DecimalFormat numberFormat = new DecimalFormat("000");
        final String currentTime = Double.toString(System.currentTimeMillis());
        return currentTime.substring(currentTime.length() - 3, currentTime.length()) +
                numberFormat.format(new Random().nextInt(100));
    }

    public static String getPassword(int n) {
        char[] pw = new char[n];
        int c  = 'A';
        int  r1 = 0;
        for (int i=0; i < n; i++)
        {
            r1 = (int)(Math.random() * 3);
            switch(r1) {
                case 0: c = '0' +  (int)(Math.random() * 10); break;
                case 1: c = 'a' +  (int)(Math.random() * 26); break;
                case 2: c = 'A' +  (int)(Math.random() * 26); break;
            }
            pw[i] = (char)c;
        }
        return new String(pw);
    }

    public static int getAge(Date dateOfBirth) {
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
            age--;
        return age;
    }

    public static String getTrimmedValue(Object value) {
        return ((String) value).trim();
    }

    /**
     * checks whether inputs are empty
     * @param field
     * @return true if empty or null
     */
    public static boolean isEmptyField(String field) {
        if (field == null){
            return true;
        } else if(field.trim().equals("")){
            return true;
        }
        return false;
    }
}