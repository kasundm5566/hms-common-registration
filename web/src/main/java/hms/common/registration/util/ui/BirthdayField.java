package hms.common.registration.util.ui;


import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.*;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.HorizontalLayout;
import hms.common.registration.ui.CommonRegistrationApp;
import org.joda.time.DateTime;
import java.util.Date;
import java.util.List;

import static hms.common.registration.util.PropertyHolder.MONTHS;

public class BirthdayField extends CustomField{

    private CommonRegistrationApp commonRegistrationApp;
    private ComboBox birthMonthField;
    private ComboBox birthYearField;
    private ComboBox birthDay;

    public BirthdayField(CommonRegistrationApp commonRegistrationApp) {
        this.commonRegistrationApp = commonRegistrationApp;
        final HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setWidth(this.getWidth(), this.getWidthUnits());

        this.setCompositionRoot(horizontalLayout);

        this.birthMonthField = comboBox(null, false, "", MONTHS);
        final List<String> years = Lists.transform(ContiguousSet.create(Range.closed(1900, DateTime.now().getYear() - 13), DiscreteDomain.integers()).asList(), new Function<Integer, String>() {
            @Override
            public String apply(Integer input) {
                return String.valueOf(input);
            }
        });
        this.birthYearField = comboBox(null, false, "", years);
        birthDay = new ComboBox(null);
        birthDay.setImmediate(true);


        birthYearField.addListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                setValuesToDayCheckBox();
            }
        });

        birthMonthField.addListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                setValuesToDayCheckBox();
            }
        });


        setValuesToDayCheckBox();

        horizontalLayout.addComponent(birthYearField);
        birthYearField.setWidth(30, UNITS_PERCENTAGE);
        birthYearField.setSizeFull();
        horizontalLayout.addComponent(birthMonthField);
        birthMonthField.setWidth(40, UNITS_PERCENTAGE);
        birthMonthField.setSizeFull();
        horizontalLayout.addComponent(birthDay);
        birthDay.setWidth(30, UNITS_PERCENTAGE);
        birthDay.setSizeFull();

        setCaption(commonRegistrationApp.getMessage("registration.user.birthday"));
    }

    private void setValuesToDayCheckBox() {
        final int i = maxDaysPerMonth(Integer.valueOf(birthYearField.getValue().toString()), monthOfYear());
        final ImmutableList<Integer> days = ContiguousSet.create(Range.closed(1, i), DiscreteDomain.integers()).asList();
        birthDay.removeAllItems();
        for (Integer day : days) {
            birthDay.addItem(day);
        }
        birthDay.setValue(days.get(0));
    }


    private ComboBox comboBox(String captionId, boolean isMandatory, String requiredErrorId,
                                    List<String> itemValueList) {
        ComboBox items = new ComboBox(captionId, itemValueList);
        items.setRequired(isMandatory);
        items.setNullSelectionAllowed(false);
        items.setImmediate(true);
        items.setValue(itemValueList.get(0));
        return items;
    }
    /*
        calculate maximum days for the month
    */
    private static int maxDaysPerMonth(int year, int month) {
        final DateTime dateTime = new DateTime(year, month, 1, 12, 0);
        return dateTime.dayOfMonth().getMaximumValue();
    }


    @Override
    public void setData(Object data) {
        if(Optional.fromNullable(data).isPresent()) {
            if(data instanceof Date) {
                final DateTime dateTime = new DateTime((Date) data);
                birthYearField.setValue(dateTime.getYear());
                birthMonthField.setValue(MONTHS.get(dateTime.getMonthOfYear() - 1));
                birthDay.setValue(dateTime.getDayOfMonth());
            }
        }
    }

    @Override
    public Object getData() {
        return new DateTime(Integer.valueOf(birthYearField.getValue().toString()), monthOfYear(), Integer.valueOf(birthDay.getValue().toString()), 12, 0).toDate();
    }

    private int monthOfYear() {
        return MONTHS.indexOf(birthMonthField.getValue()) + 1;
    }

    @Override
    public Date getValue() {
        return new DateTime(Integer.valueOf(birthYearField.getValue().toString()), monthOfYear(), Integer.valueOf(birthDay.getValue().toString()), 12, 0).toDate();
    }

    @Override
    public void setValue(Object data) throws ReadOnlyException, ConversionException {
        if(Optional.fromNullable(data).isPresent()) {
            if(data instanceof Date) {
                final DateTime dateTime = new DateTime((Date) data);
                birthYearField.setValue(dateTime.getYear());
                birthMonthField.setValue(MONTHS.get(dateTime.getMonthOfYear() - 1));
                birthDay.setValue(dateTime.getDayOfMonth());
            }
        }
    }

    @Override
    public void validate() throws Validator.InvalidValueException {
        try {
            final boolean validDate = Optional.fromNullable(birthYearField.getValue()).isPresent() && Optional.fromNullable(monthOfYear()).isPresent()
                    && Optional.fromNullable(birthDay.getValue()).isPresent();

            if(!validDate) {
                throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.invalid.birthday"));
            }
        } catch (Exception e) {
            throw new Validator.InvalidValueException(commonRegistrationApp.getMessage("registration.user.invalid.birthday"));
        }

    }

    @Override
    public Class<?> getType() {
        return Date.class;
    }
}
