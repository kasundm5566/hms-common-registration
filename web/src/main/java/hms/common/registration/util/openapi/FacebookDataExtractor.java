/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.openapi;

import hms.common.registration.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class FacebookDataExtractor extends AbstractOpenIdDataExtractor {

    public static final Logger logger = LoggerFactory.getLogger(FacebookDataExtractor.class);

    @Override
    public void doExtract(Map data, User user) {
        String id = (String) data.get("id");
        String firstName = (String) data.get("first_name");
        String gender = (String) data.get("gender");
        String lastName = (String) data.get("last_name");

        logger.debug("Found data for user {}, {}/{}/{}", new Object[]{id, firstName, lastName, gender});
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(id);
        user.setDomain("facebook");
        user.setDomainId(id);

        if (gender != null) {
            gender = gender.substring(0,1).toUpperCase() + gender.substring(1).toLowerCase();
        }
        user.setGender(gender);
    }

}
