/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.openapi;

import hms.common.rest.util.JsonBodyProvider;
import hms.common.registration.model.User;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public abstract class AbstractOpenIdDataExtractor implements OpenIdDataExtractor {

    private static final Logger logger = LoggerFactory.getLogger(AbstractOpenIdDataExtractor.class);
    private String userDetailServiceUrl;

    @Override
    public final void extract(String provider, String userId, User user) {
        LinkedList<Object> providers = new LinkedList<Object>();
        providers.add(new JsonBodyProvider());
        String userDetailsUrl = userDetailServiceUrl + "?provider=" + provider + "&user_id=" + userId;

        logger.trace("Looking for user detail {} in {}", userId, userDetailsUrl);

        WebClient regClient = WebClient.create(userDetailsUrl, providers);
        regClient.header("Content-Type", MediaType.APPLICATION_JSON);
        regClient.accept(MediaType.APPLICATION_JSON);
        Response response = regClient.invoke("GET", null);
        if (response.getEntity() instanceof Map) {
            doExtract((Map) response.getEntity(), user);
        } else {
            logger.warn("Unknown type of response received {}, {}", response.getClass(), response.getEntity());
        }
    }

    public abstract void doExtract(Map data, User user);

    public void setUserDetailServiceUrl(String userDetailServiceUrl) {
        this.userDetailServiceUrl = userDetailServiceUrl;
    }
}
