package hms.common.registration.util.ui;


import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;

import static com.vaadin.terminal.Sizeable.UNITS_PERCENTAGE;

public class GridLayoutUtil {

    public static void render(Component component, GridArea gridArea, GridLayout gridLayout) {
        gridLayout.addComponent(component, gridArea.column1(), gridArea.row1(), gridArea.column2(), gridArea.row2());
        component.setWidth(100, UNITS_PERCENTAGE);
        gridLayout.setComponentAlignment(component, Alignment.TOP_CENTER);
    }

    public static void render(Component component, GridArea gridArea, GridLayout gridLayout, int widthPercentage, Alignment alignment) {
        gridLayout.addComponent(component, gridArea.column1(), gridArea.row1(), gridArea.column2(), gridArea.row2());
        component.setWidth(widthPercentage, UNITS_PERCENTAGE);
        gridLayout.setComponentAlignment(component, alignment);
    }

    public static class GridArea {
        private int column1;
        private int row1;
        private int column2;
        private int row2;

        public GridArea(int column1, int row1, int column2, int row2) {
            this.column1 = column1;
            this.row1 = row1;
            this.column2 = column2;
            this.row2 = row2;
        }

        public int column1() {
            return column1;
        }

        public int row1() {
            return row1;
        }

        public int column2() {
            return column2;
        }

        public int row2() {
            return row2;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("GridArea{");
            sb.append("column1=").append(column1);
            sb.append(", row1=").append(row1);
            sb.append(", column2=").append(column2);
            sb.append(", row2=").append(row2);
            sb.append('}');
            return sb.toString();
        }
    }

    public static class GridComponentCursor {

        private final int maxColumns;
        private final int maxRows;
        private int cursorColumnIndex;
        private int cursorRowIndex;

        public GridComponentCursor(int maxColumns, int maxRows) {
            this.maxColumns = maxColumns;
            this.maxRows = maxRows;
            this.cursorColumnIndex = 0;
            this.cursorRowIndex = 0;
        }

        public GridArea nextCell() {
            final GridArea gridArea = new GridArea(cursorColumnIndex, cursorRowIndex, cursorColumnIndex, cursorRowIndex);
            if(cursorColumnIndex + 1 == maxColumns) {
                cursorColumnIndex = 0;
                cursorRowIndex++;
            } else {
                cursorColumnIndex++;
            }
            return gridArea;
        }

        public GridArea nextEntireRow() {
            if(cursorColumnIndex != 0) {
                cursorColumnIndex = 0;
                cursorRowIndex++;
            }
            final GridArea gridArea = new GridArea(cursorColumnIndex, cursorRowIndex, maxColumns - 1, cursorRowIndex);
            cursorColumnIndex = 0;
            cursorRowIndex++;
            return gridArea;
        }

        public GridArea nextCellWithNewLine() {
            if(cursorColumnIndex != 0) {
                cursorColumnIndex = 0;
                cursorRowIndex++;
            }
            final GridArea gridArea = new GridArea(cursorColumnIndex, cursorRowIndex, cursorColumnIndex + 1, cursorRowIndex);
            cursorColumnIndex = 0;
            cursorRowIndex++;
            return gridArea;
        }

        public int getCursorColumnIndex() {
            return cursorColumnIndex;
        }

        public int getCursorRowIndex() {
            return cursorRowIndex;
        }
    }
}
