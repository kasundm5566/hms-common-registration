/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.message;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface Message {

    void setSenderAddress(Msisdn senderAddress);
    Msisdn getSenderAddress();

    void setReceiverAddress(Msisdn msisdn);
    Msisdn getReceiverAddress();

    void setMessage(String message);
    String getMessage();

}