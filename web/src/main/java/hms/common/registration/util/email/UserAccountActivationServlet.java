/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util.email;

import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: chathuranga
 * Date: 10/14/11
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */

public class UserAccountActivationServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(UserAccountActivationServlet.class);
    private UserService dao;


    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        dao = (UserService)context.getBean("userServiceImpl");
    }

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException{
        String key;
        String id;

        logger.debug("user account activation process starts");


        logger.debug(" userServiceImpl bean is "+dao);

        key=request.getParameter("key");
        id=request.getParameter("id");


        logger.debug("key is ["+key+"] and ID is ["+id+"]");


        if(key!=null && id!=null ){


           int verifiedStatus=dao.userAccountActivateUsingEmail(key,id);

            if(verifiedStatus==1){

                logger.debug(" user with user id ["+id+"] is successfully activated ");
                response.sendRedirect("/cas/login?success=user.account.activated");
            }
            else if(verifiedStatus==2){

                   logger.debug(" user with user id ["+id+"] is already  activated ");
                   response.sendRedirect("/cas/login?success=user.account.already.activated");

            }
            else{

              logger.debug(" user with user id ["+id+"] is not  activated ");
              response.sendRedirect("/cas/login?error=user.account.activation.error");
            }

        }
        else{

            logger.debug("******unable to find the user with provided email verification key ["+key+"] and user id ["+id+"]**************** ");
            response.sendRedirect("/cas/login?error=user.account.activation.error");

        }

    }//doGet

}//ServletClass
