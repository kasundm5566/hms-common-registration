/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.sms;

import hms.common.registration.util.message.AbstractMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Dispatch the SMS MT Requests Received from the UI
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsMtDispatcherImpl implements SmsMtDispatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsMtDispatcherImpl.class);
    private SmsMtSender smsMtSender;
    private ExecutorService executorService;
    private int executorThreadCount = 60;

    @Override
    public Future sendIndividualMessage(AbstractMessage message) {
        return dispatchMessage(message);
    }

    public void init() {
        executorService = Executors.newFixedThreadPool(executorThreadCount);
    }

    private Future dispatchMessage(AbstractMessage message) {
        Future future = null;
        if(message != null) {
            future =  executorService.submit(createTask(message, smsMtSender));
        } else {
            LOGGER.info("Message size is 0. Ignoring SMS MT request for [{}]...", message.getSenderAddress());
        }
        return future;
    }

    private SmsMtDispatchTask createTask(AbstractMessage message, SmsMtSender smsMtSender) {
        final SmsMtDispatchTask smsMtDispatchTask = new SmsMtDispatchTask(smsMtSender, message);
        LOGGER.debug("Creating new task to executor. task [" + smsMtDispatchTask + "]");
        return smsMtDispatchTask;
    }

    public void setSmsMtSender(SmsMtSender smsMtSender) {
        this.smsMtSender = smsMtSender;
    }

    public void setExecutorThreadCount(int executorThreadCount) {
        this.executorThreadCount = executorThreadCount;
    }
}

class SmsMtDispatchTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsMtDispatchTask.class);
    private SmsMtSender smsMtSender;
    private AbstractMessage abstractMessage;

    SmsMtDispatchTask(SmsMtSender smsMtSender, AbstractMessage abstractMessage) {
        this.smsMtSender = smsMtSender;
        this.abstractMessage = abstractMessage;
    }

    @Override
    public void run() {
        LOGGER.debug("Sending SMS MT message Started for message [{}]", abstractMessage);
        smsMtSender.sendSingleMessage(abstractMessage);

    }
}