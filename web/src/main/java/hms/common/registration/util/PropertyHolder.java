/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class PropertyHolder {

    private static final Logger logger = LoggerFactory.getLogger(PropertyHolder.class);
    public static List<String> SUPPORTED_COUNTRIES = Collections.EMPTY_LIST;
    public static List<String> SUPPORTED_PROVINCES = Collections.EMPTY_LIST;
    public static List<String> SUPPORTED_CITIES = Collections.EMPTY_LIST;
    public static List<String> SECURITY_QUESTIONS = Collections.EMPTY_LIST;
    public static List<String> PROFESSIONS = Collections.EMPTY_LIST;
    public static List<String> GENDER_SELECTIONS = Collections.EMPTY_LIST;
    public static List<String> SUPPORTED_OPERATORS = Collections.EMPTY_LIST;
    public static Map<String, String> OPERATOR_CODE_MAP = Collections.EMPTY_MAP;
    public static Map<String, String> VCITY_OPERATORS_CODE_MAP = Collections.EMPTY_MAP;
    public static List<String> MONTHS = Collections.EMPTY_LIST;
    public static List<String> INDUSTRIES = Collections.EMPTY_LIST;
    public static List<String> ORGANIZATION_SIZE = Collections.EMPTY_LIST;
    public static String PREVIOUS_URL_QUERY_PARAM = "";
    public static Map<String, List<String>> SUPPORTED_PHONE_MODELS = new HashMap<String, List<String>>();
    public static String corporateUserTermsText = "";
    public static String individualUserTermsText = "";
    public static String corporateUserTermsFilePath = "popups/corporate_termsAndCondition_en.txt";
    public static String individualUserTermsFilePath = "popups/individual_termsAndCondition_en.txt";
    public static String PASSWORD_REGEX_PATTERN = "";
    public static String MSISDN_REGEX_PATTERN = "";
    public static String MPIN_REGEX_PATTERN = "";
    public static String BUSINESS_ID_PATTERN = "";
    public static String BUSINESS_LICENSE_PATTERN = "";
    public static final String COMMON_DATE_FORMAT = "dd-MM-yyyy";
    public static String MSISDN_VERIFICATION_MSG_TEMPLATE = "";
    public static String PGW_SELF_CARE_CONFIG_URL = "";
    public static String PROVISIONING_SP_CREATION_URL = "";
    public static String ROLE_NAME_REGEX_PATTERN = "";
    public static String BIRTH_YEAR_REGEX_PATTERN = "";
    public static String BIRTHDAY_REGEX_PATTERN = "";
    public static String DATE_REGEX_PATTERN = "";
    public static String EMAIL_REGEX_PATTERN = "";
    public static String DEFAULT_BIRTHDAY = "01/01/1980";

    public static String USER_NAME_REGEX_PATTERN = "";
    public static String SUB_CORPORATE_VIRTUAL_GROUP_BEGIN_KEYWORD = "";
    public static String USER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION = "";
    public static String USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION = "";
    public static String TABLE_PAGING_NO_OF_RECORDS_PER_PAGE = "";
    public static String TABLE_PAGING_NO_OF_PAGES_PER_VIEW = "";
    public static String MAX_MSISDN_VERIFY_CODE_REQ_COUNT = "";
    public static String MAX_MSISDEN_VERIFY_ATTEMPTS = "";
    public static String MAX_MSISDN_CHANGE_ATTEMPTS = "";

    public static String ADDRESS_LENGTH_VALIDATION = "";
    public static String FIRST_NAME_LENGTH_VALIDATION = "";
    public static String LAST_NAME_LENGTH_VALIDATION = "";
    public static String MODULE_NAME_LENGTH_VALIDATION = "";
    public static String MODULE_DESCRIPTION_LENGTH_VALIDATION = "";
    public static String ROLE_NAME_LENGTH_VALIDATION = "";
    public static String ROLE_DESCRIPTION_LENGTH_VALIDATION = "";
    public static String GROUP_NAME_LENGTH_VALIDATION = "";
    public static String GROUP_DESCRIPTION_LENGTH_VALIDATION = "";
    public static String INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN = "";
    public static String FULL_NAME_LENGTH_VALIDATION = "";
    public static String INDIVIDUAL_USER_USERNAME_REGEX_PATTERN = "";
    public static String LEFT_SIDE_BANNER_CSS = "";
    public static String BASE_DIRECTORY_PATH = "";
    public static String IMAGE_ACCESS_BASE_URL = "";
    public static String USER_PASSWORD_RESET_SMS_TEMPLATE = "";

    static {
        try {
            PropertyHolder.corporateUserTermsText = readTextFile(PropertyHolder.class, corporateUserTermsFilePath);
            PropertyHolder.individualUserTermsText = readTextFile(PropertyHolder.class, individualUserTermsFilePath);
        } catch (IOException e) {
            logger.error("Error while loading user terms page content from the file [{}] ");
        }
        //todo get the available phone models from the DB
        SUPPORTED_PHONE_MODELS.put("Nokia", Arrays.asList("1100", "N70"));
        SUPPORTED_PHONE_MODELS.put("Samsung", Arrays.asList("E2232", "E1182"));
    }

    public void setSUPPORTED_COUNTRIES(List<String> SUPPORTED_COUNTRIES) {
        PropertyHolder.SUPPORTED_COUNTRIES = SUPPORTED_COUNTRIES;
    }

    public void setSUPPORTED_PROVINCES(List<String> SUPPORTED_PROVINCES) {
        PropertyHolder.SUPPORTED_PROVINCES = SUPPORTED_PROVINCES;
    }

    public void setSUPPORTED_CITIES(List<String> SUPPORTED_CITIES) {
        PropertyHolder.SUPPORTED_CITIES = SUPPORTED_CITIES;
    }

    public void setSECURITY_QUESTIONS(List<String> SECURITY_QUESTIONS) {
        PropertyHolder.SECURITY_QUESTIONS = SECURITY_QUESTIONS;
    }

    public void setPROFESSIONS(List<String> PROFESSIONS) {
        PropertyHolder.PROFESSIONS = PROFESSIONS;
    }

    public void setCorporateUserTermsFilePath(String corporateUserTermsFilePath) {
        PropertyHolder.corporateUserTermsFilePath = corporateUserTermsFilePath;
    }

    public void setIndividualUserTermsFilePath(String individualUserTermsFilePath) {
        PropertyHolder.individualUserTermsFilePath = individualUserTermsFilePath;
    }

    public void setPASSWORD_REGEX_PATTERN(String PASSWORD_REGEX_PATTERN) {
        PropertyHolder.PASSWORD_REGEX_PATTERN = PASSWORD_REGEX_PATTERN;
    }

    public void setPHONE_NUMBER_REGEX_PATTERN(String PHONE_NUMBER_REGEX_PATTERN) {
        PropertyHolder.MSISDN_REGEX_PATTERN = PHONE_NUMBER_REGEX_PATTERN;
    }

    public void setMPIN_REGEX_PATTERN(String MPIN_REGEX_PATTERN) {
        PropertyHolder.MPIN_REGEX_PATTERN = MPIN_REGEX_PATTERN;
    }

    public void setBUSINESS_ID_PATTERN(String BUSINESS_ID_PATTERN) {
        PropertyHolder.BUSINESS_ID_PATTERN = BUSINESS_ID_PATTERN;
    }

    public void setBUSINESS_LICENSE_PATTERN(String BUSINESS_LICENSE_PATTERN) {
        PropertyHolder.BUSINESS_LICENSE_PATTERN = BUSINESS_LICENSE_PATTERN;
    }

    public void setBIRTH_YEAR_REGEX_PATTERN(String BIRTH_YEAR_REGEX_PATTERN) {
        PropertyHolder.BIRTH_YEAR_REGEX_PATTERN = BIRTH_YEAR_REGEX_PATTERN;
    }

    public void setBIRTHDAY_REGEX_PATTERN(String BIRTHDAY_REGEX_PATTERN) {
        PropertyHolder.BIRTHDAY_REGEX_PATTERN = BIRTHDAY_REGEX_PATTERN;
    }

    public void setGENDER_SELECTIONS(List<String> GENDER_SELECTIONS) {
        PropertyHolder.GENDER_SELECTIONS = GENDER_SELECTIONS;
    }

    public void setSUPPORTED_OPERATORS(List<String> SUPPORTED_OPERATORS) {
        PropertyHolder.SUPPORTED_OPERATORS = SUPPORTED_OPERATORS;
    }

    public void setMSISDN_VERIFICATION_MSG_TEMPLATE(String MSISDN_VERIFICATION_MSG_TEMPLATE) {
        PropertyHolder.MSISDN_VERIFICATION_MSG_TEMPLATE = MSISDN_VERIFICATION_MSG_TEMPLATE;
    }

    public void setPGW_SELF_CARE_CONFIG_URL(String PGW_SELF_CARE_CONFIG_URL) {
        PropertyHolder.PGW_SELF_CARE_CONFIG_URL = PGW_SELF_CARE_CONFIG_URL;
    }

    public void setPROVISIONING_SP_CREATION_URL(String PROVISIONING_SP_CREATION_URL) {
        PropertyHolder.PROVISIONING_SP_CREATION_URL = PROVISIONING_SP_CREATION_URL;
    }

    public void setMONTHS(List<String> MONTHS) {
        PropertyHolder.MONTHS = MONTHS;
    }

    public void setINDUSTRIES(List<String> INDUSTRIES) {
        PropertyHolder.INDUSTRIES = INDUSTRIES;
    }

    public void setORGANIZATION_SIZE(List<String> ORGANIZATION_SIZE) {
        PropertyHolder.ORGANIZATION_SIZE = ORGANIZATION_SIZE;
    }

    public void setFULL_NAME_LENGTH_VALIDATION(String FULL_NAME_LENGTH_VALIDATION) {
        PropertyHolder.FULL_NAME_LENGTH_VALIDATION = FULL_NAME_LENGTH_VALIDATION;
    }

    public void setROLE_NAME_REGEX_PATTERN(String ROLE_NAME_REGEX_PATTERN) {
        PropertyHolder.ROLE_NAME_REGEX_PATTERN = ROLE_NAME_REGEX_PATTERN;
    }

    public static <T> String readTextFile(Class<T> clazz, String resourceName) throws IOException {
        StringBuffer sb = new StringBuffer(1024);
        BufferedInputStream inStream = new BufferedInputStream(clazz.getClassLoader().getResourceAsStream(resourceName));
        byte[] chars = new byte[1024];
        int bytesRead;
        while ((bytesRead = inStream.read(chars)) > -1) {
            sb.append(new String(chars, 0, bytesRead));
        }
        inStream.close();
        return sb.toString();
    }

    public static List<String> getOperatorNames() {
        List<String> list = new ArrayList<String>();
        for (Map.Entry<String, String> stringStringEntry : OPERATOR_CODE_MAP.entrySet()) {
            list.add(stringStringEntry.getValue());
        }
        return list;
    }

    public void setMAX_MSISDN_VERIFY_CODE_REQ_COUNT(String MAX_MSISDN_VERIFY_CODE_REQ_COUNT) {
        PropertyHolder.MAX_MSISDN_VERIFY_CODE_REQ_COUNT = MAX_MSISDN_VERIFY_CODE_REQ_COUNT;
    }

    public void setDATE_REGEX_PATTERN(String DATE_REGEX_PATTERN) {
        PropertyHolder.DATE_REGEX_PATTERN = DATE_REGEX_PATTERN;
    }

    public void setOPERATOR_CODE_MAP(Map OPERATOR_CODE_MAP) {
        PropertyHolder.OPERATOR_CODE_MAP = OPERATOR_CODE_MAP;
    }

    public void setUserNameRegexPattern(String userNameRegexPattern) {
        PropertyHolder.USER_NAME_REGEX_PATTERN = userNameRegexPattern;
    }

    public void setSUB_CORPORATE_VIRTUAL_GROUP_BEGIN_KEYWORD(String SUB_CORPORATE_VIRTUAL_GROUP_BEGIN_KEYWORD) {
        PropertyHolder.SUB_CORPORATE_VIRTUAL_GROUP_BEGIN_KEYWORD = SUB_CORPORATE_VIRTUAL_GROUP_BEGIN_KEYWORD;
    }

    public void setUSER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION(String USER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION) {
        PropertyHolder.USER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION = USER_ACCOUNT_DELETION_DURATION_FOR_MSISDN_VERIFICATION;
    }

    public void setUSER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION(String USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION) {
        PropertyHolder.USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION = USER_ACCOUNT_DELETION_DURATION_FOR_EMAIL_VERIFICATION;
    }

    public void setTABLE_PAGING_NO_OF_RECORDS_PER_PAGE(String TABLE_PAGING_NO_OF_RECORDS_PER_PAGE) {
        PropertyHolder.TABLE_PAGING_NO_OF_RECORDS_PER_PAGE = TABLE_PAGING_NO_OF_RECORDS_PER_PAGE;
    }

    public void setTABLE_PAGING_NO_OF_PAGES_PER_VIEW(String TABLE_PAGING_NO_OF_PAGES_PER_VIEW) {
        PropertyHolder.TABLE_PAGING_NO_OF_PAGES_PER_VIEW = TABLE_PAGING_NO_OF_PAGES_PER_VIEW;
    }

    public void setADDRESS_LENGTH_VALIDATION(String ADDRESS_LENGTH_VALIDATION) {
        PropertyHolder.ADDRESS_LENGTH_VALIDATION = ADDRESS_LENGTH_VALIDATION;
    }

    public void setFIRST_NAME_LENGTH_VALIDATION(String FIRST_NAME_LENGTH_VALIDATION) {
        PropertyHolder.FIRST_NAME_LENGTH_VALIDATION = FIRST_NAME_LENGTH_VALIDATION;
    }

    public void setLAST_NAME_LENGTH_VALIDATION(String LAST_NAME_LENGTH_VALIDATION) {
        PropertyHolder.LAST_NAME_LENGTH_VALIDATION = LAST_NAME_LENGTH_VALIDATION;
    }

    public void setMODULE_NAME_LENGTH_VALIDATION(String MODULE_NAME_LENGTH_VALIDATION) {
        PropertyHolder.MODULE_NAME_LENGTH_VALIDATION = MODULE_NAME_LENGTH_VALIDATION;
    }

    public void setMODULE_DESCRIPTION_LENGTH_VALIDATION(String MODULE_DESCRIPTION_LENGTH_VALIDATION) {
        PropertyHolder.MODULE_DESCRIPTION_LENGTH_VALIDATION = MODULE_DESCRIPTION_LENGTH_VALIDATION;
    }

    public void setROLE_NAME_LENGTH_VALIDATION(String ROLE_NAME_LENGTH_VALIDATION) {
        PropertyHolder.ROLE_NAME_LENGTH_VALIDATION = ROLE_NAME_LENGTH_VALIDATION;
    }

    public void setROLE_DESCRIPTION_LENGTH_VALIDATION(String ROLE_DESCRIPTION_LENGTH_VALIDATION) {
        PropertyHolder.ROLE_DESCRIPTION_LENGTH_VALIDATION = ROLE_DESCRIPTION_LENGTH_VALIDATION;
    }

    public void setGROUP_NAME_LENGTH_VALIDATION(String GROUP_NAME_LENGTH_VALIDATION) {
        PropertyHolder.GROUP_NAME_LENGTH_VALIDATION = GROUP_NAME_LENGTH_VALIDATION;
    }

    public void setGROUP_DESCRIPTION_LENGTH_VALIDATION(String GROUP_DESCRIPTION_LENGTH_VALIDATION) {
        PropertyHolder.GROUP_DESCRIPTION_LENGTH_VALIDATION = GROUP_DESCRIPTION_LENGTH_VALIDATION;
    }

    public void setINDIVIDUAL_USER_PASSWORD_REGEX_PATTERN(String INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN) {
        PropertyHolder.INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN = INDIVIDUAL_USER_PASSWORD_REGEX_PATTERN;
    }

    public void setINDIVIDUAL_USER_USERNAME_REGEX_PATTERN(String INDIVIDUAL_USER_USERNAME_REGEX_PATTERN) {
        PropertyHolder.INDIVIDUAL_USER_USERNAME_REGEX_PATTERN = INDIVIDUAL_USER_USERNAME_REGEX_PATTERN;
    }

    public void setVCITY_OPERATORS_CODE_MAP(Map VCITY_OPERATORS_CODE_MAP) {
        PropertyHolder.VCITY_OPERATORS_CODE_MAP = VCITY_OPERATORS_CODE_MAP;
    }

    public void setEMAIL_REGEX_PATTERN(String EMAIL_REGEX_PATTERN) {
        PropertyHolder.EMAIL_REGEX_PATTERN = EMAIL_REGEX_PATTERN;
    }

    public void setDEFAULT_BIRTHDAY(String DEFAULT_BIRTHDAY) {
        PropertyHolder.DEFAULT_BIRTHDAY = DEFAULT_BIRTHDAY;
    }

    public void setMAX_MSISDEN_VERIFY_ATTEMPTS(String MAX_MSISDEN_VERIFY_ATTEMPTS) {
        PropertyHolder.MAX_MSISDEN_VERIFY_ATTEMPTS = MAX_MSISDEN_VERIFY_ATTEMPTS;
    }

    public void setMAX_MSISDN_CHANGE_ATTEMPTS(String MAX_MSISDN_CHANGE_ATTEMPTS) {
        PropertyHolder.MAX_MSISDN_CHANGE_ATTEMPTS = MAX_MSISDN_CHANGE_ATTEMPTS;
    }

    public void setPREVIOUS_URL_QUERY_PARAM(String PREVIOUS_URL_QUERY_PARAM) {
        PropertyHolder.PREVIOUS_URL_QUERY_PARAM = PREVIOUS_URL_QUERY_PARAM;
    }

    public void setLEFT_SIDE_BANNER_CSS(String LEFT_SIDE_BANNER_CSS) {
        PropertyHolder.LEFT_SIDE_BANNER_CSS = LEFT_SIDE_BANNER_CSS;
    }

    public void setBASE_DIRECTORY_PATH(String BASE_DIRECTORY_PATH) {
        PropertyHolder.BASE_DIRECTORY_PATH = BASE_DIRECTORY_PATH;
    }

    public void setIMAGE_ACCESS_BASE_URL(String IMAGE_ACCESS_BASE_URL) {
        PropertyHolder.IMAGE_ACCESS_BASE_URL = IMAGE_ACCESS_BASE_URL;
    }

    public void setUSER_PASSWORD_RESET_SMS_TEMPLATE(String USER_PASSWORD_RESET_SMS_TEMPLATE) {
        PropertyHolder.USER_PASSWORD_RESET_SMS_TEMPLATE = USER_PASSWORD_RESET_SMS_TEMPLATE;
    }
}
