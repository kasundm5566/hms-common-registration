/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.openapi;

import hms.common.registration.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class DataExtractorFacade implements OpenIdDataExtractor {

    private static final Logger logger = LoggerFactory.getLogger(DataExtractorFacade.class);

    private Map<String, OpenIdDataExtractor> dataExtractors;

    @Override
    public void extract(String provider, String userId, User user) {
        OpenIdDataExtractor dataExtractor = dataExtractors.get(provider);

        if (dataExtractor == null) {

            logger.debug("Can not find data extractor for {} in {}", provider, dataExtractors);

        } else {
            dataExtractor.extract(provider, userId, user);
        }
    }

    public void setDataExtractors(Map<String, OpenIdDataExtractor> dataExtractors) {
        this.dataExtractors = dataExtractors;
    }
}
