/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util;

/**
 * Defines Registration Module related User Roles and User Groups
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public interface UserRoles {

    static final String GROUP_CORPORATE_USER = "CORPORATE_USER";
    static final String GROUP_CONSUMER_USER = "CONSUMER_USER";
//    static final String GROUP_REGISTRATION_USER = "REGISTRATION_USER";
//    static final String ROLE_EDIT_PROFILE = "ROLE_EDIT_PROFILE";
    static final String ROLE_REG_LOGIN = "ROLE_REG_LOGIN";
//    static final String ROLE_REGISTRATION_ADMIN = "ROLE_REGISTRATION_ADMIN";
//    static final String ROLE_MANAGE_SUB_CORPORATE_USERS = "ROLE_MANAGE_SUB_CORPORATE_USERS";
}