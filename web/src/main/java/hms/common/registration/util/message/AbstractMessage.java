/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.message;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AbstractMessage implements Message {

    private long correlationId;
    private Msisdn senderAddress;
    private Msisdn receiverAddress;
    private String message;

    public void setSenderAddress(Msisdn senderAddress) {
        this.senderAddress = senderAddress;
    }

    public Msisdn getSenderAddress() {
        return senderAddress;
    }

    public Msisdn getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(Msisdn receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(long correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AbstractMessage");
        sb.append("{correlationId=").append(correlationId);
        sb.append(", senderAddress=").append(senderAddress);
        sb.append(", receiverAddress=").append(receiverAddress);
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}