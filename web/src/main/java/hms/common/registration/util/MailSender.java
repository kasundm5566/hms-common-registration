/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.MessageFormat;
import java.util.Properties;

import sun.misc.BASE64Encoder;

/**
 * Handles the mail sending for user email verification process
 * $LastChangedDate: 2011-08-30 17:38:56 +0530 (Tue, 30 Aug 2011) $
 * $LastChangedBy: dulika $
 * $LastChangedRevision: 76627 $
 */
public class MailSender {

    private static final Logger logger = LoggerFactory.getLogger(MailSender.class);

    private static final Logger snmpLogger = LoggerFactory.getLogger("snmp_logger");
    private static final Logger serviceFailLogger = LoggerFactory.getLogger("email_fail_logger");
    private BASE64Encoder base64Encoder = new BASE64Encoder();

    private String mailSendingFailTrap;
    private String mailSendingSuccessTrap;

    private String hostAddress;
    private String user;
    private String password;
    private int port;
    private Boolean smtpDebug = Boolean.TRUE;
    private String tls = "true";
    private String authentication = "true";
    private String emailVerificationTemplate;
    private String emailVerificationSubject;
    private String passwordResetEmailTemplate;
    private String passwordResetEmailSubject;
    private String emailVerificationUrl;
    private String fromAddress;

    private String casLoginUrl;
    private boolean emailSentFailed = false;

    /**
     * Sends out the use verification email
     *
     * @param username
     * @param toAddress
     * @param emailVerificationCode
     */
    public void sendUserVerificationEmail(final String name, final String username, final String toAddress,
                                          String emailVerificationCode, String userID) {
        logger.debug("Sending User Verification  Email started for User [{}] ", username);
        final String emailSubject = MessageFormat.format(emailVerificationSubject, name);
        final String emailContent = MessageFormat.format(emailVerificationTemplate,
                name, emailVerificationUrl + "?key=" + emailVerificationCode + "&id=" + userID);
        new Thread() {
            @Override
            public void run() {
                logger.debug("==============Email Description===============");
                logger.debug("From : " + fromAddress);
                logger.debug("To : " + toAddress);
                logger.debug("Subject : " + emailSubject);
                logger.debug("Content : " + emailContent);
                logger.debug("============End of Email Description==========");
                sendMail(fromAddress, toAddress, "", "", emailSubject, emailContent, username);
            }
        }.start();
    }

    /**
     * Sends out the forgot password email
     *
     * @param firstName
     * @param toAddress
     * @param username
     * @param generatedPassword
     */
    public void sendForgotPasswordReplyEmail(final String firstName, final String toAddress,
                                             final String username, String generatedPassword) {
        logger.debug("Sending Forgot PAssword Reply Email started for User [{}] ", firstName);
        final String emailContent = MessageFormat.format(passwordResetEmailTemplate,
                username, generatedPassword, casLoginUrl);
        new Thread() {
            @Override
            public void run() {
                sendMail(fromAddress, toAddress, "", "", passwordResetEmailSubject, emailContent, username);
            }
        }.start();
    }

    /**
     * Synchronized method to send email to the given email address
     *
     * @param frm
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param text
     * @return
     */
    private synchronized void sendMail(String frm, String to, String cc, String bcc, String subject, String text, String username) {

        logger.debug("Mail from : {}, to : {}, cc : {}, bcc : {}, subject : {}, Text : {}"
                , new Object[]{frm, to, cc, bcc, subject, text});

        Properties props = new Properties();
        props.put("mail.smtp.user", user);
        props.put("mail.smtp.host", hostAddress);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", tls);
        props.put("mail.smtp.auth", authentication);
        props.put("mail.smtp.debug", smtpDebug);
        Session session = Session.getDefaultInstance(props, null);
        session.setDebug(true);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.setText(text);
            msg.setSubject(subject);
            msg.setFrom(new InternetAddress(frm));
            for (String aTo : to.split(",")) {
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(aTo));
            }
            if (!cc.equals("")) {
                for (String aCc : cc.split(",")) {
                    msg.addRecipient(Message.RecipientType.CC, new InternetAddress(aCc));
                }
            }
            if (!bcc.equals("")) {
                for (String aBcc : bcc.split(",")) {
                    msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(aBcc));
                }
            }
            msg.saveChanges();
            Transport transport = session.getTransport("smtp");
            transport.connect(hostAddress, port, user, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            if (emailSentFailed) {
                snmpLogger.info(mailSendingSuccessTrap);
                emailSentFailed = false;
            }

            logger.debug("Email was sent to [{}] successfully", username);
        } catch (MessagingException e) {
            if (!emailSentFailed) {
                snmpLogger.info(mailSendingFailTrap);
                emailSentFailed = true;
            }

            serviceFailLogger.error("from:{}", base64Encoder.encode(frm.getBytes()));
            serviceFailLogger.error("to:{}", base64Encoder.encode(to.getBytes()));
            serviceFailLogger.error("cc:{}", base64Encoder.encode(cc.getBytes()));
            serviceFailLogger.error("bcc:{}", base64Encoder.encode(bcc.getBytes()));
            serviceFailLogger.error("subject:{}", base64Encoder.encode(subject.getBytes()));
            serviceFailLogger.error("content:{}\n", base64Encoder.encode(text.getBytes()));

            logger.error("Error while sending email to the user", e);
        }
        logger.debug("Email : " + msg);
    }

    public void setEmailVerificationSubject(String emailVerificationSubject) {
        this.emailVerificationSubject = emailVerificationSubject;
    }

    public void setEmailVerificationTemplate(String emailVerificationTemplate) {
        this.emailVerificationTemplate = emailVerificationTemplate;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setEmailVerificationUrl(String emailVerificationUrl) {
        this.emailVerificationUrl = emailVerificationUrl;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public void setPasswordResetEmailTemplate(String passwordResetEmailTemplate) {
        this.passwordResetEmailTemplate = passwordResetEmailTemplate;
    }

    public void setPasswordResetEmailSubject(String passwordResetEmailSubject) {
        this.passwordResetEmailSubject = passwordResetEmailSubject;
    }

    public void setSmtpDebug(Boolean smtpDebug) {
        this.smtpDebug = smtpDebug;
    }

    public void setTls(String tls) {
        this.tls = tls;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCasLoginUrl(String casLoginUrl) {
        this.casLoginUrl = casLoginUrl;
    }

    public void setMailSendingFailTrap(String mailSendingFailTrap) {
        this.mailSendingFailTrap = mailSendingFailTrap;
    }

    public void setMailSendingSuccessTrap(String mailSendingSuccessTrap) {
        this.mailSendingSuccessTrap = mailSendingSuccessTrap;
    }
}