/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util.email;

import org.apache.commons.net.util.SubnetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class LocalIPAddressFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(LocalIPAddressFilter.class);

    private static final List<SubnetUtils.SubnetInfo> subnets = new ArrayList<SubnetUtils.SubnetInfo>() {{
        add((new SubnetUtils("10.0.0.0", "10.255.255.255")).getInfo());
        add((new SubnetUtils("169.254.0.0", "169.254.255.255")).getInfo());
        add((new SubnetUtils("172.16.0.0", "172.31.255.255")).getInfo());
        add((new SubnetUtils("192.168.0.0", "192.168.255.255")).getInfo());
        add((new SubnetUtils("127.0.0.0", "127.255.255.255")).getInfo());
    }};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        logger.debug("Started to check whether the IP belongs to the Local IP ranges");

        logger.trace("getLocalAddr  [{}]", servletRequest.getLocalAddr());
        logger.trace("getRemoteAddr  [{}]", servletRequest.getRemoteAddr());
        logger.trace("getRemoteHost [{}]", servletRequest.getRemoteHost());
        logger.trace("getLocalName  [{}]", servletRequest.getLocalName());

        String remoteIPAddress = ((HttpServletRequest) servletRequest).getHeader("X-FORWARDED-FOR");

        if (remoteIPAddress != null) {

            String IPArray[] = remoteIPAddress.split("[ ,]");

            if (IPArray.length > 1) {
                logger.debug(" Multiple IP addresses( [{}] IP Addresses )were found", Arrays.asList(IPArray));
                remoteIPAddress = IPArray[0];
                logger.debug(" First IP Address [{}] was taken as the Remote IP ", remoteIPAddress);
            }
            logger.debug("remote public ip address[X-FORWARDED-FOR] is [{}]", remoteIPAddress);
        } else {
            remoteIPAddress = servletRequest.getRemoteAddr();
            logger.debug("remote public ip address[X-FORWARDED-FOR] is  null and remote address is " + remoteIPAddress);
        }

        if (isLocalAddress(remoteIPAddress)) {
            logger.debug(" IP Address [" + remoteIPAddress + "] belongs to the local IP ranges");
            //delegating the request to the next filter
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            logger.debug(" IP Address [" + remoteIPAddress + "] does not belong to the local IP ranges and displaying 404 error");
            ((HttpServletResponse) servletResponse).setStatus(404);
        }

    }

    private boolean isLocalAddress(String ipAddress) {
        for (SubnetUtils.SubnetInfo subnet : subnets) {
            if (subnet.isInRange(ipAddress)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {
        //nothing to destroy
    }
}
