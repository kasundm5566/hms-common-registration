/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

import hms.common.registration.model.Module;
import hms.common.registration.model.Role;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RoleWrapperBean {

    private String name;
    private String description;
    private Module module;
    private String rolePrefix = "";
    private String listAllRoles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public String getRolePrefix() {
        return rolePrefix;
    }

    public void setRolePrefix(String rolePrefix) {
        this.rolePrefix = rolePrefix;
    }

    public String getListAllRoles() {
        return listAllRoles;
    }

    public void setListAllRoles(String listAllRoles) {
        this.listAllRoles = listAllRoles;
    }

    public void persistRole (Role role) {
        role.setName(rolePrefix.toUpperCase() + name.toUpperCase());
        role.setDescription(description);
        role.setModule(module);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\n ==================================================================")
                .append("\n Permission Name: \t").append(name)
                .append("\n Permission Description: \t").append(description)
                .append("\n Permission Role Prefix: \t").append(rolePrefix)
                .append("\n Permission Module: \t").append(module.toString())
                .append("\n Permission List all Roles: \t").append(listAllRoles)
                .append("\n ==================================================================").toString();
    }

}
