/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.sms;

import hms.common.registration.util.message.AbstractMessage;

import java.util.concurrent.Future;

/**
 * Handles the SMS MT Message sending for user msisdn verification process
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface SmsMtDispatcher {

    Future sendIndividualMessage(AbstractMessage message);


}