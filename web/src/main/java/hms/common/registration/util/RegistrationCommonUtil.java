/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.common.registration.util;

import hms.common.registration.ui.CommonRegistrationApp;
import org.jasig.cas.client.validation.Assertion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static hms.common.registration.ui.common.util.ExternalLinks.CAS_LOGOUT_URL;
import static hms.common.registration.util.PropertyHolder.DEFAULT_BIRTHDAY;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class RegistrationCommonUtil {

    private static String getMatchedValue(String matchingValue, Map<String, String> patterns) {
        if (matchingValue == null) {
            return "";
        }

        for (final Map.Entry<String, String> entry : patterns.entrySet()) {
            Pattern pattern = Pattern.compile(entry.getKey());
            if (pattern.matcher(matchingValue).matches()) {
                return entry.getValue();
            }
        }

        return "";
    }

    public static String getBranding(CommonRegistrationApp commonRegistrationApp) {
        String serviceUrl = commonRegistrationApp.getServiceUrl();

        return getMatchedValue(serviceUrl, (Map<String, String>) commonRegistrationApp.getBean("brandingPatterns"));
    }

    public static Map<String, String> getUserDetails(Assertion assertion) {
        Map<String, String> userDetails = new HashMap<String, String>();
        Map<String, Object> attributes = assertion.getPrincipal().getAttributes();

        List<String> names = (List<String>) attributes.get("name");
        List<String> values = (List<String>) attributes.get("value");

        for (int i = 0; i < names.size(); i++) {
            userDetails.put(names.get(i), values.get(i));
        }

        return userDetails;
    }

    public static String getRedirectUrl(CommonRegistrationApp commonRegistrationApp, boolean status) {
        String serviceUrl = commonRegistrationApp.getServiceUrl();

        if (serviceUrl == null) {
            return CAS_LOGOUT_URL;
        }

        String matchedValue;

        if (status) {
            matchedValue = getMatchedValue(serviceUrl, (Map<String, String>) commonRegistrationApp.getBean("successRedirectPatterns"));
            if (matchedValue.isEmpty() || matchedValue.equals("same")) {
                matchedValue = serviceUrl;
            }
        } else {
            matchedValue = getMatchedValue(serviceUrl, (Map<String, String>) commonRegistrationApp.getBean("failureRedirectPatterns"));
            if (!matchedValue.isEmpty() && !matchedValue.equals("same")) {
                matchedValue = CAS_LOGOUT_URL + "?service=" + matchedValue;
            } else {
                matchedValue = CAS_LOGOUT_URL + "?service=" + serviceUrl;
            }
        }

        return matchedValue;
    }

    public static Date getDefaultBirthday() {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(DEFAULT_BIRTHDAY);
        } catch (Exception e) {
            return new Date();
        }
    }
}
