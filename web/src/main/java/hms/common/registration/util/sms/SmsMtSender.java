/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.util.sms;

import hms.common.registration.util.message.AbstractMessage;
import hms.common.registration.util.message.Msisdn;
import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.*;

import static hms.common.registration.api.rest.RestApiKeys.*;

/**
 * Handles the SMS sending part for user MSISDN verification process
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsMtSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsMtSender.class);
    private static final Logger snmpLogger = LoggerFactory.getLogger("snmp_logger");
    private static final Logger serviceFailLogger = LoggerFactory.getLogger("sms_fail_logger");

    private String smsSendingFailTrap;
    private String sdpConnectionSuccessTrap;
    private String sdpConnectionFailTrap;
    private String smsMtDispatchUrl;
    private String systemAppId;
    private String systemAppPassword;
    private boolean sdpDown = false;

    /**
     * @param message
     * @return
     */
    public Map sendSingleMessage(AbstractMessage message) {
        try {
            String correlationId = String.valueOf(message.getCorrelationId());
            NDC.push(correlationId);
            LOGGER.debug("Creating SMS message for telephone no[" + message.getReceiverAddress() + "] Started");
            final Map<String, Object> httpParameters = createMessageParameters(message.getMessage(),
                    message.getReceiverAddress().getMsisdn(),
                    message.getSenderAddress().getMsisdn(),
                    systemAppId,
                    systemAppPassword);
            return sendMessage(httpParameters);
        } finally {
            NDC.remove();
        }
    }

    private Map<String, Object> createMessageParameters(String message,
                                                        String destinationAddresses,
                                                        String senderAddress,
                                                        String systemAppId,
                                                        String systemAppPassword) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(APPLICATION_ID, systemAppId);
        parameters.put(APP_PASSWORD, systemAppPassword);
        parameters.put(MESSAGE_KEY, message);
        List<Object> addressList = new ArrayList<Object>();
        addressList.add(TEL_LIST_PREFIX + destinationAddresses);
        parameters.put(ADDRESS_KEY, addressList);
        parameters.put(RECIPIENT_KEY, senderAddress);
        return parameters;
    }

    private Map sendMessage(Map requestMessage) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        try {
            Map response = (Map) makeRestCall(requestMessage, Map.class);
            LOGGER.debug("Received Respoponse" + response.toString());

            if (response != null && response.get(MESSAGE_ID_KEY) != null) {
                Long messageId = Long.parseLong((String) response.get(MESSAGE_ID_KEY));
                if (((String) response.get(STATUS_CODE_KEY)).equals(SUCCESS__CODE) && messageId != null) {
                    LOGGER.info("Message Successfully processed for appId[" + systemAppId + "]");
                    responseMap.put(MESSAGE_ID_KEY, messageId);
                    responseMap.put(SUCCESS_STATUS_CODE, successStatusCode);
                    if(sdpDown) {
                       snmpLogger.info(sdpConnectionSuccessTrap);
                       sdpDown = false;
                    }
                    return responseMap;
                } else {
                    LOGGER.error("Message Possessing Error appId[" + systemAppId + "]");
                    responseMap.put(STATUS_DETAILS_KEY, (String) response.get(STATUS_DETAILS_KEY));
                    serviceFailLogger.error(requestMessage.toString());
                    return responseMap;
                }
            } else {
                LOGGER.error("Error while sending message to SDP. AppId[" + systemAppId + "]");
                responseMap.put(STATUS_DETAILS_KEY, RESPONCE_NULL_ERROR_MESSAGE);
                serviceFailLogger.error(requestMessage.toString());
                return responseMap;
            }
        } catch (Throwable e) {
            LOGGER.error("Error while sending message to SDP. AppId[" + systemAppId + "]", e);
            responseMap.put(STATUS_DETAILS_KEY, EXCEPTION_OCCURED_ERROR_MESSAGE);
            if(!sdpDown) {
                snmpLogger.info(sdpConnectionFailTrap);
                sdpDown = true;
            }
            serviceFailLogger.error(requestMessage.toString());
            return responseMap;
        }
    }

    public Object makeRestCall(Map<String, ? extends Object> parameters, Class aClass) {
        LinkedList<Object> providers = new LinkedList<Object>();
        providers.add(new JsonBodyProvider());
        WebClient regClient = WebClient.create(smsMtDispatchUrl, providers);
        regClient.header("Content-Type", MediaType.APPLICATION_JSON);
        regClient.accept(MediaType.APPLICATION_JSON);
        LOGGER.debug("Sending message" + parameters.toString());
        return regClient.invoke("POST", parameters, aClass);
    }

    public void setSmsMtDispatchUrl(String smsMtDispatchUrl) {
        this.smsMtDispatchUrl = smsMtDispatchUrl;
    }

    public void setSystemAppId(String systemAppId) {
        this.systemAppId = systemAppId;
    }

    public void setSystemAppPassword(String systemAppPassword) {
        this.systemAppPassword = systemAppPassword;
    }

    public void setSmsSendingFailTrap(String smsSendingFailTrap) {
        this.smsSendingFailTrap = smsSendingFailTrap;
    }

    public void setSdpConnectionFailTrap(String sdpConnectionFailTrap) {
        this.sdpConnectionFailTrap = sdpConnectionFailTrap;
    }

    public void setSdpConnectionSuccessTrap(String sdpConnectionSuccessTrap) {
        this.sdpConnectionSuccessTrap = sdpConnectionSuccessTrap;
    }
}