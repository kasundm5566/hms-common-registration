/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration;

import hms.common.registration.userservice.Authority;
import hms.common.registration.userservice.RegistrationUser;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.User;
import hms.common.registration.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashSet;
import java.util.Set;

/**
 * A spring security service to use by this module.
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RepositoryUserDetailService  implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(RepositoryUserDetailService.class);

    private UserService userService;
    private String registrationModuleName;


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException, DataAccessException {
        try {
            logger.debug("Find user by name [{}]", userName);
            User userByName = userService.findUserByName(userName);

            RegistrationUser customUser = new RegistrationUser();
            customUser.setEnabled(true);
            customUser.setAccountNonExpired(true);
            customUser.setAccountNonLocked(true);
            customUser.setCredentialsNonExpired(true);

            customUser.setUsername(userName);
            customUser.setPassword("");
            Set<Authority> authorities = new HashSet<Authority>();
                for (String role : userByName.getRolesAsStrings(registrationModuleName)) {
                    if (role != null && !role.isEmpty()) {
                        authorities.add(new Authority(role.trim()));
                    }
                }
            logger.debug("Found user with roles [{}]", userByName.getRolesAsStrings(registrationModuleName));
            customUser.setRoles(authorities);
            return customUser;
        } catch (DataManipulationException e) {
            throw new BadCredentialsException(" [ " + userName + " ] Not Found", e);
        } catch(NullPointerException e) {
            throw new AuthenticationCredentialsNotFoundException(" [ " + userName + " ] Not Found", e);
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setRegistrationModuleName(String registrationModuleName) {
        this.registrationModuleName = registrationModuleName;
    }
}
