/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.api.rest;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface RestApiKeys {

    public static final String APPLICATION_ID = "applicationId";
    public static final String APP_PASSWORD = "password";
    public static final String MESSAGE_KEY = "message";
    public static final String ADDRESS_KEY = "destinationAddresses";
    public static final String RECIPIENT_KEY = "sourceAddress";
    public static final String MESSAGE_ID_KEY = "requestId";
    public static final String STATUS_CODE_KEY = "statusCode";
    public static final String STATUS_DETAILS_KEY = "statusDetail";
    public static final String SUCCESS_STATUS_CODE = "success-status-code";
    public static final String SUCCESS__CODE = "S1000";
    public static final String EXCEPTION_OCCURED_ERROR_MESSAGE= "Exception Occured while sending the message";
    public static final String RESPONCE_NULL_ERROR_MESSAGE = "Connectivity problem occured please check the sdp connection";
    public static final String sendFailureStatusCode = "SMS-MT-7001";
    public static final String successStatusCode = "SMS-MT-2000";
    public static final String TEL_LIST_PREFIX = "tel:";

}
