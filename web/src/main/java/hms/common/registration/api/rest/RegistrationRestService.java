/*
 * (C) Copyright 2008-2012 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.common.registration.api.rest;

import com.google.common.base.Optional;
import hms.common.registration.api.RegistrationRestApi;
import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.request.*;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.common.Condition;
import hms.common.registration.exception.DataManipulationException;
import hms.common.registration.model.*;
import hms.common.registration.processor.AuthenticationProcessor;
import hms.common.registration.processor.SessionProcessor;
import hms.common.registration.rest.RestRequestHandler;
import hms.common.registration.service.UserGroupService;
import hms.common.registration.service.UserService;
import hms.common.registration.util.*;
import hms.common.registration.util.message.Msisdn;
import hms.common.registration.util.message.SmsMessage;
import hms.common.registration.util.sms.SmsMtDispatcher;
import org.apache.commons.collections.map.HashedMap;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static hms.common.registration.api.common.StatusCodes.UNSUPPORTED_REQUEST_TYPE;
import static hms.common.registration.util.Encrypter.MD5;
import static hms.common.registration.util.PropertyHolder.*;
import static hms.common.registration.util.RegistrationFeatureRegistry.isAutoSpCreationEnabled;
import static hms.common.registration.util.RegistrationFeatureRegistry.isMsisdnToEmailFormattingEnabled;
import static hms.common.registration.util.ValidateUser.isValidUser;

/**
 * REST - API Server Implementation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Path("/")
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
public class RegistrationRestService implements RegistrationRestApi {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationRestService.class);

    private static final int MAX_PASSWORD_LENGTH = 8;
    private AuthenticationProcessor authenticationProcessor;
    private SessionProcessor sessionProcessor;
    private UserService userService;
    private MailSender mailSender;
    private UserGroupService userGroupService;
    private RestRequestHandler restRequestHandler;
    private SmsMtDispatcher smsMtDispatcher;
    private String defaultSenderAddress;
    private Properties validationProperties;
    private ServiceProvider serviceProvider;

    private Map<String, String> autoLoginNewUserDefaultValues;
    private static final String USER_ENABLED_KEY = "enabled";
    private static final String USER_MSISDN_VERIFIED_KEY = "msisdnVerified";
    private static final String USER_STATUS_KEY = "status";
    private static final String USER_OPERATOR_KEY = "operator";
    private static final String USER_GROUP_NAME_KEY = "userGroupName";
    private static final String MSISDN_TO_EMAIL_FORMAT = "%s@gmail.com";

    public void setAutoLoginNewUserDefaultValues(Map<String, String> autoLoginNewUserDefaultValues) {
        this.autoLoginNewUserDefaultValues = autoLoginNewUserDefaultValues;
    }

    /**
     * Handle email verification on user requests through verification email and activate the
     * email for the particular user
     *
     * @param verificationCode
     * @return
     * @deprecated
     */
    @GET
    @Path("/confirm/{emailVerificationCode}")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @Deprecated
    public Response verifyEmail(@PathParam("emailVerificationCode") String verificationCode) {
        logger.debug("Email verification request received.");
        try {
            userService.activateEmail(verificationCode.trim());
        } catch (DataManipulationException e) {
            logger.error("Error while activating user email with the verification code [{}]", verificationCode);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.OK).build();
    }

    @POST
    @Path("authentication")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Map<String, Object> authenticate(Map<String, Object> m) {
        logger.debug("Using authentication request = [{}] received.");
        try {
            return restRequestHandler.execute(UserAuthenticationRequestMessage.convertFromMap(m))
                    .convertToMap();
        } catch (ParseException e) {
            logger.warn("Unable to parse request", e);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE).convertToMap();
        }
    }

    @POST
    @Path("discovery/authentication")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Map<String, Object> authenticateDiscoveryAPICall(Map<String, Object> m) {
        logger.debug("Using authentication request = [{}] received.");
        try {
            return restRequestHandler.execute(UserAuthenticationRequestMessage.convertFromMapForDiscoveryAPI(m))
                    .convertToMapForDiscoveryAPI();
        } catch (ParseException e) {
            logger.warn("Unable to parse request", e);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE).convertToMap();
        }
    }

    @POST
    @Path("sms/mt")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Response verifyMsisdn(SdpRestApiRequestMessage sdpRestApiMessage) {
        logger.debug("MSISDN verification request [{}] received.", sdpRestApiMessage);
        return Response.status(Response.Status.OK).build();
    }

    @POST
    @Path("user/status")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Map<String, Object> changeUserState(Map<String, Object> m) {
        try {
            return restRequestHandler.execute(UserStateChangeRequestMessage.convertFromMap(m))
                    .convertToMap();
        } catch (ParseException e) {
            logger.warn("Unable to parse request", e);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE).convertToMap();
        }
    }

    @POST
    @Path("user/detail")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Map<String, Object> getUserDetails(Map<String, Object> m) {
        try {
            return restRequestHandler.execute(UserDetailRequestMessage.convertFromMap(m))
                    .convertToMap();
        } catch (ParseException e) {
            logger.warn("Unable to parse request", e);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE).convertToMap();
        }
    }

    @POST
    @Path("user/detail/msisdn")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Map<String, Object> getUserDetailsByMsisdn(Map<String, Object> m) {
        logger.debug("UserDetailByMsisdnRequestMessage received {}", m);
        try {
            return restRequestHandler.execute(UserDetailByMsisdnRequestMessage.convertFromMap(m))
                    .convertToMap();
        } catch (ParseException e) {
            logger.warn("Unable to parse request", e);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE).convertToMap();
        }
    }

    @POST
    @Path("user/detail/username")
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public Map<String, Object> getUserDetailsByUsername(Map<String, Object> m) {
        try {
            return restRequestHandler.execute(UserDetailByUsernameRequestMessage.convertFromMap(m))
                    .convertToMap();
        } catch (ParseException e) {
            logger.warn("Unable to parse request", e);
            return createErrorResponse(UNSUPPORTED_REQUEST_TYPE).convertToMap();
        }
    }

    @Override
    @POST
    @Path("user/create")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> createUser(Map<String, Object> request) {

        logger.debug("Create user from the details given [{}]", request);

        Map<String, Object> response = new HashMap<String, Object>();
        List<String> commonErrors = new ArrayList<String>();

        final String email = (String) request.get("email");
        final String msisdn = (String) request.get("msisdn");
        final String username = (String) request.get("username");

        try {
            User userWithName = userService.findUserByName(username);
            User userWithMsisdn = userService.findUserByMsisdn(msisdn);
            Optional<User> userWithEmail = userService.findUserByEmail(email);

            if (userWithName != null) {
                logger.debug("A user found with same username [{}] and user id is [{}]", username, userWithName.getUserId());
                response.put("status-code", "E1330");
                commonErrors.add("username is already registered");
            } else if (userWithMsisdn != null) {
                logger.debug("A user found with same msisdn [{}] and user id is [{}]", msisdn, userWithMsisdn.getUserId());
                response.put("status-code", "E1331");
                commonErrors.add("msisdn is already registered");
            } else if (userWithEmail.isPresent()) {
                logger.debug("A user found with the same email [{}] and user id is [{}]", email, userWithEmail.get().getUserId());
                response.put("status-code", "1332");
                commonErrors.add("email is already in use");
            } else {

                logger.debug("Creating user with given details");

                userWithName = new User();

                String msisdnVerificationCode = WebUtils.generateVerificationCode();
                userWithName.setFirstName((String) request.get("first-name"));
                userWithName.setLastName((String) request.get("last-name"));
                userWithName.setUsername(username);
                if (!"internal".equals(request.get("domain"))) {
                    userWithName.setDomainId((username).substring((username).indexOf('#') + 1));
                }
                userWithName.setPassword(MD5((String) request.get("password")));
                userWithName.setMpin((String) request.get("mpin"));
                userWithName.setMobileNo(msisdn);
                userWithName.setMsisdnChangeAttempts(1);
                userWithName.setMsisdnVerificationAttempts(0);
                userWithName.setMsisdnVerificationCodeReqCount(1);
                userWithName.setMsisdnVerificationCode(msisdnVerificationCode);
                userWithName.setEmail((String) request.get("email"));
                userWithName.setDomain((String) request.get("domain"));
                userWithName.setOperator((String) request.get("operator"));
                userWithName.setEnabled(true);
                userWithName.setUserType(UserType.INDIVIDUAL);
                UserGroup userGroup = userGroupService.findUserGroupByGroupName("CONSUMER_USER");
                userWithName.setUserGroup(userGroup);

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                userWithName.setBirthday(dateFormat.parse((String) request.get("birth-date")));
                //todo handle transaction
                userService.persist(userWithName);

                logger.debug("Created user in the system.");
                //sending verification code sms
                if (request.get("verify-msisdn") != null && Boolean.parseBoolean((String) request.get("verify-msisdn"))) {
                    logger.debug("Sending sms verification code");
                    sendMessage(defaultSenderAddress, userWithName.getMobileNo(), userWithName.getOperator().toLowerCase(),
                            MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, msisdnVerificationCode));
                    userWithName.setLastSmsSendDate(new Date());
                    userService.merge(userWithName);
                } else {
                    userWithName.setMsisdnVerified(true);
                    if (isValidUser(userWithName)) {
                        userWithName.setUserStatus(UserStatus.ACTIVE);
                    }
                    userService.merge(userWithName);
                    logger.debug("Will not send the verification code");
                }
                response.put("status-code", "S1000");
            }

        } catch (DataManipulationException ex) {
            logger.debug("Exception occurs while inserting user details inside the createUser method " + ex);
            response.put("status-code", "E1300");
            commonErrors.add("internal server error occurred");
        } catch (Exception ex) {
            logger.debug("error occurs while creating user", ex);
            response.put("status-code", "E1301");
            commonErrors.add("system.error");
        }

        response.put("common-errors", commonErrors);
        return response;
    }

    @Override
    @POST
    @Path("user/edit")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> editUser(Map<String, Object> request) {

        logger.debug("-----inside edit user ----- ");

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        String birthDate = null;

        try {

            User user = userService.findUserByName((String) request.get("username"));
            if (user != null) {
                user.setFirstName((String) request.get("first-name"));
                user.setLastName((String) request.get("last-name"));

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
                birthDate = (String) request.get("birth-date");

                user.setBirthday(dateFormat.parse(birthDate));

                userService.merge(user);
                logger.debug(" user details were successfully updated");
                response.put("status-code", "S1000");
            } else {
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);

            }

        } catch (ParseException ex) {

            logger.debug(" exception occurs while converting the birth date to java.util.Date [" + birthDate + "] " + ex);
            response.put("status-code", "E1300");
            common_errors.add("invalid.birth.date");
            response.put("common-errors", common_errors);
        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while updating user details inside the editUser method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }


        return response;

    }

    @POST
    @Path("user/verify/code")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> requestVerificationCode(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        try {
            User user = userService.findUserByName((String) request.get("username"));
            if (user != null) {
                boolean verificationCodeRequestAttemptExceed =
                        user.getMsisdnVerificationCodeReqCount() != null &&
                                user.getMsisdnVerificationCodeReqCount() >= Integer.parseInt(MAX_MSISDN_VERIFY_CODE_REQ_COUNT);
                if (verificationCodeRequestAttemptExceed) {
                    logger.debug("[{}] 's verification code requests exceeded for msisdn [{}]", user.getUsername(), user.getMobileNo());
                    common_errors.add("You already exceed verification code requests attempts");
                    response.put("status-code", "E1300");
                } else {
                    final int verificationAttempts = user.getMsisdnVerificationCodeReqCount() == null ? 0 : user.getMsisdnVerificationCodeReqCount();
                    String verificationCode = WebUtils.generateVerificationCode();

                    user.setMsisdnVerificationCodeReqCount(verificationAttempts + 1);
                    user.setMsisdnVerificationAttempts(0);
                    user.setLastSmsSendDate(new Date());
                    user.setMsisdnVerificationCode(verificationCode);
                    user.setMsisdnVerified(false);

                    logger.debug("Sending verification code [{}]", verificationCode);
                    sendMessage(defaultSenderAddress, user.getMobileNo(), user.getOperator().toLowerCase(),
                            MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, verificationCode));
                    response.put("status-code", "S1000");
                    common_errors.add("Success");
                    userService.merge(user);
                }
            }


        } catch (DataManipulationException dmex) {
            logger.error("error occurred while requesting new verification code " + dmex);
            response.put("status-code", "E1300");
            common_errors.add("Internal server error");
        }

        response.put("common-errors", common_errors);
        return response;
    }

    @Override
    @POST
    @Path("user/retrieve")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> retrieveUser(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("----inside user details retrieving method ------");

        try {
            User user = userService.findUserByName((String) request.get("username"));

            if (user != null) {

                String displayName = "";
                //getting the user display name
                if (user.getUserType().equals(UserType.CORPORATE)) {

                    CorporateUser corporateUser = (CorporateUser) user;
                    displayName = corporateUser.getContactPersonName();
                } else {

                    displayName = user.getFirstName();
                }

                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date birthday = user.getBirthday();
                //TODO hack to bypass null b'day records
                birthday = (birthday == null) ? new Date() : birthday;
                String userBirthday = formatter.format(birthday);
                logger.debug(" user found " + user);
                response.put("status-code", "S1000");
                response.put("username", user.getUsername());
                response.put("first-name", user.getFirstName());
                response.put("last-name", user.getLastName());
                response.put("birth-date", userBirthday);
                response.put("msisdn", user.getMobileNo());
                response.put("display-name", displayName);
                response.put("email", user.getEmail());
                response.put("userType", user.getUserType());
                String imagePath = (user.getImage() == null) ? "" : user.getImage();
                response.put("image", imagePath);
                response.put("status-code", "S1000");
            } else {

                logger.debug("user not found ");
                response.put("status-code", "E1300");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while retrieving user details inside the retrieveUser method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);

        }
        return response;

    }//retrieveUser

    @Override
    @POST
    @Path("user/retrieve-by-user-id")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> retrieveUserByUserId(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("User retrieve request received.");

        try {
            User user = userService.findUserByUserId((String) request.get("user-id"));

            if (user != null) {

                String displayName = "";
                //getting the user display name
                if (user.getUserType().equals(UserType.CORPORATE)) {

                    CorporateUser corporateUser = (CorporateUser) user;
                    displayName = corporateUser.getContactPersonName();
                } else {

                    displayName = user.getFirstName();
                }

                String userBirthday = null;
                if (user.getBirthday() != null) {
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    userBirthday = formatter.format(user.getBirthday());
                }
                response.put("status-code", "S1000");
                response.put("username", user.getUsername());
                response.put("first-name", user.getFirstName());
                response.put("birth-date", userBirthday);
                response.put("last-name", user.getLastName());
                response.put("msisdn", user.getMobileNo());
                response.put("display-name", displayName);
                response.put("status-code", "S1000");
            } else {
                logger.debug("user not found ");
                response.put("status-code", "E1300");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException ex) {
            logger.debug(" exception occurs while retrieving user details inside the retrieveUser method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);

        }
        return response;

    }

    @Override
    @POST
    @Path("user/retrieve-by-user-msisdn")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> retrieveUserByUserMsisdn(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("User retrieve by msisdn request received.");

        try {
            User user = userService.findUserByMsisdn((String) request.get("msisdn"));

            if (user != null) {
                response.put("user-status", user.getUserStatus().name());
                response.put("mpin", user.getMpin());
                response.put("status-code", "S1000");
            } else {
                logger.debug("user not found ");
                response.put("status-code", "E1300");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException ex) {
            logger.debug(" Exception occurs while retrieving user details inside the retrieveUser method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);

        }
        return response;
    }


    @Override
    @POST
    @Path("user/password/change")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> changeUserPassword(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("----inside changeUserPassword method ------");

        try {
            User user = userService.findUserByName((String) request.get("username"));

            String oldPassword = ((String) request.get("old-password"));
            String newPassword = ((String) request.get("new-password"));

            if (user != null) {

                if ((user.getPassword()).equals(oldPassword)) {

                    logger.debug(" user found and user old password matches with the database records ");
                    user.setPassword(newPassword);
                    userService.merge(user);
                    logger.debug("password was changed");
                    response.put("status-code", "S1000");

                } else {

                    logger.debug(" user found and user old password does not match with the database records ");
                    response.put("status-code", "E1300");
                    common_errors.add("old.password.does.not.match");
                    response.put("common-errors", common_errors);
                }
            } else {

                logger.debug("Unable to find the user details for username [" + request.get("username") + "]");
                response.put("status-code", "E1300");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while changing the password inside changeUserPassword method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }

        return response;

    }//changeUserPassword


    @Override
    @POST
    @Path("user/mpin/change")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> changeUserMpin(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("----inside changeUserMpin method ------");

        try {
            User user = userService.findUserByName((String) request.get("username"));

            String oldMpin = ((String) request.get("old-mpin"));
            String newMpin = ((String) request.get("new-mpin"));

            if (user != null) {

                if (user.getMpin() == null) {

                    logger.debug(" user found and user has no mpin currently ");
                    user.setMpin(newMpin);
                    userService.merge(user);
                    logger.debug("mpin was changed");
                    response.put("status-code", "S1000");

                } else if ((user.getMpin().equals(oldMpin))) {
                    logger.debug(" user found and user old mpin matches with the database records ");
                    user.setMpin(newMpin);
                    userService.merge(user);
                    logger.debug("mpin was changed");
                    response.put("status-code", "S1000");
                } else {

                    logger.debug(" user found and user old mpin does not match with the database records ");
                    response.put("status-code", "E1300");
                    common_errors.add("old.mpin.does.not.match");
                    response.put("common-errors", common_errors);
                }
            } else {

                logger.debug("Unable to find the user details for username [" + request.get("username") + "]");
                response.put("status-code", "E1300");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while changing the mpin inside changeUserMpin method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }

        return response;

    }//changeUserMpin


    @Override
    @POST
    @Path("user/mpin/change-by-msisdn")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> changeUserMpinByMsisdn(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("----inside changeUserMpin method ------");

        try {
            User user = userService.findUserByMsisdn((String) request.get("msisdn"));

            String oldMpin = ((String) request.get("old-mpin"));
            String newMpin = ((String) request.get("new-mpin"));

            if (user != null) {

                if (user.getMpin() == null) {

                    logger.debug(" user found and user has no mpin currently ");
                    user.setMpin(newMpin);
                    userService.merge(user);
                    logger.debug("mpin was changed");
                    response.put("status-code", "S1000");

                } else if ((user.getMpin().equals(oldMpin))) {
                    logger.debug(" user found and user old mpin matches with the database records ");
                    user.setMpin(newMpin);
                    userService.merge(user);
                    logger.debug("mpin was changed");
                    response.put("status-code", "S1000");
                } else {

                    logger.debug(" user found and user old mpin does not match with the database records ");
                    response.put("status-code", "E1300");
                    common_errors.add("old.mpin.does.not.match");
                    response.put("common-errors", common_errors);
                }
            } else {

                logger.debug("Unable to find the user details for username [" + request.get("username") + "]");
                response.put("status-code", "E1300");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while changing the mpin inside changeUserMpin method " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }

        return response;

    }//changeUserMpin

    @POST
    @Path("authenticate/using-provider")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> authenticateUsingProvider(Map<String, Object> request) {
        logger.debug("Authentication using external provider request received.");
        return authenticationProcessor.authenticate(request);
    }

    @POST
    @Path("user/session-validate")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> sessionValidation(Map<String, Object> request) {
        logger.debug("Session validation request received.");
        return sessionProcessor.validate(request);
    }


    @POST
    @Path("user/logout")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> logout(Map<String, Object> request) {
        logger.debug("Logout user request received.");
        return sessionProcessor.logout(request);
    }

    @Override
    @POST
    @Path("user/verify")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> verifyMsisdn(Map<String, Object> request) {

        logger.debug("User verification request received.");

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        User user = null;
        String userName = (String) request.get("username");
        String verificationCode = (String) request.get("msisdn-verification-code");

        try {

            user = userService.findUserByName(userName);

            if (user != null) {
                Integer verificationAttempts = user.getMsisdnVerificationAttempts();

                logger.debug(" verification code typed by user is [" + verificationCode + "] and verification code in database is " + user.getMsisdnVerificationCode() + "]");
                if (verificationAttempts < Integer.parseInt(PropertyHolder.MAX_MSISDEN_VERIFY_ATTEMPTS)) {

                    if (verificationCode.equals(user.getMsisdnVerificationCode())) {

                        logger.debug("verification matches and user table is updated");

                        user.setMsisdnVerified(true);

                        //TODO brand feature verify
                        if (isValidUser(user)) {
                            user.activate();
                        }
                        if (user.isCorporateUser()
                                && isAutoSpCreationEnabled()
                                && user.isEnabled()) {
                            userService.createDefaultSpForUser(user);
                        }
                        userService.merge(user);
                        response.put("status-code", "S1000");

                    } else {
                        //verification code does not equal

                        user.setMsisdnVerificationAttempts(verificationAttempts + 1);
                        userService.merge(user);
                        logger.debug(" verification code does not match ");
                        response.put("status-code", "E1340");
                        common_errors.add("verification.code.does.not.match");

                    }
                } else {
                    logger.debug("verification attempts exceeded for user [{}]", userName);
                    response.put("status-code", "E1342");
                    common_errors.add("verification attempts exceed please request new");
                }
            } else {
                //user not found
                logger.debug(" unable to find the user details for the username [" + userName + "]");
                response.put("status-code", "E1341");
                common_errors.add("user.is.not.found");
            }

        } catch (DataManipulationException ex) {
            //database exception
            logger.debug(" exception occurs while updating/selecting user details " + ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");

        }
        response.put("common-errors", common_errors);
        return response;

    }


    @Override
    @POST
    @Path("user/username/availability")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> checkUsernameAvailability(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("-----executing username availability checking method ----- ");

        String username = (String) request.get("username");

        try {
            User user = userService.findUserByName(username);

            if (user != null) {
                //username is already registered
                logger.debug("user found and username is [" + user.getUsername() + "] and user id [" + user.getUserId() + "]");
                response.put("status-code", "E1300");
                common_errors.add("username.is.already.registered");
                response.put("common-errors", common_errors);

            } else {

                //username is not registered in the database
                response.put("status-code", "S1000");
                logger.debug(" username is not available in the database");
            }

        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while checking the availability of the username ", ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);

        }

        return response;

    }//checkUsernameAvailability


    @Override
    @POST
    @Path("user/verified/status")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> checkUserVerifiedStatus(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();

        logger.debug("-----executing checkUserVerifiedStatus method ----- ");

        String username = (String) request.get("username");

        try {
            User user = userService.findUserByName(username);

            if (user != null) {
                //user is found
                logger.debug("user found and username is [" + user.getUsername() + "] and user id [" + user.getUserId() + "]");

                if (user.isMsisdnVerified()) {

                    response.put("status-code", "E1300");
                    logger.debug(" user [" + username + "] MSISDN is already verified");
                    common_errors.add("msisdn.is.already.verified");
                    response.put("common-errors", common_errors);


                } else {

                    logger.debug(" user [" + username + "] MSISDN is not verified yet");
                    response.put("status-code", "S1000");
                }


            } else {

                //user is not found
                response.put("status-code", "E1300");
                logger.debug(" user details for the username [" + username + "] not found in the database");
                common_errors.add("user.is.not.found");
                response.put("common-errors", common_errors);
            }

        } catch (DataManipulationException ex) {

            logger.debug(" exception occurs while checking the user verified status", ex);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);

        }

        return response;

    }//checkUserVerifiedStatus

    @POST
    @Path("user/mpin/authenticate")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> authenticateMpin(Map<String, Object> authenticateRequest) {
        logger.debug("Authenticate with mpin request received [{}]", authenticateRequest);
        Map<String, Object> response = new HashMap<String, Object>();

        try {
            String msisdn = (String) authenticateRequest.get("msisdn");
            User user = userService.findUserByMsisdnAndStatus(msisdn, UserStatus.ACTIVE);
            if (user == null) {
                response.put("statusCode", "E1300");
            } else {
                if (user.getMpin().equals(authenticateRequest.get("m-pin"))) {
                    response.put("statusCode", "S1000");
                } else {
                    response.put("statusCode", "E5108");
                }
            }

        } catch (DataManipulationException e) {
            response.put("statusCode", "E5000");
        }

        return response;
    }

    //sending user verification code sms in WAP user registration time
    private void sendMessage(String senderAddress, String receiverAddress, String operator,
                             String message) {
        SmsMessage verificationSms = new SmsMessage();
        verificationSms.setSenderAddress(new Msisdn(senderAddress, operator));
        verificationSms.setReceiverAddress(new Msisdn(receiverAddress, operator));
        verificationSms.setMessage(message);
        logger.debug("MSISDN verification process started with sending SMS......" + verificationSms);
        smsMtDispatcher.sendIndividualMessage(verificationSms);
        //user.setLastSmsSendDate(new Date());
        //userService.update(user);
    }

    private BasicUserResponseMessage createErrorResponse(StatusCodes statusCode) {
        BasicUserResponseMessage response = new BasicUserResponseMessage();
        response.setStatusCode(statusCode);
        return response;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setRestRequestHandler(RestRequestHandler restRequestHandler) {
        this.restRequestHandler = restRequestHandler;
    }

    public void setSmsMtDispatcher(SmsMtDispatcher smsMtDispatcher) {
        this.smsMtDispatcher = smsMtDispatcher;
    }

    public void setDefaultSenderAddress(String DefaultSenderAddress) {
        defaultSenderAddress = DefaultSenderAddress;
    }

    public String getDefaultSenderAddress() {
        return defaultSenderAddress;
    }

    public void setValidationProperties(Properties validationProperties) {
        this.validationProperties = validationProperties;
    }

    public void setUserGroupService(UserGroupService userGroupService) {
        this.userGroupService = userGroupService;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public void setAuthProcessor(AuthenticationProcessor authProcessor) {
        this.authenticationProcessor = authProcessor;
    }

    public void setSessionValidationProcessor(SessionProcessor sessionProcessor) {
        this.sessionProcessor = sessionProcessor;
    }


    @POST
    @Path("user/verify/code/new")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> requestNewVerificationCode(Map<String, Object> request) {

        final Map<String, Object> response = new HashMap<String, Object>();
        final List common_errors = new ArrayList<String>();

        class ProcessHelper {
            void putMsisdnChangeExceedMsg() {
                response.put("status-code", "E1337");
                common_errors.add("Msisdn change attempts exceeded");
            }

            void putCodeReqCountExceedMsg() {
                response.put("status-code", "E1335");
                common_errors.add("Code Request count exceeded");
            }

            void putMobileNumberNotAvailableMsg() {
                response.put("status-code", "E1336");
                common_errors.add("Mobile number not available");
            }

            void putSuccessRespMsg() {
                response.put("status-code", "S1000");
                common_errors.add("Success");
            }

            void updateUserOnSuccess(User user) {
                String verificationCode = WebUtils.generateVerificationCode();
                user.setMsisdnVerificationAttempts(0);
                user.setLastSmsSendDate(new Date());
                user.setMsisdnVerificationCode(verificationCode);
                user.setMsisdnVerified(false);
            }

            void sendVerificationCode(User user) {
                sendMessage(defaultSenderAddress, user.getMobileNo(), user.getOperator().toLowerCase(),
                        MessageFormat.format(MSISDN_VERIFICATION_MSG_TEMPLATE, user.getMsisdnVerificationCode()));
            }
        }


        try {
            User userFoundForUserName = userService.findUserByName((String) request.get("username"));
            String msisdn = (String) request.get("msisdn");
            User userFoundForMsisdn = userService.findUserByMsisdn(msisdn);
            ProcessHelper processHelper = new ProcessHelper();

            boolean isValidRequest =
                    (userFoundForMsisdn == null) ? true : userFoundForUserName.getUsername().equals(userFoundForMsisdn.getUsername());

            if (userFoundForUserName != null && isValidRequest) {

                boolean requestForSameMsisdn = userFoundForUserName.getMobileNo().equals(msisdn.trim());
                boolean verificationCodeRequestCountExceed =
                        userFoundForUserName.getMsisdnVerificationCodeReqCount() != null &&
                                userFoundForUserName.getMsisdnVerificationCodeReqCount() >= Integer.parseInt(MAX_MSISDN_VERIFY_CODE_REQ_COUNT);

                Integer msisdnChangeAttempts = userFoundForUserName.getMsisdnChangeAttempts();
                int maxMsisdnChangesAttempts = Integer.parseInt(MAX_MSISDN_CHANGE_ATTEMPTS);

                if (!verificationCodeRequestCountExceed) {
                    if (requestForSameMsisdn) {
                        //send code && update request count +1
                        int newVerificationCodeReqCount = userFoundForUserName.getMsisdnVerificationCodeReqCount() + 1;
                        userFoundForUserName.setMsisdnVerificationCodeReqCount(newVerificationCodeReqCount);
                        processHelper.updateUserOnSuccess(userFoundForUserName);
                        userService.merge(userFoundForUserName);
                        processHelper.putSuccessRespMsg();
                        processHelper.sendVerificationCode(userFoundForUserName);

                    } else {
                        if (msisdnChangeAttempts < maxMsisdnChangesAttempts) {
                            //change msisdn and send && update req count = 1 && update msisdn msisdn_change_count ++
                            int newVerificationCodeReqCount = 1;
                            userFoundForUserName.setMsisdnVerificationCodeReqCount(newVerificationCodeReqCount);
                            userFoundForUserName.setMsisdnChangeAttempts(userFoundForUserName.getMsisdnChangeAttempts() + 1);
                            userFoundForUserName.setMobileNo(msisdn);
                            processHelper.updateUserOnSuccess(userFoundForUserName);
                            userService.merge(userFoundForUserName);
                            processHelper.putSuccessRespMsg();
                            processHelper.sendVerificationCode(userFoundForUserName);
                        } else {
                            //send error msisdn change attempts exceeded
                            processHelper.putMsisdnChangeExceedMsg();
                        }
                    }
                } else {
                    if (requestForSameMsisdn) {
                        //put error exceed code request
                        processHelper.putCodeReqCountExceedMsg();

                    } else {
                        if (msisdnChangeAttempts < maxMsisdnChangesAttempts) {
                            //change msisdn and send && update req count && update code req to 1
                            int newVerificationCodeReqCount = 1;
                            userFoundForUserName.setMsisdnVerificationCodeReqCount(newVerificationCodeReqCount);
                            userFoundForUserName.setMsisdnChangeAttempts(userFoundForUserName.getMsisdnChangeAttempts() + 1);
                            userFoundForUserName.setMobileNo(msisdn);
                            processHelper.updateUserOnSuccess(userFoundForUserName);
                            userService.merge(userFoundForUserName);
                            processHelper.putSuccessRespMsg();
                            processHelper.sendVerificationCode(userFoundForUserName);

                        } else {
                            //send error msisdn change count exceeded
                            processHelper.putMsisdnChangeExceedMsg();
                        }
                    }
                }
            } else if (userFoundForUserName != null && !isValidRequest) {
                processHelper.putMobileNumberNotAvailableMsg();
            }

        } catch (DataManipulationException dmex) {
            logger.error("error occurred while requesting new verification code " + dmex);
            response.put("status-code", "E1300");
            common_errors.add("Internal server error");
        }

        response.put("common-errors", common_errors);
        return response;
    }

    @POST
    @Path("user/account/recover")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> recoverAccount(Map<String, Object> request) {

        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();
        String recoveryText = (String) request.get("recover-text");
        logger.debug("retrieve account recovery request from [{}]", recoveryText);
        recoveryText = recoveryText.trim();
        try {
            Optional<User> userOptional = userService.findUserByEmail(recoveryText);
            User userByUserName = userService.findUserByName(recoveryText);

            if (!userOptional.isPresent() && userByUserName == null) {
                response.put("status-code", "E1350");
                common_errors.add("Either username or email address you entered is not registered with App Store");

            } else {
                User user = (userOptional.isPresent()) ? userOptional.get() : userByUserName;
                String generatedPassword = WebUtils.getPassword(MAX_PASSWORD_LENGTH);
                user.setPassword(Encrypter.MD5(generatedPassword));
                userService.merge(user);
                logger.debug("[{}]'s password updated successfully", user.getUsername());
                mailSender.sendForgotPasswordReplyEmail(user.getFirstName(), user.getEmail(),
                        user.getUsername(), generatedPassword);
                logger.debug("Sending Forgot Password Request for User Name [{} Finished ",
                        user.getUsername());
                common_errors.add("An email with the new password has been sent to your email account");
                response.put("status-code", "S1000");
            }
        } catch (NoSuchAlgorithmException
                | UnsupportedEncodingException
                | DataManipulationException e) {
            common_errors.add("Internal server error");
            response.put("status-code", "E1300");
            logger.error("Error occurred while handling account recovery request ", e);
        }
        response.put("common-errors", common_errors);
        return response;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @POST
    @Path("user/update")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> UserProfileUpdate(Map<String, Object> request) {

        logger.debug("-----inside update user profile ----- ");

        Map<String, Object> response = new HashMap();
        List common_errors = new ArrayList<String>();

        BASE64Decoder base64Decoder = new BASE64Decoder();
        String fileName = "";
        final String BASE_DIRECTORY_PATH = PropertyHolder.BASE_DIRECTORY_PATH;
        final String DIRECTORY_NAME = "user";
        if (!request.get("image").toString().equals("")) {
            fileName = request.get("username").toString();
        }
        final String IMAGE_ACCESS_URL = PropertyHolder.IMAGE_ACCESS_BASE_URL + DIRECTORY_NAME + File.separator + fileName;

        try {
            User user = userService.findUserByName((String) request.get("username"));
            if (user != null) {
                if (user.getEmail().equals(request.get("email").toString())) {
                    user.setFirstName(request.get("firstName").toString());
                    user.setLastName(request.get("lastName").toString());
                    user.setEmail(request.get("email").toString());
                    if (!request.get("image").toString().equals("")) {
                        uploadFile(BASE_DIRECTORY_PATH, DIRECTORY_NAME, base64Decoder.decodeBuffer(request.get("image").toString()), fileName);
                        user.setImage(IMAGE_ACCESS_URL);
                    }
                    userService.merge(user);
                    logger.debug("user details were successfully updated");
                    response.put("status-code", "S1000");
                } else {
                    if (userService.isEmailAvailableForUserId(request.get("email").toString(), (String) request.get("username"))) {
                        user.setFirstName(request.get("firstName").toString());
                        user.setLastName(request.get("lastName").toString());
                        user.setEmail(request.get("email").toString());
                        if (!request.get("image").toString().equals("")) {
                            uploadFile(BASE_DIRECTORY_PATH, DIRECTORY_NAME, base64Decoder.decodeBuffer(request.get("image").toString()), fileName);
                            user.setImage(IMAGE_ACCESS_URL);
                        }
                        userService.merge(user);
                        logger.debug("user details were successfully updated");
                        response.put("status-code", "S1000");
                    } else {
                        logger.debug("email already used in corporate account");
                        response.put("status-code", "E1332");
                        common_errors.add("email.already.used");
                        response.put("common-errors", common_errors);
                    }
                }
            } else {
                common_errors.add("user.is.not.found");
                logger.debug("user not found");
                response.put("common-errors", common_errors);
            }
        } catch (IOException e) {
            logger.error("exception occurs while updating user details inside the UserProfileUpdate method ", e);
            response.put("status-code", "E1300");
            common_errors.add("io.operation.error");
            response.put("common-errors", common_errors);
        } catch (DataManipulationException e) {
            logger.error("exception occurs while updating user details inside the UserProfileUpdate method ", e);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }
        return response;
    }

    @POST
    @Path("autologin/user/create")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> createAutoLoginUser(Map<String, Object> request) {

        logger.debug("Create auto-login user request received.");

        Map<String, Object> response = new HashMap<>();
        List<String> commonErrors = new ArrayList<>();

        final String msisdn = (String) request.get("msisdn");

        try {
            User userWithName = userService.findUserByName(msisdn);
            User userWithMsisdn = userService.findUserByMsisdn(msisdn);
            Optional<User> userWithEmail = userService.findUserByEmail(msisdn);

            if (userWithName != null) {
                logger.debug("A user found with same username [{}] and user id is [{}]", msisdn, userWithName.getUserId());
                response.put("status-code", "E1330");
                commonErrors.add("username is already registered");
            } else if (userWithMsisdn != null) {
                logger.debug("A user found with same msisdn [{}] and user id is [{}]", msisdn, userWithMsisdn.getUserId());
                response.put("status-code", "E1331");
                commonErrors.add("msisdn is already registered");
            } else if (userWithEmail.isPresent()) {
                logger.debug("A user found with the same email [{}] and user id is [{}]", msisdn, userWithEmail.get().getUserId());
                response.put("status-code", "1332");
                commonErrors.add("email is already in use");
            } else {

                logger.debug("Creating auto-login user with given details");

                userWithName = new User();

                userWithName.setUsername(msisdn);
                userWithName.setMobileNo(msisdn);

                if (isMsisdnToEmailFormattingEnabled()) {
                    String formattedEmail = String.format(MSISDN_TO_EMAIL_FORMAT, msisdn);
                    userWithName.setEmail(formattedEmail);
                } else {
                    userWithName.setEmail(msisdn);
                }

                userWithName.setLastLogin(DateTime.now().toDate());
                userWithName.setCurrentLogin(DateTime.now().toDate());
                userWithName.setUserType(UserType.INDIVIDUAL);

                // Default values if not configured 'autoLoginNewUserDefaultValues'
                userWithName.setEnabled(true);
                userWithName.setMsisdnVerified(true);
                UserStatus userStatus = UserStatus.INITIAL;

                if (autoLoginNewUserDefaultValues != null) {
                    if (autoLoginNewUserDefaultValues.containsKey(USER_ENABLED_KEY)) {
                        userWithName.setEnabled(Boolean.valueOf(autoLoginNewUserDefaultValues.get(USER_ENABLED_KEY)));
                    }
                    if (autoLoginNewUserDefaultValues.containsKey(USER_MSISDN_VERIFIED_KEY)) {
                        userWithName.setMsisdnVerified(Boolean.valueOf(autoLoginNewUserDefaultValues.get(USER_MSISDN_VERIFIED_KEY)));
                    }
                    if (autoLoginNewUserDefaultValues.containsKey(USER_STATUS_KEY)) {
                        userStatus = UserStatus.valueOf(autoLoginNewUserDefaultValues.get(USER_STATUS_KEY));
                    }
                    if (autoLoginNewUserDefaultValues.containsKey(USER_OPERATOR_KEY)) {
                        userWithName.setOperator(autoLoginNewUserDefaultValues.get(USER_OPERATOR_KEY));
                    }
                    if (autoLoginNewUserDefaultValues.containsKey(USER_GROUP_NAME_KEY)) {
                        UserGroup userGroup = userGroupService.findUserGroupByGroupName(autoLoginNewUserDefaultValues.get(USER_GROUP_NAME_KEY));
                        userWithName.setUserGroup(userGroup);
                    }
                }

                userService.persist(userWithName, userStatus);
                logger.debug("Created auto-login user in the system.");
                response.put("status-code", "S1000");
            }

        } catch (DataManipulationException ex) {
            logger.debug("Exception occurs while inserting user details inside the createAutoLoginUser method ", ex);
            response.put("status-code", "E1300");
            commonErrors.add("internal server error occurred");
        } catch (Exception ex) {
            logger.debug("error occurs while creating auto-login user", ex);
            response.put("status-code", "E1301");
            commonErrors.add("system.error");
        }

        response.put("common-errors", commonErrors);
        return response;
    }

    @POST
    @Path("user/password/reset")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Override
    public Map<String, Object> resetUserPassword(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();
        List<String> commonErrors = new ArrayList<>();
        String msisdn = (String) request.get("msisdn");
        msisdn = msisdn.trim();
        logger.debug("reset password of the user [{}]", msisdn);

        try {
            User userByMsisdn = userService.findUserByMsisdn(msisdn);

            if (userByMsisdn == null) {
                response.put("status-code", "E1350");
                commonErrors.add("No user found for the msisdn");
            } else {
                String generatedPassword = WebUtils.getPassword(MAX_PASSWORD_LENGTH);
                userByMsisdn.setPassword(MD5(generatedPassword));
                userService.merge(userByMsisdn);
                logger.debug("[{}]'s password reset successfully", userByMsisdn.getUsername());

                sendPasswordResetMessage(defaultSenderAddress, userByMsisdn.getMobileNo(), userByMsisdn.getOperator().toLowerCase(),
                        MessageFormat.format(USER_PASSWORD_RESET_SMS_TEMPLATE, generatedPassword));

                logger.debug("Sending password reset sms to the msisdn [{}] Finished ", msisdn);
                response.put("status-code", "S1000");
            }
        } catch (NoSuchAlgorithmException
                | UnsupportedEncodingException
                | DataManipulationException e) {
            commonErrors.add("Internal server error");
            response.put("status-code", "E1300");
            logger.error("Error occurred while handling password reset request ", e);
        }
        response.put("common-errors", commonErrors);
        return response;
    }

    //sending user password reset sms
    private void sendPasswordResetMessage(String senderAddress, String receiverAddress, String operator,
                                          String message) {
        SmsMessage passwordResetSms = new SmsMessage();
        passwordResetSms.setSenderAddress(new Msisdn(senderAddress, operator));
        passwordResetSms.setReceiverAddress(new Msisdn(receiverAddress, operator));
        passwordResetSms.setMessage(message);
        logger.debug("Sending password reset sms to the msisdn [{}]" + receiverAddress);
        smsMtDispatcher.sendIndividualMessage(passwordResetSms);
    }

    private void uploadFile(String baseDirectoryPath, String dirName, byte[] image, String fileName) {

        String dirPath = baseDirectoryPath + File.separator + dirName;
        String filePath = dirPath + File.separator + fileName;
        File dir = new File(dirPath);
        BufferedOutputStream fos = null;

        try {
            if (dir.exists()) {
                if (!dir.isDirectory()) {
                    dir.delete(); // is this ok ???
                    dir = new File(dirPath);
                    dir.mkdir();
                }
            } else {
                dir.mkdir();
            }

            File file = new File(filePath);

            if (file.exists()) {
                assert (file.delete());
                file = new File(filePath);
                file.createNewFile();
            } else {
                file.createNewFile();
            }

            fos = new BufferedOutputStream(new FileOutputStream(file));
            fos.write(image);
            fos.flush();
            file.setLastModified(System.currentTimeMillis());
        } catch (IOException e) {
            logger.error("exception occurs while uploading the user profile picture inside the UserProfileUpdate method ", e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    logger.error("exception occurs while closing the stream inside the UserProfileUpdate method ", e);
                }
            }
        }
    }

    @Override
    @GET
    @Path("users/start/{start}/limit/{limit}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> findAllUsers(@PathParam("start") int start, @PathParam("limit") int limit) {
        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();
        List<User> userList;
        List<Map<String, Object>> userListFiltered = new ArrayList<>();
        try {
            logger.debug("Finding all users...");
            userList = userService.getAllAvailableUsersWithPaging(start, limit);
            if (userList.size() > 0) {
                for (User user : userList) {
                    Map<String, Object> usersDetailsMap = new HashedMap();
                    String firstName = "", lastName = "";
                    if (user.getFirstName() != null) {
                        firstName = user.getFirstName();
                    }
                    if (user.getLastName() != null) {
                        lastName = user.getLastName();
                    }
                    usersDetailsMap.put("fullName", (firstName + " " + lastName));
                    usersDetailsMap.put("email", user.getEmail());
                    usersDetailsMap.put("mobileNumber", user.getMobileNo());
                    userListFiltered.add(usersDetailsMap);
                }
                response.put("status-code", "S1000");
                response.put("users", userListFiltered);
            } else {
                logger.debug("users not found ");
                response.put("status-code", "E1300");
                common_errors.add("users.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (DataManipulationException e) {
            logger.debug(" exception occurs while retrieving users inside the findAllUsers method ", e);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }
        return response;
    }

    @Override
    @GET
    @Path("users/count")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> allUsersCount() {
        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();
        try {
            logger.debug("Finding all users count...");
            int usersCount = userService.getAllUsersCount();
            response.put("status-code", "S1000");
            response.put("count", usersCount);
        } catch (DataManipulationException e) {
            logger.debug(" exception occurs while retrieving users inside the findAllUsers method ", e);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }
        return response;
    }

    @Override
    @GET
    @Path("users/search/{field}/{value}/start/{start}/limit/{limit}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Map<String, Object> findUsersByField(@PathParam("start") int start, @PathParam("limit") int limit, @PathParam("field") String field, @PathParam("value") String value) {
        Map<String, Object> response = new HashMap<String, Object>();
        List common_errors = new ArrayList<String>();
        List<User> userList;
        List<Map<String, Object>> userListFiltered = new ArrayList<>();
        List<Condition> conditions = new ArrayList<>();
        int countOfSearchResults = 0;
        try {
            logger.debug("Finding user by condition...");
            if (field.equals("name")) {
                conditions.add(new Condition("firstName", "%" + value + "%", "like"));
                userList = userService.findUserListByConditionList(start, limit, conditions);
                countOfSearchResults = userService.findUserIdListByConditionList(conditions).size();
                conditions.clear();
                conditions.add(new Condition("lastName", "%" + value + "%", "like"));
                List<User> userListFoundByLastName = userService.findUserListByConditionList(start, limit, conditions);
                for (User user : userListFoundByLastName) {
                    if (!userList.contains(user)) {
                        userList.add(user);
                    }
                }
                countOfSearchResults += userService.findUserIdListByConditionList(conditions).size();
            } else {
                conditions.add(new Condition(field, "%" + value + "%", "like"));
                userList = userService.findUserListByConditionList(start, limit, conditions);
                countOfSearchResults = userService.findUserIdListByConditionList(conditions).size();
            }
            if (userList.size() > 0) {
                for (User user : userList) {
                    Map<String, Object> usersDetailsMap = new HashedMap();
                    String firstName = "", lastName = "";
                    if (user.getFirstName() != null) {
                        firstName = user.getFirstName();
                    }
                    if (user.getLastName() != null) {
                        lastName = user.getLastName();
                    }
                    usersDetailsMap.put("fullName", (firstName + " " + lastName));
                    usersDetailsMap.put("email", user.getEmail());
                    usersDetailsMap.put("mobileNumber", user.getMobileNo());
                    userListFiltered.add(usersDetailsMap);
                }
                response.put("status-code", "S1000");
                response.put("users", userListFiltered);
                response.put("count", countOfSearchResults);
            } else {
                logger.debug("users not found ");
                response.put("status-code", "E1300");
                common_errors.add("users.not.found");
                response.put("common-errors", common_errors);
            }
        } catch (Exception e) {
            logger.debug(" exception occurs while retrieving users inside the findAllUsers method ", e);
            response.put("status-code", "E1300");
            common_errors.add("system.is.busy");
            response.put("common-errors", common_errors);
        }
        return response;
    }
}